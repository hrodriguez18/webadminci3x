<?php

	/**
	 * ESTA CLASE SE ENCARGA DE ENVIAR LA PETICION VIA REST PARA ENVIAR MENSAJE DE
   * CAMBIO DE CONTRASEÑA O TOKEN
	 */
	class Sms
	{

		public $phone='';
		public $templateMsn='';
		public $token='';
		public $personName='';
		public $apiUser='apiuser';
		public $apiPass='1p3s2r_mP1y';
		public $errors=array();

		public function send(){

			$data=array();

			if($this->phone!=''){
				$status=1;
			}else{
				array_push($this->errors, "No set Phone");
				$status=0;
			}
			if($this->templateMsn!=''){
				$status=1;
			}else{
				array_push($this->errors, "No set TemplateName");
				$status=0;
			}
			if($this->token!=''){
				$status=1;
			}else{
				array_push($this->errors, "No set Token");
				$status=0;
			}
			if($this->personName!=''){
				$status=1;
			}else{
				array_push($this->errors, "No set Person Name");
				$status=0;
			}
			if($this->apiUser!=''){
				$status=1;
			}else{
				array_push($this->errors, "No set API User");
				$status=0;
			}

			if($this->apiPass!=''){
				$status=1;
			}else{
				array_push($this->errors, "No set API Pass");
				$status=0;
			}

			switch ($status) {
				case 	1:
					$data= array(
						"identity"=>"apiuser",
						"credential"=>"1p3us2r_mP1y",
						"messages"=>array(array(
							"id"=>"MPay",
							"destination"=>$this->phone,
							"type"=>"sms",
							"template_name"=>$this->templateMsn,
							"parameters"=>array(
								array("key"=>"TOKEN","value"=>$this->token),
								array("key"=>"NOMBRE","value"=>$this->personName)
							))
						)
					);
					$data_string = json_encode($data);
			    $result = file_get_contents('https://connections.quanticvision.com/mpay/public/http', null, stream_context_create(array(
			      'http' => array(
			      'method' => 'POST',
			      'header' => 'Content-Type: application/json' . "\r\n"
			      .'Content-Length: ' . strlen($data_string) . "\r\n",
			      'content' => $data_string,
			      ),
			      )));
			      $result=json_decode($result);
			      $id=$result[0];
			      return $id;
					break;
				default:
					return FALSE;
					break;
			}
		}
	}


 ?>

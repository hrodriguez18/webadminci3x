<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class procesadores_m extends MY_Model {
 	public $_table_name = 'esb.Billers';
	protected $_primary_key = 'Cod_Biller';
	protected $_primary_filter = 'intval';
	protected $_order_by = 'Descripcion';
	public $rules = array('Descripcion'=>array('field'=>'Descripcion','label'=>'Descripcion','rules'=>'trim|required|min_length[4]')
	);

	protected $_timestamps = FALSE;


	function __construct() {
		parent::__construct();
	}


}
?>

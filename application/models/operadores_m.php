<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class operadores_m extends MY_Model {
 	public $_table_name = 'esb.Operadores';
	protected $_primary_key = 'Cod_Operador';
	protected $_primary_filter = 'intval';
	protected $_order_by = 'Cod_Operador';

  public $_TableToExport='';
  public $_footer_ContentToExport='';
  public $_pdfTitle='Operadores';
  public $_pdfDescription='informe de operadores existentes';


	public $rules = array('Descripcion'=>array('field'=>'Descripcion','label'=>'Descripcion','rules'=>'trim|required')
	);

	public $status=array('A'=>'Activo','I'=>'Inactivo');




	protected $_timestamps = FALSE;


	function __construct() {
		parent::__construct();
	}


	public function optbiller(){
		$this->db->select('*');
		$query=$this->db->get('esb.Billers');
		$opt=array();
		if($query->num_rows()>0){
			foreach ($query->result() as $value) {
				$opt[$value->Cod_Biller]=$value->Descripcion;
			}
		}
		return $opt;
	}

	public function opt_paises(){
		$this->db->select('Cod_Pais,Descripcion');
		$query=$this->db->get('esb.Pais');
		$opt=array();
		if($query->num_rows()>0){
			foreach ($query->result() as $value) {
				$opt[$value->Cod_Pais]=$value->Descripcion;
			}
		}
		return $opt;
	}

	public function opt_canales(){
		$this->db->select('Id,Canal');
		$query=$this->db->get('esb.Canales');
		$opt=array();
		if($query->num_rows()>0){
			foreach ($query->result() as $value) {
				$opt[$value->Id]=$value->Canal;
			}
		}
		return $opt;
	}

	public function opt_colores(){
		$this->db->select('Id,Color');
		$query=$this->db->get('esb.Colores');
		$opt=array();
		if($query->num_rows()>0){
			foreach ($query->result() as $value) {
				$opt[$value->Id]=$value->Color;
			}
		}
		return $opt;
	}



	public function Form(){
		$option_for_processor=$this->optbiller();
		$form="";


		$Descripcion=array('name'=>'Descripcion','type'=>'text','placeholder'=>'Descripcion','class'=>'form-control','id'=>'Descripcion','value'=>'','min'=>0);

		$pais=$this->opt_paises();
		$canales=$this->opt_canales();
		$colores=array('name'=>'Color','type'=>'color','class'=>'form-control','id'=>'Color','value'=>'');



		$Descripcion['value']=set_value('Descripcion');
		$canales['value']=set_value('Canal');
		$colores['value']=set_value('Color');
		$pais['value']=set_value('Pais');


		$form.='<div class="form-group">'.form_error($Descripcion['id']).form_input($Descripcion).'</div>';

		$form.='<div class="form-group">'.form_error('Pais').form_label('Selecciona Un Pais','Pais').form_dropdown('Pais',$pais,set_value('Pais'),'class="form-control"').'</div>';

		$form.='<div class="form-group">'.form_error('Canal').form_label('Selecciona Un Canal','Canal').form_dropdown('Canal',$canales,set_value('Canal'),'class="form-control"').'</div>';

		$form.='<div class="form-group">'.form_label('Selecciona Un Color','Color').form_input($colores).'</div>';

		$form.='<div class="form-group">'.form_error('status').form_label('Selecciona Un Estatus','status').form_dropdown('status',$this->status,set_value('status'),'class="form-control"').'</div>';

		return $form;
	}

	public function  FormForEdit($data_by_id){
		$option_for_processor=$this->optbiller();
		$form="";


		$Descripcion=array('name'=>'Descripcion','type'=>'text','placeholder'=>'Descripcion','class'=>'form-control','id'=>'Descripcion','value'=>'','min'=>0);

		$pais=$this->opt_paises();
		$canales=$this->opt_canales();
    $colores=array('name'=>'Color','type'=>'color','class'=>'form-control','id'=>'Color','value'=>'');

    $colores['value']=$data_by_id->Color;

		$Descripcion['value']=$data_by_id->Descripcion;


		$cod_op=array('name'=>'Cod_Operador','type'=>'hidden', 'value'=>$data_by_id->Cod_Operador);

		$form.=form_input($cod_op);


		$form.='<div class="form-group">'.form_error($Descripcion['id']).form_input($Descripcion).'</div>';

		$form.='<div class="form-group">'.form_error('Pais').form_label('Selecciona Un Pais','Pais').form_dropdown('Pais',$pais,$data_by_id->Pais,'class="form-control"').'</div>';

		$form.='<div class="form-group">'.form_error('Canal').form_label('Selecciona Un Canal','Canal').form_dropdown('Canal',$canales,$data_by_id->canalId,'class="form-control"').'</div>';

    $form.='<div class="form-group">'.form_label('Selecciona Un Color','Color').form_input($colores).'</div>';

		$form.='<div class="form-group">'.form_error('status').form_label('Selecciona Un Estatus','status').form_dropdown('status',$this->status,$data_by_id->Estado,'class="form-control"').'</div>';

		return $form;
	}

	public function getDataForOperadores(){
		$this->db->select('Operadores.Cod_Operador,
                      Operadores.Descripcion,
                      Operadores.Estado,
                      Operadores.Pais,
                      Operadores.Color,
                      Operadores.fechaRegistro
                        ');
    $this->db->select('Canales.Canal as C');
    $this->db->join('esb.Canales','Operadores.canalId=Canales.Id','left');
		return $this->db->get($this->_table_name)->result();

	}

  public function Export(){
    $this->db->select('Operadores.Descripcion,
                      Operadores.Pais,
                      Canales.Canal as Canal,
                      Operadores.Color,
                      Operadores.Estado,
                      Operadores.fechaRegistro');
    $this->db->join('esb.Canales','Operadores.canalId=Canales.Id','left');
    $query=$this->db->get($this->_table_name)->result_array();
    $this->table->set_heading('Descripción','País','Canal','Color','Status','Fecha de Registro');
    $this->_TableToExport=$this->table->generate($query);
    $this->generar();
  }

}
?>

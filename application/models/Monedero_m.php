<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Monedero_m extends MY_SuperModel{
  //datos para hacer la consulta la base de datos
  protected $_tablename='esb.usuariosMonedero';
	public $_primary_key='id';
	protected $_primary_filter='intval';
	protected $_order_by='id';

  protected $_form_group_class=' ';
  //datos para crear el FormularioPaisEdit

  //RELACION PARA GENERAR LA TABLA DE ESTA CLASE
	protected $_headingTable=array();
	//usado para buscar los datos de la db
	//usado para mostrar los datos en la tabla e incluir los btn actions

	//para generar los informes en PDF

	public $_headersTable=array('REGISTRADO','CORREO','NOMBRE','APELLIDO','CEDULA',
                              'SEXO','NACIMIENTO','PAIS','DIRECCION',
                              'TELEFONO','CUENTA M.','ROL','ESTADO');

	public $_selectToExport=array(


		'usuariosMonedero.created_at',
		'usuariosMonedero.email',
		'usuariosMonedero.name',
		'usuariosMonedero.lastname',
		'usuariosMonedero.cedula',
		'usuariosMonedero.sexo',
		'usuariosMonedero.fnacimiento',
		'usuariosMonedero.paisId',
		'usuariosMonedero.address',
		'usuariosMonedero.phone',
		'usuariosMonedero.cuentaMonedero',
		'rolesTerminal.descripcion as ROL',
		'usuariosMonedero.status'


	);
	public $_orderName='';
	public $_order='';
	public $_join=array('table'=>'esb.rolesTerminal ','relation'=>' usuariosMonedero.rolId=rolesTerminal.id','type'=>'left');


  //Reglas para validar los campos
  public  $_rules=array('cedula'=>array('field'=>'cedula','label'=>'Cedula','rules'=>'trim|required|max_length[50]'));

  //INPUTS PARA LA CREACION DEL FORMULARIO, SE RECOMIENDA QUE LOS CAMPOS HIDDEN SE COLOQUEN AL FINAL,
	public $_inputs=array(

		'email'=>array(
			'name'=>'email',
			'type'=>'number',
			'placeholder'=>'Telefono',
			'value'=>'',
			'label'=>'Teléfono',
			'class'=>'form-control'
		),
		'name'=>array(
			'name'=>'name',
			'type'=>'text',
			'placeholder'=>'name',
			'value'=>'',
			'label'=>'name',
			'class'=>'form-control'
		),
		'lastname'=>array(
			'name'=>'lastname',
			'type'=>'text',
			'placeholder'=>'lastname',
			'value'=>'',
			'label'=>'lastname',
			'class'=>'form-control'
		),
		'cedula'=>array(
			'name'=>'cedula',
			'type'=>'text',
			'placeholder'=>'cedula',
			'value'=>'',
			'label'=>'cedula',
			'class'=>'form-control'
		),
		'fnacimiento'=>array(
			'name'=>'fnacimiento',
			'type'=>'date',
			'value'=>'',
			'label'=>'Birthdate',
			'class'=>'form-control'
		),
		'limiteId'=>array(
			'name'=>'limiteId',
			'id'=>'limiteId',
			'type'=>'selector',
			'table_rel'=>'esb.limitesMonedero',
			'opt'=>'id',
			'description'=>'descripcion',
			'class'=>'form-control',
			'value'=>'',
			'label'=>'Límites Monedero'
		),
		'address'=>array(
			'name'=>'address',
			'type'=>'textarea',
			'value'=>'',
			'label'=>'address',
			'class'=>'form-control',
			'rows'=>'3'
		),
        'sexo'=>array(
        	'name'=>'sexo',
			'id'=>'sexo',
			'type'=>'selector',
			'table_rel'=>'esb.sexo',
			'opt'=>'name',
			'description'=>'name',
			'class'=>'form-control',
			'value'=>'',
			'label'=>'sexo'
		),
        'paisId'=>array(
        	'name'=>'paisId',
			'id'=>'paisId',
			'type'=>'selector',
			'table_rel'=>'esb.Pais',
			'opt'=>'Cod_Pais',
			'description'=>'Descripcion',
			'class'=>'form-control',
			'value'=>'',
			'label'=>'country'
		),
        'subsidioId'=>array(
        	'name'=>'subsidioId',
			'id'=>'subsidioId',
			'type'=>'selector',
			'table_rel'=>'esb.subsidios',
			'opt'=>'id',
			'description'=>'descripcion',
			'class'=>'form-control',
			'value'=>'',
			'label'=>'subsidy'
		),
		'rolId'=>array(
			'name'=>'rolId',
			'id'=>'rolId',
			'type'=>'selector',
			'table_rel'=>'esb.rolesTerminal',
			'opt'=>'id',
			'description'=>'descripcion',
			'class'=>'form-control',
			'value'=>'',
			'label'=>'rol_id'
		),
		'grupoTrxId'=>array(
			'name'=>'grupoTrxId',
			'id'=>'grupoTrxId',
			'type'=>'selector',
			'table_rel'=>'esb.grupoTransacciones',
			'opt'=>'id',
			'description'=>'description',
			'class'=>'form-control',
			'value'=>'',
			'label'=>'TransactionsGroup'
		),
		'status'=>array(
			'name'=>'status',
			'id'=>'status',
			'type'=>'selector',
			'table_rel'=>'esb.status',
			'opt'=>'value',
			'description'=>'description',
			'class'=>'form-control',
			'value'=>'',
			'label'=>'status'
		),
        'id'=>array(
        	'name'=>'id',
			'type'=>'hidden',
			'value'=>''
		)

	);

  //datos para exportar a PDF
  public $_TableToExport='';
  public $_footer_ContentToExport='';
  public $_pdfTitle='BANCO NACIONAL DE PANAMÁ';
	public $_pdfDescription='REPORTE DE CNB';
  public $_dateVariable='created_at';

  //usado para buscar los datos de la db
  public $_datatable=array('usuariosMonedero.id','usuariosMonedero.email','usuariosMonedero.name',
                            'usuariosMonedero.lastname','usuariosMonedero.phone',
                            'usuariosMonedero.status','usuariosMonedero.cedula','usuariosMonedero.sexo',
                            'usuariosMonedero.fnacimiento','usuariosMonedero.paisId','usuariosMonedero.address',
                            'usuariosMonedero.cuentaMonedero','rolesTerminal.descripcion as ROL','usuariosMonedero.created_at');

  public $_joinForBootstrapTable=array('rol'=>array('table_join'=>'esb.rolesTerminal ','table_rel'=>' usuariosMonedero.rolId=rolesTerminal.id '));
  //usado para mostrar los datos en la tabla e incluir los btn actions
  public $_dataForTable=array('name','email','rolId','cedula','lastname');
  //hacer el algun join en la tabla

  public $_jointable=array();


  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
    $this->load->library('table');
    $this->table->set_heading('FECHA', 'CNB OPERADOR', 'CNB AGENCIA','CNB USER','CNB TERMINAL','DESCRIPCION TRX','MONTO TRX','MONTO COMISION');
  }

}

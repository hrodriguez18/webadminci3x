<?php
/**
 * Created by PhpStorm.
 * User: hrodriguez
 * Date: 09/14/18
 * Time: 03:14 PM
 */

class ExportToPdf extends CI_Model
{

	//Parametros para realizar la busqueda
	public $_tablename='';
	public $_primary_key='id';
	public $_primary_filter='intval';
	public $_order_by='id';
	public $_order='DESC';
	public $fecha_inicio='';
	public $fecha_fin='';
	public $_timestamps='';
	public $_limit=0;
	public $_search='';
	public $_offset=0;
	public $_selection=array();
	public $_valueSearch='';
	public $_searchingValue='';
	public $_isMap=FALSE;

	public $_especificFieldToSearch='';
	public $_especificValueToSearch='';

	public $withCount=FALSE;
	public $withCountCNB=FALSE;

	/*
	 *
	 *$_joinRelation=array(
	 *
	 * array(

					'table'=>'esb.rolesTerminal',
					'where_equals'=>'usuariosMonedero.rolId=rolesTerminal.id',
					'option'=>'left'
				)
		)
	 *
	 *
	 *
	 * */
	public $_joinRelation=array();
	public $_camposBusqueda=array();

	//parametros para definir el PDF
	public $_filename='';
	public $_headersTablePdf=array();
	public $_contentPdf='';
	public $_footerContentPdf='';
	public $_titlePdf='REPORTE BANCO NACIONAL DE PANAMA';
	public $_Subject='REPORTE NBP';
	public $_descriptionPdf='REPORTE DE USUARIOS MONEDERO';

	// template de la tabla PDF

	public $_template = array(

			'table_open' => '<table border="0" cellpadding="4" cellspacing="0">',

			'thead_open'            => '<thead>',
			'thead_close'           => '</thead>',

			'heading_row_start'     => '<tr>',
			'heading_row_end'       => '</tr>',
			'heading_cell_start'    => '<th>',
			'heading_cell_end'      => '</th>',

			'tbody_open'            => '<tbody>',
			'tbody_close'           => '</tbody>',

			'row_start'             => '<tr>',
			'row_end'               => '</tr>',
			'cell_start'            => '<td>',
			'cell_end'              => '</td>',

			'row_alt_start'         => '<tr>',
			'row_alt_end'           => '</tr>',
			'cell_alt_start'        => '<td>',
			'cell_alt_end'          => '</td>',

			'table_close'           => '</table>'
		);



	public function __construct()
	{
		parent::__construct();
		$this->load->library('Pdf');
	}

	public function generar() {

		$pdf = new Pdf('L', 'mm', 'USLEGAL', true, 'UTF-8', false);
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('hrodriguez');
		$pdf->SetTitle($this->_titlePdf);
		$pdf->SetSubject($this->_Subject);
		$pdf->SetKeywords('REPORTE');

		// datos por defecto de cabecera, se pueden modificar en el archivo tcpdf_config_alt.php de libraries/config

		$logo='/logobnp.jpg';
		$pdf->SetHeaderData($logo, PDF_HEADER_LOGO_WIDTH, strtoupper($this->_titlePdf), strtoupper($this->_descriptionPdf) . ' | GENERADO POR: '.strtoupper($this->session->userdata('name')).'. | FECHA DE CREACION : '.date('d-m-Y H:i:s') , array(0, 64, 255), array(0, 64, 128));
		$pdf->setFooterData($tc = array(0, 64, 0), $lc = array(0, 64, 128));

		// datos por defecto de cabecera, se pueden modificar en el archivo tcpdf_config.php de libraries/config
		$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
		// se pueden modificar en el archivo tcpdf_config.php de libraries/config
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		// se pueden modificar en el archivo tcpdf_config.php de libraries/config
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		// se pueden modificar en el archivo tcpdf_config.php de libraries/config
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		//relación utilizada para ajustar la conversión de los píxeles
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
		// ---------------------------------------------------------
		// establecer el modo de fuente por defecto
		$pdf->setFontSubsetting(true);

		// Establecer el tipo de letra

		//Si tienes que imprimir carácteres ASCII estándar, puede utilizar las fuentes básicas como
		// Helvetica para reducir el tamaño del archivo.
		$pdf->SetFont('freemono', '', 12, '', true);

		// Añadir una página
		// Este método tiene varias opciones, consulta la documentación para más información.
		$pdf->AddPage('L', 'A3');
		//fijar efecto de sombra en el texto
		$pdf->setTextShadow(array('enabled' => true, 'depth_w' => 0.2, 'depth_h' => 0.2, 'color' => array(196, 196, 196), 'opacity' => 1, 'blend_mode' => 'Normal'));
		//preparamos y maquetamos el contenido a crear
		//$html = $this->load->view('reports/testTable','',true);
		// Imprimimos el texto con writeHTMLCell()
		$pdf->writeHTML($this->_contentPdf.$this->_footerContentPdf, true, false, true, false, '');
		//$pdf->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $this->_contentPdf.$this->_footerContentPdf, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = '', $autopadding = true);
		// ---------------------------------------------------------
		// Cerrar el documento PDF y preparamos la salida
		// Este método tiene varias opciones, consulte la documentación para más información.

		ob_end_clean();
		$this->load->helper('download');
		header('Content-Disposition: attachment; filename="'.$this->_filename.'"');
		$data= $pdf->Output($this->_filename,'D');
		force_download($this->_filename, $data);
	}


	public function getPdfTrxByUsers(){


		if($this->input->post('inicio')){
			$this->fecha_inicio=$this->input->post('inicio').' 00:00:00';
			$this->fecha_fin=$this->input->post('fin').' 23:59:00';

		}

		if($this->input->post('search')){
			$this->_search=$this->input->post('search');
		}



		if(count($this->_selection)>0){
			for($i=0; $i < count($this->_selection); $i++) {
				$this->db->select($this->_selection[$i]);
			}
		}else{
			$this->db->select('*');
		}

		if(count($this->_camposBusqueda)> 0 && $this->_search!=''){
			$i=0;
			$this->db->like($this->_camposBusqueda[$i],$this->_search);
			for($i;$i<count($this->_camposBusqueda);$i++){
				$this->db->or_like($this->_camposBusqueda[$i],$this->_search);
			}

		}
		$users=$this->db->get('esb.usuariosMonedero');
		$Usuarios=array();
		$rows=array();

		if($users->num_rows()>0){
			foreach($users->result() as $value){

				//Buscar el numero de transacciones en el rango de tiempo.
				$this->db->select('id');

				if(strlen($this->fecha_inicio)>0){
					$this->db->where('stamp > ', $this->fecha_inicio);
					$this->db->where('stamp < ', $this->fecha_fin);
				}
				$this->db->where('cuenta',$value->cuentaMonedero);
				$noTransacciones=$this->db->get('esb.transactionMonederoLog')->num_rows();

				//Datos de la ultima transaccion;

				$this->db->select('transactionMonederoLog.stamp, transactionMonederoLog.descripcionTrx');
				$this->db->where('cuenta',$value->cuentaMonedero);
				$this->db->order_by('stamp','DESC');
				$this->db->limit(1);
				$transacciones=$this->db->get('esb.transactionMonederoLog');

				if($transacciones->num_rows()>0){
					foreach($transacciones->result() as $trx){
						$lastTransaction=$trx->descripcionTrx;
						$dateLastTrx=$trx->stamp;
					}
				}

				array_push($Usuarios,array(
					'id'=>$value->id,
					'name'=>$value->name,
					'lastname'=>$value->lastname,
					'cedula'=>$value->cedula,
					'telefono'=>$value->phone,
					'cuentaMonedero'=>$value->cuentaMondero,
					'noTransacciones'=>$noTransacciones,
					'lastTransaction'=>$lastTransaction,
					'dateLastTrx'=>$dateLastTrx
				));
			}
		}

		$this->table->set_template($this->_template);
		$this->table->set_heading($this->_headersTablePdf);
		$this->_contentPdf=$this->table->generate($Usuarios);
		$this->generar();


	}

	public function getPdf(){

		$this->SetProperties();

		if(count($this->_selection)>0){
			for($i=0; $i < count($this->_selection); $i++) {
				$this->db->select($this->_selection[$i]);
			}
		}else{
			$this->db->select('*');
		}

		if(count($this->_joinRelation)>0){
			for($i=0;$i<count($this->_joinRelation); $i++){
				$this->db->join($this->_joinRelation[$i]['table'],$this->_joinRelation[$i]['where_equals'],$this->_joinRelation[$i]['option']);
			}
		}


		if($this->_especificFieldToSearch != '' && $this->_especificValueToSearch != ''){
			$this->db->where($this->_especificFieldToSearch,$this->_especificValueToSearch);
		}

		if($this->fecha_inicio != '' && $this->fecha_fin != ''){
			$this->db->where($this->_timestamps.' > ',$this->fecha_inicio);
			$this->db->where($this->_timestamps.' < ',$this->fecha_fin );
		}

		if(count($this->_camposBusqueda)>0 && $this->_search != ''){
			$i=0;
			$this->db->like($this->_camposBusqueda[$i],$this->_search);
			$this->db->where($this->_timestamps.' > ',$this->fecha_inicio);
			$this->db->where($this->_timestamps.' < ',$this->fecha_fin );
			for($i;$i<count($this->_camposBusqueda);$i++){
				$this->db->or_like($this->_camposBusqueda[$i],$this->_search);
				$this->db->where($this->_timestamps.' > ',$this->fecha_inicio);
				$this->db->where($this->_timestamps.' < ',$this->fecha_fin );
			}
		}

		if($this->_limit>0){
			$this->db->limit($this->_limit,$this->_offset);
		}
		if($this->_order_by != ''){
			$this->db->order_by($this->_order_by,$this->_order);
		}


		$query=$this->db->get($this->_tablename)->result_array();

		$i=0;
		if($this->withCount){
			foreach ($query as  $value) {
				$this->db->select('*');
				$this->db->where('userMonedero',$value['email']);
				$query[$i]['cantidadTransacciones']=$this->db->get('esb.transactionMonederoLog')->num_rows();


				$this->db->select('*');
				$this->db->where('userId',$value['id']);
				$query[$i]['cantidadLogin']=$this->db->get('esb.loginMonedero')->num_rows();

				$i++;
			}
		}
		$i=0;
		if($this->withCountCNB){
			foreach ($query as  $value) {
				$this->db->select('*');
				$this->db->where('cnbOperador',$value['Descripcion']);


				$query[$i]['cantidadTransacciones']=$this->db->get('esb.transactionLog')->num_rows();


				$this->db->select('*');
				$this->db->where('operator',$value['Descripcion']);
				$query[$i]['cantidadLogin']=$this->db->get('esb.Log_Login')->num_rows();

				$i++;
			}
		}

		//var_dump($query);


		$this->table->set_template($this->_template);
		$this->table->set_heading($this->_headersTablePdf);
		$this->_contentPdf=$this->table->generate($query);
		$this->generar();
	}



	private function SetProperties(){
		$data = json_decode(file_get_contents('php://input'), true);

		if(isset($data['limit'])){
			$this->_limit= $data['limit'];
		}

		if(isset($data['offset'])){
			$this->_offset= $data['offset'];
		}

		if(isset($data['search'])){
			$this->_search=utf8_decode($data['search']);
		}

		if(isset($data['sort'])){
			$this->_sort=$data['sort'];
		}

		if(isset($data['sortOrder'])){
			$this->_order=$data['sortOrder'];
		}

		if(isset($data['sort'])){
			$this->_order_by=$data['sort'];
		}

		if(isset($data['fecha_inicio'])){
			$this->fecha_inicio=$data['fecha_inicio'].' 00:00:00';
		}

		if(isset($data['fecha_fin'])){
			$this->fecha_fin=$data['fecha_fin'].' 23:59:00';
		}

	}




}

?>

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class terminales_m extends MY_SuperModel {

  protected $_tablename='esb.Terminales';
  public  $_primary_key='ID_Usuario';
  protected $_primary_filter='intval';
  protected $_order_by='ID_Usuario';

  public  $_rules=array('Cod_Operador'=>array('field'=>'Cod_Operador','label'=>'Operador','rules'=>'trim|required|max_length[150]'),
                        'Cod_Agencia'=>array('field'=>'Cod_Agencia','label'=>'Agencia','rules'=>'trim|required|max_length[150]'),
                        'Estado'=>array('field'=>'Estado','label'=>'Estatus','rules'=>'trim|required|max_length[150]'),
                        'horarioId'=>array('field'=>'horarioId','label'=>'Horario','rules'=>'trim|required|max_length[150]'),
                        'terminalPhone'=>array('field'=>'terminalPhone','label'=>'Telefono','rules'=>'trim|required|max_length[11]'),
                        'limiteId'=>array('field'=>'limiteId','label'=>'Limite','rules'=>'trim|required|callback_diferente_de_default'),
                        'grupoTrxId'=>array('field'=>'grupoTrxId','label'=>'Grupo Transacciones','rules'=>'trim|required|max_length[150]')
                        );
  public $_inputs=array(
        'Cod_Operador'=>array('name'=>'Cod_Operador','id'=>'Cod_Operador','type'=>'selector','table_rel'=>'esb.Operadores','opt'=>'Cod_Operador','description'=>'Descripcion','class'=>'form-control','default'=>'0','label'=>'Seleccionar Comercio Facturador','jquery'=>'onchange="UpdateAgencia()"'),
        'Cod_Agencia'=>array('name'=>'Cod_Agencia','id'=>'Cod_Agencia','type'=>'selector','table_rel'=>'esb.Agencias','opt'=>'Cod_Agencia','description'=>'Descripcion','class'=>'form-control','default'=>'1','label'=>'Seleccionar Agencia Facturadora','jquery'=>'onload="VerificarOpToUpdate()"'),
        'Estado'=>array('name'=>'Estado','id'=>'Estado','type'=>'selector','table_rel'=>'esb.status','opt'=>'value','description'=>'description','class'=>'form-control','default'=>'A','label'=>'Status'),
        'limiteId'=>array('name'=>'limiteId','id'=>'limiteId','type'=>'selector','table_rel'=>'esb.Limites','opt'=>'Id','description'=>'descripcion','class'=>'form-control','default'=>'1','label'=>'Límite'),
        'binFilter'=>array('name'=>'binFilter','id'=>'binFilter','type'=>'selector','table_rel'=>'esb.grupoBines','opt'=>'Id','description'=>'descripcion','class'=>'form-control','default'=>'1','label'=>'Filtro Bin'),
        'horarioId'=>array('name'=>'horarioId','id'=>'horarioId','type'=>'selector','table_rel'=>'esb.grupoHorarios','opt'=>'id','description'=>'description','class'=>'form-control','default'=>'1','label'=>'Horario'),
        'grupoTrxId'=>array('name'=>'grupoTrxId','id'=>'grupoTrxId','type'=>'selector','table_rel'=>'esb.grupoTransacciones','opt'=>'id','description'=>'description','class'=>'form-control','default'=>'1','label'=>'Grupo Transacciones (comisiones).'),

	  'terminalPhone'=>array(

		  'name'=>'terminalPhone',
		  'id'=>'terminalPhone',
		  'type'=>'number',
		  'min'=>0,
		  'class'=>'form-control',
		  'label'=>'phone',
		  'placeholder'=>'Teléfono'

	  ),

	  'ID_Usuario'=>array('name'=>'ID_Usuario','type'=>'hidden','value'=>'')
      );
  protected $_timestamps=FALSE;
  protected $_form='';
  protected $_form_group_class='form-group ';
  protected $_attr_submit_button='';
  protected $_class_label='label label-warning';

  protected $_ruta='#';

	function __construct() {
		parent::__construct();
	}

	public function PopulateTable(){
		$this->db->select('Terminales.ID_Usuario,
                        Terminales.Cod_Operador,
                        Terminales.Cod_Agencia,
                        Terminales.Estado,
                        Terminales.No_Imei,
                        Terminales.No_SwipeCard,
                        Terminales.merchantId1,
                        Terminales.merchantId2,
                        Terminales.terminalNumber,
                        Terminales.limiteId,
                        Terminales.consumoDia,
                        Terminales.consumoMes,
                        Terminales.disponibleDia,
                        Terminales.disponibleMes,
                        Terminales.consumoTx,
                        Terminales.binFilter,
                        Terminales.terminalPhone,
                        Terminales.disponibleTx'
                      );
		$this->db->select('Agencias.Descripcion as AGENCIAS');
		$this->db->select('Operadores.descripcion as OPERADORES');
		$this->db->join('esb.Agencias','Terminales.Cod_Agencia=Agencias.Cod_Agencia','left');
		$this->db->join('esb.Operadores','Terminales.Cod_Operador=Operadores.Cod_Operador','left');
		return $this->db->get('esb.Terminales')->result();
	}

  public function optAgencias($cod){
    $this->db->select('Agencias.Cod_Agencia,Agencias.Descripcion');
    $this->db->where('Cod_Operador',$cod);
    $query=$this->db->get('esb.Agencias');
    $opt='';
    if($query->num_rows()>0){

      foreach ($query->result() as  $value) {
        $opt.='<option value="'.$value->Cod_Agencia.'">'.$value->Descripcion.'</option>';
      }
    }
    return $opt;
  }

}
?>

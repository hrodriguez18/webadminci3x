<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LimitesMonedero_m extends MY_SuperModel{

  //VARIABLES PARA HACER LAS CONSULTAS A LA BASE DE DATOS
  	public $_tablename='esb.limitesMonedero';
  	public $_primary_key='id';
  	public $_primary_filter='intval';
  	public $_order_by='id';



  //VARIABLES QUE SE NECESITAN PARA GENERAR EL FORMULARIO
  //type selector
  /*
  array('name'=>'imagen','id'=>'imagen',
  'type'=>'selector','table_rel'=>'esb.LogosEmpresas',
  'opt'=>'ruta','description'=>'nombre','class'=>'form-control',
  'default'=>'1','label'=>'Logo de Empresa')
  */
    public $documentationForForm='';
  	public  $_rules=array(
				'descripcion'=>array(
					'field'=>'descripcion',
					'label'=>'description',
					'rules'=>'trim|required|max_length[50]|min_length[6]'
				),
                  'limiteRetiroMes'=>array(
                                  'field'=>'limiteRetiroMes',
                                  'label'=>'limiteRetiroMes',
                                  'rules'=>'trim|required|max_length[11]'
                                ),

                  'limiteDepositoMes'=>array(
                                  'field'=>'limiteDepositoMes',
                                  'label'=>'limiteDepositoMes',
                                  'rules'=>'trim|required|max_length[11]'
                                )


    );
  	public $_inputs=array(

							'descripcion'=>array(
								'name'=>'descripcion',
								'id'=>'descripcion',
								'placeholder'=>'Descripción',
								'label'=>'description',
								'type'=>'text',
								'class'=>'form-control'
							),
                          'limiteRetiroMes'=>array(
                                          'name'=>'limiteRetiroMes',
                                          'id'=>'limiteRetiroMes',
                                          'placeholder'=>'Limite Retiro Mensual',
                                          'min'=>0,
                                          'label'=>'limiteRetiroMes',
                                          'type'=>'number',
                                          'class'=>'form-control'
                                        ),

                          'limiteDepositoMes'=>array(
                                            'name'=>'limiteDepositoMes',
                                            'id'=>'limiteDepositoMes',
                                            'type'=>'number',
							  				'min'=>0,
                                            'placeholder'=>'limiteDepositoMes',
                                            'label'=>'limiteDepositoMes',
                                            'class'=>'form-control'
                                          ),


                          'id'=>array(
                                        'name'=>'id',
                                        'id'=>'id',
                                        'type'=>'hidden'
                                      )

                        );

  	protected $_timestamps=FALSE;
  	protected $_form='';
  	protected $_form_group_class='form-group ';
  	protected $_attr_submit_button='';
  	protected $_class_label='';
  //ESTA RUTA LE PERMITE A LA VISTA SABER HACIA DONDE BUSCAR EL ECHO DEL FORMULARIO
  	protected $_ruta='';

  	//RELACION PARA GENERAR LA TABLA DE ESTA CLASE
  	protected $_headingTable=array();
  	//usado para buscar los datos de la db
  	protected $_datatable=array();
  	protected $_joinForBootstrapTable=array();
  	//usado para mostrar los datos en la tabla e incluir los btn actions
  	protected $_dataForTable=array();
  	protected $_jointable=array();
  	//para generar los informes en PDF
  	public $_TableToExport='';
  	public $_footer_ContentToExport='';
  	public $_pdfTitle='';
  	public $_pdfDescription='';
  	public $_headersTable=array();
  	public $_selectToExport=array();
  	public $_orderName='';
  	public $_order='';
  	public $_join=array();
  	public $_dateVariable='';

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
	  //
	  //
  }



}

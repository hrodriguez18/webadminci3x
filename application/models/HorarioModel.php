<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class HorarioModel extends MY_SuperModel {

	protected $_tablename='esb.grupoHorarios';
	protected $_primary_key='id';
	protected $_primary_filter='intval';
	protected $_order_by='id';

	public  $_rules=array('description'=>array('field'=>'description','label'=>'Descripcion','rules'=>'trim|required'),
		'startSchedule'=>array('field'=>'startSchedule','label'=>'Hora Inicio','rules'=>'trim|required'),
		'endSchedule'=>array('field'=>'endSchedule','label'=>'Hora Fin','rules'=>'trim|required')
		);
	public $_inputs=array('id'=>array('name'=>'id','id'=>'id','type'=>'hidden','value'=>''),
		'description'=>array('name'=>'description','type'=>'text','placeholder'=>'Descripción','label'=>'Descripcion','value'=>'','class'=>'form-control'),
			'startSchedule'=>array('name'=>'startSchedule','id'=>'startSchedule','value'=>'','type'=>'time','placeholder'=>'Inicio','label'=>'Hora Inicio','class'=>'form-control'),
			'endSchedule'=>array('name'=>'endSchedule','id'=>'endSchedule','value'=>'','type'=>'time','placeholder'=>'Fin','label'=>'Hora Fin','class'=>'form-control'),
			'grupoDiasId'=>array('name'=>'grupoDiasId','id'=>'grupoDiasId','type'=>'selector','table_rel'=>'esb.grupoDias','opt'=>'id','description'=>'description','class'=>'form-control','default'=>'1','label'=>'Grupo Dias'),

			'excepciones'=>array('name'=>'excepciones','id'=>'excepciones','type'=>'selector','table_rel'=>'esb.grupoExcepciones','opt'=>'id','description'=>'description','class'=>'form-control','default'=>'1','label'=>'Excepciones'),


			'status'=>array('name'=>'status','id'=>'status','type'=>'selector','table_rel'=>'esb.status','opt'=>'value','description'=>'description','class'=>'form-control','default'=>'A','label'=>'Estado'),

			);
	protected $_timestamps=FALSE;
	protected $_form='';
	protected $_form_group_class='form-group';
	protected $_attr_submit_button='class="btn btn-success"';
	protected $_class_label='class="label label-warning"';

	protected $_ruta='AdminHorarios/';

	//RELACION PARA GENERAR LA TABLA DE ESTA CLASE
	protected $_headingTable=array();
	//usado para buscar los datos de la db
	protected $_datatable=array();
	//usado para mostrar los datos en la tabla e incluir los btn actions
	protected $_dataForTable=array();
	protected $_jointable=array();



	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}

	public function DataTable(){
		$this->db->select('grupoHorarios.id,grupoHorarios.description,grupoHorarios.status,grupoHorarios.exceptionId');
		$this->db->select('grupoExcepciones.description  as EXC');
		$this->db->join('esb.grupoExcepciones','grupoHorarios.exceptionId=grupoExcepciones.id','left');
		return $this->db->get($this->_tablename)->result();
	}

	public function AlmacenarNuevoHorario($data){
		$this->db->insert('esb.grupoHorarios',$data);
		return $this->db->insert_id();
	}

	public function description($id){
		$this->db->select('description');
		$this->db->where('id',$id);
		$info='';
		$query=$this->db->get('esb.grupoHorarios');
		foreach ($query->result() as $value) {
			$info=$value->description;
		}
		return $info;
	}

	public function CleanRelationGroup($idHorario){
		$this->db->where('horarioId',$idHorario);
		$this->db->delete('esb.relationHorariosDays');
		return TRUE;
	}

	public function UpdateHorario($id,$data){
		$this->db->where('id',$id);
		$this->db->update('esb.grupoHorarios',$data);
	}

	public function TableHorario($id){
		$this->db->select('*');
		$this->db->where('horarioId',$id);
		$query=$this->db->get('esb.relationHorariosDays');
		$dias=array('Lunes'=>'L','Martes'=>'M','Miercoles'=>'W','Jueves'=>'J','Viernes'=>'V','Sabado'=>'S','Domingo'=>'D');
		$datos=array();
		$count=0;
		$table='';
		for ($i=0; $i <=6 ; $i++) {
			$datos[$i]=array();
		}
		foreach ($query->result() as $value){
			$i=0;
			foreach($dias as $nombre => $dia){
				if($value->day==$dias[$nombre]){
					$datos[$i]['day']=$value->day;
					$datos[$i]['hora_inicio']=$value->hora_inicio;
					$datos[$i]['hora_fin']=$value->hora_fin;
				}
				$i++;
			}
		}
		$i=0;
		foreach($dias as $nombre => $dia){
			if(isset($datos[$i]['day'])){
				$table.='<tr><td>'.$nombre.'</td>
				<td><input type="time" name="hora_inicio[]" class="form-control" value="'.$datos[$i]['hora_inicio'].'"/></td>
				<td><input type="time" name="hora_fin[]" class="form-control" value="'.$datos[$i]['hora_fin'].'"/></td>
				<td><input type="checkbox" name="dias[]"  checked="true" value="'.$dia.'"/><td></tr>';
			}else{
				$table.='<tr><td>'.$nombre.'</td>
				<td><input type="time" name="hora_inicio[]" class="form-control" value="00:00"/></td>
				<td><input type="time" name="hora_fin[]" class="form-control" value="00:00"/></td>
				<td><input type="checkbox" name="dias[]"   value="'.$dia.'"/><td></tr>';
			}
			$i++;
		}

		return $table;
	}

	public function deleteHorario($id){
		$this->db->where('id',$id);
		$this->db->delete('esb.grupoHorarios');

		$this->db->where('horarioId',$id);
		$this->db->delete('esb.relationHorariosDays');

		return true;
	}

	public function SelectorExcepciones(){
		$this->db->select('*');
		$query=$this->db->get('esb.grupoExcepciones');
		$option=array();
		foreach ($query->result() as $value) {
			$option[$value->id]=$value->description;
		}
		return form_dropdown('excepciones',$option,'','class="form-control"');
	}




}

/* End of file HorarioModel.php */
/* Location: ./application/models/HorarioModel.php */

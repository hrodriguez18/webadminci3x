<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DashboardModel extends CI_Model
{

	public $_tablename='';
	public $_order_by='';
	public $_filter='intval';
	public $_primary_key='';
	public $_order='';
	public $_fieldsSelected=array();
	public $_valueToSearch='';
	public $_especificValue='';
	public $_especificField='';
	public $_fieldToSearch=array();
	public $_timestamps='';
	public $_fechaInicio='';
	public $_fechaFin='';
	public $_limit =0, $_offset = 0;


	/*
	 * for $_joins;
	 * array(
	 * 		'table'=>'table_name',
	 * 		'relation'=>'join_table.equals=table_name.equals',
	 * 		'type'=>'left'
	 * 		)
	 *
	 * */
	public $_joins=array();



	public function __construct()
	{
		parent::__construct();
		//Do your magic here;


	}

	public function getData(){

		if(count($this->_fieldsSelected)>0){
			foreach ($this->_fieldsSelected as $field){
				$this->db->select($field);
			}
		}else{
			$this->db->select('*');
		}

		if(count($this->_joins)>0){
			foreach ($this->_joins as  $join){
				$this->db->join($join['table'],$join['relation'],$join['type']);
			}
		}

		if(count($this->_fieldToSearch)>0){

			foreach ($this->_fieldToSearch as $fields){
				$this->db->where($this->_fields, $this->_valueToSearch);
			}
		}

		if($this->_especificField != '' && $this->_especificValue !=''){
			$this->db->where($this->_especificField,$this->_especificValue);
		}

		if($this->_fechaFin != '' && $this->_fechaInicio !=''){
			$this->db->where($this->_timestamps.' >',$this->_fechaInicio);
			$this->db->where($this->_timestamps.' <',$this->_fechaFin);
		}

		if($this->_limit > 0){
			$this->db->limit($this->_limit,$this->_offset);
		}

		if($this->_order_by != '' && $this->_order != ''){
			$this->db->order_by($this->_order_by,$this->_order);
		}
		//var_dump($this->db->last_query());
		return json_encode($this->db->get($this->_tablename)->result());
	}


	public function getCounters(){
		$data['debito']=number_format($this->getDebito(),'2','.',',');
		$data['transacciones']=$this->getTransactions();
		$data['credito']=number_format($this->getCredito(),'2','.',',');
		$data['nameLastKpi']='Devoluciones';

		if($this->_tablename=='esb.transactionMonederoLog'){
			$data['devoluciones']=$this->getTransferencias();
			$data['nameLastKpi']='Transferencias';
		}

		if($this->_tablename=='esb.transactionLog'){
			$data['devoluciones']=$this->getDevoluciones();
			$data['nameLastKpi']='Devoluciones';
		}

		$data['balance']=number_format($this->getBalance(),'2','.',',');

		return $this->load->view('paper/components/counters', $data, TRUE);
	}

	public function getDevoluciones(){
		$this->db->select('id');
		$this->db->where('tipoTrx','120');
		$this->db->where('stamp > ',$this->_fechaInicio);
		$this->db->where('stamp < ', $this->_fechaFin);
		$query=$this->db->get($this->_tablename);
		$montos=$query->num_rows();
		return $montos;
	}

	public function getTransferencias(){
		$this->db->select('id');
		$this->db->where('tipoTrx','105');
		$this->db->where('stamp > ',$this->_fechaInicio);
		$this->db->where('stamp < ', $this->_fechaFin);
		$query=$this->db->get($this->_tablename);
		$montos=$query->num_rows();
		return $montos;
	}

	public function getBalance(){
		$balance=$this->getCredito() - $this->getDebito();
		return $balance;
	}


	public function getAllBalance(){

		$this->load->helper('date');

		if($this->input->post('year')){
			$year=$this->input->post('year');
			$mes=$this->input->post('month');
		}else{
			$year=date('Y');
			$mes=date('m');
		}

		$lastDay=days_in_month($mes, $year);
		$fechaInicio=$year.'-'.$mes.'-01 00:00:00';
		$fechaFin=$year.'-'.$mes.'-'.$lastDay.' 23:59:00';



		$this->db->select_sum('montoTrx');
		$this->db->where('aux','-');
		$this->db->where('stamp > ',$fechaInicio);
		$this->db->where('stamp < ',$fechaFin);
		$query=$this->db->get('esb.transactionMonederoLog')->result_array();
		$debitoMonedero=$query[0]['montoTrx'];

		$this->db->select_sum('montoTrx');
		$this->db->where('aux','+');
		$this->db->where('stamp > ',$fechaInicio);
		$this->db->where('stamp < ', $fechaFin);
		$query=$this->db->get('esb.transactionMonederoLog')->result_array();
		$creditoMonedero=$query[0]['montoTrx'];

		$this->db->select_sum('montoTrx');
		$this->db->where('aux','-');
		$this->db->where('stamp > ',$fechaInicio);
		$this->db->where('stamp < ', $fechaFin);
		$query=$this->db->get('esb.transactionLog')->result_array();
		$debitoCnb=$query[0]['montoTrx'];

		$this->db->select_sum('montoTrx');
		$this->db->where('aux','+');
		$this->db->where('stamp > ',$fechaInicio);
		$this->db->where('stamp < ', $fechaFin);
		$query=$this->db->get('esb.transactionLog')->result_array();
		$creditoCnb=$query[0]['montoTrx'];


		return $creditoCnb+$creditoMonedero-$debitoCnb-$debitoMonedero;

	}

	public function getTransactions(){
		$arreglo=json_decode($this->getData());
		return count($arreglo);
	}

	public function getDebito(){
		$this->db->select_sum('montoTrx');
		$this->db->where('aux','-');
		$this->db->where('stamp > ',$this->_fechaInicio);
		$this->db->where('stamp < ', $this->_fechaFin);
		$query=$this->db->get($this->_tablename)->result_array();
		$montos=$query[0]['montoTrx'];
		return $montos;
	}
	public function getCredito(){
		$this->db->select_sum('montoTrx');
		$this->db->where('aux','+');
		$this->db->where('stamp > ',$this->_fechaInicio);
		$this->db->where('stamp < ', $this->_fechaFin);
		$query=$this->db->get($this->_tablename)->result_array();
		$montos=$query[0]['montoTrx'];
		return $montos;
	}

	public function getAverage(){
		$promedio=number_format(0,"2",".",",");

		if($this->getTransactions() != 0){
			$valor=floatval($this->getSales()/$this->getTransactions());
			$promedio=number_format($valor,"2",".",",");
		}
		return $promedio;
	}

	public function FormSearchCounters(){
		$y=intval(date("Y"));
		$y_ini=$y-4;
		$y_fin=$y+7;
		$fecha=array();
		for($i=$y_ini;$i<=$y_fin;$i++){
			$fecha[$i]=$i;
		}

		$mes=array(1=>'ene',
			2=>'feb',
			3=>'mar',
			4=>'abr',
			5=>'may',
			6=>'jun',
			7=>'jul',
			8=>'ago',
			9=>'sep',
			10=>'oct',
			11=>'nov',
			12=>'dic');

		foreach ($mes as $key=>$value){
			$mes[$key]=$this->lang->line($value);
		}

		$formulario="";

		$opt=array("value"=>"Cod_Pais","description"=>"Descripcion");

		$pais=$this->getarray("esb.Pais",$opt);



		$formulario.='<div class="col-lg-4"><div class="form-group ">'.form_label($this->lang->line('year'),'anio');
		$formulario.=form_dropdown("anio",$fecha,intval(date("Y")),'class="selectpicker" data-style="btn-info" id="anio" onChange="UpdateKPI()"').'</div></div>';

		$formulario.='<div class="col-lg-4"><div class="form-group ">'.form_label($this->lang->line('month'),'mes');
		$formulario.=form_dropdown("mes",$mes,intval(date("m")),'class="selectpicker" data-style="btn-info " id="mes" onChange="UpdateKPI()"').'</div></div>';

		$formulario.='<div class="col-lg-4"><div class="form-group">'.form_label($this->lang->line('country'),'pais');
		$formulario.=form_dropdown("pais",$pais,'PAN','class="selectpicker" data-style="btn-info " id="pais" onChange="UpdateKPI()"').'</div></div>';


		return $formulario;
	}


	public function getArray($tablename,$data=array("value"=>"","description"=>"")){
		$this->db->select('*');
		$this->db->order_by($data['description'],'ASC');
		$query=$this->db->get($tablename);

		$selector=array();
		if($query->num_rows()>0){
			foreach ($query->result() as $key){
				$selector[$key->$data["value"]]=$key->$data["description"];
			}
		}
		return $selector;
	}

	public function FormSearchCountersCNB(){
		$y=intval(date("Y"));
		$y_ini=$y-4;
		$y_fin=$y+7;
		$fecha=array();
		for($i=$y_ini;$i<=$y_fin;$i++){
			$fecha[$i]=$i;
		}

		$mes=array(1=>'ene',
			2=>'feb',
			3=>'mar',
			4=>'abr',
			5=>'may',
			6=>'jun',
			7=>'jul',
			8=>'ago',
			9=>'sep',
			10=>'oct',
			11=>'nov',
			12=>'dic');

		foreach ($mes as $key=>$value){
			$mes[$key]=$this->lang->line($value);
		}

		$formulario="";

		$optOperador=array("value"=>"Descripcion","description"=>"Descripcion");

		$Operadores=$this->getarray("esb.Operadores",$optOperador);


		$optAgencias=array("value"=>"Descripcion","description"=>"Descripcion");

		$Agencias=$this->getarray("esb.Agencias",$optAgencias);



		$formulario.='<div class="col-lg-4"><div class="form-group ">'.form_label($this->lang->line('year'),'anio');
		$formulario.=form_dropdown("anio",$fecha,intval(date("Y")),'class="selectpicker" data-style="btn-info" id="anio" onChange="UpdateKPI()"').'</div></div>';

		$formulario.='<div class="col-lg-4"><div class="form-group ">'.form_label($this->lang->line('month'),'mes');
		$formulario.=form_dropdown("mes",$mes,intval(date("m")),'class="selectpicker" data-style="btn-info " id="mes" onChange="UpdateKPI()"').'</div></div>';

		$formulario.='<div class="col-lg-4"><div class="form-group">'.form_label('CNB','operador');
		$formulario.=form_dropdown("operador",$Operadores,'','class="selectpicker" data-style="btn-info " id="operador" onChange="UpdateKPI()"').'</div></div>';


		//$formulario.='<div class="col-lg-4"><div class="form-group">'.form_label('Agencia','agencia');
		//$formulario.=form_dropdown("agencia",$Agencias,'','class="selectpicker" data-style="btn-info " id="agencia" onChange="UpdateKPI()"').'</div></div>';

		return $formulario;
	}


	public function FormSearchCountersAdmin(){
		$y=intval(date("Y"));
		$y_ini=$y-4;
		$y_fin=$y+7;
		$fecha=array();
		for($i=$y_ini;$i<=$y_fin;$i++){
			$fecha[$i]=$i;
		}

		$mes=array(1=>'ene',
			2=>'feb',
			3=>'mar',
			4=>'abr',
			5=>'may',
			6=>'jun',
			7=>'jul',
			8=>'ago',
			9=>'sep',
			10=>'oct',
			11=>'nov',
			12=>'dic');

		foreach ($mes as $key=>$value){
			$mes[$key]=$this->lang->line($value);
		}

		$formulario="";

		$optOperador=array("value"=>"Descripcion","description"=>"Descripcion");

		$Operadores=$this->getarray("esb.Operadores",$optOperador);


		$optAgencias=array("value"=>"Descripcion","description"=>"Descripcion");

		$Agencias=$this->getarray("esb.Agencias",$optAgencias);



		$formulario.='<div class="col-lg-6"><div class="form-group ">'.form_label($this->lang->line('year'),'anio');
		$formulario.=form_dropdown("anio",$fecha,intval(date("Y")),'class="selectpicker" data-style="btn-info" id="anio" onChange="UpdateKPI()"').'</div></div>';

		$formulario.='<div class="col-lg-6"><div class="form-group ">'.form_label($this->lang->line('month'),'mes');
		$formulario.=form_dropdown("mes",$mes,intval(date("m")),'class="selectpicker" data-style="btn-info " id="mes" onChange="UpdateKPI()"').'</div></div>';

		//$formulario.='<div class="col-lg-4"><div class="form-group">'.form_label('CNB','operador');
		//$formulario.=form_dropdown("operador",$Operadores,'','class="selectpicker" data-style="btn-info " id="operador" onChange="UpdateKPI()"').'</div></div>';


		//$formulario.='<div class="col-lg-4"><div class="form-group">'.form_label('Agencia','agencia');
		//$formulario.=form_dropdown("agencia",$Agencias,'','class="selectpicker" data-style="btn-info " id="agencia" onChange="UpdateKPI()"').'</div></div>';

		return $formulario;
	}

	public function getAllCounter(){

		$mes=date('m');
		$year=date('Y');

		if($this->input->post('year')){
			$year=$this->input->post('year');
		}

		if($this->input->post('month')){
			$mes=$this->input->post('month');
		}


		$lastDay=days_in_month($mes, $year);

		$fechaInicio=$year.'-'.$mes.'-01 00:00:00';
		$fechaFin=$year.'-'.$mes.'-'.$lastDay.' 23:59:00';



		$objCnb=new DashboardModel();
		$objCnb->_tablename='esb.transactionLog';

		$objCnb->_order='DESC';
		$objCnb->_order_by='id';
		$objCnb->_limit=0;
		$objCnb->_timestamps='stamp';

		$objCnb->_fechaInicio=$fechaInicio;
		$objCnb->_fechaFin=$fechaFin;



		$objMonedero=new DashboardModel();
		$objMonedero->_tablename='esb.transactionMonederoLog';
		$objMonedero->_order='DESC';
		$objMonedero->_order_by='id';
		$objMonedero->_limit=0;
		$objMonedero->_timestamps='stamp';

		$objMonedero->_fechaInicio=$fechaInicio;
		$objMonedero->_fechaFin=$fechaFin;


		/*$objMpos=new DashboardModel();
		$objMpos->_tablename='esb.Log_Transaccional';
		$objMpos->_order='DESC';
		$objMpos->_order_by='TranNum';
		$objMpos->_primary_key='TranNum';
		$objMpos->_limit=0;
		$objMpos->_timestamps='timeStamp';

		$objMpos->_fechaInicio=$fechaInicio;
		$objMpos->_fechaFin=$fechaFin;*/

		$data['debito']=number_format($objCnb->getDebito()+$objMonedero->getDebito(),'2','.',',');
		$data['transacciones']=$objCnb->getTransactions()+$objMonedero->getTransactions();
		$data['credito']=number_format($objCnb->getCredito()+$objMonedero->getCredito(),'2','.',',');
		$data['devoluciones']=$objCnb->getDevoluciones()+$objMonedero->getDevoluciones();
		$data['balance']=number_format($this->getAllBalance(),'2','.',',');



		return $this->load->view('paper/components/counters', $data, TRUE);

	}



}

/* End of file .php */


?>

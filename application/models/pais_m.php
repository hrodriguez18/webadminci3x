<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class pais_m extends MY_Model {
 	public $_table_name = 'esb.Pais';
	protected $_primary_key = 'Cod_Pais';
	protected $_primary_filter = 'strval';
	protected $_order_by = 'Cod_Moneda';
	public $rules = array(

		'Cod_Pais'=>array('field'=>'cod_pais','label'=>'Código Pais','rules'=>'trim|required|alpha|max_length[3]|callback_unico_pais'),
		'Descripcion'=>array('field'=>'descripcion','label'=>'Descripción','rules'=>'trim|required'),
		'moneda'=>array('field'=>'moneda','label'=>'Moneda','rules'=>'trim|required|numeric'),
		'lat1'=>array('field'=>'lat1','label'=>'Latitud Inicial.','rules'=>'trim|required'),
		'lat2'=>array('field'=>'lat2','label'=>'Latitud Final.','rules'=>'trim|required'),
		'lon1'=>array('field'=>'lon1','label'=>'Longitud Inicial','rules'=>'trim|required'),
		'lon2'=>array('field'=>'lon2','label'=>'Longitud Final','rules'=>'trim|required')
	);

	public $rules_update = array(

		'Cod_Pais'=>array('field'=>'cod_pais','label'=>'Código Pais','rules'=>'trim|required|alpha|max_length[3]'),
		'Descripcion'=>array('field'=>'descripcion','label'=>'Descripción','rules'=>'trim|required'),
		'moneda'=>array('field'=>'moneda','label'=>'Moneda','rules'=>'trim|required|numeric'),
		'lat1'=>array('field'=>'lat1','label'=>'Latitud Inicial.','rules'=>'trim|required'),
		'lat2'=>array('field'=>'lat2','label'=>'Latitud Final.','rules'=>'trim|required'),
		'lon1'=>array('field'=>'lon1','label'=>'Longitud Inicial','rules'=>'trim|required'),
		'lon2'=>array('field'=>'lon2','label'=>'Longitud Final','rules'=>'trim|required')
	);

	protected $_timestamps = FALSE;

	private $template = array(
        'table_open'            => '<table border="0" cellpadding="4" cellspacing="0" class="table table-hover table-striped" id="bootstrap-table">',

        'thead_open'            => '<thead>',
        'thead_close'           => '</thead>',

        'heading_row_start'     => '<tr>',
        'heading_row_end'       => '</tr>',
        'heading_cell_start'    => '<th data-sortable="true">',
        'heading_cell_end'      => '</th>',

        'tbody_open'            => '<tbody id="body_table_moneda">',
        'tbody_close'           => '</tbody>',

        'row_start'             => '<tr>',
        'row_end'               => '</tr>',
        'cell_start'            => '<td>',
        'cell_end'              => '</td>',

        'row_alt_start'         => '<tr>',
        'row_alt_end'           => '</tr>',
        'cell_alt_start'        => '<td>',
        'cell_alt_end'          => '</td>',

        'table_close'           => '</table>'
	);



	function __construct() {
		parent::__construct();
	}

	public function unico_pais($str){
		$this->db->select('*');
		$this->db->where('Cod_Pais',$str);
		$query=$this->db->get('esb.Pais');
		if($query->num_rows()>0){
			$this->form_validation->set_message('unico_pais', 'Este Valor ya Existe...');
			return false;
		}else{
			return true;
		}
		return false;
	}




	public function SetTable($Column,$data){
		$this->table->set_template($this->template);
		$this->table->set_heading($Column);
		$i=0;
		$info=array();
		foreach($data as  $value) {
			$editar='<button class="btn btn-primary" onclick="editMoneda('.$value->Cod_Moneda.')"><i class="ti-pencil-alt" ></i></button>';
			$eliminar='<button class="btn btn-danger" onclick="deleteMoneda('.$value->Cod_Moneda.')"><i class="ti-trash"></i></button>';
			$info[$i]=array(($i+1),$value->Cod_Moneda,$value->Descripcion,$value->DescripcionCorta,$editar,$eliminar);
			$i++;
		}
		return $this->table->generate($info);
		//return var_dump($data);
	}

	public function RefreshTableMoneda(){
		$query=$this->get();
		$i=1;
		$info="";
		foreach ($query as $value) {
			$info.='<tr><td></td><td>'.$value->Cod_Moneda.'</td><td>'.$value->Descripcion.'</td><td>'.$value->DescripcionCorta.'</td><td></td></tr>';
			$i++;
		}
		return $info;
	}

	public function PopulateTable(){
		$this->db->select('Pais.Cod_Pais,Pais.Descripcion,Pais.Bandera,Monedas.Descripcion as moneda');
		$this->db->join('esb.Monedas','Pais.Cod_Moneda=Monedas.Cod_Moneda','left');
		return $this->db->get('esb.Pais')->result();
	}

	public function optionMoneda(){
		$this->db->select('*');
		$query=$this->db->get('esb.Monedas');
		$option=array();
		foreach ($query->result() as  $value) {
			$option[$value->Cod_Moneda]=$value->Descripcion;
		}

		return $option;
	}

	public function FormularioPais(){
		$CodPais=array('name'=>'cod_pais',
			'id'=>'cod_pais',
			'type'=>'text',
			'placeholder'=>'Código País',
			'value'=>set_value('cod_pais'),
			'class'=>'form-control'

		);
		$descripcion=array('name'=>'descripcion',
			'id'=>'descripcion',
			'placeholder'=>'Descripcion',
			'value'=>set_value('descripcion'),
			'class'=>'form-control',
			'style'=>'resize:none;',
			'rows'=>'3'
		);
		$bandera=array('name'=>'bandera',
			'id'=>'bandera',
			'type'=>'file',
			'placeholder'=>'Código Moneda',
			'value'=>set_value('bandera'),
			'class'=>'form-control',
			'required'=>'true'
		);


		$moneda=$this->optionMoneda();
		$LATI=array('name'=>'lat1',
			'id'=>'lat1',
			'type'=>'text',
			'placeholder'=>'Límite Latitud Inicial',
			'value'=>set_value('lat1'),
			'class'=>'form-control'

		);
		$LATF=array('name'=>'lat2',
			'id'=>'lat2',
			'type'=>'text',
			'placeholder'=>'Límite Latitud Final',
			'value'=>set_value('lat2'),
			'class'=>'form-control'

		);
		$LONI=array('name'=>'lon1',
			'id'=>'lon1',
			'type'=>'text',
			'placeholder'=>'Límite Longitud Inicial',
			'value'=>set_value('lon1'),
			'class'=>'form-control'

		);
		$LONF=array('name'=>'lon2',
			'id'=>'lon2',
			'type'=>'text',
			'placeholder'=>'Límite Longitud Final',
			'value'=>set_value('lon2'),
			'class'=>'form-control'
		);


		$formulario='<div class="form-group">'.form_error($CodPais['name']).$this->lang->line('countryCode').form_input($CodPais,set_value($CodPais['name'])).'</div>';
		$formulario.='<div class="form-group">'.form_error($descripcion['name']).$this->lang->line('name').form_textarea($descripcion,set_value($descripcion['name'])).'</div>';
		$formulario.='<div class="form-group">'.form_error($bandera['name']).$this->lang->line('flag').form_input($bandera).'</div>';
		$formulario.='<div class="form-group">'.$this->lang->line('currency').form_dropdown('moneda',$moneda,set_value('moneda'),'class="form-control"').'</div>';

		$formulario.='<div class="form-group">'.form_error($LATI['name']).$this->lang->line('Lat1').form_input($LATI).'</div>';
		$formulario.='<div class="form-group">'.form_error($LATF['name']).$this->lang->line('Lat2').form_input($LATF,set_value($LATF['name'])).'</div>';
		$formulario.='<div class="form-group">'.form_error($LONI['name']).$this->lang->line('Lon1').form_input($LONI,set_value($LONI['name'])).'</div>';
		$formulario.='<div class="form-group">'.form_error($LONF['name']).$this->lang->line('Lon2').form_input($LONF,set_value($LONF['name'])).'</div>';

		return $formulario;

	}

	public function FormularioPaisEdit($datos){
		$CodPais=array('name'=>'cod_pais',
			'id'=>'cod_pais',
			'type'=>'text',
			'placeholder'=>'Código País',
			'value'=>$datos->Cod_Pais,
			'class'=>'form-control',
			'readonly'=>'true'

		);
		$descripcion=array('name'=>'descripcion',
			'id'=>'descripcion',
			'placeholder'=>'Descripcion',
			'value'=>$datos->Descripcion,
			'class'=>'form-control',
			'style'=>'resize:none;',
			'rows'=>'3'
		);
		$bandera=array('name'=>'bandera',
			'id'=>'bandera',
			'type'=>'file',
			'placeholder'=>'Código Moneda',
			'value'=>'',
			'class'=>'form-control',
			'required'=>'true'
		);


		$moneda=$this->optionMoneda();
		$LATI=array('name'=>'lat1',
			'id'=>'lat1',
			'type'=>'text',
			'placeholder'=>'Límite Latitud Inicial',
			'value'=>$datos->Lat1,
			'class'=>'form-control'

		);
		$LATF=array('name'=>'lat2',
			'id'=>'lat2',
			'type'=>'text',
			'placeholder'=>'Límite Latitud Final',
			'value'=>$datos->Lat2,
			'class'=>'form-control'

		);
		$LONI=array('name'=>'lon1',
			'id'=>'lon1',
			'type'=>'text',
			'placeholder'=>'Límite Longitud Inicial',
			'value'=>$datos->Lon1,
			'class'=>'form-control'

		);
		$LONF=array('name'=>'lon2',
			'id'=>'lon2',
			'type'=>'text',
			'placeholder'=>'Límite Longitud Final',
			'value'=>$datos->Lon2,
			'class'=>'form-control'

		);


		$formulario='<div class="form-group">'.form_error($CodPais['name']).$this->lang->line('countryCode').form_input($CodPais,set_value($CodPais['name'])).'</div>';
		$formulario.='<div class="form-group">'.form_error($descripcion['name']).$this->lang->line('name').form_textarea($descripcion,set_value($descripcion['name'])).'</div>';
		$formulario.='<div class="form-group">'.form_error($bandera['name']).$this->lang->line('flag').form_input($bandera).'</div>';
		$formulario.='<div class="form-group">'.$this->lang->line('currency').form_dropdown('moneda',$moneda,set_value('moneda'),'class="form-control"').'</div>';

		$formulario.='<div class="form-group">'.$this->lang->line('lat1').form_error($LATI['name']).form_input($LATI).'</div>';
		$formulario.='<div class="form-group">'.$this->lang->line('lat2').form_error($LATF['name']).form_input($LATF,set_value($LATF['name'])).'</div>';
		$formulario.='<div class="form-group">'.$this->lang->line('lon1').form_error($LONI['name']).form_input($LONI,set_value($LONI['name'])).'</div>';
		$formulario.='<div class="form-group">'.$this->lang->line('lon2').form_error($LONF['name']).form_input($LONF,set_value($LONF['name'])).'</div>';

		return $formulario;

	}






}
?>

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MontosModel extends MY_SuperModel {

	protected $_tablename='esb.montos';
	protected $_primary_key='id';
	protected $_primary_filter='intval';
	protected $_order_by='id';

	public  $_rules=array('description'=>array('field'=>'description','label'=>'Descripción','rules'=>'trim|required|max_length[50]'),
		'montoFijo'=>array('field'=>'montoFijo','label'=>'Monto Fijo','rules'=>'trim|required'),
		'montoPorcentual'=>array('field'=>'montoPorcentual','label'=>'Monto Porcentual','rules'=>'trim|required'),
		'montoPorcentualCNB'=>array('field'=>'montoPorcentualCNB','label'=>'Comisión CNB (%)','rules'=>'trim|required')
);

	public $_inputs=array('id'=>array('name'=>'id','type'=>'hidden','value'=>''),
				'description'=>array('name'=>'description','type'=>'text','placeholder'=>'Descripción','value'=>'','label'=>'Descripción','class'=>'form-control'),
				'montoFijo'=>array('name'=>'montoFijo','type'=>'number','placeholder'=>'Precio Fijo','value'=>'','label'=>'Precio Fijo','class'=>'form-control','step'=>'any'),
				'montoPorcentual'=>array('name'=>'montoPorcentual','type'=>'number','placeholder'=>'Precio (%)','value'=>'','label'=>'Precio (%)','class'=>'form-control','step'=>'any'),
				'ComisionCNB'=>array('name'=>'montoPorcentualCNB','type'=>'number','placeholder'=>'Comisión CNB (%) ','value'=>'','label'=>'Comisión CNB (%) ','class'=>'form-control','step'=>'any')
				);
	protected $_timestamps=FALSE;
	protected $_form='';
	protected $_form_group_class='form-group ';
	protected $_attr_submit_button='';
	protected $_class_label='label label-warning';

	protected $_ruta='AdminMontos/';

	//RELACION PARA GENERAR LA TABLA DE ESTA CLASE
	protected $_headingTable=array('ID','Descripcion','Logo','Tipo');
	//usado para buscar los datos de la db
	protected $_datatable=array('id','description','imagen','tipo');
	//usado para mostrar los datos en la tabla e incluir los btn actions
	protected $_dataForTable=array('description','imagen','tipo');
	protected $_jointable=array();


	protected $template = array(
        'table_open'            => '<table class="table table-striped">',

        'thead_open'            => '<thead>',
        'thead_close'           => '</thead>',

        'heading_row_start'     => '<tr>',
        'heading_row_end'       => '</tr>',
        'heading_cell_start'    => '<th>',
        'heading_cell_end'      => '</th>',

        'tbody_open'            => '<tbody>',
        'tbody_close'           => '</tbody>',

        'row_start'             => '<tr>',
        'row_end'               => '</tr>',
        'cell_start'            => '<td>',
        'cell_end'              => '</td>',

        'row_alt_start'         => '<tr>',
        'row_alt_end'           => '</tr>',
        'cell_alt_start'        => '<td>',
        'cell_alt_end'          => '</td>',

        'table_close'           => '</table>'
	);

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}

	public function PopulateTable(){
		$this->db->select('*');
		return $this->db->get($this->_tablename)->result();
	}


	public function Table(){
		$_id=$this->_primary_key;
		$this->table->set_template($this->template);
		$headingArray=array();

		if(count($this->_headingTable)>0){
			$this->table->set_heading($this->_headingTable);
		}
		if(count($this->_datatable)>0){
			foreach ($this->_datatable as $value){
				$this->db->select($value);
			}
		}

		if(count($this->_jointable)>0){
			foreach ($this->_jointable as $key) {
				$this->db->join($key['table_join'],$key['table_rel'],'left');
			}
		}

		$this->db->order_by($this->_order_by,'ASC');

		$query=$this->db->get($this->_tablename);
		$i=1;
		foreach ($query->result() as $key ){
			$dataArray=array();
			array_push($dataArray, $i);
			$i++;
			foreach ($this->_dataForTable as &$value){
				if($value=='imagen'){
					array_push($dataArray, '<img src="'.$key->$value.'" width="50">');
				}else{
					array_push($dataArray, $key->$value);
				}

			}
			$btn_actions='<a href="'.base_url().$this->_ruta.'Edit/'.$key->$_id.'" class="btn btn-sm btn-info"><i class="fa fa-edit"></i></a> <a href="'.base_url().$this->_ruta.'Delete/'.$key->$_id.'" class="btn btn-sm btn-danger"><i class="fa fa-eraser"></i></a>';
			array_push($dataArray, $btn_actions);
			$this->table->add_row($dataArray);

		}

		return $this->table->generate();

	}



}

/* End of file TransaccionesModel.php */
/* Location: ./application/models/TransaccionesModel.php */

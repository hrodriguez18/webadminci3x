<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login_m extends My_Model {

	protected $_table_name = 'adm.users';
	protected $_primary_key = 'userid';
	protected $_primary_filter = 'intval';
	protected $_order_by = 'userid';

	public $rules = array('username'=>array('field'=>'username','label'=>'Usuario','rules'=>'trim|required'),
		'password'=>array('field'=>'password','label'=>'Contraseña','rules'=>'trim|required')

	);



	function __construct() {
		parent::__construct();
		$this->load->model('logUsers');
	}

	public function Login($data){
		$this->db->select('*');
		$this->db->where('username',$data['username']);
		//$this->db->or_where('email',$data['username']);
		$query=$this->db->get('adm.users');
		$data_session=array();

		$status='';

		$is_logged=0;

		if($query->num_rows()>0){
			foreach ($query->result() as $value) {
				$data_session['userid']=$value->userid;
				$data_session['username']=$value->username;
				$data_session['email']=$value->email;
				$data_session['name']=$value->name;
				$data_session['operatorid']=$value->operatorid;
				$data_session['agencyid']=$value->agencyid;
				$data_session['rol_id']=$value->rol_id;
				$data_session['expire_session']=time()+ $this->config->item('sess_expiration');
				$data_session['status']=$value->status;
				$data_session['wrongPass']=$value->wrongPassword;
				$is_logged=$value->is_logged;
				$temp_pass=$value->password;
				$status=$value->status;
			}

			if($is_logged==1){
				$this->session->set_flashdata('blocked','<p class="text-danger">Este usuario mantiene una sesión activa, no pueden existir 2 sesiones simultaneas</p>');
				redirect('Login','refresh');
				return FALSE;
			}

			if($status=='I' || $data_session['wrongPass']==3 ){
				$fail['wrongPassword']=0;
				$this->db->where('userid',$data_session['userid']);
				$this->db->update('adm.users',$fail);

				$this->session->set_flashdata('blocked','<p class="text-danger">Este usuario ha sido bloqueado, favor contacte al administrador de sistema</p>');
				redirect('Login','refresh');
				return FALSE;
			}else{
				if($this->encrypt->decode($temp_pass)==$data['password']){
					$data_session['is_logged']=TRUE;
					$data_session['lang']='es';
					$data_session['permisos']=array();
					$this->db->select('*');
					$this->db->where('rol_id',$data_session['rol_id']);
					$roles_permisos=$this->db->get('adm.roles_permisos');
					if($roles_permisos->num_rows()>0){
						foreach($roles_permisos->result() as $value){
							$this->db->select('*');
							$this->db->where('Id',$value->permiso_id);
							$this->db->order_by('Id', 'ASC');
							$data=$this->db->get('adm.permisos')->row_array();
							array_push($data_session['permisos'],$data);

						}
					}
					/*$this->load->model('Parameters');
					$parametros = new Parameters();
					$parametros->sess_expiration;
					$this->config->set_item('sess_expiration', intval($parametros->sess_expiration));
					*/


					$this->session->set_userdata($data_session);
					$datalog['userid']=$data_session['userid'];
					$datalog['action']='Inicio de sesion Correcto -> '.$data_session['username'] ;
					$datalog['status']='Completado';
					$datalog['username']=$data_session['username'];
					$datalog['name']=$data_session['name'];
					$datalog['category']='Login';
					$datalog['ip']=$this->input->ip_address();
					$this->logUsers->save($datalog);

					$fail['wrongPassword']=0;
					$fail['status']='A';
					$fail['is_logged']=0;
					$this->db->where('userid',$data_session['userid']);
					$this->db->update('adm.users',$fail);
					return TRUE;
				}else{
					$fail['wrongPassword']=$data_session['wrongPass']+1;
					if($fail['wrongPassword']==3){
						$fail['status']='I';
						$this->db->where('userid',$data_session['userid']);
						$this->db->update('adm.users',$fail);

					}else{
						$this->db->where('userid',$data_session['userid']);
						$this->db->update('adm.users',$fail);
					}

					$datalog['userid']=$data_session['userid'];
					$datalog['action']='Intento de Inicio de sesion Fallido "wrong password" -> '.$data_session['username'] ;
					$datalog['status']='No Completado';
					$datalog['category']='Login';
					$datalog['username']=$data_session['username'];
					$datalog['name']=$data_session['name'];
					$datalog['ip']=$this->input->ip_address();
					$this->logUsers->save($datalog);
					$this->session->set_userdata('is_logged',FALSE);
					$this->session->set_userdata('user_no_such',FALSE);
					$this->session->set_userdata('password_wrong',TRUE);
					return FALSE;
				}
			}
		}else{
			$datalog['userid']=$this->session->userdata('userid');
			$datalog['action']='Intento de Inicio de sesion Fallido "user no found" -> '.$data['username'];
			$datalog['status']='No Completado';
			$datalog['category']='Login';
			$datalog['username']=$data['username'];
			$datalog['ip']=$this->input->ip_address();
			$this->logUsers->save($datalog);
			$this->session->set_userdata('is_logged',FALSE);
			$this->session->set_userdata('user_no_such',TRUE);
			return FALSE;
		}
	}








}
?>

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class adminsystem_m extends MY_Model {
 	public $_table_name = 'adm.users';
	protected $_primary_key = 'userid';
	protected $_primary_filter = 'intval';
	protected $_order_by = 'userid';
	protected $_timestamps = FALSE;




	public $rules = array('username'=>array('field'=>'username','label'=>'','rules'=>'trim|required|callback_usuario_unico'),
		'email'=>array('field'=>'email','label'=>'','rules'=>'trim|required|valid_email|callback_email_unico'),
		'password'=>array('field'=>'password','label'=>'','rules'=>'trim|required|min_length[8]|max_length[16]|callback_regex_match|callback_noContiene|callback_noContenerNombre'),
		'name'=>array('field'=>'name','label'=>'','rules'=>'trim|required'),
		'rol_id'=>array('field'=>'rol_id','label'=>'','rules'=>'trim|required|callback_diferente_de_0'),
		'status'=>array('field'=>'status','label'=>'','rules'=>'trim|required|callback_diferente_de_0')
	);

	public $rules_update = array('username'=>array('field'=>'username','label'=>'','rules'=>'trim|required'),
		'email'=>array('field'=>'email','label'=>'','rules'=>'trim|required|valid_email'),

		'name'=>array('field'=>'name','label'=>'','rules'=>'trim|required'),
		'rol_id'=>array('field'=>'rol_id','label'=>'','rules'=>'trim|required|callback_diferente_de_0'),
		'status'=>array('field'=>'status','label'=>'','rules'=>'trim|required|callback_diferente_de_0')
	);


	public $status_array=array('0'=>'-- Select --','A'=>'Activo','I'=>'Inactivo');

	//INPUTS DEL FORMULARIO

	private $userid=array('name'=>'userid','value'=>'','type'=>'hidden','id'=>'userid','placeholder'=>'', 'class'=>'form-control');

	private $username=array('name'=>'username','value'=>'','type'=>'text','id'=>'username','placeholder'=>'', 'class'=>'form-control');

	private $password=array('name'=>'password','value'=>'','type'=>'password','id'=>'password','placeholder'=>'', 'class'=>'form-control');

	private $name=array('name'=>'name','value'=>'','type'=>'text','id'=>'name','placeholder'=>'', 'class'=>'form-control');

	private $email=array('name'=>'email','value'=>'','type'=>'email','id'=>'email','placeholder'=>'', 'class'=>'form-control');

	private $type=array('name'=>'type','value'=>'','type'=>'number','id'=>'type','placeholder'=>'', 'class'=>'form-control');

	private $operationid=array('name'=>'operationid','value'=>'','type'=>'number','id'=>'operationid','placeholder'=>'', 'class'=>'form-control');

	private $agencyid=array('name'=>'agencyid','value'=>'','type'=>'number','id'=>'agencyid','placeholder'=>'', 'class'=>'form-control');

	private $terminalid=array('name'=>'terminalid','value'=>'','type'=>'number','id'=>'terminalid','placeholder'=>'', 'class'=>'form-control');

	private $rol_id=array('name'=>'rol_id','value'=>'','type'=>'number','id'=>'rol_id','placeholder'=>'', 'class'=>'form-control');

	private $category_id=array('name'=>'category_id','value'=>'','type'=>'number','id'=>'category_id','placeholder'=>'', 'class'=>'form-control');

	private $status=array('name'=>'status','value'=>'','type'=>'number','id'=>'status','placeholder'=>'', 'class'=>'form-control');

	private $fEntrance=array('name'=>'fEntrance','value'=>'','type'=>'number','id'=>'fEntrance','placeholder'=>'', 'class'=>'form-control');

	function __construct() {
		parent::__construct();

		$this->username['placeholder']=lang('username');
		$this->password['placeholder']=lang('password');
		$this->name['placeholder']=lang('name');
		$this->email['placeholder']=lang('email');
		$this->type['placeholder']=lang('type');
		$this->operationid['placeholder']=lang('operationid');
		$this->agencyid['placeholder']=lang('agencyid');
		$this->terminalid['placeholder']=lang('terminalid');
		$this->rol_id['placeholder']=lang('rol_id');
		$this->category_id['placeholder']=lang('category_id');
		$this->status['placeholder']=lang('status');
		$this->fEntrance['placeholder']=lang('fEntrance');


	}


	public function opt_roles(){
		$this->db->select('*');
		$query=$this->db->get('adm.roles');
		$opt=array('0'=>'-- Select --');
		if($query->num_rows()>0){

			foreach ($query->result() as $value) {
				$opt[$value->Id]=$value->Descripcion;
			}
		}
		return $opt;
	}


	public function Form($data_by_id=NULL){

		if($data_by_id!=NULL){
			$this->userid['value']=set_value('userid',$data_by_id->userid,true);
			$this->username['value']=set_value('username',$data_by_id->username,true);
			$this->password['value']=set_value('password','',true);
			$this->name['value']=set_value('name',$data_by_id->name,true);
			$this->email['value']=set_value('email',$data_by_id->email,true);
			$this->type['value']=set_value('type',$data_by_id->type,true);
			//$this->operationid['value']=set_value('operationid',$data_by_id->operationid,true);
			$this->agencyid['value']=set_value('agencyid',$data_by_id->agencyid,true);
			$this->terminalid['value']=set_value('terminalid',$data_by_id->terminalid,true);
			$this->rol_id['value']=set_value('rol_id',$data_by_id->rol_id,true);
			$this->category_id['value']=set_value('category_id',$data_by_id->category_id,true);
			$this->status['value']=set_value('status',$data_by_id->status,true);
			$this->fEntrance['value']=set_value('fEntrance',$data_by_id->fEntrance,true);
		}else{
			$this->userid['value']=set_value('userid','',true);
			$this->username['value']=set_value('username','',true);
			$this->password['value']=set_value('password','',true);
			$this->name['value']=set_value('name','',true);
			$this->email['value']=set_value('email','',true);
			$this->type['value']=set_value('type','',true);
			//$this->operationid['value']=set_value('operationid','',true);
			$this->agencyid['value']=set_value('agencyid','',true);
			$this->terminalid['value']=set_value('terminalid','',true);
			$this->rol_id['value']=set_value('rol_id','',true);
			$this->category_id['value']=set_value('category_id','',true);
			$this->status['value']=set_value('status','',true);
			$this->fEntrance['value']=set_value('fEntrance','',true);
		}

		$form=form_input($this->userid);

		$form.='<div class="row">';

		$form.='<div class="col-lg-12"><div class="form-group">'.form_label(lang('username'),'username').form_input($this->username).form_error('username').'</div></div>';
		$form.='<div class="col-lg-12"><div class="form-group">'.form_label(lang('email'),'email').form_input($this->email).form_error($this->email['id']).'</div></div>';

		if($data_by_id==NULL){
			$form.='<div class="col-lg-12"><div class="form-group">'.form_label(lang('password'),'password').form_input($this->password).form_error($this->password['id']).'</div></div>';
		}

		$form.='</div>';

		$form.='<div class="row">';

		$form.='<div class="col-lg-12"><div class="form-group">'.form_label(lang('name'),'name').form_input($this->name).form_error($this->name['id']).'</div></div>';

		$form.='<div class="col-lg-12"><div class="form-group">'.form_label(lang('rol_id'),'rol_id').form_dropdown('rol_id',$this->opt_roles(),set_value('rol_id',$this->rol_id['value'],true),'class="form-control"').form_error('rol_id').'</div></div>';

		$form.='<div class="col-lg-12"><div class="form-group">'.form_label(lang('status'),'status').form_dropdown('status',$this->status_array,set_value('status',$this->status['value'],true),'class="form-control"').form_error('status').'</div></div>';

		$form.='</div>';


		return $form;

	}

	public function countUserByData($field,$value){
		$this->db->select('userid');
		$this->db->where($field,$value);
		$query=$this->db->get('adm.users');
		return $query->num_rows();
	}


}
?>

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class permisos extends MY_SuperModel{

  protected $_tablename='adm.permisos';
	protected $_primary_key='Id';
	protected $_primary_filter='intval';
	protected $_order_by='Id';

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
  }

  public function getMisPermisos(){
    $id_rol=$this->session->userdata('rol_id');
    $this->db->select('roles_permisos.rol_id,roles_permisos.permiso_id');
    $this->db->select('permisos.Descripcion as D');
    $this->db->join('adm.permisos','roles_permisos.permiso_id=permisos.Id','left');
    $this->db->where('rol_id',$id_rol);
    $query=$this->db->get('adm.roles_permisos');
    $permisos=array();
    foreach ($query->result() as $value) {
      $permisos[$value->permiso_id]=$value->D;
    }
    return $permisos;
  }

}

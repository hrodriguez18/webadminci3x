<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class AdminModel extends CI_Model {


	function __construct() {
		parent::__construct();
	}

	public function OnReturnOptions($table="",$key_value=array("","")){
		if(strlen($table)!=0){
			$this->db->select($table.'.'.$key_value[0].','.$table.'.'.$key_value[1]);
			$query=$this->db->get('esb.'.$table);
			$options=array();
			foreach ($query->result() as  $value) {
				$options[$value->$key_value[0]]=$value->$key_value[1];
			}
			$options[0]="Todos";
			return $options;
		}
	}

	public function FormVentas(){
		$y=intval(date("Y"));
		$y_ini=$y-4;
		$y_fin=$y+7;
		$fecha=array();
		for($i=$y_ini;$i<=$y_fin;$i++){
			$fecha[$i]=$i;
		}

		$mes=array(1=>'ene',
					2=>'feb',
					3=>'mar',
					4=>'abr',
					5=>'may',
					6=>'jun',
					7=>'jul',
					8=>'ago',
					9=>'sep',
					10=>'oct',
					11=>'nov',
					12=>'dic');

		foreach ($mes as $key=>$value){
			$mes[$key]=$this->lang->line($value);
		}

		$formulario="";

		$opt=array("Cod_Pais","Descripcion");
		$opt_canal=array('Canal','Canal');
		$opt_autorizador=array('Descripcion','Descripcion');

		$pais=$this->OnReturnOptions("Pais",$opt);
		$canal=$this->OnReturnOptions("Canales",$opt_canal);
		$autorizador=$this->OnReturnOptions('Billers',$opt_autorizador);

		$formulario.='<div class="col-lg-2"><div class="form-group ">'.form_label($this->lang->line('year'),'anio');
		$formulario.=form_dropdown("anio",$fecha,intval(date("Y")),'class="selectpicker" data-style="btn-info" id="anio" onChange="MostrarInfoVentas()"').'</div></div>';

		$formulario.='<div class="col-lg-2"><div class="form-group ">'.form_label($this->lang->line('month'),'mes');
		$formulario.=form_dropdown("mes",$mes,intval(date("m")),'class="selectpicker" data-style="btn-info " id="mes" onChange="MostrarInfoVentas()"').'</div></div>';

		$formulario.='<div class="col-lg-2"><div class="form-group">'.form_label($this->lang->line('country'),'pais');
		$formulario.=form_dropdown("pais",$pais,0,'class="selectpicker" data-style="btn-info " id="pais" onChange="MostrarInfoVentas()"').'</div></div>';

		$formulario.='<div class="col-lg-3"><div class="form-group">'.form_label($this->lang->line('Channels'),'canal');
		$formulario.=form_dropdown("canal",$canal,0,'class="selectpicker" data-style="btn-info " id="canal" onChange="MostrarInfoVentas()"').'</div></div>';


		$formulario.='<div class="col-lg-3"><div class="form-group">'.form_label($this->lang->line('aut'),'autorizador');

		$formulario.=form_dropdown("autorizador",$autorizador,0,'class="selectpicker" data-style="btn-info " id="autorizador" onChange="MostrarInfoVentas()"').'</div></div>';


		return $formulario;
	}

	public function DatosVentasQuery($datos=array('year'=>'','month'=>'','pais'=>'0','canal'=>'0','autorizador'=>'0')){


		$start_date=(string)($datos['year'].'-'.$datos['month'].'-01'.' 00:00:00');
		$end_date=$datos['year'].'-'.(string)(intval($datos['month'])+1).'-01 00:00:00';


		$this->db->select("esb.tranNum");
		$this->db->where('timeStamp >', $start_date);
		$this->db->where('timeStamp <', $end_date);
		if($datos['pais']!='0'){
			$this->db->where('country', $datos['pais']);
		}
		if($datos['canal']!='0'){
			$this->db->where('canal', $datos['canal']);
		}
		if($datos['autorizador']!='0'){
			$this->db->where('biller', $datos['autorizador']);
		}

		$info=array('cuenta'=>$this->db->count_all_results('esb.Log_Transaccional'),'suma'=>'0.00');

		$this->db->select_sum("totalAmount",'Suma');
		$this->db->where('timeStamp >', $start_date);
		$this->db->where('timeStamp <', $end_date);
		if($datos['pais']!='0'){
			$this->db->where('country', $datos['pais']);
		}
		if($datos['canal']!='0'){
			$this->db->where('canal', $datos['canal']);
		}
		if($datos['autorizador']!='0'){
			$this->db->where('biller', $datos['autorizador']);
		}

		$query=$this->db->get("esb.Log_Transaccional");
		if($query->num_rows()>0){
			foreach ($query->result() as $value){
				if(intval($value->Suma)>0){
					$info['suma']=$value->Suma;
				}

			}

		}

		$this->db->select("esb.tranNum");
		$this->db->where('timeStamp >', $start_date);
		$this->db->where('timeStamp <', $end_date);
		if($datos['pais']!='0'){
			$this->db->where('country', $datos['pais']);
		}
		if($datos['canal']!='0'){
			$this->db->where('canal', $datos['canal']);
		}
		if($datos['autorizador']!='0'){
			$this->db->where('biller', $datos['autorizador']);
		}
		$this->db->where('status','Reversa exitosa');
		$info['devoluciones']=$this->db->count_all_results('esb.Log_Transaccional');


		$vista=$this->load->view($this->config->item('theme').'/rows/counters',$info,true);

		return $vista;
	}

	public function DatosVentas(){

		$start_date=date("Y-m").'-01'.' 00:00:00';
		$end_date=date("Y")."-".(string)(intval(date("m"))+1).'-01 00:00:00';
		$this->db->select("esb.tranNum");
		$this->db->where('timeStamp >', $start_date);
		$this->db->where('timeStamp <', $end_date);

		$info=array('cuenta'=>$this->db->count_all_results('esb.Log_Transaccional'),'suma'=>'0.00');

		$this->db->select_sum("totalAmount",'Suma');
		$this->db->where('timeStamp >', $start_date);
		$this->db->where('timeStamp <', $end_date);

		$query=$this->db->get("esb.Log_Transaccional");
		if($query->num_rows()>0){
			foreach ($query->result() as $value){
				if(intval($value->Suma)>0){
					$info['suma']=$value->Suma;
				}

			}

		}

		$this->db->select("esb.tranNum");
		$this->db->where('timeStamp >', $start_date);
		$this->db->where('timeStamp <', $end_date);
		$this->db->where('status','Reversa exitosa');
		$info['devoluciones']=$this->db->count_all_results('esb.Log_Transaccional');

		return $info;
	}

	public function GraphicalView($year,$pais,$canal,$autorizador){
		$repuesta=array();

		for($i=1;$i<=12;$i++){
			$mesActual=$year.'-'.$i;
			$fechaActual=$mesActual.'-01 00:00:00';
			$mesSiguiente=(string)$year.'-'.(string)intval($i+1).'-01 00:00:00';
			if($i==12){
				$mesSiguiente='2019-01-01 00:00:00';
			}

			$this->db->select("count(*) as cuenta, SUM(totalAmount) as suma");
			$this->db->where("timeStamp >",$fechaActual);
			$this->db->where("timeStamp <",$mesSiguiente);
			if($pais!='0'){
				$this->db->where('country',$pais);
			}
			if($canal!='0'){
				$this->db->where('canal',$canal);
			}
			if($autorizador!='0'){
				$this->db->where('biller',$autorizador);
			}
			$query=$this->db->get("esb.Log_Transaccional");

			foreach ($query->result() as $value) {


				if($value->suma==NULL){
					$suma=0;
				}else{
					$suma=$value->suma;
				}
				$respuesta[$i-1]=array('y'=>$mesActual,'a'=>$value->cuenta, 'b'=>$suma);
			}
		}
		return $respuesta;
	}

	public function FormMapas(){
		$opciones=array();
		$query=array();
		$opciones[0]='Seleccionar Operador';
		if($this->session->userdata('rol_id')=='1'){
			$this->db->select('*');
			$query=$this->db->get('esb.Operadores');
			foreach ($query->result() as $value) {
				$opciones[$value->Cod_Operador]=$value->Descripcion;
			}
		}else{
			$this->db->select('*');
			$this->db->where('Cod_Operador',$this->session->userdata('operator_id'));
			$query=$this->db->get('esb.Operadores');
			foreach ($query->result() as $value) {
				$opciones[$value->Cod_Operador]=$value->Descripcion;
			}

		}

		$form=form_label('Operadores','operators').form_dropdown('operators',$opciones,0,'class="selectpicker" data-style="btn-info" onChange="SetPoints()" id="operador_id"');
		return $form;
	}

	public function GetPointForMaps($operador='107000'){
		$puntos=array();
		$operadores=array();
		$agencias=array();

		if($operador!='0'){
			$this->db->select('*');
			$this->db->where('Cod_Operador',$operador);
			$query=$this->db->get('esb.Agencias');
			$i=0;
			foreach ($query->result() as  $agency) {
				$agencias[$i]['d']=$agency->Descripcion;
				$agencias[$i]['c']=$agency->Cod_Operador;

				$i++;
			}
			for($p=0;$p<count($agencias);$p++){
				$this->db->select('*');
				$this->db->where('agency',$agencias[$p]['d']);
				$this->db->order_by('tranNum','desc');
				$this->db->limit(1);
				$log=$this->db->get('esb.Log_Transaccional');
				foreach ($log->result() as  $venta){
					$this->db->select('Color');
					$this->db->where('Cod_Operador',$agencias[$p]['c']);
					$m=$this->db->get('esb.Operadores');
					$color='';
					foreach ($m->result() as $c) {
						$color=$c->Color;
					}
					$puntos[$p]=array('color'=>$color,'title'=>'Usuario: '.$venta->username.' | Monto: '.$venta->totalAmount.' | Agencia: '.$agencias[$p]['d']  , 'location'=>array('lat'=>floatval($venta->latitud),'lng'=>floatval($venta->longitud)), 'num_agencias'=>$agencias[$p]);
				}
			}
		}

		if($operador=='0'){
			$k=0;
			$this->db->select('*');
			$query=$this->db->get('esb.Operadores');
			foreach ($query->result() as $op){
				$operadores[$k]=$op->Cod_Operador;
				$k++;
			}
			$l=0;

			for($h=0;$h<count($operadores);$h++){
				$this->db->select('*');
				$this->db->where('Cod_Operador',$operadores[$h]);
				$agen=$this->db->get('esb.Agencias');
				foreach ($agen->result() as $valor) {
					$agencias[$l]['d']=$valor->Descripcion;
					$agencias[$l]['c']=$valor->Cod_Operador;
					$l++;
				}
			}

			for($p=0;$p<count($agencias);$p++){
				$this->db->select('*');
				$this->db->where('agency',$agencias[$p]['d']);
				$this->db->order_by('tranNum','desc');
				$this->db->limit(1);
				$log=$this->db->get('esb.Log_Transaccional');
				foreach ($log->result() as  $venta){
					$this->db->select('Color');
					$this->db->where('Cod_Operador',$agencias[$p]['c']);
					$m=$this->db->get('esb.Operadores');
					$color='';
					foreach ($m->result() as $c) {
						$color=$c->Color;
					}
					$puntos[$p]=array('color'=>$color,'title'=>'Usuario: '.$venta->username.' | Monto: '.$venta->totalAmount.' | Agencia : '.$agencias[$p]['d'], 'location'=>array('lat'=>floatval($venta->latitud),'lng'=>floatval($venta->longitud)));
				}
			}
		}
		return $puntos;
	}

	public function fill_radom(){

		for($i=2;$i<=382;$i++){
			$id=$i;
			$lat=rand(-90,90);
			$lon=rand(-180,180);
			$position=array('latitud'=>$lat,'longitud'=>$lon);
			$this->db->set($position);
			$this->db->where('tranNum',$id);
			$this->db->where('longitud','1');
			$this->db->update('esb.Log_Transaccional');

		}

		echo "Fins";


	}





}
?>

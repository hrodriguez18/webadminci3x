<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MposModel extends CI_Model{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
  }

  public function populateFaqs(){
    $this->db->select('*');
    return $this->db->get('esb.FAQ')->result();
  }
  public function populateAccesos(){
    $hoyi=date('Y-m-d').' 00:00:00';
    $hoyf=date('Y-m-d').' 23:59:00';
    $this->db->select('*');
    $this->db->limit(1000);
    $this->db->order_by('timeStamp','DESC');
    $this->db->where('timeStamp > ',$hoyi);
    $this->db->where('timeStamp < ',$hoyf);
    return $this->db->get('esb.Log_Login')->result();
  }
  public function populateTransacciones(){
    $hoyi=date('Y-m-d').' 00:00:00';
    $hoyf=date('Y-m-d').' 23:59:00';
    $this->db->select('
    Log_Transaccional.timeStamp,Log_Transaccional.operator,
    Log_Transaccional.agency,Log_Transaccional.address,
    Log_Transaccional.cardno,Log_Transaccional.currencyCode,
    Log_Transaccional.cardAmount,Log_Transaccional.currency,
    Log_Transaccional.tax,Log_Transaccional.totalAmount,
    Log_Transaccional.authorizationCode,Log_Transaccional.Longitud,
    Log_Transaccional.Latitud,
    Log_Transaccional.answerCode,
    Log_Transaccional.tranNum
    ');

    $this->db->where('timeStamp > ',$hoyi);
    $this->db->where('timeStamp < ',$hoyf);
    $this->db->order_by('timeStamp','DESC');
    return $this->db->get('esb.Log_Transaccional')->result();
  }

  public function populateAlertas(){
    $hoyi=date('Y-m-d').' 00:00:00';
    $hoyf=date('Y-m-d').' 23:59:00';
    $this->db->select('
    Log_Transaccional.timeStamp,Log_Transaccional.operator,
    Log_Transaccional.agency,Log_Transaccional.address,
    Log_Transaccional.cardno,Log_Transaccional.currencyCode,
    Log_Transaccional.cardAmount,Log_Transaccional.currency,
    Log_Transaccional.tax,Log_Transaccional.totalAmount,
    Log_Transaccional.authorizationCode,Log_Transaccional.Longitud,
    Log_Transaccional.Latitud,
    Log_Transaccional.answerCode,
    Log_Transaccional.tranNum
    ');


    $this->db->where('timeStamp > ',$hoyi);
    $this->db->where('timeStamp < ',$hoyf);
    $this->db->where('answerCode != 00');


    $this->db->limit(1000);
    $this->db->order_by('timeStamp','DESC');
    return $this->db->get('esb.Log_Transaccional')->result();
  }

}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Agency_m extends MY_SuperModel{
  //VARIABLES PARA HACER LAS CONSULTAS A LA BASE DE DATOS
  	protected $_tablename='esb.Agencias';
  	public $_primary_key='Cod_Agencia';
  	protected $_primary_filter='intval';
  	protected $_order_by='Cod_Agencia';


  //VARIABLES QUE SE NECESITAN PARA GENERAR EL FORMULARIO
  	public  $_rules=array(

  				'Descripcion'=>array(

                        'field'=>'Descripcion',
                        'label'=>'Descripcion',
                        'rules'=>'trim|required'
				),
				'Estado'=>array(

                    'field'=>'Estado',
                    'label'=>'Estado',
                    'rules'=>'trim|required|callback_diferente_de_default'

				),
				'Cod_Operador'=>array(
                        'field'=>'Cod_Operador',
                        'label'=>'operator',
                        'rules'=>'trim|required|callback_diferente_de_default'
				),
                'actividadComercialId'=>array(
                      'field'=>'actividadComercialId',
                      'label'=>'Actividad Comercial',
                      'rules'=>'trim|required|callback_diferente_de_default'
				),
                'telefono'=>array(
                      'field'=>'telefono',
                      'label'=>'telefono',
                      'rules'=>'trim|numeric|min_length[8]|required|callback_withoutSignal'
				),
                'cuenta'=>array(
                      'field'=>'cuenta',
                      'label'=>'cuenta',
                      'rules'=>'trim|required'
				),
				'tipoDocumento'=>array(
					'field'=>'tipoDocumento',
					'label'=>'tipoDocumento',
					'rules'=>'trim|required|callback_diferente_de_default'
				),
              	'documento'=>array(
                    'field'=>'documento',
                    'label'=>'documento',
                    'rules'=>'trim|required'
				),
				'cuentaComision'=>array(
                    'field'=>'cuentaComision',
                    'label'=>'cuentaComision',
                    'rules'=>'trim|required'
				),
				'latitud'=>array(
					'field'=>'latitud',
					'label'=>'Latitud',
					'rules'=>'trim|required'
				),
				'longitud'=>array(
					'field'=>'longitud',
					'label'=>'Longitud',
					'rules'=>'trim|required'
				),
				'Direccion'=>array(
                        'field'=>'Direccion',
                        'label'=>'Direccion',
                        'rules'=>'trim|required'
				),

	);
  	public $_inputs=array(

              'Cod_Operador'=>array(

                'name'=>'Cod_Operador',
                'id'=>'Cod_Operador',
                'type'=>'selector',
                'table_rel'=>'esb.Operadores',
                'opt'=>'Cod_Operador',
                'description'=>'Descripcion',
                'class'=>'form-control',
                'default'=>'',
                'label'=>'operator'

              ),

              'Descripcion'=>array(

                'name'=>'Descripcion',
                'id'=>'Descripcion',
                'type'=>'text',
                'class'=>'form-control',
                'label'=>'description',
                'value'=>'',
                'placeholder'=>'description'

              ),




              'Estado'=>array(

                'name'=>'Estado',
                'id'=>'Estado',
                'type'=>'selector',
                'table_rel'=>'esb.status',
                'opt'=>'value',
                'description'=>'description',
                'class'=>'form-control',
                'default'=>'',
                'label'=>'status'

              ),

              'Direccion'=>array(

                'name'=>'Direccion',
                'id'=>'Direccion',
                'type'=>'textarea',
                'class'=>'form-control',
                'label'=>'address',
                'value'=>'',
                'placeholder'=>'address',
                'rows'=>'3'
              ),

              'actividadComercialId'=>array(

                'name'=>'actividadComercialId',
                'id'=>'actividadComercialId',
                'type'=>'selector',
                'table_rel'=>'esb.actividadComercial',
                'opt'=>'id',
                'description'=>'description',
                'class'=>'form-control',
                'default'=>'',
                'label'=>'Comercialactivity'

              ),

              'telefono'=>array(

                'name'=>'telefono',
                'id'=>'telefono',
                'type'=>'number',
                'min'=>0,
                'class'=>'form-control',
                'label'=>'phone',
                'value'=>'',
                'placeholder'=>'phone'

              ),

              'cuenta'=>array(

                'name'=>'cuenta',
                'id'=>'cuenta',
                'type'=>'number',
                'class'=>'form-control',
                'label'=>'account',
                'value'=>'',
                'placeholder'=>'account'

              ),

              'documento'=>array(

                'name'=>'documento',
                'id'=>'documento',
                'type'=>'text',
                'class'=>'form-control',
                'label'=>'documento',
                'value'=>'',
                'placeholder'=>'documento'

              ),

              'tipoDocumento'=>array(

                'name'=>'tipoDocumento',
                'id'=>'tipoDocumento',
                'label'=>'tipoDoc',
                'value'=>'',
                'type'=>'selector',
                'table_rel'=>'esb.tipoDocumento',
                'opt'=>'value',
                'description'=>'description',
                'class'=>'form-control',
                'default'=>'',

              ),

              'cuentaComision'=>array(

                'name'=>'cuentaComision',
                'id'=>'cuentaComision',
                'type'=>'text',
                'class'=>'form-control',
                'label'=>'commissionaccount',
                'value'=>'',
                'placeholder'=>'commissionaccount'

              ),

		'latitud'=>array(

			'name'=>'latitud',
			'id'=>'latitud',
			'type'=>'text',
			'class'=>'form-control',
			'label'=>'latitud',
			'value'=>'',
			'placeholder'=>'latitud'

		),

		'longitud'=>array(

			'name'=>'longitud',
			'id'=>'longitud',
			'type'=>'text',
			'class'=>'form-control',
			'label'=>'longitud',
			'value'=>'',
			'placeholder'=>'longitud'

		),

              'Cod_Agencia'=>array(

                'name'=>'Cod_Agencia',
                'id'=>'Cod_Agencia',
                'type'=>'hidden',
                'value'=>''
              )

              );
  	public $documentationForForm='';
  	protected $_timestamps=FALSE;
  	protected $_form='';
  	protected $_form_group_class='';
  	protected $_attr_submit_button='';
  	protected $_class_label='';
  //ESTA RUTA LE PERMITE A LA VISTA SABER HACIA DONDE BUSCAR EL ECHO DEL FORMULARIO
  	protected $_ruta='';

  	//RELACION PARA GENERAR LA TABLA DE ESTA CLASE
  	protected $_headingTable=array();
  	//usado para buscar los datos de la db


  	protected $_datatable=array(

                          'Agencias.Cod_Agencia as id',
                          'Agencias.Descripcion',
                          'Agencias.Estado',
                          'Agencias.telefono',
                          'Agencias.cuenta',
                          'Agencias.documento',
                          'Agencias.tipoDocumento',
                          'Agencias.cuentaComision',
                          'ActividadComercial.description as Actividad',
                          'Operadores.Descripcion as Operador'
                    );


  	protected $_joinForBootstrapTable=array(

                                  0=>array(
                                          'table_join'=>'esb.actividadComercial',
                                          'table_rel'=>'Agencias.actividadComercialId=actividadComercial.id'
                                        ),

                                  1=>array(
                                          'table_join'=>'esb.Operadores',
                                          'table_rel'=>'Agencias.Cod_Operador=Operadores.Cod_Operador'
                                        ),

                                  );
  	//usado para mostrar los datos en la tabla e incluir los btn actions
  	protected $_dataForTable=array(
              'Agencias.Cod_Agencia',
              'Agencias.Descripcion',
              'Agencias.Estado',
              'Agencias.telefono',
              'Agencias.cuenta',
              'Agencias.documento',
              'Agencias.tipoDocumento',
              'Agencias.cuentaComision',
              'Operadores.Descripcion'

    );




  	protected $_jointable=array();
  	//para generar los informes en PDF
  	public $_TableToExport='';
  	public $_footer_ContentToExport='';
  	public $_pdfTitle='';
  	public $_pdfDescription='';
  	public $_headersTable=array();
  	public $_selectToExport=array();
  	public $_orderName='';
  	public $_order='';
  	public $_join=array();
  	public $_dateVariable='';


  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
  }

}

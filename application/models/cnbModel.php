<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class cnbModel extends MY_SuperModel{

  protected $_tablename='esb.transactionLog';
	protected $_primary_key='id';
	protected $_primary_filter='intval';
	protected $_order_by='id';
  public $_TableToExport='';
  public $_footer_ContentToExport='';
  public $_pdfTitle='BANCO NACIONAL DE PANAMÁ';
	public $_pdfDescription='REPORTE DE CNB';

  public $_columnSearch='';
  public $_columnValue='';

  public $_selectToExport="";

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
    $this->load->library('table');
    $this->table->set_heading('FECHA', 'CNB OPERADOR', 'CNB AGENCIA','CNB USER','CNB TERMINAL','DESCRIPCION TRX','MONTO TRX','MONTO COMISION');
  }

  public function PopulateTable(){
    $this->db->select('*');
    $this->db->order_by('stamp','DESC');
    $this->db->limit(1000);
    return $this->db->get($this->_tablename)->result();
  }

  public function ExportToPdfByType(){
    $this->db->select('stamp,cnbOperador,cnbAgencia,cnbUser,cnbTerminal,descripcionTrx,currency,montoTrx,montoComision');
    $fecha_inicio=$this->input->post('fecha_inicio').' 00:00:00';
    $fecha_fin=$this->input->post('fecha_fin').' 23:59:00';
    $tipo=$this->input->post('tipo');
    $this->_pdfDescription='REPORTE DE CNB por tipo ('.$tipo.')';
    //$this->db->select('*');
    if($this->input->post('fecha_inicio') && $this->input->post('fecha_fin') && $this->input->post('tipo')){
      $this->db->where('stamp > ',$fecha_inicio);
      $this->db->where('stamp < ',$fecha_fin);
      $this->db->where('descripcionTrx',$tipo);
    }
    $this->db->order_by('stamp','ASC');
    //$this->db->limit(1000);

    $query=$this->db->get($this->_tablename);

    foreach ($query->result() as $value) {
      $this->table->add_row($value->stamp,
                            $value->cnbOperador,
                            $value->cnbAgencia,
                            $value->cnbUser,
                            $value->cnbTerminal,
                            $value->descripcionTrx,
                            $value->currency.' '.$value->montoTrx,
                            $value->currency.' '.$value->montoComision
                            );

    }

    $this->db->select('montoTrx,montoComision,aux');
    if($this->input->post('fecha_inicio') && $this->input->post('fecha_fin')){
      $this->db->where('stamp > ',$this->input->post('fecha_inicio'));
      $this->db->where('stamp < ',$this->input->post('fecha_fin'));
    }
    $q=$this->db->get($this->_tablename);

    $total_MontoComision=0;
    $total_MontoDebito=0;
    $total_MontoCredito=0;
    $saldoTotal=0;

    foreach ($q->result() as $v){
      if($v->aux == '+'){
        $total_MontoCredito += floatval($v->montoTrx);
      }
      if($v->aux == '-'){
        $total_MontoDebito += floatval($v->montoTrx);
      }
      $total_MontoComision+= floatval($v->montoComision);

    }


    $saldoTotal=$total_MontoCredito-$total_MontoDebito;

    $this->_TableToExport=$this->table->generate();
    $this->_footer_ContentToExport.='<br><br><p>Monto Débito: '.$total_MontoDebito.'</p>';
    $this->_footer_ContentToExport.='<p>Monto Crédito: '.$total_MontoCredito.'</p>';
    $this->_footer_ContentToExport.='<p>Monto Comisión: '.$total_MontoComision.'</p>';
    $this->_footer_ContentToExport.='<p>===============================================================================</p>';
    $this->_footer_ContentToExport.='<br><br><p>Saldo Total (crédito - débito): '.floatval($total_MontoCredito-$total_MontoDebito).'</p>';

    $this->generar();
    //var_dump($query);

  }


  //cnbOperador,cnbAgencia,cnbUser,cnbTerminal,descripcionTrx,currency,montoTrx,montoComision
  public function Reporte(){
    $this->db->select('stamp,cnbOperador,cnbAgencia,cnbUser,cnbTerminal,descripcionTrx,currency,montoTrx,montoComision');
    $fecha_inicio=$this->input->post('inicio').' 00:00:00';
    $fecha_fin=$this->input->post('fin').' 23:59:00';
    //$this->db->select('*');
    if($this->input->post('inicio') && $this->input->post('fin')){
      $this->db->where('stamp > ',$fecha_inicio);
      $this->db->where('stamp < ',$fecha_fin);
    }
    $this->db->order_by($this->_orderName,$this->_order);
    //$this->db->limit(1000);

    $query=$this->db->get($this->_tablename);

    foreach ($query->result() as $value) {
      $this->table->add_row($value->stamp,
                            $value->cnbOperador,
                            $value->cnbAgencia,
                            $value->cnbUser,
                            $value->cnbTerminal,
                            $value->descripcionTrx,
                            $value->currency.' '.$value->montoTrx,
                            $value->currency.' '.$value->montoComision
                            );

    }

    $this->db->select('montoTrx,montoComision,aux');
    if($this->input->post('inicio') && $this->input->post('fin')){
      $this->db->where('stamp > ',$fecha_inicio);
      $this->db->where('stamp < ',$fecha_fin);
    }
    $q=$this->db->get($this->_tablename);

    $total_MontoComision=0;
    $total_MontoDebito=0;
    $total_MontoCredito=0;
    $saldoTotal=0;

    foreach ($q->result() as $v){
      if($v->aux == '+'){
        $total_MontoCredito += floatval($v->montoTrx);
      }
      if($v->aux == '-'){
        $total_MontoDebito += floatval($v->montoTrx);
      }
      $total_MontoComision+= floatval($v->montoComision);

    }


    $saldoTotal=$total_MontoCredito-$total_MontoDebito;

    $this->_TableToExport=$this->table->generate();
      $this->_footer_ContentToExport.='<p style="color:blue;text-transform: uppercase;font-weight: bold; font-size:1.3em;">=========================================descripciones=======================================</p>';
    $this->_footer_ContentToExport.='<br><br><p style="black:blue;text-transform: uppercase;font-weight: bold; font-size:1.3em;">Monto Débito: '.$total_MontoDebito.'</p>';
    $this->_footer_ContentToExport.='<p style="color:black;text-transform: uppercase;font-weight: bold; font-size:1.3em;">Monto Crédito: '.$total_MontoCredito.'</p>';
    $this->_footer_ContentToExport.='<p style="color:black;text-transform: uppercase;font-weight: bold; font-size:1.3em;">Monto Comisión: '.$total_MontoComision.'</p>';
    $this->_footer_ContentToExport.='<p style="color:blue;text-transform: uppercase;font-weight: bold; font-size:1.3em;">===============================================================================================</p>';
    $this->_footer_ContentToExport.='<br><br><p style="black:blue;text-transform: uppercase;font-weight: bold; font-size:1.3em;">Saldo Total (crédito - débito): '.floatval($total_MontoCredito-$total_MontoDebito).'</p>';
    $this->_footer_ContentToExport.='<p style="color:blue;text-transform: uppercase;font-weight: bold; font-size:1.3em;">===============================================================================================</p>';
    $this->_footer_ContentToExport.='<br><br><p style="color:blue;text-transform: uppercase;font-weight: bold; font-size:1.3em;">Periodo</p>';
    $this->_footer_ContentToExport.='<br><br><p style="color:black;text-transform: uppercase;font-weight: bold; font-size:1.3em;">Desde : '.$fecha_inicio.' Hasta : '.$fecha_fin.'</p>';

    $this->generar();
    //var_dump($query);
  }

  public function searchByDate(){
    $this->db->select('*');
    $fecha_inicio=$this->input->post('fecha_inicio').' 00:00:00';
    $fecha_fin=$this->input->post('fecha_fin').' 23:59:00';
    if($this->input->post('fecha_inicio') && $this->input->post('fecha_fin')){
      $this->db->where('stamp > ',$fecha_inicio);
      $this->db->where('stamp < ',$fecha_fin);
    }
    $this->db->order_by('stamp','ASC');
    return $this->db->get($this->_tablename)->result();
  }

  public function searchByDateAndType(){
    $this->db->select('*');
    $fecha_inicio=$this->input->post('fecha_inicio').' 00:00:00';
    $fecha_fin=$this->input->post('fecha_fin').' 23:59:00';
    $tipo=$this->input->post('tipo');
    if($this->input->post('fecha_inicio') && $this->input->post('fecha_fin') && $this->input->post('tipo')){
      $this->db->where('stamp > ',$fecha_inicio);
      $this->db->where('stamp < ',$fecha_fin);
      $this->db->where('descripcionTrx',$tipo);
    }
    $this->db->order_by('stamp','ASC');
    return $this->db->get($this->_tablename)->result();
  }

  public function exportLog(){
    if($this->input->post('inicio')){
			$fecha_inicio=$this->input->post('inicio');
		}
		if($this->input->post('inicio')){
			$fecha_fin=$this->input->post('fin');
		}

		if(count($this->_selectToExport)>0){
			for ($i=0; $i < count($this->_selectToExport); $i++) {
				$this->db->select($this->_selectToExport[$i]);
			}
		}else{
			$this->db->select('*');
		}
		if($this->_orderName!='' && $this->_order!=''){
			$this->db->order_by($this->_orderName,$this->_order);
		}

		if(isset($fecha_inicio)  && isset($fecha_fin)){
			$fecha_inicio.=' 00:00:00';
			$fecha_fin.=' 23:59:00';
			$this->db->where($this->_dateVariable.' >',$fecha_inicio);
			$this->db->where($this->_dateVariable.' <',$fecha_fin);
		}

		if(count($this->_join) > 0){

			$this->db->join($this->_join['table'],$this->_join['relation'],$this->_join['type']);
		}
		$query=$this->db->get($this->_tablename)->result_array();
		$this->table->set_heading($this->_headersTable);
		$this->_TableToExport=$this->table->generate($query);
		$this->generar();
  }

  public function ReporteByType(){


      $this->db->select('stamp,cnbOperador,cnbAgencia,cnbUser,cnbTerminal,descripcionTrx,currency,montoTotal,comisionTotal');
      $fecha_inicio=$this->input->post('inicio').' 00:00:00';
      $fecha_fin=$this->input->post('fin').' 23:59:00';
      //$this->db->select('*');


      if($this->_columnSearch != '' && $this->_columnValue !=''){
        $this->db->where($this->_columnSearch ,$this->_columnValue);
      }
      if($this->input->post('inicio') && $this->input->post('fin')){
        $this->db->where('stamp > ',$fecha_inicio);
        $this->db->where('stamp < ',$fecha_fin);
      }
      $this->db->order_by($this->_orderName,$this->_order);
      //$this->db->limit(1000);

      $query=$this->db->get($this->_tablename);

      foreach ($query->result() as $value) {
        $this->table->add_row($value->stamp,
                              $value->cnbOperador,
                              $value->cnbAgencia,
                              $value->cnbUser,
                              $value->cnbTerminal,
                              $value->descripcionTrx,
                              $value->currency.' '.$value->montoTotal,
                              $value->currency.' '.$value->comisionTotal
                              );

      }

      $this->db->select('montoTotal,comisionTotal,aux');
      if($this->input->post('inicio') && $this->input->post('fin')){
        $this->db->where('stamp > ',$fecha_inicio);
        $this->db->where('stamp < ',$fecha_fin);
      }
      if($this->_columnSearch != '' && $this->_columnValue !=''){
        $this->db->where($this->_columnSearch ,$this->_columnValue);
      }
      $q=$this->db->get($this->_tablename);

      $total_MontoComision=0;
      $total_MontoDebito=0;
      $total_MontoCredito=0;
      $saldoTotal=0;

      foreach ($q->result() as $v){
        if($v->aux == '+'){
          $total_MontoCredito += floatval($v->montoTotal);
        }
        if($v->aux == '-'){
          $total_MontoDebito += floatval($v->montoTotal);
        }
        $total_MontoComision+= floatval($v->comisionTotal);

      }


      $saldoTotal=$total_MontoCredito-$total_MontoDebito;

      $this->_TableToExport=$this->table->generate();
        $this->_footer_ContentToExport.='<p style="color:blue;text-transform: uppercase;font-weight: bold; font-size:1.3em;">=========================================descripciones=======================================</p>';
      $this->_footer_ContentToExport.='<br><br><p style="black:blue;text-transform: uppercase;font-weight: bold; font-size:1.3em;">Monto Débito: '.$total_MontoDebito.'</p>';
      $this->_footer_ContentToExport.='<p style="color:black;text-transform: uppercase;font-weight: bold; font-size:1.3em;">Monto Crédito: '.$total_MontoCredito.'</p>';
      $this->_footer_ContentToExport.='<p style="color:black;text-transform: uppercase;font-weight: bold; font-size:1.3em;">Monto Comisión: '.$total_MontoComision.'</p>';
      $this->_footer_ContentToExport.='<p style="color:blue;text-transform: uppercase;font-weight: bold; font-size:1.3em;">===============================================================================================</p>';
      $this->_footer_ContentToExport.='<br><br><p style="black:blue;text-transform: uppercase;font-weight: bold; font-size:1.3em;">Saldo Total (crédito - débito): '.floatval($total_MontoCredito-$total_MontoDebito).'</p>';
      $this->_footer_ContentToExport.='<p style="color:blue;text-transform: uppercase;font-weight: bold; font-size:1.3em;">===============================================================================================</p>';
      $this->_footer_ContentToExport.='<br><br><p style="color:blue;text-transform: uppercase;font-weight: bold; font-size:1.3em;">Periodo</p>';
      $this->_footer_ContentToExport.='<br><br><p style="color:black;text-transform: uppercase;font-weight: bold; font-size:1.3em;">Desde : '.$fecha_inicio.' Hasta : '.$fecha_fin.'</p>';

      $this->generar();
      //var_dump($query);

  }

public function ReportesDeIngreso(){


      $this->db->select('stamp,cnbOperador,cnbAgencia,cnbUser,cnbTerminal,descripcionTrx,currency,montoTrx,montoComision');
      $fecha_inicio=$this->input->post('inicio').' 00:00:00';
      $fecha_fin=$this->input->post('fin').' 23:59:00';
      //$this->db->select('*');

      if($this->input->post('inicio') && $this->input->post('fin')){
        $this->db->where('stamp > ',$fecha_inicio);
        $this->db->where('stamp < ',$fecha_fin);
        $this->db->where('aux' ,'+');
      }
      $this->db->order_by($this->_orderName,$this->_order);
      //$this->db->limit(1000);

      $query=$this->db->get($this->_tablename);

      foreach ($query->result() as $value) {
        $this->table->add_row($value->stamp,
                              $value->cnbOperador,
                              $value->cnbAgencia,
                              $value->cnbUser,
                              $value->cnbTerminal,
                              $value->descripcionTrx,
                              $value->currency.' '.$value->montoTrx,
                              $value->currency.' '.$value->montoComision
                              );

      }

      $this->db->select('montoTrx,montoComision,aux');
      if($this->input->post('inicio') && $this->input->post('fin')){
        $this->db->where('stamp > ',$fecha_inicio);
        $this->db->where('stamp < ',$fecha_fin);
      }
      if($this->_columnSearch != '' && $this->_columnValue !=''){
        $this->db->where($this->_columnSearch ,$this->_columnValue);
      }
      $q=$this->db->get($this->_tablename);

      $total_MontoComision=0;
      $total_MontoDebito=0;
      $total_MontoCredito=0;
      $saldoTotal=0;

      foreach ($q->result() as $v){
        if($v->aux == '+'){
          $total_MontoCredito += floatval($v->montoTrx);
        }
        if($v->aux == '-'){
          $total_MontoDebito += floatval($v->montoTrx);
        }
        $total_MontoComision+= floatval($v->montoComision);

      }


      $saldoTotal=$total_MontoCredito-$total_MontoDebito;

      $this->_TableToExport=$this->table->generate();
        $this->_footer_ContentToExport.='<p style="color:blue;text-transform: uppercase;font-weight: bold; font-size:1.3em;">=========================================descripciones=======================================</p>';
      $this->_footer_ContentToExport.='<br><br><p style="black:blue;text-transform: uppercase;font-weight: bold; font-size:1.3em;">Monto Débito: '.$total_MontoDebito.'</p>';
      $this->_footer_ContentToExport.='<p style="color:black;text-transform: uppercase;font-weight: bold; font-size:1.3em;">Monto Crédito: '.$total_MontoCredito.'</p>';
      $this->_footer_ContentToExport.='<p style="color:black;text-transform: uppercase;font-weight: bold; font-size:1.3em;">Monto Comisión: '.$total_MontoComision.'</p>';
      $this->_footer_ContentToExport.='<p style="color:blue;text-transform: uppercase;font-weight: bold; font-size:1.3em;">===============================================================================================</p>';
      $this->_footer_ContentToExport.='<br><br><p style="black:blue;text-transform: uppercase;font-weight: bold; font-size:1.3em;">Saldo Total (crédito - débito): '.floatval($total_MontoCredito-$total_MontoDebito).'</p>';
      $this->_footer_ContentToExport.='<p style="color:blue;text-transform: uppercase;font-weight: bold; font-size:1.3em;">===============================================================================================</p>';
      $this->_footer_ContentToExport.='<br><br><p style="color:blue;text-transform: uppercase;font-weight: bold; font-size:1.3em;">Periodo</p>';
      $this->_footer_ContentToExport.='<br><br><p style="color:black;text-transform: uppercase;font-weight: bold; font-size:1.3em;">Desde : '.$fecha_inicio.' Hasta : '.$fecha_fin.'</p>';

      $this->generar();
      //var_dump($query);

  }


}

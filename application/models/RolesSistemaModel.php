<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class RolesSistemaModel extends MY_SuperModel {

	protected $_tablename='adm.roles';
	protected $_primary_key='Id';
	protected $_primary_filter='intval';
	protected $_order_by='Id';

	public  $_rules=array('descripcion'=>array('field'=>'descripcion','label'=>'Descripción','rules'=>'trim|required|max_length[150]')
	);

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}


	public function ListaPermisos($id=NULL){

		$this->db->select('*');
		$this->db->order_by('Descripcion','ASC');
		$query=$this->db->get('adm.permisos');


		$lista='';
		foreach ($query->result() as $key => $value) {
			$lista.='<li class="list-group-item list-group-item-warning" id="'.$value->Id.'">'.$value->Descripcion.'</li>';
		}

		return $lista;
	}

	public function permisosSeteados($permisos){
		$this->db->select('*');
		$this->db->order_by('Descripcion','ASC');
		$query=$this->db->get('adm.permisos');

		$lista='';
		foreach ($query->result() as $value) {
			if(in_array($value->Id, $permisos)){
				$lista.='<li class="list-group-item list-group-item-success" id="'.$value->Id.'">'.$value->Descripcion.'</li>';
			}
		}
		return $lista;
	}

	public function PermisosDisponibles($permisosDisponibles){
		$this->db->select('*');
		$this->db->order_by('Descripcion','ASC');
		$query=$this->db->get('adm.permisos');

		$lista='';
		foreach ($query->result() as $value) {
			if(!in_array($value->Id, $permisosDisponibles)){
				$lista.='<li class="list-group-item list-group-item-warning" id="'.$value->Id.'">'.$value->Descripcion.'</li>';
			}
		}
		return $lista;
	}


}

/* End of file RolesSistema.php */
/* Location: ./application/models/RolesSistema.php */

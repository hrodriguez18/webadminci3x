<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ExcepcionModel extends MY_SuperModel {

	protected $_tablename='esb.grupoExcepciones';
	protected $_primary_key='id';
	protected $_primary_filter='intval';
	protected $_order_by='id';

	public  $_rules=array();
	public $_inputs=array();
	protected $_timestamps=FALSE;
	protected $_form='';
	protected $_form_group_class='form-group';
	protected $_attr_submit_button='class="btn btn-success"';
	protected $_class_label='class="label label-warning"';

	protected $_ruta='AdminExcepciones/';

	//RELACION PARA GENERAR LA TABLA DE ESTA CLASE
	protected $_headingTable=array();
	//usado para buscar los datos de la db
	protected $_datatable=array();
	//usado para mostrar los datos en la tabla e incluir los btn actions
	protected $_dataForTable=array();
	protected $_jointable=array();



	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}


	public function SaveGrupoExcepciones($data){
		$this->db->insert('esb.grupoExcepciones',$data);
		return $this->db->insert_id();
	}

	public function DataTable(){
		$this->db->select('*');
		return $this->db->get($this->_tablename)->result();
	}

	public function TraerInputsDays($id){
		$this->db->select('*');
		$this->db->where('grupoExcepcionesId',$id);
		$query=$this->db->get('esb.relGrupoExcepcionesDias');
		$html='';
		foreach ($query->result() as $value) {
			$html.='<tr><td><input type="date" value="'.$value->fechaExcepcion.'" class="form-control" name="day[]"/></td></tr>';
		}
		return $html;
	}

	public function UpdateGrupoExcepciones($data,$id){
		$this->db->where('id',$id);
		$this->db->update($this->_tablename,$data);
	}

	public function LimpiarExcepcionDeDias($idGrupo){
		$this->db->where('grupoExcepcionesId',$idGrupo);
		$this->db->delete('esb.relGrupoExcepcionesDias');
	}



}

/* End of file ExcepcionModel.php */
/* Location: ./application/models/ExcepcionModel.php */

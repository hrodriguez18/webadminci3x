<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class merchants_m extends MY_Model {
 	public $_table_name = 'esb.merchant';
	protected $_primary_key = 'Id';
	protected $_primary_filter = 'intval';
	protected $_order_by = 'Id';
	public $rules = array('merchantId'=>array('field'=>'merchantId','label'=>'Merchant ID','rules'=>'trim|required'),
		'dbaNumber'=>array('field'=>'dbaNumber','label'=>'EPX DBA','rules'=>'trim|required'),
		'custumer_number'=>array('field'=>'custumer_number','label'=>'Custumer','rules'=>'trim|required'),
		'terminalNumber'=>array('field'=>'terminalNumber','label'=>'EPX Terminal','rules'=>'trim|required'),
		'descripcion'=>array('field'=>'descripcion','label'=>'ACP Password','rules'=>'trim|required')
	);

	public $merchantId=array('name'=>'merchantId','type'=>'number','placeholder'=>'Merchant ID','class'=>'form-control','id'=>'merchantId','value'=>'','min'=>0);

	public $epx=array('name'=>'dbaNumber','type'=>'text','placeholder'=>'EPX DBA / ACP USER','class'=>'form-control','id'=>'dbaNumber','value'=>'');

	public $custumer=array('name'=>'custumer_number','type'=>'text','placeholder'=>'Custumer Number','class'=>'form-control','id'=>'custumer_number','value'=>'');
	public $epx_terminal=array('name'=>'terminalNumber','type'=>'text','placeholder'=>'EPX Terminal / ACP TParty','class'=>'form-control','id'=>'terminalNumber','value'=>'');
	public $epx_descripcion_acp=array('name'=>'descripcion','type'=>'text','placeholder'=>'EPC Descripción / ACP Password','class'=>'form-control','id'=>'descripcion','value'=>'','rows'=>3);

	public $array_status=array('I'=>'Inactivo','A'=>'Activo');








	protected $_timestamps = FALSE;


	function __construct() {
		parent::__construct();
	}


	public function optbiller(){
		$this->db->select('*');
		$query=$this->db->get('esb.Billers');
		$opt=array();
		if($query->num_rows()>0){
			foreach ($query->result() as $value) {
				$opt[$value->Cod_Biller]=$value->Descripcion;
			}
		}
		return $opt;
	}

	public function Form(){
		$option_for_processor=$this->optbiller();
		$form="";
		$this->merchantId['value']=set_value('merchantId');
		$this->epx['value']=set_value('dbaNumber');
		$this->custumer['value']=set_value('custumer_number');
		$this->epx_terminal['value']=set_value('terminalNumber');
		$this->epx_descripcion_acp['value']=set_value('descripcion');


		$form.='<div class="form-group">'.form_error($this->merchantId['id']).$this->lang->line('merchantId').form_input($this->merchantId).'</div>';
		$form.='<div class="form-group">'.form_error($this->epx['id']).$this->lang->line('dbaNumber').form_input($this->epx).'</div>';
		$form.='<div class="form-group">'.form_error($this->custumer['id']).$this->lang->line('customerNumber').form_input($this->custumer).'</div>';
		$form.='<div class="form-group">'.form_error($this->epx_terminal['id']).$this->lang->line('terminalNumber').form_input($this->epx_terminal,set_value($this->epx_terminal['id'])).'</div>';
		$form.='<div class="form-group">'.form_error($this->epx_descripcion_acp['id']).$this->lang->line('descriptionMer').form_textarea($this->epx_descripcion_acp,set_value($this->epx_descripcion_acp['id'])).'</div>';
		$form.='<div class="form-group">'.form_error('status').$this->lang->line('status').form_dropdown('status',$this->array_status,set_value('status'),'class="form-control"').'</div>';
		$form.='<div class="form-group">'.form_error('processor').$this->lang->line('biller').form_dropdown('processor',$option_for_processor,set_value('processor'),'class="form-control"').'</div>';





		return $form;
	}

	public function FormForEdit($data_by_id){

		$option_for_processor=$this->optbiller();
		$form="";
		$this->merchantId['value']=$data_by_id->merchantId;
		$this->epx['value']=$data_by_id->dbaNumber;
		$this->custumer['value']=$data_by_id->customerNumber;
		$this->epx_terminal['value']=$data_by_id->terminalNumber;
		$this->epx_descripcion_acp['value']=$data_by_id->descripcion;

		$id=array('name'=>'Id','type'=>'hidden','value'=>$data_by_id->Id);









		$form.=form_input($id);
		$form.='<div class="form-group">'.form_error($this->merchantId['id']).$this->lang->line('merchantId').form_input($this->merchantId).'</div>';
		$form.='<div class="form-group">'.form_error($this->epx['id']).$this->lang->line('dbaNumber').form_input($this->epx).'</div>';
		$form.='<div class="form-group">'.form_error($this->custumer['id']).$this->lang->line('customerNumber').form_input($this->custumer).'</div>';
		$form.='<div class="form-group">'.form_error($this->epx_terminal['id']).$this->lang->line('terminalNumber').form_input($this->epx_terminal,set_value($this->epx_terminal['id'])).'</div>';
		$form.='<div class="form-group">'.form_error($this->epx_descripcion_acp['id']).$this->lang->line('descriptionMer').form_textarea($this->epx_descripcion_acp,set_value($this->epx_descripcion_acp['id'])).'</div>';
		$form.='<div class="form-group">'.form_error('status').$this->lang->line('status').form_dropdown('status',$this->array_status,$data_by_id->status,'class="form-control"').'</div>';
		$form.='<div class="form-group">'.form_error('processor').$this->lang->line('biller').form_dropdown('processor',$option_for_processor,$data_by_id->procesadorId,'class="form-control"').'</div>';

		return $form;

	}

	public function getMerchantData(){
		$this->db->select('*');
		$this->db->select('Billers.Descripcion as procesadorId');
		$this->db->join('esb.Billers', 'merchant.procesadorId=Billers.Cod_Biller','left');
		return $this->db->get('esb.merchant')->result();
	}




}
?>

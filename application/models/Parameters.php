<?php
/**
 * Created by PhpStorm.
 * User: hrodriguez
 * Date: 10/05/18
 * Time: 11:01 AM
 */

class Parameters extends CI_Model
{
	private $_tablename='adm.parameters';

	public $sess_expiration='';

	public function __construct()
	{
		parent::__construct();
		$this->SetData();

	}

	private function SetData(){
		$this->db->select('*');
		$query=$this->db->get($this->_tablename);
		$property=get_class_vars('Parameters');
		foreach ($query->result() as $value){
			if($value->name=='sess_expiration'){
				$this->sess_expiration=$value->value;
			}

		}


	}

}

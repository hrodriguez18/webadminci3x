<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class moneda_m extends MY_Model {
 	public $_table_name = 'esb.Monedas';
	protected $_primary_key = 'Cod_Moneda';
	protected $_primary_filter = 'intval';
	protected $_order_by = 'Cod_Moneda';
	public $rules = array(

		'Cod_Moneda'=>array('field'=>'Cod_Moneda','label'=>'Código Moneda','rules'=>'trim|required|numeric|callback_unico'),
		'Descripcion'=>array('field'=>'Descripcion','label'=>'Descripción','rules'=>'trim|required'),
		'DescripcionCorta'=>array('field'=>'DescripcionCorta','label'=>'Etiqueta','rules'=>'trim|required|max_length[3]')
	);
	public $rules_update = array(

		'Cod_Moneda'=>array('field'=>'Cod_Moneda','label'=>'Código Moneda','rules'=>'trim|required|numeric'),
		'Descripcion'=>array('field'=>'Descripcion','label'=>'Descripción','rules'=>'trim|required'),
		'DescripcionCorta'=>array('field'=>'DescripcionCorta','label'=>'Etiqueta','rules'=>'trim|required|max_length[3]')
	);
	protected $_timestamps = FALSE;

	private $template = array(
        'table_open'            => '<table border="0" cellpadding="4" cellspacing="0" class="table table-hover table-striped" id="bootstrap-table">',

        'thead_open'            => '<thead>',
        'thead_close'           => '</thead>',

        'heading_row_start'     => '<tr>',
        'heading_row_end'       => '</tr>',
        'heading_cell_start'    => '<th data-sortable="true">',
        'heading_cell_end'      => '</th>',

        'tbody_open'            => '<tbody id="body_table_moneda">',
        'tbody_close'           => '</tbody>',

        'row_start'             => '<tr>',
        'row_end'               => '</tr>',
        'cell_start'            => '<td>',
        'cell_end'              => '</td>',

        'row_alt_start'         => '<tr>',
        'row_alt_end'           => '</tr>',
        'cell_alt_start'        => '<td>',
        'cell_alt_end'          => '</td>',

        'table_close'           => '</table>'
	);



	function __construct() {
		parent::__construct();
	}

	public function unico($str){
		$this->db->select('*');
		$this->db->where('Cod_Moneda',$str);
		$query=$this->db->get('esb.Monedas');
		if($query->num_rows()>0){
			$this->form_validation->set_message('unico', 'Este Valor ya Existe...');
			return false;
		}else{
			return true;
		}
		return false;
	}


	public function SetTable($Column,$data){
		$this->table->set_template($this->template);
		$this->table->set_heading($Column);
		$i=0;
		$info=array();
		foreach($data as  $value) {
			$editar='<button class="btn btn-primary" onclick="editMoneda('.$value->Cod_Moneda.')"><i class="ti-pencil-alt" ></i></button>';
			$eliminar='<button class="btn btn-danger" onclick="deleteMoneda('.$value->Cod_Moneda.')"><i class="ti-trash"></i></button>';
			$info[$i]=array(($i+1),$value->Cod_Moneda,$value->Descripcion,$value->DescripcionCorta,$editar,$eliminar);
			$i++;
		}
		return $this->table->generate($info);
		//return var_dump($data);
	}

	public function RefreshTableMoneda(){
		$query=$this->get();
		$i=1;
		$info="";
		foreach ($query as $value) {
			$info.='<tr><td></td><td>'.$value->Cod_Moneda.'</td><td>'.$value->Descripcion.'</td><td>'.$value->DescripcionCorta.'</td><td></td></tr>';
			$i++;
		}
		return $info;
	}




}
?>

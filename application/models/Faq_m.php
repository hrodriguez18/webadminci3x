<?php
/**
 * Created by PhpStorm.
 * User: hrodriguez
 * Date: 10/01/18
 * Time: 02:55 AM
 */


defined('BASEPATH') OR exit('No direct script access allowed');

class Faq_m extends MY_SuperModel
{

	//VARIABLES PARA HACER LAS CONSULTAS A LA BASE DE DATOS
	public $_tablename='esb.FAQ';
	public $_primary_key='Id';
	public $_primary_filter='intval';
	public $_order_by='Id';

	//VARIABLES QUE SE NECESITAN PARA GENERAR EL FORMULARIO
	//type selector
	/*
    array('name'=>'imagen','id'=>'imagen',
    'type'=>'selector','table_rel'=>'esb.LogosEmpresas',
    'opt'=>'ruta','description'=>'nombre','class'=>'form-control',
    'default'=>'1','label'=>'Logo de Empresa')
    */
	public $documentationForForm='';


	public  $_rules=array(
		'Titulo'=>array(
			'field'=>'Titulo',
			'label'=>'Titulo',
			'rules'=>'trim|required|max_length[150]'
		),

		'Texto'=>array(
			'field'=>'Texto',
			'label'=>'Texto',
			'rules'=>'trim|required'
		),

		'Estado'=>array(
			'field'=>'Estado',
			'label'=>'Estado',
			'rules'=>'trim|required|callback_diferente_de_default'
		),
		'tipo'=>array(
			'field'=>'tipo',
			'label'=>'tipoFaq',
			'rules'=>'trim|required|callback_diferente_de_default'
		)

	);
	public $_inputs=array(

		'Titulo'=>array(
			'name'=>'Titulo',
			'id'=>'Titulo',
			'placeholder'=>'Titulo',
			'label'=>'titulo',
			'type'=>'text',
			'class'=>'form-control'
		),

		'Texto'=>array(
			'name'=>'Texto',
			'id'=>'Texto',
			'type'=>'textarea',
			'placeholder'=>'Texto',
			'label'=>'texto',
			'class'=>'form-control'
		),

		'tipo'=>array(
			'name'=>'tipo',
			'id'=>'tipo',
			'type'=>'selector',
			'table_rel'=>'esb.tipoFaq',
			'opt'=>'id',
			'description'=>'name',
			'class'=>'form-control',
			'default'=>'1',
			'label'=>'tipoFaq'
		),

		'Estado'=>array(
			'name'=>'Estado',
			'id'=>'Estado',
			'type'=>'selector',
			'table_rel'=>'esb.status',
			'opt'=>'value',
			'description'=>'description',
			'class'=>'form-control',
			'default'=>'A',
			'label'=>'status'
		),
		'Id'=>array(
			'name'=>'Id',
			'id'=>'Id',
			'type'=>'hidden'
		)

	);

	protected $_timestamps=FALSE;
	protected $_form='';
	protected $_form_group_class='form-group ';
	protected $_attr_submit_button='';
	protected $_class_label='';


	public function __construct()
	{
		parent::__construct();

		//Do your magic here
	}


}

/* End of file .php */

?>

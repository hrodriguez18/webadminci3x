
  <?php
  defined('BASEPATH') OR exit('No direct script access allowed');

  class populateModel extends CI_Model{

    public $_tablename='';
    public $_primary_key='id';
    public $_primary_filter='intval';
    public $_order_by='id';
    public $_order='DESC';
    public $fecha_inicio='';
    public $fecha_fin='';
    public $_timestamps='';
    public $_limit=0;
    public $_search='';
    public $_offset=0;
    public $_selection=array();
    public $_valueSearch='';
    public $_searchingValue='';
    public $_isMap=FALSE;
    public $_joinRelation=array();
    public $_camposBusqueda=array();
    public $withCount=FALSE;
    public $withCountCNB=FALSE;
    public $_Comisiones=FALSE;
    public $VistaUserMonedero=FALSE;


    public function __construct()
    {
      parent::__construct();
      //Codeigniter : Write Less Do More


    }

    public function populateUsersMonedero(){

		$this->SetPropertiesUsersMonedero();

		$pop=array();
		if(count($this->_selection)>0){
			for($i=0; $i < count($this->_selection); $i++) {
				$this->db->select($this->_selection[$i]);
			}
		}else{
			$this->db->select('*');
		}

		if(count($this->_joinRelation)>0){
			for($i=0;$i<count($this->_joinRelation); $i++){
				$this->db->join($this->_joinRelation[$i]['table'],$this->_joinRelation[$i]['where_equals'],$this->_joinRelation[$i]['option']);
			}
		}

		if($this->fecha_inicio!='' && $this->fecha_fin != ''){
			$this->db->where($this->_timestamps.' > ',$this->fecha_inicio);
			$this->db->where($this->_timestamps.' < ',$this->fecha_fin );
		}

		if(count($this->_camposBusqueda)> 0 && $this->_search!=''){
			$i=0;
			$this->db->like($this->_camposBusqueda[$i],$this->_search);
			for($i;$i<count($this->_camposBusqueda);$i++){
				$this->db->or_like($this->_camposBusqueda[$i],$this->_search);
			}

		}

		if($this->_limit>0){
			$this->db->limit($this->_limit,$this->_offset);
		}
		if($this->_order_by != ''){

			$this->db->order_by($this->_order_by,$this->_order);
		}

		$pop['rows']=$this->db->get($this->_tablename)->result();
		$i=0;

		if($this->_isMap){
			foreach ($pop['rows'] as  $value) {
				$pop['rows'][$i]->mapa='<a onclick="ShowPointmap(\'https://maps.googleapis.com/maps/api/staticmap?center='.$value->lat.','.$value->lon.'&zoom=15&size=600x400&maptype=roadmap&markers=color:red%7Clabel:C%7C'.$value->lat.','.$value->lon.'&key=AIzaSyCjTI0FdJCGl26y2XJdFNZwP0LDYj2DL_A\')" href="#">MAPA</a>';
				$i++;
			}
		}

		if($this->withCount){
			foreach ($pop['rows'] as  $value) {
				$this->db->select('*');
				$this->db->where('userMonedero',$value->email);
				$pop['rows'][$i]->cantidadTransacciones=$this->db->get('esb.transactionMonederoLog')->num_rows();


				$this->db->select('*');
				$this->db->where('userId',$value->id);
				$pop['rows'][$i]->cantidadLogin=$this->db->get('esb.loginMonedero')->num_rows();

				$i++;
			}
		}

		if($this->withCountCNB){
			foreach ($pop['rows'] as  $value) {
				$this->db->select('*');
				$this->db->where('cnbOperador',$value->Descripcion);
				$pop['rows'][$i]->cantidadTransacciones=$this->db->get('esb.transactionLog')->num_rows();


				$this->db->select('*');
				$this->db->where('operator',$value->Descripcion);
				$pop['rows'][$i]->cantidadLogin=$this->db->get('esb.Log_Login')->num_rows();

				$i++;
			}
		}

		if($this->_search != ''){
			//$pop['total']=$this->db->count_all_results();
			$pop['total']= count($pop['rows']);
		}else{

			$pop['total']= $this->db->count_all($this->_tablename);
			//$pop['total']=$this->db->count_all_results();
		}
		return json_encode($pop);

	}

	public function populateTrx(){

		  $pop=array();
		  if(count($this->_selection)>0){
			  for($i=0; $i < count($this->_selection); $i++) {
				  $this->db->select($this->_tablename.'.'.$this->_selection[$i]);
			  }
		  }else{
			  $this->db->select('*');
		  }

		  if($this->fecha_inicio!='' && $this->fecha_fin != ''){
			  $this->db->where($this->_timestamps.' > ',$this->fecha_inicio);
			  $this->db->where($this->_timestamps.' < ',$this->fecha_fin );
		  }

		  if($this->_valueSearch !='' && $this->_searchingValue !=''){
			  $this->db->where($this->_valueSearch,$this->_searchingValue);
		  }

		  if($this->_search!=''){
			  $this->db->where($this->_primary_key,$this->_search);
			  if(count($this->_selection)){
				  foreach ($this->_selection as $key ) {
					  $this->db->or_where($key,$this->_search);
				  }
			  }
		  }


		$resultado=$this->db->get($this->_tablename);



		if($this->_limit>0){
			$this->db->limit($this->_limit,$this->_offset);
		}
		if($this->_order_by != ''){
			$this->db->order_by($this->_order_by,$this->_order);
		}

		$pop['total']=$resultado->num_rows();
		$pop['rows']=$resultado->result();





		  $i=0;


		  if($this->_isMap){
			  foreach ($pop['rows'] as  $value) {
				  $pop['rows'][$i]->mapa='<a onclick="ShowPointmap(\'https://maps.googleapis.com/maps/api/staticmap?center='.$value->lat.','.$value->lon.'&zoom=15&size=600x400&maptype=roadmap&markers=color:red%7Clabel:C%7C'.$value->lat.','.$value->lon.'&key=AIzaSyCjTI0FdJCGl26y2XJdFNZwP0LDYj2DL_A\')" href="#">MAPA</a>';
				  $i++;
			  }
		  }

		  return json_encode($pop);
	  }

    public function populate(){

    	$this->SetProperties();
      $pop=array();
      if(count($this->_selection)>0){
        for($i=0; $i < count($this->_selection); $i++) {
           $this->db->select($this->_tablename.'.'.$this->_selection[$i]);
          }
      }else{
        $this->db->select('*');
      }

        if($this->fecha_inicio!='' && $this->fecha_fin != ''){
          $this->db->where($this->_timestamps.' > ',$this->fecha_inicio);
          $this->db->where($this->_timestamps.' < ',$this->fecha_fin );
        }

        if($this->_valueSearch !='' && $this->_searchingValue !=''){
          $this->db->where($this->_valueSearch,$this->_searchingValue);
        }

        if($this->_search!=''){
            $this->db->where($this->_primary_key,$this->_search);
            if(count($this->_selection)){
              foreach ($this->_selection as $key ) {
                $this->db->or_where($key,$this->_search);
              }
            }
        }
        if($this->_limit>0){
          $this->db->limit($this->_limit,$this->_offset);
        }
        if($this->_order_by != ''){
          $this->db->order_by($this->_order_by,$this->_order);
        }

        $pop['rows']=$this->db->get($this->_tablename)->result();
        $i=0;

        if($this->_isMap){
          foreach ($pop['rows'] as  $value) {
              $pop['rows'][$i]->mapa='<a onclick="ShowPointmap(\'https://maps.googleapis.com/maps/api/staticmap?center='.$value->lat.','.$value->lon.'&zoom=15&size=600x400&maptype=roadmap&markers=color:red%7Clabel:C%7C'.$value->lat.','.$value->lon.'&key=AIzaSyCjTI0FdJCGl26y2XJdFNZwP0LDYj2DL_A\')" href="#">MAPA</a>';
              $i++;
          }
        }

        if($this->_search != ''){
          $pop['total']=$this->db->count_all_results();
        }else{
          $pop['total']= $this->db->count_all($this->_tablename);
        }
        return json_encode($pop);
    }


	  /**
	   * @return false|string
	   */
	  public function populate_join(){
	  	$this->SetProperties();
		  $pop=array();
		  if(count($this->_selection)>0){
			  for($i=0; $i < count($this->_selection); $i++) {
				  $this->db->select($this->_selection[$i]);
			  }
		  }else{
			  $this->db->select('*');
		  }

		  if(count($this->_joinRelation)>0){
		  	for($i=0;$i<count($this->_joinRelation); $i++){
		  		$this->db->join($this->_joinRelation[$i]['table'],$this->_joinRelation[$i]['where_equals'],$this->_joinRelation[$i]['option']);
			}
		  }

		  if($this->fecha_inicio!='' && $this->fecha_fin != ''){
			  $this->db->where($this->_timestamps.' > ',$this->fecha_inicio);
			  $this->db->where($this->_timestamps.' < ',$this->fecha_fin );
		  }

		  if(count($this->_camposBusqueda)> 0 && $this->_search!=''){
			  $i=0;
			  $this->db->like($this->_camposBusqueda[$i],$this->_search);
			  for($i;$i<count($this->_camposBusqueda);$i++){
				  $this->db->or_like($this->_camposBusqueda[$i],$this->_search);
			  }

		  }

		  if($this->_limit>0){
			  $this->db->limit($this->_limit,$this->_offset);
		  }

		  if($this->_order_by != ''){

		  	$splitted = explode('.',$this->_tablename);
		  	$pos=strpos($this->_order_by,$splitted[1]);
		  	if($pos === false){
				$this->_order_by=$splitted[1].'.'.$this->_order_by;
		  	}
		  	$this->db->order_by($this->_order_by,$this->_order);

		  }

		  $pop['rows']=$this->db->get($this->_tablename)->result();
		  $i=0;

		  if($this->_isMap){
			  foreach ($pop['rows'] as  $value) {
				  $pop['rows'][$i]->mapa='<a onclick="ShowPointmap(\'https://maps.googleapis.com/maps/api/staticmap?center='.$value->lat.','.$value->lon.'&zoom=15&size=600x400&maptype=roadmap&markers=color:red%7Clabel:C%7C'.$value->lat.','.$value->lon.'&key=AIzaSyCjTI0FdJCGl26y2XJdFNZwP0LDYj2DL_A\')" href="#">MAPA</a>';
				  $i++;
			  }
		  }

		  if($this->withCount){
			  foreach ($pop['rows'] as  $value) {
			  	$this->db->select('*');
			  	$this->db->where('userMonedero',$value->email);
				$pop['rows'][$i]->cantidadTransacciones=$this->db->get('esb.transactionMonederoLog')->num_rows();


				$this->db->select('*');
				$this->db->where('userId',$value->id);
				$pop['rows'][$i]->cantidadLogin=$this->db->get('esb.loginMonedero')->num_rows();

			  	$i++;
			  }
		  }

		  if($this->withCountCNB){
			  foreach ($pop['rows'] as  $value) {
				  $this->db->select('*');
				  $this->db->where('cnbOperador',$value->Descripcion);
				  $pop['rows'][$i]->cantidadTransacciones=$this->db->get('esb.transactionLog')->num_rows();


				  $this->db->select('*');
				  $this->db->where('operator',$value->Descripcion);
				  $pop['rows'][$i]->cantidadLogin=$this->db->get('esb.Log_Login')->num_rows();

				  $i++;
			  }
		  }

		  if($this->_search != ''){
			  $pop['total']=$this->db->count_all_results();
		  }else{
			  $pop['total']= $this->db->count_all($this->_tablename);
		  }
		  return json_encode($pop);
	  }


    public function populateIngresos(){
      $pop=array();
      if(count($this->_selection)>0){
        for($i=0; $i < count($this->_selection); $i++) {
           $this->db->select($this->_selection[$i]);
          }
      }else{
        $this->db->select('*');
      }

        if($this->fecha_inicio!='' && $this->fecha_fin != ''){
          $this->db->where($this->_timestamps.' > ',$this->fecha_inicio);
          $this->db->where($this->_timestamps.' < ',$this->fecha_fin );
          $this->db->where('aux','+');
        }

        if($this->_search!=''){
            if(count($this->_selection)){
				$this->db->like($this->_selection[0],$this->_search);
              foreach ($this->_selection as $key ) {
                $this->db->or_like($key,$this->_search);
              }
            }
        }
        if($this->_limit>0){
          $this->db->limit($this->_limit,$this->_offset);
        }
        if($this->_order_by != ''){
          $this->db->order_by($this->_tablename.'.'.$this->_order_by,$this->_order);
        }

        $pop['rows']=$this->db->get($this->_tablename)->result();
        $i=0;
        foreach ($pop['rows'] as  $value) {
            $pop['rows'][$i]->mapa='<a onclick="ShowPointmap(\'https://maps.googleapis.com/maps/api/staticmap?center='.$value->lat.','.$value->lon.'&zoom=15&size=600x400&maptype=roadmap&markers=color:red%7Clabel:C%7C'.$value->lat.','.$value->lon.'&key=AIzaSyCjTI0FdJCGl26y2XJdFNZwP0LDYj2DL_A\')" href="#">MAPA</a>';
            $i++;
        }
		if($this->_search != ''){
			$pop['total']=$this->db->count_all_results();
		}else{
			$pop['total']= $this->db->count_all($this->_tablename);
		}
        return json_encode($pop);
    }


    /*FUNCION PARA EL MAPA :(*/

    public function populateForMap(){
      $pop=array();
      if(count($this->_selection)>0){
        for($i=0; $i < count($this->_selection); $i++) {
           $this->db->select($this->_tablename.'.'.$this->_selection[$i]);
          }
      }else{
        $this->db->select('*');
      }


        if($this->fecha_inicio!='' && $this->fecha_fin != ''){
          $this->db->where($this->_timestamps.' > ',$this->fecha_inicio);
          $this->db->where($this->_timestamps.' < ',$this->fecha_fin );
        }



        if($this->_search!=''){
            $this->db->where($this->_primary_key,$this->_search);
            if(count($this->_selection)){
              foreach ($this->_selection as $key ) {
                $this->db->or_where($key,$this->_search);
              }
            }
        }
        if($this->_limit>0){
          $this->db->limit($this->_limit,$this->_offset);
        }
        if($this->_order_by != ''){
          $this->db->order_by($this->_order_by,$this->_order);
        }

        $pop['rows']=$this->db->get($this->_tablename)->result();
        foreach ($pop['rows'] as  $value) {
            $pop['rows']['mapa']='<a onclick="ShowPointmap(\'https://maps.googleapis.com/maps/api/staticmap?center='.$value->lat.','.$value->lon.'&zoom=15&size=600x400&maptype=roadmap&markers=color:red%7Clabel:C%7C'.$value->lat.','.$value->lon.'&key=AIzaSyCjTI0FdJCGl26y2XJdFNZwP0LDYj2DL_A\')" href="#">MAPA</a>';
        }
		if($this->_search != ''){
			$pop['total']=$this->db->count_all_results();
		}else{
			$pop['total']= $this->db->count_all($this->_tablename);
		}
        return json_encode($pop);
    }


    public function getComisionesCNBToExportExcel(){
		  $totalDeAgencias=0;
		  $rows=array();
		  $comisionesOperadores=array();
		  $this->SetProperties();
		  $this->db->select('*');
		  $operadores=$this->db->get('esb.Operadores')->result_array();
		  foreach ($operadores as $operador){
			  $this->db->select('Agencias.Descripcion,Agencias.cuentaComision');
			  $this->db->where('Cod_Operador',$operador['Cod_Operador']);
			  $Agencias=$this->db->get('esb.Agencias')->result_array();
			  foreach ($Agencias as $agencia){
				  $totalDeAgencias++;
				  $this->db->select('transactionLog.montoComision,transactionLog.montoComisionCNB');
				  $this->db->where('cnbAgencia',$agencia['Descripcion']);
				  if(strlen($this->fecha_inicio)>0){
					  $this->db->where($this->_timestamps.' >',$this->fecha_inicio);
					  $this->db->where($this->_timestamps.' <',$this->fecha_fin);
				  }
				  $comisiones=$this->db->get('esb.transactionLog')->result_array();
				  $comisionCNB=0;
				  $comisionbanco=0;
				  foreach ($comisiones as $monto){
					  $comisionbanco+=$monto['montoComision'];
					  $comisionCNB+=$monto['montoComisionCNB'];
				  }
				  array_push($comisionesOperadores,array(

						  'Operador'=>$operador['Descripcion'],
						  'Agencia'=>$agencia['Descripcion'],
						  'cuentaComision'=>$agencia['cuentaComision'],
						  'comisionCNB'=>$comisionCNB,
						  'comisionBanco'=>$comisionbanco
					  )

				  );
			  }

		  }

		  return $comisionesOperadores;
	  }
    public function getComisionesCNB(){
    	$totalDeAgencias=0;
    	$rows=array();
		$comisionesOperadores=array('total'=>'','rows'=>'');
    	$this->SetProperties();
    	$this->db->select('*');
    	if(strlen($this->_search)>0){
    		$this->db->like('Descripcion',$this->_search);
		}
    	$operadores=$this->db->get('esb.Operadores')->result_array();
    	foreach ($operadores as $operador){
    		$this->db->select('Agencias.Descripcion,Agencias.cuentaComision');
    		$this->db->where('Cod_Operador',$operador['Cod_Operador']);
    		$Agencias=$this->db->get('esb.Agencias')->result_array();
    		foreach ($Agencias as $agencia){
    			$totalDeAgencias++;
				$this->db->select('transactionLog.montoComision,transactionLog.montoComisionCNB');
				$this->db->where('cnbAgencia',$agencia['Descripcion']);
				if(strlen($this->fecha_inicio)>0){
					$this->db->where($this->_timestamps.' >',$this->fecha_inicio);
					$this->db->where($this->_timestamps.' <',$this->fecha_fin);
				}
				$comisiones=$this->db->get('esb.transactionLog')->result_array();
				$comisionCNB=0;
				$comisionbanco=0;
				foreach ($comisiones as $monto){
					$comisionbanco+=$monto['montoComision'];
					$comisionCNB+=$monto['montoComisionCNB'];
				}
				array_push($rows,array(

					'Operador'=>$operador['Descripcion'],
					'Agencia'=>$agencia['Descripcion'],
					'cuentaComision'=>$agencia['cuentaComision'],
					'comisionCNB'=>$comisionCNB,
					'comisionBanco'=>$comisionbanco
				)

				);
			}

		}

		$comisionesOperadores['total']=$totalDeAgencias;
		$comisionesOperadores['rows']=$rows;

		return json_encode($comisionesOperadores);
	}


	public function transaccionesPorUsusarios(){

    	if($this->input->post('inicio')){
			$this->fecha_inicio=$this->input->post('inicio').' 00:00:00';
			$this->fecha_fin=$this->input->post('fin').' 23:59:00';

		}

		if($this->input->post('search')){
			$this->_search=$this->input->post('search');
		}



		$this->SetProperties();


		if(count($this->_selection)>0){
			for($i=0; $i < count($this->_selection); $i++) {
				$this->db->select($this->_selection[$i]);
			}
		}else{
			$this->db->select('*');
		}

		if(count($this->_camposBusqueda)> 0 && $this->_search!=''){
			$i=0;
			$this->db->like($this->_camposBusqueda[$i],$this->_search);
			for($i;$i<count($this->_camposBusqueda);$i++){
				$this->db->or_like($this->_camposBusqueda[$i],$this->_search);
			}

		}
    	$users=$this->db->get('esb.usuariosMonedero');
    	$Usuarios=array();
    	$rows=array();

    	if($users->num_rows()>0){
    		foreach($users->result() as $value){

    			//Buscar el numero de transacciones en el rango de tiempo.
    			$this->db->select('id');

    			if(strlen($this->fecha_inicio)>0){
					$this->db->where('stamp > ', $this->fecha_inicio);
					$this->db->where('stamp < ', $this->fecha_fin);
				}
    			$this->db->where('cuenta',$value->cuentaMonedero);
				$noTransacciones=$this->db->get('esb.transactionMonederoLog')->num_rows();

				//Datos de la ultima transaccion;

				$this->db->select('transactionMonederoLog.stamp, transactionMonederoLog.descripcionTrx');
				$this->db->where('cuenta',$value->cuentaMonedero);
				$this->db->order_by('stamp','DESC');
				$this->db->limit(1);
				$transacciones=$this->db->get('esb.transactionMonederoLog');
				$lastTransaction='';
				$dateLastTrx='';
				if($transacciones->num_rows()>0){
					foreach($transacciones->result() as $trx){
						$lastTransaction=$trx->descripcionTrx;
						$dateLastTrx=$trx->stamp;
					}
				}

				array_push($rows,array(
					'id'=>$value->id,
					'name'=>$value->name,
					'lastname'=>$value->lastname,
					'cedula'=>$value->cedula,
					'telefono'=>$value->phone,
					'cuentaMonedero'=>$value->cuentaMonedero,
					'noTransacciones'=>$noTransacciones,
					'lastTransaction'=>$lastTransaction,
					'dateLastTrx'=>$dateLastTrx
				));
			}
		}

		$Usuarios['rows']=$rows;
    	$Usuarios['total']= $this->db->count_all_results();



		return json_encode($Usuarios);



	}


    private function SetProperties(){

		if($this->_Comisiones){
			$this->fecha_inicio=date('Y-m-d').' 00:00:00';
			$this->fecha_fin=date('Y-m-d').' 23:59:00';
		}

		if($this->VistaUserMonedero){
			if($this->input->post('inicio')){
				$fecha_de_inicio=$this->input->post('inicio');
				$fecha_de_fin=$this->input->post('fin');
			}else{
				$fecha_de_inicio=date('Y-m-d');
				$fecha_de_fin=date('Y-m-d');
			}
		}

		$data = json_decode(file_get_contents('php://input'), true);

		if(isset($data['limit'])){
			$this->_limit= $data['limit'];
		}

		if(isset($data['offset'])){
			$this->_offset= $data['offset'];
		}

		if(isset($data['search'])){
			$this->_search=utf8_decode($data['search']);
		}

		if(isset($data['sort'])){
			$this->_sort=$data['sort'];
		}

		if(isset($data['sortOrder'])){
			$this->_order=$data['sortOrder'];
		}

		if(isset($data['sort'])){
			$this->_order_by=$data['sort'];
		}

		if(isset($data['fecha_inicio'])){
			$this->fecha_inicio=$data['fecha_inicio'].' 00:00:00';
		}

		if(isset($data['fecha_fin'])){
			$this->fecha_fin=$data['fecha_fin'].' 23:59:00';
		}

		if(isset($data['inicio'])){
			$this->fecha_inicio=$data['inicio'].' 00:00:00';
		}

		if(isset($data['fin'])){
			$this->fecha_fin=$data['fin'].' 23:59:00';
		}



	}

	public function SetPropertiesUsersMonedero(){

		$data = json_decode(file_get_contents('php://input'), true);

		if(isset($data['limit'])){
			$this->_limit= $data['limit'];
		}

		if(isset($data['offset'])){
			$this->_offset= $data['offset'];
		}

		if(isset($data['search'])){
			$this->_search=utf8_decode($data['search']);
		}

		if(isset($data['sort'])){
			$this->_sort=$data['sort'];
		}

		if(isset($data['sortOrder'])){
			$this->_order=$data['sortOrder'];
		}

		if(isset($data['sort'])){
			$this->_order_by=$data['sort'];
		}

		if(isset($data['fecha_inicio'])){
			$this->fecha_inicio=$data['fecha_inicio'].' 00:00:00';
		}

		if(isset($data['fecha_fin'])){
			$this->fecha_fin=$data['fecha_fin'].' 23:59:00';
		}

		if(isset($data['inicio'])){
			$this->fecha_inicio=$data['inicio'].' 00:00:00';
		}

		if(isset($data['fin'])){
			$this->fecha_fin=$data['fin'].' 23:59:00';
		}
	}
}

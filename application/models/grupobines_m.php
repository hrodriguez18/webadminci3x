<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class grupobines_m extends MY_Model {
 	public $_table_name = 'esb.grupoBines';
	protected $_primary_key = 'Id';
	protected $_primary_filter = 'intval';
	protected $_order_by = 'Id';


	public $rules = array(
		'descripcion'=>array('field'=>'descripcion','label'=>'Descripcion','rules'=>'trim|required|xss_clean||max_length[20]'),
		'Estado'=>array('field'=>'status','label'=>'Estado','rules'=>'trim|required|xss_clean|callback_diferente_de_0')
	);

	public $status=array('0'=>'Favor Seleccione Un Estado','A'=>'Activo','I'=>'Inactivo');


	protected $_timestamps = FALSE;

	
	function __construct() {
		parent::__construct();
	}

	public function opt_operador(){

		$this->db->select('*');
		$query=$this->db->get('esb.Operadores');
		$opt=array();
		$opt[0]='Favor Seleccione Operador';
		if($query->num_rows()>0){
			foreach ($query->result() as $value) {
				$opt[$value->Cod_Operador]=$value->Descripcion;
			}	
		}
		
		return $opt;
	}

	public function opt_agencia(){

		$this->db->select('*');
		$query=$this->db->get('esb.Agencias');
		$opt=array();
		$opt[0]='Favor Seleccione Agencia';
		if($query->num_rows()>0){
			foreach ($query->result() as $value) {
				$opt[$value->Cod_Agencia]=$value->Descripcion;
			}	
		}
		
		return $opt;
	}

	public function opt_limites(){
		$this->db->select('*');
		$query=$this->db->get('esb.Limites');
		$opt=array();
		$opt[0]='Favor Seleccione Límite';
		if($query->num_rows()>0){
			foreach ($query->result() as $value) {
				$opt[$value->Id]=$value->descripcion;
			}	
		}
		
		return $opt;

	}


	public function Form(){

		

		$descripcion=array('name'=>'descripcion','type'=>'text','placeholder'=>'Descripcion','class'=>'form-control','id'=>'descripcion','value'=>set_value('descripcion'));
		
		$form='<div class="row">';
		
		$form.='<div class="col-lg-12"><div class="form-group">'.form_label('Descripción','descripcion').form_error('descripcion').form_input($descripcion).'</div></div>';

		$form.='<div class="col-lg-12"><div class="form-group">'.form_label('Estatus','estatus').form_error('status').form_dropdown('status',$this->status,set_value('status',0,true),'class="form-control"').'</div></div>';
		
		$form.='</div>';

		return $form;
	}

	public function  FormForEdit($data_by_id){

		$id=array('name'=>'id','type'=>'hidden','value'=>$data_by_id->Id);
		

		$descripcion=array('name'=>'descripcion','type'=>'text','placeholder'=>'Descripcion','class'=>'form-control','id'=>'descripcion','value'=>$data_by_id->descripcion);
		
		$form='<div class="row">';
		$form.=form_input($id);
		
		$form.='<div class="col-lg-12"><div class="form-group">'.form_label('Descripción','descripcion').form_error('descripcion').form_input($descripcion).'</div></div>';

		$form.='<div class="col-lg-12"><div class="form-group">'.form_label('Estatus','status').form_error('status').form_dropdown('status',$this->status,$data_by_id->status,'class="form-control"').'</div></div>';
		
		$form.='</div>';

		return $form;
	}



	public function infoTable(){
		echo "<pre>";
		var_dump($this->db->field_data('esb.Terminales'));
		echo "</pre>";
	}

}
?>

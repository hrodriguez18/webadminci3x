<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class faqs extends MY_SuperModel{

  protected $_tablename='esb.FAQ';
  protected $_primary_key='Id';
  protected $_primary_filter='intval';
  protected $_order_by='Id';

  public  $_rules=array('Titulo'=>array('field'=>'Titulo','label'=>'Titulo','rules'=>'trim|required|max_length[100]'),
    'Texto'=>array('field'=>'Texto','label'=>'Texto','rules'=>'trim|required')
);

  public $_inputs=array('Id'=>array('name'=>'Id','type'=>'hidden','value'=>''),
        'Titulo'=>array('name'=>'Titulo','type'=>'text','placeholder'=>'Titulo, Descripcion','value'=>'','label'=>'Titulo','class'=>'form-control'),
        'Texto'=>array('name'=>'Texto','type'=>'textarea','placeholder'=>'Respuesta de FAQs','value'=>'','label'=>'Respuesta','class'=>'form-control'),
        'Estado'=>array('name'=>'Estado','id'=>'Estado','type'=>'selector','table_rel'=>'esb.status','opt'=>'value','description'=>'description','class'=>'form-control','default'=>'A','label'=>'Estado')
      );
  protected $_timestamps=FALSE;
  protected $_form='';
  protected $_form_group_class='form-group ';
  protected $_attr_submit_button='';
  protected $_class_label='label label-warning';

  protected $_ruta='#';

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
  }

}

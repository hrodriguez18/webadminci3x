<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users_model extends CI_Model {

    public function __construct() 
	{
        parent::__construct();
		$this->load->helper('language');
		$this->lang->load('users');
		$this->lang->load('crud');
    }
	
	/*M�todos b�sicos*/
	public function select($limit = 0, $start = 0, $fields ='*', $where = NULL) 
	{
		if(!empty($where))
			$this->db->where($where);
		if($limit > 0)
			$this->db->limit($limit, $start);
		
		return $this->db
					->select($fields, FALSE)
					->get('adm.users');
	}
	public function insert($fields) 
	{
        $this->db->insert('users', $fields);
		return $this->db->insert_id();
	}
	public function update($where, $fields) 
	{   
        $this->db
			 ->where($where)
			 ->update('adm.users', $fields);
	}
	public function get($fields = '*', $where = NULL) 
	{
		$result = $this->select(1, 0, $fields, $where);
		if($result->num_rows() > 0)
			return $result->row();
		else
			return NULL;
	}
	public function total($where = NULL)
	{
		$total = $this->get("COUNT(*) as 'count'", $where);
		if(!empty($total))
			return array('COUNT'=>$total->count, 'SUM'=>0);
		else
			return array('COUNT'=>0, 'SUM'=>0);
	}
	
	/*M�todos adicionales*/
	public function get_valid_detail($id)
	{
		$user = $this->utilities->get_user_session();
		if(!empty($user))
		{
			$where['userid'] = $id;
			switch($user['type'])
			{
				//ROOT:
				case 1: $where['type >'] = 1;
					break;
				//Admin
				case 2: $where['type >'] = 2;
					break;
				//Support
				case 3: $where['type >'] = 3;
					break;
				//Operator
				case 4: $where['type >'] = 4;
					$where['operatorid'] = $user['operatorid'];
					break;
				//Agency
				case 5: $where['type >'] = 5;
					$where['agencyid'] = $user['agencyid'];
					break;
				//Terminal
				case 6: $where['userid'] = 0;
					break;
				default: $where['userid'] = 0;
					break;
			}
			return $this->get('*', $where);
		}
		else
			return NULL;
	}
	public function get_valid_list($limit = 0, $start = 0, $fields ='*', $where = NULL)
	{
		$user = $this->utilities->get_user_session();
		if(!empty($user))
		{
			switch($user['type'])
			{
				//ROOT:
				case 1: $where['type >'] = 1;
					break;
				//Admin
				case 2: $where['type >'] = 2;
					break;
				//Support
				case 3: $where['type >'] = 3;
					break;
				//Operator
				case 4: $where['type >'] = 4;
					$where['operatorid'] = $user['operatorid'];
					break;
				//Agency
				case 5: $where['type >'] = 5;
					$where['agencyid'] = $user['agencyid'];
					break;
				//Terminal
				case 6: $where['type'] = 6;
					$where['terminalid'] = $user['terminalid'];
					break;
				//Mobile
				case 6: $where['type'] = 7;
					$where['billerid'] = $user['billerid'];
					break;
				default: $where['userid'] = 0;
					break;
			}
			return $this->select($limit, $start, $fields, $where);
		}
		else
			return NULL;
	}
	public function get_valid_list_array($where = NULL)
	{
		$result = $this->get_valid_list(0, 0, 'userid, username', $where);
		
		$list[''] = lang('-ALL-');
		foreach($result->result() as $element)
		{
			$list[$element->userid] = $element->username;
		}
		return $list;
	}
	public function get_valid_total($where = NULL)
	{
		$user = $this->utilities->get_user_session();
		if(!empty($user))
		{
			switch($user['type'])
			{
				//ROOT:
				case 1: $where['type >'] = 1;
					break;
				//Admin
				case 2: $where['type >'] = 2;
					break;
				//Support
				case 3: $where['type >'] = 3;
					break;
				//Operator
				case 4: $where['type >'] = 4;
					$where['operatorid'] = $user['operatorid'];
					break;
				//Agency
				case 5: $where['type >'] = 5;
					$where['agencyid'] = $user['agencyid'];
					break;
				//Terminal
				case 6: $where['userid'] = 0;
					break;
				default: $where['userid'] = 0;
					break;
			}
			return $this->total($where);
		}
		else
			return NULL;
	}
	public function get_valid_types()
	{
		$user = $this->utilities->get_user_session();
		if(!empty($user))
		{
			switch($user['type'])
			{
				//ROOT:
				case 1: $types = array(''=>lang('-ALL-'), 2=>'ADMIN', 3=>'SUPPORT', 4=>'OPERATOR', 5=>'AGENCY', 6=>'TERMINAL', 7=>'MOBILE');
					break;
				//Admin
				case 2: $types = array(''=>lang('-ALL-'), 3=>'SUPPORT', 4=>'OPERATOR', 5=>'AGENCY', 6=>'TERMINAL', 7=>'MOBILE');
					break;
				//Support
				case 3: $types = array(''=>lang('-ALL-'), 4=>'OPERATOR', 5=>'AGENCY', 6=>'TERMINAL');
					break;
				//Operator
				case 4: $types = array(''=>lang('-ALL-'), 5=>'AGENCY', 6=>'TERMINAL');
					break;
				//Agency
				case 5: $types = array(''=>lang('-ALL-'), 6=>'TERMINAL');
					break;
				//Terminal
				case 6: $types = array(''=>lang('-ALL-'));
					break;
				//Mobile
				case 7: $types = array(''=>lang('-ALL-'));
					break;
				default: $types = array(''=>lang('-ALL-'));
					break;
			}
			return $types;
		}
		else
			return NULL;
	}
	public function get_valid_status()
	{
		return array(''=>lang('-ALL-'), 1=>lang('ENABLED'), 2=>lang('CHANGE_PASSWORD'), 3=>lang('BLOCKED'), 4=>lang('BLOCKED_ERROR'));
	}
}

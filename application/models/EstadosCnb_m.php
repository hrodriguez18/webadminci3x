<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class EstadosCnb_m extends MY_SuperModel{
  //datos para hacer la consulta la base de datos
  protected $_tablename='esb.usuarios';
	public $_primary_key='id';
	protected $_primary_filter='intval';
	protected $_order_by='id';

  protected $_form_group_class=' ';
  //datos para crear el FormularioPaisEdit

  //RELACION PARA GENERAR LA TABLA DE ESTA CLASE
	protected $_headingTable=array();
	//usado para buscar los datos de la db
	//usado para mostrar los datos en la tabla e incluir los btn actions

	//para generar los informes en PDF

	public $_headersTable=array('FECHA', 'USUARIO', 'TERMINAL','TIPO TRX','MONTO','FORMA PAGO','CUENTA','DESCRIPCION','TERMINAL','CURRENCY','LAT','LON');

	public $_selectToExport=array('.id',
                            '.name',
                            '.lastname',
                            '.phone',
                            '.cedula',
                            '.email',
                          'ConsultaSaldoCNB.saldo as SALDO');
	public $_orderName='';
	public $_order='';
	public $_join=array('table'=>'esb.ConsultaSaldoCNB','relation'=>'usuarios.cuentaMonedero=ConsultaSaldoCNB.agenciaCNBId','type'=>'left');
	public $_dateVariable='stamp';

  //datos para exportar a PDF
  public $_TableToExport='';
  public $_footer_ContentToExport='';
  public $_pdfTitle='BANCO NACIONAL DE PANAMÁ';
	public $_pdfDescription='REPORTE DE Estados de Cuenta';

  //usado para buscar los datos de la db
  public $_datatable=array('usuariosMonedero.id',
                            'usuariosMonedero.name',
                            'usuariosMonedero.lastname',
                            'usuariosMonedero.phone',
                            'usuariosMonedero.cedula',
                            'usuariosMonedero.email',
                          'usuariosMonedero.cuentaMonedero',
                          'ConsultaSaldoMonedero.saldo as SALDO'
                        );

  public $_joinForBootstrapTable=array('saldo'=>array('table_join'=>'esb.ConsultaSaldoMonedero','table_rel'=>'usuariosMonedero.cuentaMonedero=ConsultaSaldoMonedero.cuentaMonedero'));
  //usado para mostrar los datos en la tabla e incluir los btn actions
  public $_dataForTable=array('name','email','rolId','cedula','lastname');
  //hacer el algun join en la tabla

  public $_jointable=array('table'=>'esb.ConsultaSaldoMonedero','relation'=>'usuariosMonedero.cuentaMonedero=ConsultaSaldoMonedero.cuentaMonedero','type'=>'left');


  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
  }

  public function populate($limit=NULL,$offset=NULL,$search=NULL,$sort=NULL,$sortOrder=NULL){
			$arrayJson=array();
	    $arrayJson['total']=$this->db->count_all($this->_tablename);

			if(count($this->_datatable)>0){
				for ($i=0; $i <count($this->_datatable) ; $i++) {
					$this->db->select($this->_datatable[$i]);
				}
				if(count($this->_joinForBootstrapTable)>0){
					foreach ($this->_joinForBootstrapTable as $key) {
						$this->db->join($key['table_join'],$key['table_rel'],'left');
					}

				}

			}else{
				$this->db->select('*');
			}

			if($search != ''){
				for ($i=0; $i <count($this->_dataForTable) ; $i++){
					if($i==0){
						$this->db->like($this->_dataForTable[$i], $search, 'both');
					}else{
						$this->db->or_like($this->_dataForTable[$i], $search, 'both');
					}
				}
			}

	    if($limit && $offset){
	      $this->db->limit($offset,$limit);
	    }
	    if($sort){
	      $this->db->order_by($sort,$sortOrder);
	    }else{
	      $this->db->order_by($this->_primary_key,'DESC');
	    }
	    //$this->db->limit($limit);
	    //$this->db->group_by(array($this->_primary_key,'stamp'));
	    $query= $this->db->get($this->_tablename,$limit,$offset);

	    if($query->num_rows()>0){
        $i=0;
	      foreach ($query->result() as $value) {

	        $arrayJson['rows'][$i]['id']=$value->id;
	        $arrayJson['rows'][$i]['name']=$value->name;
	        $arrayJson['rows'][$i]['lastname']=$value->lastname;
	        $arrayJson['rows'][$i]['email']=$value->email;
	        $arrayJson['rows'][$i]['cedula']=$value->cedula;
	        $arrayJson['rows'][$i]['phone']=$value->phone;
          $arrayJson['rows'][$i]['saldo']=$value->SALDO;
	        $arrayJson['rows'][$i]['Descargar']='<a href="#" class="btn btn-danger btn-sm" onclick="DescargarSaldo(\''.$value->cuentaMonedero.'\')"><i class="fas fa-download"></i></a>';
          $i++;
	      }
	    }
	    return $arrayJson;
	}

  public function EstadoDeCuenta($cuenta){
    $this->db->select('ConsultaSaldoMonedero.saldo');
    $this->db->where('cuentaMonedero',$cuenta);
    return $this->db->get('esb.ConsultaSaldoMonedero')->result_array();

  }

  public function ExportToPdf($cuenta){

		if(count($this->_selectToExport)>0){
			for ($i=0; $i < count($this->_selectToExport); $i++) {
				$this->db->select($this->_selectToExport[$i]);
			}
		}else{
			$this->db->select('*');
		}
    if($cuenta!=''){
      $this->db->where('usuariosMonedero.cuentaMonedero',$cuenta);
    }

		if(count($this->_join) > 0){
			$this->db->join($this->_join['table'],$this->_join['relation'],$this->_join['type']);
		}
		$query=$this->db->get($this->_tablename)->result_array();
		$this->table->set_heading($this->_headersTable);
		$this->_TableToExport=$this->table->generate($query);
		$this->generar();

  }




}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class logUsers extends MY_SuperModel{

  protected $_tablename='adm.logUsers';
	protected $_primary_key='id';
	protected $_primary_filter='intval';
	protected $_order_by='id';

  //Datos para exportar a PDF
  public $_TableToExport='';
	public $_footer_ContentToExport='';
	public $_pdfTitle='LOGs DE Sistema';
	public $_pdfDescription='eventos ocurridos en el sistema por periodo definido';
	public $_headersTable=array('FECHA','EVENTO','CATEGORIA','ESTADO','NOMBRE','USUARIO');
	public $_selectToExport=array('stamp','action','category','status','name','username');
	public $_orderName='stamp';
	public $_order='DESC';
	public $_join=array();
	public $_dateVariable='stamp';

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
  }

  public function populate($limit=NULL,$offset=NULL,$search=NULL,$sort=NULL,$sortOrder=NULL){
    $arrayJson=array();
    $arrayJson['total']=$this->db->count_all($this->_tablename);
    $this->db->select('*');
    if($search){
      $this->db->like('action', $search,'both');
      $this->db->or_like('name', $search,'both');
      $this->db->or_like('username', $search,'both');
      $this->db->or_like('category', $search,'both');
      $this->db->or_like('status', $search,'both');
      $this->db->or_like('userid', $search,'both');
    }
    if($limit && $offset){
      $this->db->limit($offset,$limit);
    }
    if($sort){
      $this->db->order_by($sort,$sortOrder);
    }else{
      $this->db->order_by($this->_primary_key,'DESC');

    }
    $this->db->limit($limit);
    //$this->db->group_by(array($this->_primary_key,'stamp'));


    $query= $this->db->get($this->_tablename,$limit,$offset);

    if($query->num_rows()>0){
      foreach ($query->result() as $key => $value) {
        $arrayJson['rows'][$key]=$value;
      }
    }
    return $arrayJson;
  }


}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Email_m extends MY_SuperModel{

  //VARIABLES PARA HACER LAS CONSULTAS A LA BASE DE DATOS
  	public $_tablename='adm.emailNotification';
  	public $_primary_key='id';
  	public $_primary_filter='intval';
  	public $_order_by='id';

  //VARIABLES QUE SE NECESITAN PARA GENERAR EL FORMULARIO
  //type selector
  /*
  array('name'=>'imagen','id'=>'imagen',
  'type'=>'selector','table_rel'=>'esb.LogosEmpresas',
  'opt'=>'ruta','description'=>'nombre','class'=>'form-control',
  'default'=>'1','label'=>'Logo de Empresa')
  */
    public $documentationForForm='';
  	public  $_rules=array(
                  'email'=>array(
                                  'field'=>'email',
                                  'label'=>'email',
                                  'rules'=>'trim|required|max_length[80]|callback_isEmail|callback_uniqueEmail'
                                ),

                  'password'=>array(
                                  'field'=>'password',
                                  'label'=>'password',
                                  'rules'=>'trim|required|max_length[80]'
                                ),

                  'port'=>array(
                                  'field'=>'port',
                                  'label'=>'port',
                                  'rules'=>'trim|required|max_length[6]'
                                ),

                  'server'=>array(
                                  'field'=>'server',
                                  'label'=>'server',
                                  'rules'=>'trim|required|max_length[80]'
                                ),

                    'funcion'=>array(
                                    'field'=>'funcion',
                                    'label'=>'funcion',
                                    'rules'=>'callback_uniqueFunction'
                                  )


    );
  	public $_inputs=array(

                          'email'=>array(
                                          'name'=>'email',
                                          'id'=>'email',
                                          'placeholder'=>'email',
                                          'label'=>'email',
                                          'type'=>'email',
                                          'class'=>'form-control'
                                        ),

                          'password'=>array(
                                            'name'=>'password',
                                            'id'=>'password',
                                            'type'=>'password',
                                            'placeholder'=>'password',
                                            'label'=>'password',
                                            'class'=>'form-control'
                                          ),


                          'server'=>array(
                                            'name'=>'server',
                                            'id'=>'server',
                                            'type'=>'text',
                                            'placeholder'=>'server',
                                            'label'=>'server',
                                            'class'=>'form-control'
                                          ),

                          'port'=>array(
                                            'name'=>'port',
                                            'id'=>'port',
                                            'type'=>'number',
                                            'placeholder'=>'port',
                                            'label'=>'port',
                                            'class'=>'form-control'
                                          ),

                          'method'=>array(
                                            'name'=>'method',
                                            'id'=>'method',
                                            'type'=>'selector',
                                            'table_rel'=>'adm.methods',
                                            'opt'=>'value',
                                            'description'=>'value',
                                            'class'=>'form-control',
                                            'default'=>'smtp',
                                            'label'=>'method'
                                          ),

                          'funcion'=>array(
                                            'name'=>'funcion',
                                            'id'=>'funcion',
                                            'type'=>'selector',
                                            'table_rel'=>'adm.emailFunctions',
                                            'opt'=>'name',
                                            'description'=>'name',
                                            'class'=>'form-control',
                                            'default'=>'Reset Password',
                                            'label'=>'functions'
                                          ),

                        'status'=>array(
                                          'name'=>'status',
                                          'id'=>'status',
                                          'type'=>'selector',
                                          'table_rel'=>'esb.status',
                                          'opt'=>'value',
                                          'description'=>'description',
                                          'class'=>'form-control',
                                          'default'=>'A',
                                          'label'=>'status'
                                        ),

                          'id'=>array(
                                        'name'=>'id',
                                        'id'=>'id',
                                        'type'=>'hidden'
                                      )

                        );

  	protected $_timestamps=FALSE;
  	protected $_form='';
  	protected $_form_group_class='form-group ';
  	protected $_attr_submit_button='';
  	protected $_class_label='';
  //ESTA RUTA LE PERMITE A LA VISTA SABER HACIA DONDE BUSCAR EL ECHO DEL FORMULARIO
  	protected $_ruta='Email/';

  	//RELACION PARA GENERAR LA TABLA DE ESTA CLASE
  	protected $_headingTable=array();
  	//usado para buscar los datos de la db
  	protected $_datatable=array();
  	protected $_joinForBootstrapTable=array();
  	//usado para mostrar los datos en la tabla e incluir los btn actions
  	protected $_dataForTable=array();
  	protected $_jointable=array();
  	//para generar los informes en PDF
  	public $_TableToExport='';
  	public $_footer_ContentToExport='';
  	public $_pdfTitle='';
  	public $_pdfDescription='';
  	public $_headersTable=array();
  	public $_selectToExport=array();
  	public $_orderName='';
  	public $_order='';
  	public $_join=array();
  	public $_dateVariable='';

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
    $this->documentationForForm=$this->lang->line('documentation_email');
  }

  public function countEmail($str){
    $this->db->select('id');
    $this->db->where('email',$str);
    $query =  $this->db->get($this->_tablename);
    return $query->num_rows();
  }

  public function countFunction($str){
    $this->db->select('id');
    $this->db->where('name',$str);
    $query =  $this->db->get('adm.emailFunctions');
    return $query->num_rows();
  }

}

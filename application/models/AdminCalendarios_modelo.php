<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AdminCalendarios_modelo extends MY_SuperModel {
	protected $_tablename='esb.grupoDias';
	protected $_primary_key='id';
	protected $_primary_filter='intval';
	protected $_order_by='id';

	public  $_rules=array('description'=>array('field'=>'description','label'=>'Descripción','rules'=>'trim|required|max_length[20]'));

	protected $_form_group_class='form-group ';
	protected $_attr_submit_button='class="btn btn-success" ';

	protected $_class_label='class=" label label-danger"';

	protected $_ruta='Admin/';


	//RELACION PARA GENERAR LA TABLA DE ESTA CLASE
	protected $_headingTable=array('No.','NAME','LASTNAME','AGE','GENDER','ACTION');
	protected $_datatable=array('contacto.id','contacto.name','contacto.lastname','contacto.age','gender.description as GENDER');
	protected $_jointable=array('gender'=>array('table_join'=>'gender','table_rel'=>'contacto.gender=gender.id'));

	protected $_dataForTable=array('name','lastname','age','GENDER');


	public  $_inputs=array('description'=>array('type'=>'text','placeholder'=>'Descripcion de Grupo','name'=>'description','id'=>'description','class'=>'form-control','value'=>'','label'=>'Descripción')

	);


	protected $_timestamps=FALSE;
	protected $_form='';

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}




	

}

/* End of file adminModel.php */
/* Location: ./application/models/adminModel.php */
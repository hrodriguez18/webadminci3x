<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class limites_m extends MY_Model {

 	public $_table_name = 'esb.Limites';
	protected $_primary_key = 'Id';
	protected $_primary_filter = 'intval';
	protected $_order_by = 'Id';


	public $rules = array('descripcion'=>array('field'=>'descripcion','label'=>'Descripcion','rules'=>'trim|required'),
		'status'=>array('field'=>'status','label'=>'Estado','rules'=>'trim|required|callback_diferente_de_0'),
		'limiteTx'=>array('field'=>'limiteTx','label'=>'Limite TX','rules'=>'trim|required'),
		'limiteDia'=>array('field'=>'limiteDia','label'=>'Límite Día','rules'=>'trim|required'),
		'limiteMes'=>array('field'=>'limiteMes','label'=>'Límite Mes','rules'=>'trim|required'),
		'disponibleMes'=>array('field'=>'disponibleMes','label'=>'Disponible Mes','rules'=>'trim|required'),
		'disponibleDia'=>array('field'=>'disponibleDia','label'=>'Disponible Día','rules'=>'trim|required')
	);

	public $status=array('0'=>'Favor Seleccione Un Estado','A'=>'Activo','I'=>'Inactivo');


	protected $_timestamps = FALSE;


	function __construct() {
		parent::__construct();
	}

	public function opt_operador(){

		$this->db->select('*');
		$query=$this->db->get('esb.Operadores');
		$opt=array();
		$opt[0]='Favor Seleccione Operador';
		if($query->num_rows()>0){
			foreach ($query->result() as $value) {
				$opt[$value->Cod_Operador]=$value->Descripcion;
			}
		}

		return $opt;
	}








	public function Form(){



		$div='<div class="row"';




		$Descripcion=array('name'=>'descripcion','type'=>'text','placeholder'=>'Descripcion','class'=>'form-control','id'=>'descripcion','value'=>set_value('descripcion','',true));

		$limiteTx=array('name'=>'limiteTx','type'=>'number','placeholder'=>'Límite TX','class'=>'form-control','id'=>'limiteTx','value'=>set_value('limiteTx','',true), 'step'=>'.00');

		$limiteDia=array('name'=>'limiteDia','type'=>'number','placeholder'=>'Límite Día','class'=>'form-control','id'=>'limiteDia','value'=>set_value('limiteDia','',true), 'step'=>'.00' );
		$limiteMes=array('name'=>'limiteMes','type'=>'number','placeholder'=>'Límites Mes','class'=>'form-control','id'=>'limiteMes','value'=>set_value('limiteMes','',true), 'step'=>'.00' );

		$disponibleDia=array('name'=>'disponibleDia','type'=>'number','placeholder'=>'Disponible Por Día','class'=>'form-control','id'=>'disponibleDia','value'=>set_value('disponibleDia','',true), 'step'=>'.00' );

		$disponibleMes=array('name'=>'disponibleMes','type'=>'number','placeholder'=>'Disponible Por Mes','class'=>'form-control','id'=>'disponibleMes','value'=>set_value('disponibleMes','',true) , 'step'=>'.00');



		$form='';


		$form.='<div class="row"><div class="col-lg-4"><div class="form-group">'.form_label($Descripcion['placeholder'],$Descripcion['id']).form_error($Descripcion['id']).form_input($Descripcion).'</div></div>';

		$form.='<div class="col-lg-4"><div class="form-group">'.form_label($limiteTx['placeholder'],$limiteTx['id']).form_error($limiteTx['id']).form_input($limiteTx).'</div></div>';

		$form.='<div class="col-lg-4"><div class="form-group">'.form_label($limiteDia['placeholder'],$limiteDia['id']).form_error($limiteDia['id']).form_input($limiteDia).'</div></div></div>';

		$form.='<div class="row"><div class="col-lg-4"><div class="form-group">'.form_label($limiteMes['placeholder'],$limiteMes['id']).form_error($limiteMes['id']).form_input($limiteMes).'</div></div>';

		/*$form.='<div class="col-lg-4"><div class="form-group">'.form_label($disponibleDia['placeholder'],$disponibleDia['id']).form_error($disponibleDia['id']).form_input($disponibleDia).'</div></div>';
		$form.='<div class="col-lg-4"><div class="form-group">'.form_label($disponibleMes['placeholder'],$disponibleMes['id']).form_error($disponibleMes['id']).form_input($disponibleMes).'</div></div></div>';
*/
		$form.='<div class="row"><div class="col-lg-4"><div class="form-group">'.form_label('Estado','status').form_error('status').form_dropdown('status',$this->status,set_value('status',0,true),'class="form-control"').'</div></div></div>';


		return $form;
	}

	public function  FormForEdit($data_by_id){
		$div='<div class="row"';

		$id=array('name'=>'id','type'=>'hidden','value'=>$data_by_id->Id);


		$Descripcion=array('name'=>'descripcion','type'=>'text','placeholder'=>'Descripcion','class'=>'form-control','id'=>'Descripcion','value'=>$data_by_id->descripcion);

		$limiteTx=array('name'=>'limiteTx','type'=>'text','placeholder'=>'Límite TX','class'=>'form-control','id'=>'limiteTx','value'=>$data_by_id->limiteTx);

		$limiteDia=array('name'=>'limiteDia','type'=>'text','placeholder'=>'Límite Día','class'=>'form-control','id'=>'limiteDia','value'=>$data_by_id->limiteDia );
		$limiteMes=array('name'=>'limiteMes','type'=>'text','placeholder'=>'Límites Mes','class'=>'form-control','id'=>'limiteMes','value'=>$data_by_id->limiteMes );
/*
		$disponibleDia=array('name'=>'disponibleDia','type'=>'text','placeholder'=>'Disponible Por Día','class'=>'form-control','id'=>'disponibleDia','value'=>$data_by_id->disponibleDia );

		$disponibleMes=array('name'=>'disponibleMes','type'=>'text','placeholder'=>'Disponible Por Mes','class'=>'form-control','id'=>'sisponibleMes','value'=>$data_by_id->disponibleMes);

*/
		$form=form_input($id);




		$form.='<div class="row"><div class="col-lg-4"><div class="form-group">'.form_label($Descripcion['placeholder'],$Descripcion['id']).form_error($Descripcion['id']).form_input($Descripcion).'</div></div>';

		$form.='<div class="col-lg-4"><div class="form-group">'.form_label($limiteTx['placeholder'],$limiteTx['id']).form_error($limiteTx['id']).form_input($limiteTx).'</div></div>';

		$form.='<div class="col-lg-4"><div class="form-group">'.form_label($limiteDia['placeholder'],$limiteDia['id']).form_error($limiteDia['id']).form_input($limiteDia).'</div></div></div>';



		$form.='<div class="row"><div class="col-lg-4"><div class="form-group">'.form_label($limiteMes['placeholder'],$limiteMes['id']).form_error($limiteMes['id']).form_input($limiteMes).'</div></div>';
/*
		$form.='<div class="col-lg-4"><div class="form-group">'.form_label($disponibleDia['placeholder'],$disponibleDia['id']).form_error($disponibleDia['id']).form_input($disponibleDia).'</div></div>';




		$form.='<div class="col-lg-4"><div class="form-group">'.form_label($disponibleMes['placeholder'],$disponibleMes['id']).form_error($disponibleMes['id']).form_input($disponibleMes).'</div></div></div>';
*/
		$form.='<div class="row"><div class="col-lg-4"><div class="form-group">'.form_label('Estado','status').form_error('estado').form_dropdown('status',$this->status,$data_by_id->status,'class="form-control"').'</div></div></div>';



		return $form;
	}

	public function getForTable(){

		$this->db->select('Agencias.Cod_Agencia');
		$this->db->select('Agencias.Descripcion As "D"');
		$this->db->select('Agencias.Estado');
		$this->db->select('Agencias.Direccion');
		$this->db->select('Agencias.actividadComercial');
		$this->db->select('Operadores.Descripcion As "O"');
		//$this->db->join('esb.Pais', 'agencias.Pais=Pais.Cod_Pais','left');
		$this->db->join('esb.Operadores','Agencias.Cod_Operador=Operadores.Cod_Operador','left');


		return $this->db->get($this->_table_name)->result();

	}

}
?>

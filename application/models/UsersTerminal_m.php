<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class UsersTerminal_m extends MY_SuperModel {
	public $_tablename='esb.usuarios';
	public $_primary_key='id';
	protected $_primary_filter='intval';
	protected $_order_by='id';


	//VARIABLES QUE SE NECESITAN PARA GENERAR EL FORMULARIO
	public  $_rules=array(

		'name'=>array(

			'field'=>'name',
			'label'=>'Nombre',
			'rules'=>'trim|required'
		),
		'lastname'=>array(

			'field'=>'lastname',
			'label'=>'Apellido',
			'rules'=>'trim|required'

		),
		'cedula'=>array(
			'field'=>'cedula',
			'label'=>'cedula',
			'rules'=>'trim|required'
		),


	);
	public $_inputs=array(


		'name'=>array(

			'name'=>'name',
			'id'=>'name',
			'type'=>'text',
			'class'=>'form-control',
			'label'=>'name',
			'placeholder'=>'Nombre'

		),

		'lastname'=>array(

			'name'=>'lastname',
			'id'=>'lastname',
			'type'=>'text',
			'class'=>'form-control',
			'label'=>'lastname',
			'placeholder'=>'Apellido'

		),

		'cedula'=>array(

			'name'=>'cedula',
			'id'=>'cedula',
			'type'=>'text',
			'class'=>'form-control',
			'label'=>'cedula',
			'value'=>'',
			'placeholder'=>'Cédula'

		),

		'fnacimiento'=>array(

			'name'=>'fnacimiento',
			'id'=>'fnacimiento',
			'type'=>'date',
			'class'=>'form-control',
			'label'=>'Birthdate',
			'value'=>'',
		),

		'sexo'=>array(

			'name'=>'sexo',
			'id'=>'sexo',
			'type'=>'selector',
			'table_rel'=>'esb.sexo',
			'opt'=>'name',
			'description'=>'name',
			'class'=>'form-control',
			'default'=>'',
			'label'=>'sexo'

		),



		'rolId'=>array(

			'name'=>'rolId',
			'id'=>'rolId',
			'type'=>'selector',
			'table_rel'=>'esb.rolesTerminal',
			'opt'=>'id',
			'description'=>'descripcion',
			'class'=>'form-control',
			'default'=>'',
			'label'=>'rol_id'

		),
		'email'=>array(

			'name'=>'email',
			'id'=>'email',
			'type'=>'number',
			'class'=>'form-control',
			'label'=>'phone'

		),


		'id'=>array(

			'name'=>'id',
			'id'=>'id',
			'type'=>'hidden',
			'value'=>''
		)

	);
	public $documentationForForm='';
	protected $_timestamps=FALSE;
	protected $_form='';
	protected $_form_group_class='';
	protected $_attr_submit_button='';
	protected $_class_label='';
	//ESTA RUTA LE PERMITE A LA VISTA SABER HACIA DONDE BUSCAR EL ECHO DEL FORMULARIO
	protected $_ruta='';

	//RELACION PARA GENERAR LA TABLA DE ESTA CLASE
	protected $_headingTable=array();
	//usado para buscar los datos de la db


	protected $_datatable=array(

		'usuarios.id',
		'usuarios.email',
		'usuarios.name',
		'usuarios.lastname',
		'usuarios.status',
		'usuarios.fnacimiento',
		'usuarios.phone1',
		'rolesTerminal.descripcion as rol'
	);


	protected $_joinForBootstrapTable=array(

		0=>array(
			'table_join'=>'esb.rolesTerminal',
			'table_rel'=>'usuarios.rolId=rolesTerminal.id'
		),

	);
	//usado para mostrar los datos en la tabla e incluir los btn actions
	protected $_dataForTable=array(
		'usuarios.id',
		'usuarios.email',
		'usuarios.name',
		'usuarios.lastname',
		'usuarios.email',
		'usuarios.fnacimiento',
		'usuarios.phone1',
		'rolesTerminal.descripcion as rol'

	);




	protected $_jointable=array();
	//para generar los informes en PDF
	public $_TableToExport='';
	public $_footer_ContentToExport='';
	public $_pdfTitle='';
	public $_pdfDescription='';
	public $_headersTable=array();
	public $_selectToExport=array();
	public $_orderName='';
	public $_order='';
	public $_join=array();
	public $_dateVariable='';


	public function __construct()
	{
		parent::__construct();
		//Codeigniter : Write Less Do More
	}


}
?>

<?php
/**
 * Created by PhpStorm.
 * User: hrodriguez
 * Date: 10/01/18
 * Time: 02:55 AM
 */


defined('BASEPATH') OR exit('No direct script access allowed');

class ActividadesComerciales_m extends MY_SuperModel
{

	//VARIABLES PARA HACER LAS CONSULTAS A LA BASE DE DATOS
	public $_tablename='esb.actividadComercial';
	public $_primary_key='id';
	public $_primary_filter='intval';
	public $_order_by='description';

	//VARIABLES QUE SE NECESITAN PARA GENERAR EL FORMULARIO
	//type selector
	/*
    array('name'=>'imagen','id'=>'imagen',
    'type'=>'selector','table_rel'=>'esb.LogosEmpresas',
    'opt'=>'ruta','description'=>'nombre','class'=>'form-control',
    'default'=>'1','label'=>'Logo de Empresa')
    */
	public $documentationForForm='';


	public  $_rules=array(
		'description'=>array(
			'field'=>'description',
			'label'=>'description',
			'rules'=>'trim|required|max_length[150]'
		)

	);
	public $_inputs=array(

		'description'=>array(
			'name'=>'description',
			'id'=>'description',
			'placeholder'=>'description',
			'label'=>'description',
			'type'=>'text',
			'class'=>'form-control'
		),
		'id'=>array(
			'name'=>'id',
			'id'=>'id',
			'type'=>'hidden'
		)

	);

	protected $_timestamps=FALSE;
	protected $_form='';
	protected $_form_group_class='form-group ';
	protected $_attr_submit_button='';
	protected $_class_label='';

	public function __construct()
	{
		parent::__construct();

		//Do your magic here
	}


}

/* End of file .php */

?>

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class GrupoTransaccionesModel extends MY_SuperModel {

	protected $_tablename='esb.grupoTransacciones';
	protected $_primary_key='id';
	protected $_primary_filter='intval';
	protected $_order_by='id';

	public  $_rules=array('description'=>array('field'=>'description','label'=>'Descripción','rules'=>'trim|required|max_length[100]')
	);

	public $_inputs=array('id'=>array('name'=>'id','type'=>'hidden','value'=>''),
				'description'=>array('name'=>'description','type'=>'text','placeholder'=>'Descripción','value'=>'','label'=>'Descripción','class'=>'form-control')
			);
	protected $_timestamps=FALSE;
	protected $_form='';
	protected $_form_group_class='form-group ';
	protected $_attr_submit_button='';
	protected $_class_label='label label-warning';

	protected $_ruta='AdminGrupoTransacciones/';

	//RELACION PARA GENERAR LA TABLA DE ESTA CLASE
	protected $_headingTable=array('ID','Descripcion','Logo','Tipo');
	//usado para buscar los datos de la db
	protected $_datatable=array('id','description','imagen','tipo');
	//usado para mostrar los datos en la tabla e incluir los btn actions
	protected $_dataForTable=array('description','imagen','tipo');
	protected $_jointable=array();

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}

	public function PopulateTable(){
		$this->db->select('*');
		return $this->db->get($this->_tablename)->result();
	}

	public function OptTransacciones($trx){
		$this->db->select('*');
		$query=$this->db->get('esb.transacciones');
		$opt=array();
		foreach ($query->result() as $value) {
			if(!in_array($value->id, $trx)){
				$opt[$value->id]=$value->description;
			}	
		}
		return $opt;
	}
	public function OptMontos(){
		$this->db->select('*');
		$query=$this->db->get('esb.montos');
		$opt=array();
		foreach ($query->result() as $value) {	
			$opt[$value->id]=$value->description;
		}
		return $opt;
	}

	public function SaveRelGrupoTrx($rel){
		$this->db->insert('esb.relGrupoMontoTrx',$rel);
		return true;
	}

	public function OptLimites(){
		$this->db->select('*');
		$query=$this->db->get('esb.transactionLimits');
		$opt=array();
		foreach ($query->result() as $value) {
			$opt[$value->id]=$value->description;
		}
		return $opt;
	}

	public function ListaGrupoTrx($id){
		$this->db->select('*');
		$this->db->where('idGrupoTrx',$id);
		$query=$this->db->get('esb.relGrupoMontoTrx');
		$selectores=array();
		$optionT=$this->OptTransacciones($selectores);
		$optMontos=$this->OptMontos();
		$optLimites=$this->OptLimites();
		$form='<div class="form-group" id="contentTrans">';
		foreach ($query->result() as $value) {

			$form.='<div class="row">';
			$form.='<div class="col-lg-4"><div class="form-group">'.form_label('Transacción','trx').form_dropdown('trx[]', $optionT, $value->idTrx,'class="form-control"').'</div></div>';
			$form.='<div class="col-lg-4"><div class="form-group">'.form_label('Monto','montos').form_dropdown('montos[]', $optMontos, $value->idMontoTrx,'class="form-control"').'</div></div>';
			$form.='<div class="col-lg-4"><div class="form-group">'.form_label('Límites','limites').form_dropdown('limites[]', $optLimites, $value->idLimit,'class="form-control"').'</div></div>';
			
			$form.='</div>';
			
		}
		$form.='</div>';
		if(count($optionT)>0){
			return $form;
		}	
	}

	public function CleanRelationGrupoTrxMontos($id){
		$this->db->where('idGrupoTrx',$id);
		$this->db->delete('esb.relGrupoMontoTrx');
		return true;
	}


	
	

}

/* End of file TransaccionesModel.php */
/* Location: ./application/models/TransaccionesModel.php */

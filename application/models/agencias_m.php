<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class agencias_m extends MY_Model {
 	public $_table_name = 'esb.Agencias';
	protected $_primary_key = 'Cod_Agencia';
	protected $_primary_filter = 'intval';
	protected $_order_by = 'Cod_Agencia';


	public $rules = array('Descripcion'=>array('field'=>'Descripcion','label'=>'Descripcion','rules'=>'trim|required'),
		'estado'=>array('field'=>'estado','label'=>'estado','rules'=>'trim|required|callback_diferente_de_0'),
		'operador'=>array('field'=>'operador','label'=>'operador','rules'=>'trim|required|callback_diferente_de_0'),
	);

	public $status=array('0'=>'Favor Seleccione Un Estado','A'=>'Activo','I'=>'Inactivo');


	protected $_timestamps = FALSE;


	function __construct() {
		parent::__construct();
	}

	public function opt_operador(){

		$this->db->select('*');
		$query=$this->db->get('esb.Operadores');
		$opt=array();
		$opt[0]='Favor Seleccione Operador';
		if($query->num_rows()>0){
			foreach ($query->result() as $value) {
				$opt[$value->Cod_Operador]=$value->Descripcion;
			}
		}

		return $opt;
	}








	public function Form(){

		$form="";

		$operador=$this->opt_operador();


		$Descripcion=array('name'=>'Descripcion','type'=>'text','placeholder'=>'Descripcion','class'=>'form-control','id'=>'Descripcion','value'=>'','min'=>0);

		$direccion=array('name'=>'direccion','placeholder'=>'Direccion','class'=>'form-control','id'=>'direccion','value'=>'','rows'=>2);
		$direccion['value']=set_value('direccion','',true);

		$Descripcion['value']=set_value('descripcion','',true);

		$actividad=$this->OptActividadComercial();


		$form.='<div class="form-group">'.form_error('operador').form_dropdown('operador',$operador,set_value('operador',0,true),'class="form-control"').'</div>';

		$form.='<div class="form-group">'.form_error('Descripcion').form_input($Descripcion).'</div>';

		$form.='<div class="form-group">'.form_error('estado').form_dropdown('estado',$this->status,'0','class="form-control"').'</div>';

		$form.='<div class="form-group">'.form_error('direccion').form_textarea($direccion).'</div>';

		$form.='<div class="form-group">'.form_error('actividad_comercial').form_dropdown('actividad_comercial',$actividad,set_value('actividad_comercial','1',TRUE),'class="form-control"').'</div>';

		return $form;
	}

  public function OptActividadComercial(){
    $this->db->select('*');
    $query=$this->db->get('esb.actividadComercial');
    $opt=array();
    foreach ($query->result() as $value) {
      $opt[$value->id]=$value->description;
    }
    return $opt;
  }

	public function  FormForEdit($data_by_id){
		$form="";

		$cod_agencia=array('name'=>'Cod_Agencia','type'=>'hidden','value'=>$data_by_id->Cod_Agencia);

		$operador=$this->opt_operador();


		$Descripcion=array('name'=>'Descripcion','type'=>'text','placeholder'=>'Descripcion','class'=>'form-control','id'=>'Descripcion','value'=>'','min'=>0);

		$direccion=array('name'=>'direccion','placeholder'=>'Direccion','class'=>'form-control','id'=>'direccion','value'=>'','rows'=>2);
		$actividad=$this->OptActividadComercial();
		$direccion['value']=$data_by_id->Direccion;

		$Descripcion['value']=$data_by_id->Descripcion;




		$form.=form_input($cod_agencia);
		$form.='<div class="form-group">'.form_error('operador').form_dropdown('operador',$operador,set_value('operador',$data_by_id->Cod_Operador,true),'class="form-control"').'</div>';

		$form.='<div class="form-group">'.form_error('Descripcion').form_input($Descripcion).'</div>';

		$form.='<div class="form-group">'.form_error('estado').form_dropdown('estado',$this->status,$data_by_id->Estado,'class="form-control"').'</div>';

		$form.='<div class="form-group">'.form_error('direccion').form_textarea($direccion).'</div>';

		$form.='<div class="form-group">'.form_error('actividad_comercial').form_dropdown('actividad_comercial',$actividad,set_value('actividad_comercial',$data_by_id->actividadComercialId,TRUE)).'</div>';

		return $form;
	}

	public function getForTable(){

		$this->db->select('Agencias.Cod_Agencia');
		$this->db->select('Agencias.Descripcion As "D"');
		$this->db->select('Agencias.Estado');
		$this->db->select('Agencias.Direccion');
		$this->db->select('Agencias.actividadComercialId');
		$this->db->select('Operadores.Descripcion As "O"');
		//$this->db->join('esb.Pais', 'agencias.Pais=Pais.Cod_Pais','left');
		$this->db->join('esb.Operadores','Agencias.Cod_Operador=Operadores.Cod_Operador','left');


		return $this->db->get($this->_table_name)->result();

	}

}
?>

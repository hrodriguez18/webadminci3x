<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ecommerceModel extends MY_SuperModel{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
    $this->load->library('calendar');
  }

  public function PopulateTableDiario(){
    $this->db->select('e_transactionLog.id,
                      e_transactionLog.acquirerId,
                      e_transactionLog.idCommerce,
                      e_transactionLog.purchaseOperationNumber,
                      e_transactionLog.purchaseAmount,
                      e_transactionLog.purchaseCurrencyCode,
                      e_transactionLog.shippingFirstName,
                      e_transactionLog.shippingLastName,
                      e_transactionLog.shippingEmail,
                      e_transactionLog.shippingAddress,
                      e_transactionLog.shippingZIP,
                      e_transactionLog.shippingCity,
                      e_transactionLog.shippingState,
                      e_transactionLog.shippingCountry,
                      e_transactionLog.descriptionProducts,
                      e_transactionLog.reserved5,
                      e_transactionLog.authorizationResult,
                      e_transactionLog.authorizationCode,
                      e_transactionLog.bin,
                      e_transactionLog.brand,
                      e_transactionLog.purchaseOperationNumber,
                      e_transactionLog.operador,
                      e_transactionLog.currency,
                      e_transactionLog.realAmount,
                      e_transactionLog.timestamp');
    $this->db->where('timestamp >',date('Y-m-d 00:00:00'));
    $this->db->where('timestamp <',date('Y-m-d 23:59:00'));
    $this->db->order_by('timestamp','DESC');
    $this->db->limit(1000);
    return $this->db->get('ecom.e_transactionLog')->result();
  }



  public function PopulateTableSemanal(){
    $arreglo=getdate(strtotime("last sunday"));
		$fechaInicial=$arreglo['year'].'-'.$arreglo['mon'].'-'.$arreglo['mday'].' 00:00:00';
		$fechaFinal='';
		$totalDay=$this->calendar->get_total_days($arreglo['mon'],$arreglo['year']);
		for ($i=0; $i <7 ; $i++){
			$fechaFinal = $arreglo['year'].'-'.$arreglo['mon'].'-'.$arreglo['mday'].' 23:59:00';
			if($arreglo['mday']==$totalDay){
				$arreglo['mday']=1;
				$arreglo['mon']++;
			}else{
				$arreglo['mday']++;
			}
		}
    $fechaFinal=strval($fechaFinal);
    $fechaInicial=strval($fechaInicial);
    $this->db->select('e_transactionLog.id,
                      e_transactionLog.acquirerId,
                      e_transactionLog.idCommerce,
                      e_transactionLog.purchaseOperationNumber,
                      e_transactionLog.purchaseAmount,
                      e_transactionLog.purchaseCurrencyCode,
                      e_transactionLog.shippingFirstName,
                      e_transactionLog.shippingLastName,
                      e_transactionLog.shippingEmail,
                      e_transactionLog.shippingAddress,
                      e_transactionLog.shippingZIP,
                      e_transactionLog.shippingCity,
                      e_transactionLog.shippingState,
                      e_transactionLog.shippingCountry,
                      e_transactionLog.descriptionProducts,
                      e_transactionLog.reserved5,
                      e_transactionLog.authorizationResult,
                      e_transactionLog.authorizationCode,
                      e_transactionLog.bin,
                      e_transactionLog.brand,
                      e_transactionLog.purchaseOperationNumber,
                      e_transactionLog.operador,
                      e_transactionLog.currency,
                      e_transactionLog.realAmount,
                      e_transactionLog.timestamp');
    $this->db->where('timestamp >',$fechaInicial);
    $this->db->where('timestamp <',$fechaFinal);
    $this->db->order_by('timestamp','DESC');
    $this->db->limit(1000);
    return $this->db->get('ecom.e_transactionLog')->result();
  }


  public function PopulateTableMensual(){

		$fechaInicial=date('Y-m').'-01 00:00:00';
		$totalDay=$this->calendar->get_total_days(date('m'),date('Y'));
    $fechaFinal=date('Y-m').'-'.$totalDay.' 23:59:00';
    $this->db->select('e_transactionLog.id,
                      e_transactionLog.acquirerId,
                      e_transactionLog.idCommerce,
                      e_transactionLog.purchaseOperationNumber,
                      e_transactionLog.purchaseAmount,
                      e_transactionLog.purchaseCurrencyCode,
                      e_transactionLog.shippingFirstName,
                      e_transactionLog.shippingLastName,
                      e_transactionLog.shippingEmail,
                      e_transactionLog.shippingAddress,
                      e_transactionLog.shippingZIP,
                      e_transactionLog.shippingCity,
                      e_transactionLog.shippingState,
                      e_transactionLog.shippingCountry,
                      e_transactionLog.descriptionProducts,
                      e_transactionLog.reserved5,
                      e_transactionLog.authorizationResult,
                      e_transactionLog.authorizationCode,
                      e_transactionLog.bin,
                      e_transactionLog.brand,
                      e_transactionLog.purchaseOperationNumber,
                      e_transactionLog.operador,
                      e_transactionLog.currency,
                      e_transactionLog.realAmount,
                      e_transactionLog.timestamp');
    $this->db->where('timestamp >',$fechaInicial);
    $this->db->where('timestamp <',$fechaFinal);
    $this->db->order_by('timestamp','DESC');
    $this->db->limit(1000);
    return $this->db->get('ecom.e_transactionLog')->result();

  }
  public function PopulateTableAnual(){

    $fechaInicial=date('Y').'-01-01 00:00:00';

    $fechaFinal=date('Y').'-12-31 23:59:00';
    $this->db->select('e_transactionLog.id,
                      e_transactionLog.acquirerId,
                      e_transactionLog.idCommerce,
                      e_transactionLog.purchaseOperationNumber,
                      e_transactionLog.purchaseAmount,
                      e_transactionLog.purchaseCurrencyCode,
                      e_transactionLog.shippingFirstName,
                      e_transactionLog.shippingLastName,
                      e_transactionLog.shippingEmail,
                      e_transactionLog.shippingAddress,
                      e_transactionLog.shippingZIP,
                      e_transactionLog.shippingCity,
                      e_transactionLog.shippingState,
                      e_transactionLog.shippingCountry,
                      e_transactionLog.descriptionProducts,
                      e_transactionLog.reserved5,
                      e_transactionLog.authorizationResult,
                      e_transactionLog.authorizationCode,
                      e_transactionLog.bin,
                      e_transactionLog.brand,
                      e_transactionLog.purchaseOperationNumber,
                      e_transactionLog.operador,
                      e_transactionLog.currency,
                      e_transactionLog.realAmount,
                      e_transactionLog.timestamp');
    $this->db->where('timestamp >',$fechaInicial);
    $this->db->where('timestamp <',$fechaFinal);
    $this->db->order_by('timestamp','DESC');
    $this->db->limit(1000);
    return $this->db->get('ecom.e_transactionLog')->result();
  }
  public function PopulateTablePersonalizado(){}


  public function VentasDiarias(){
    $start_date=date('Y-m-d').' 00:00:00';
    $end_date=date('Y-m-d').' 23:59:00';

    $this->db->select_sum("purchaseAmount",'Suma');
		$this->db->where('timestamp >', $start_date);
		$this->db->where('timestamp <', $end_date);
    $query=$this->db->get('ecom.e_transactionLog');
    $suma=0;
    foreach ($query->result() as  $value) {
      $suma=$value->Suma;
    }
    return $suma;
  }

  public function PromedioVentasDiarias(){
    $a=$this->VentasDiarias();
    $b=$this->CuentaDeTransaDiarias();
    if($b==0){
      return number_format(0, 2, '.', '');
    }else{
      return number_format(($a/$b), 2, '.', '');
    }
  }

  public function CuentaDeTransaDiarias(){
    $start_date=date('Y-m-d').' 00:00:00';
    $end_date=date('Y-m-d').' 23:59:00';
    $this->db->select('id');
    $this->db->where('timestamp >', $start_date);
		$this->db->where('timestamp <', $end_date);
    $query=$this->db->get('ecom.e_transactionLog');
    return $query->num_rows();
  }

  public function topMarca($limit){

    $fechaInicial=date('Y-m').'-01 00:00:00';
		$totalDay=$this->calendar->get_total_days(date('m'),date('Y'));
    $fechaFinal=date('Y-m').'-'.$totalDay.' 23:59:00';
    $this->db->select_sum("purchaseAmount",'Suma');
    $this->db->select('brand');
    $this->db->order_by('Suma','DESC');
    $this->db->group_by('brand');
    $this->db->limit($limit);
    $this->db->where('timestamp >=',$fechaInicial);
    $this->db->where('timestamp <=',$fechaFinal);
    return $this->db->get('ecom.e_transactionLog')->row();
  }

  public function TopOneMarca(){
    $obj=$this->topMarca(1);
    if($obj){
      return $obj->brand;
    }
  }

  public function TotalVentas(){
    $fechaInicial=date('Y').'-01-01 00:00:00';

    $fechaFinal=date('Y').'-12-31 23:59:00';

    $this->db->select_sum("purchaseAmount",'Suma');
		$this->db->where('timestamp >', $fechaInicial);
		$this->db->where('timestamp <', $fechaFinal);
    $query=$this->db->get('ecom.e_transactionLog');
    $suma=0;
    foreach ($query->result() as  $value) {
      $suma=$value->Suma;
    }
    return $suma;
  }

  public function VentaMensual(){}
  public function VentaAnual(){}

    public function GraphicalView(){
      $repuesta=array();
      $year=strval(date('Y'));
  		for($i=1;$i<=12;$i++){
  			$mesActual=$year.'-'.$i;
  			$fechaActual=$mesActual.'-01 00:00:00';
  			$mesSiguiente=(string)$year.'-'.(string)intval($i+1).'-01 00:00:00';
  			if($i==12){
  				$mesSiguiente='2019-01-01 00:00:00';
  			}
  			$this->db->select("count(*) as cuenta, SUM(purchaseAmount) as suma");
  			$this->db->where("timeStamp >",$fechaActual);
  			$this->db->where("timeStamp <",$mesSiguiente);
  			$query=$this->db->get("ecom.e_transactionLog");

  			foreach ($query->result() as $value) {
  				if($value->suma==NULL){
  					$suma=0;
  				}else{
  					$suma=$value->suma;
  				}
  				$respuesta[$i-1]=array('y'=>$mesActual,'a'=>$value->cuenta, 'b'=>$suma);
  			}
  		}
  		return $respuesta;
    }

}

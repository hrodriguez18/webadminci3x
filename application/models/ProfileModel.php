<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ProfileModel extends MY_SuperModel{

  protected $_tablename='adm.users';
  protected $_primary_key='userid';
  protected $_primary_filter='intval';
  protected $_order_by='userid';
  public  $_rules=array('password'=>array('field'=>'password','label'=>'Contraseña Antigua','rules'=>'callback__validaHoldPass'),
                        'newPassword'=>array('field'=>'newPassword','label'=>'Nueva Contraseña','rules'=>'matches[newPasswordConfirm]|max_length[100]'),
                        'newPasswordConfirm'=>array('field'=>'newPasswordConfirm','label'=>'Confirmar Contraseña','rules'=>'matches[newPassword]|max_length[100]')
                        );
  protected $_timestamps=FALSE;
  protected $_form='';
  protected $_form_group_class='form-group ';
  protected $_attr_submit_button='btn btn-success';
  protected $_class_label='label label-info';





  public function __construct()
  {
    parent::__construct();

  }




}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class RelationRolesPermisosModel extends MY_SuperModel {

	protected $_tablename='adm.roles_permisos';
	protected $_primary_key='id';
	protected $_primary_filter='intval';
	protected $_order_by='id';



	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}
	public function getPermisosAgregado($id){
		$this->db->select('*');
		$this->db->where('rol_id',$id);
		return $this->db->get($this->_tablename)->result();
	}

	public function Clean($id){
		$this->db->where('rol_id',$id);
		$this->db->delete($this->_tablename);
		return true;
	}


}

/* End of file RelationRolesPermisosModel.php */
/* Location: ./application/models/RelationRolesPermisosModel.php */

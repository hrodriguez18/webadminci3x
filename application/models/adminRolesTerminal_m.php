<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class adminRolesTerminal_m extends MY_Model {
 	public $_table_name = 'esb.rolesTerminal';
	protected $_primary_key = 'id';
	protected $_primary_filter = 'intval';
	protected $_order_by = 'id';
	protected $_timestamps = FALSE;


	public $rules = array('descripcion'=>array('field'=>'descripcion','label'=>'','rules'=>'trim|required'),
		'status'=>array('field'=>'status','label'=>'','rules'=>'trim|required|callback_diferente_de_0')
	);

	public $status_array=array('0'=>'-- Select --','A'=>'Activo','I'=>'Inactivo');

	//INPUTS DEL FORMULARIO

	private $id=array('name'=>'id','value'=>'','type'=>'hidden','id'=>'id','placeholder'=>'', 'class'=>'form-control');

	private $descripcion=array('name'=>'descripcion','value'=>'','type'=>'text','id'=>'descripcion','placeholder'=>'', 'class'=>'form-control');



	function __construct() {
		parent::__construct();
		$this->descripcion['placeholder']=lang('descripcion');
	}


	public function opt_roles(){
		$this->db->select('*');
		$query=$this->db->get('adm.roles');
		$opt=array('0'=>'-- Select --');
		if($query->num_rows()>0){

			foreach ($query->result() as $value) {
				$opt[$value->Id]=$value->Descripcion;
			}
		}
		return $opt;
	}


	public function Form($data_by_id=NULL){

		if($data_by_id!=NULL){
			$status=$data_by_id->status;
			$this->id['value']=set_value('id',$data_by_id->id,true);
			$this->descripcion['value']=set_value('descripcion',$data_by_id->descripcion,true);
			$this->descripcion['readonly']='true';
		}else{
			$status='0';
			$this->id['value']=set_value('id','',true);
			$this->descripcion['value']=set_value('descripcion','',true);
		}

		$form=form_input($this->id);

		$form.='<div class="row">';

		$form.='<div class="col-lg-6"><div class="form-group">'.form_label(lang('descripcion'),'descripcion').form_input($this->descripcion).form_error('descripcion').'</div></div>';

		$form.='<div class="col-lg-6"><div class="form-group">'.form_label(lang('status'),'status').form_dropdown('status',$this->status_array,set_value('status',$status,true),'class="form-control" id="status"').form_error('status').'</div></div>';

		$form.='</div>';
		return $form;

	}

	public function ListaPermisos($id=NULL){

		$data=array('ListaPermisosActuales'=>'','idRol'=>'','descripcion'=>'','ListaPermisosDisponibles'=>'');
		$listaCompleta=array();
		$permisosAdquiridos=array();

		if($id!=NULL){
			$this->db->select('*');
			$this->db->select('permisosTerminal.descripcion as DESCRIPT');
			$this->db->join('esb.permisosTerminal','rolesPermisosTerminal.permiso_id=permisosTerminal.id','left');
			$this->db->where('rol_id',$id);
			$query=$this->db->get('esb.rolesPermisosTerminal');
			foreach ($query->result() as  $value) {
				$data['ListaPermisosActuales'].='<li class="list-group-item list-group-item-success" id="'.$value->permiso_id.'">'.$value->DESCRIPT.'</li>';
				array_push($permisosAdquiridos, $value->permiso_id);
			}
		}

		$this->db->select('*');
		$listaCompleta=$this->db->get('esb.permisosTerminal');
		$k=0;
		foreach ($listaCompleta->result() as  $value) {
			if(!in_array($value->id, $permisosAdquiridos)){
				$data['ListaPermisosDisponibles'].='<li class="list-group-item list-group-item-warning" id="'.$value->id.'">'.$value->descripcion.'</li>';
			}
			$k++;
		}

		return $data;
	}

	public function InsertarRol($name,$status){
		$data=array('descripcion'=>$name,'status'=>$status);
		$this->db->insert('esb.rolesTerminal',$data);
		return $this->db->insert_id();
	}

	public function saveTerminalPermiso($data){
		$this->db->insert('esb.rolesPermisosTerminal',$data);
		return true;
	}

	public function LimpiarRol($id){
		$this->db->where('rol_id',$id);
		$this->db->delete('esb.rolesPermisosTerminal');
		return true;
	}

	public function UpdateRol($name,$status,$id){
		$data=array('descripcion'=>$name,'status'=>$status);
		//$this->db->set($data);
		$this->db->where('id',$id);
		$this->db->update('esb.rolesTerminal',$data);
		return true;
	}

}
?>

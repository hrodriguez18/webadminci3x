<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class adminPermisosTerminal_m extends MY_Model {
 	public $_table_name = 'esb.binTerminal';
	protected $_primary_key = 'Id';
	protected $_primary_filter = 'intval';
	protected $_order_by = 'Id';


	public $rules = array(
		'descripcion'=>array('field'=>'descripcion','label'=>'Descripcion','rules'=>'trim|required|max_length[20]'),


		'grupo'=>array('field'=>'grupo','label'=>'Grupo','rules'=>'trim|required ')
	);

	protected $_timestamps = FALSE;


	function __construct() {
		parent::__construct();
	}


	public function GetGrupoBines(){
		$this->db->select('*');
		$query=$this->db->get('esb.grupoBines');
		$datos=array();
		$i=0;
		if($query->num_rows()>0){
			foreach ($query->result() as  $value) {
				$datos[$i]=array('id'=>$value->Id,'descripcion'=>$value->descripcion);
				$i++;
			}
		}
		return $datos;
	}

	public function PopulateTable(){
		$datos=$this->GetGrupoBines();
		if($datos!=NULL){
			$arrayBinTerminal=array();
			for ($i=0; $i <count($datos) ; $i++) {
				$this->db->select('*');
				$this->db->where('grupoBinId',$datos[$i]['id']);
				$query=$this->db->get('esb.binTerminal');
				if($query->num_rows()>0){
					$cuenta=$query->num_rows();
					$c=0;
					foreach ($query->result() as  $value) {
						$c++;
						if($c==$cuenta){
							array_push($arrayBinTerminal, array('descripcion'=>$value->descripcion,'idGrupo'=>$datos[$i]['id']));
						}

					}
				}
			}
			return 	$arrayBinTerminal;
		}else{
			return NULL;
		}

	}

	public function opt_operador(){

		$this->db->select('*');
		$query=$this->db->get('esb.Operadores');
		$opt=array();
		$opt[0]='Favor Seleccione Operador';
		if($query->num_rows()>0){
			foreach ($query->result() as $value) {
				$opt[$value->Cod_Operador]=$value->Descripcion;
			}
		}

		return $opt;
	}

	public function opt_Bine(){

		$this->db->select('*');
		$query=$this->db->get('esb.bines');
		$opt=array();
		$opt[0]='Seleccione Bine';
		if($query->num_rows()>0){
			foreach ($query->result() as $value) {
				$opt[$value->Id]=$value->descripcion;
			}
		}

		return $opt;
	}

	public function opt_grupoBines(){
		$this->db->select('*');
		$query=$this->db->get('esb.grupoBines');
		$opt=array();
		$opt[0]='Seleccione GrupoBine';
		if($query->num_rows()>0){
			foreach ($query->result() as $value) {
				$opt[$value->Id]=$value->descripcion;
			}
		}

		return $opt;

	}

	public function InsertarGrupo($name){
		$data=array('descripcion'=>$name,'status'=>'A');
		$this->db->insert('esb.grupoBines',$data);
		return $this->db->insert_id();
	}




	public function infoTable(){
		echo "<pre>";
		var_dump($this->db->field_data('esb.Terminales'));
		echo "</pre>";
	}

	public function BinDisponibles(){
		$this->db->select('*');
		$query=$this->db->get('esb.bines');
		$lista='';
		if($query->num_rows()>0){
			foreach ($query->result() as $value) {
				$lista.='<li class="list-group-item list-group-item-success" id="'.$value->Id.'">'.$value->descripcion.'</li>';
			}
		}
		return $lista;
	}

	public function BinBloqueados(){

	}


	public function InfoBinByGroup($id){

		$data=array('ListabinesBloqueados'=>'','idGrupo'=>'','descripcion'=>'','GRUPO'=>'','ListabinesDesbloqueados'=>'');
		$binesBloqueados=array();
		$this->db->select('binTerminal.Id,binTerminal.grupoBinId,binTerminal.binId,binTerminal.descripcion,bines.descripcion as BINES,grupoBines.descripcion as GRUPO');
		$this->db->join('esb.bines','binTerminal.binId=bines.Id','left');
		$this->db->join('esb.grupoBines','binTerminal.grupoBinId=grupoBines.Id','left');
		$this->db->where('grupoBinId',$id);
		$query=$this->db->get('esb.binTerminal');

		foreach ($query->result() as  $value) {
			$data['ListabinesBloqueados'].='<li class="list-group-item list-group-item-danger" id="'.$value->binId.'">'.$value->BINES.'</li>';
			$data['idGrupo']=$value->grupoBinId;
			$data['descripcion']=$value->descripcion;
			$data['GRUPO']=$value->GRUPO;
			array_push($binesBloqueados, $value->binId);
		}

		$this->db->select('*');
		$bines=$this->db->get('esb.bines');
		$k=0;
		foreach ($bines->result() as  $value) {
			if(!in_array($value->Id, $binesBloqueados)){
				$data['ListabinesDesbloqueados'].='<li class="list-group-item list-group-item-success" id="'.$value->Id.'">'.$value->descripcion.'</li>';
			}
			$k++;
		}


		return $data;
	}

	public function LimpiarBinesBloqueados($id){
		$this->db->where('grupoBinId',$id);
		$this->db->delete('esb.binTerminal');
	}

	public function BorrarBinTerminal($id){
		$this->db->where('Id',$id);
		if($this->db->delete('esb.grupoBines')){
			return true;
		}else{
			return false;
		}

	}

}
?>

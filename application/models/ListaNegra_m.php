<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ListaNegra_m extends MY_SuperModel{
  public $_tablename='esb.blackList';
	public $_primary_key='id';
	public $_primary_filter='intval';
	public $_order_by='id';




  public  $_rules=array(
							'telefono'=>array(
								'field'=>'telefono',
								'label'=>'Teléfono',
								'rules'=>'trim|required|max_length[100]|callback_withoutSignal|required|callback_telefonoUnico'
							),
							'documento'=>array(
								'field'=>'documento',
								'label'=>'Cédula',
								'rules'=>'trim|required|max_length[100]|callback_cedulaRepetida'
							),
						    'email'=>array(
							  'field'=>'email',
							  'label'=>'email',
							  'rules'=>'trim|required|max_length[100]|callback_CorreoUnico'
						    ),
							'status'=>array(
								'field'=>'status',
								'label'=>'status',
								'rules'=>'trim|required|callback_diferente_de_default'
							)
                        );


  public $_inputs=array(
						  'documento'=>array(
							  'name'=>'documento',
							  'id'=>'documento',
							  'label'=>'Cedula',
							  'type'=>'text',
							  'placeholder'=>'Ingresar Documento...',
							  'value'=>'',
							  'class'=>'form-control'
						  ),


  							'telefono'=>array(
  								'name'=>'telefono',
								'id'=>'telefono',
								'label'=>'Telefono',
								'type'=>'number',
								'min'=>0,
								'placeholder'=>'Ingresar Telefono...',
								'value'=>'',
								'class'=>'form-control'
							),

							'email'=>array(
								'name'=>'email',
								'id'=>'email',
								'type'=>'email',
								'label'=>'email',
								'placeholder'=>'Ingresar Correo',
								'value'=>'',
								'class'=>'form-control'
							),
							'status'=>array(
								'name'=>'status',
								'id'=>'status',
								'type'=>'selector',
								'table_rel'=>'esb.status',
								'opt'=>'value',
								'description'=>'description',
								'class'=>'form-control',
								'default'=>'A',
								'label'=>'Estado'
							),
							'id'=>array(
								'name'=>'id',
								'type'=>'hidden',
								'id'=>'id',
								'value'=>''
							)
  	);
	public $_timestamps=FALSE;
	public $_form='';
	public $_form_group_class=' form-group ';
	public $_attr_submit_button='';
	public $_class_label='label label-danger ';
	public $_ruta='ListaNegra/';

  //para generar los informes en PDF
	public $_TableToExport='';
	public $_footer_ContentToExport='';
	public $_pdfTitle='INFORME DE LISTA NEGRA';
	public $_pdfDescription='';
	public $_headersTable=array('TELEFONO','CEDULA','ESTADO','Correo');
	public $_selectToExport=array('blackList.telefono','blackList.documento','status.description as S','blackList.email');
	public $_orderName='id';
	public $_order='DESC';
	public $_join=array('table'=>'esb.status','relation'=>'blackList.status=status.value','type'=>'left');
	public $_dateVariable='';


  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
  }

}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MonederoTransacciones_m extends MY_SuperModel{
  //datos para hacer la consulta la base de datos
  protected $_tablename='esb.transactionMonederoLog';
	public $_primary_key='transactionMonederoLog.id';
	protected $_primary_filter='intval';
	protected $_order_by='transactionMonederoLog.id';

  protected $_form_group_class=' ';
  //datos para crear el FormularioPaisEdit

  //RELACION PARA GENERAR LA TABLA DE ESTA CLASE
	protected $_headingTable=array();
	//usado para buscar los datos de la db
	//usado para mostrar los datos en la tabla e incluir los btn actions

	//para generar los informes en PDF

	public $_headersTable=array('FECHA', 'USUARIO', 'TERMINAL','TIPO TRX','MONTO','FORMA PAGO','CUENTA','DESCRIPCION','TERMINAL','CURRENCY','LAT','LON');

	public $_selectToExport=array('transactionMonederoLog.stamp',
                                'transactionMonederoLog.userMonedero',
                                'transactionMonederoLog.terminalMonedero',
                                'transactionMonederoLog.tipoTrx',
                                'transactionMonederoLog.montoTrx',
                                'transactionMonederoLog.formaPago',
                                'transactionMonederoLog.cuenta',
                                'transactionMonederoLog.descripcionTrx',
                                'transactionMonederoLog.terminalId',
                                'transactionMonederoLog.currency',
                                'transactionMonederoLog.lat',
                                'transactionMonederoLog.lon');
	public $_orderName='';
	public $_order='';
	public $_join=array();
	public $_dateVariable='stamp';

  //Reglas para validar los campos
  public  $_rules=array('email'=>array('field'=>'email','label'=>'()','rules'=>'trim|email|required|max_length[80]'),
		'cedula'=>array('field'=>'cedula','label'=>'()','rules'=>'trim|required|max_length[50]'),
		'phone'=>array('field'=>'phone','label'=>'()','rules'=>'trim|required|max_length[50]')
  );

  //INPUTS PARA LA CREACION DEL FORMULARIO, SE RECOMIENDA QUE LOS CAMPOS HIDDEN SE COLOQUEN AL FINAL,
	/*public $_inputs=array('email'=>array('name'=>'email','type'=>'email','placeholder'=>'email','value'=>'','label'=>'email','class'=>'form-control'),
				'name'=>array('name'=>'name','type'=>'text','placeholder'=>'name','value'=>'','label'=>'name','class'=>'form-control'),
				'lastname'=>array('name'=>'lastname','type'=>'text','placeholder'=>'lastname','value'=>'','label'=>'lastname','class'=>'form-control'),
				'phone'=>array('name'=>'phone','type'=>'text','placeholder'=>'phone','value'=>'','label'=>'phone','class'=>'form-control'),
				'cedula'=>array('name'=>'cedula','type'=>'text','placeholder'=>'cedula','value'=>'','label'=>'cedula','class'=>'form-control'),
				'fnacimiento'=>array('name'=>'fnacimiento','type'=>'date','value'=>'','label'=>'Birthdate','class'=>'form-control'),
				'address'=>array('name'=>'address','type'=>'textarea','value'=>'','label'=>'address','class'=>'form-control','rows'=>'3'),
        'sexo'=>array('name'=>'sexo','id'=>'sexo','type'=>'selector','table_rel'=>'esb.sexo','opt'=>'name','description'=>'name','class'=>'form-control','value'=>'','label'=>'sexo'),
        'paisId'=>array('name'=>'paisId','id'=>'paisId','type'=>'selector','table_rel'=>'esb.Pais','opt'=>'Cod_Pais','description'=>'Descripcion','class'=>'form-control','value'=>'','label'=>'country'),
        'subsidioId'=>array('name'=>'subsidioId','id'=>'subsidioId','type'=>'selector','table_rel'=>'esb.subsidios','opt'=>'id','description'=>'descripcion','class'=>'form-control','value'=>'','label'=>'subsidy'),
				'rolId'=>array('name'=>'rolId','id'=>'rolId','type'=>'selector','table_rel'=>'esb.rolesTerminal','opt'=>'id','description'=>'descripcion','class'=>'form-control','value'=>'','label'=>'rol_id'),
				'grupoTrxId'=>array('name'=>'grupoTrxId','id'=>'grupoTrxId','type'=>'selector','table_rel'=>'esb.grupoTransacciones','opt'=>'id','description'=>'description','class'=>'form-control','value'=>'','label'=>'TransactionsGroup'),
				'status'=>array('name'=>'status','id'=>'status','type'=>'selector','table_rel'=>'esb.status','opt'=>'value','description'=>'description','class'=>'form-control','value'=>'','label'=>'status'),
        'id'=>array('name'=>'id','type'=>'hidden','value'=>'')
			);*/

  //datos para exportar a PDF
  public $_TableToExport='';
  public $_footer_ContentToExport='';
  public $_pdfTitle='BANCO NACIONAL DE PANAMÁ';
	public $_pdfDescription='REPORTE DE CNB';

  //usado para buscar los datos de la db
  public $_datatable=array('transactionMonederoLog.id',
                            'transactionMonederoLog.stamp',
                            'transactionMonederoLog.userMonedero',
                            'transactionMonederoLog.terminalMonedero',
                            'transactionMonederoLog.tipoTrx',
	  						'transactionMonederoLog.email',
                            'transactionMonederoLog.montoTrx',
                            'transactionMonederoLog.montoTotal',
                            'transactionMonederoLog.formaPago',
                            'transactionMonederoLog.cuenta',
	  						'transactionMonederoLog.terminalId',
                            'transactionMonederoLog.descripcionTrx',
                            'transactionMonederoLog.currency',
                            'transactionMonederoLog.lat',
                            'transactionMonederoLog.lon',
	  						'tipoTransaccionesCnb.name as TIPO'
	  );

  //
	//
	public $_joinForBootstrapTable=array(
		'tipoTrx'=>array(
			'table_join'=>'esb.tipoTransaccionesCnb ',
			'table_rel'=>' transactionMonederoLog.tipoTrx=tipoTransaccionesCnb.id '
		)
	);
  //usado para mostrar los datos en la tabla e incluir los btn actions
  public $_dataForTable=array(
  							'transactionMonederoLog.descripcionTrx',
	  						'transactionMonederoLog.descripcionTrx',
	  						'transactionMonederoLog.terminalMonedero',
	  						'transactionMonederoLog.cuenta',
	  						'transactionMonederoLog.email',
						  	'transactionMonederoLog.userMonedero',
						  	'transactionMonederoLog.montoTotal',
  	);
  //hacer el algun join en la tabla

  public $_jointable=array(


  );


  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
    $this->load->library('table');
    $this->table->set_heading($this->_headersTable);
  }

  public function populate($limit=NULL,$offset=NULL,$search=NULL,$sort=NULL,$sortOrder=NULL){
  	$arrayJson=array();


			if(count($this->_datatable)>0){
				for ($i=0; $i <count($this->_datatable) ; $i++) {
					$this->db->select($this->_datatable[$i]);
				}
				if(count($this->_joinForBootstrapTable)>0){
					foreach ($this->_joinForBootstrapTable as $key) {
						$this->db->join($key['table_join'],$key['table_rel'],'left');
					}

				}

			}else{
				$this->db->select('*');
			}



	  $fecha_inicio=date('Y-m-d').' 00:00:00';
	  $fecha_fin=date('Y-m-d').' 23:59:00';

	  $search='';

	  $data = json_decode(file_get_contents('php://input'), true);

	  if(isset($data['fecha_inicio'])){
		  $fecha_inicio= $data['fecha_inicio'].' 00:00:00';
		  $fecha_fin= $data['fecha_fin'].' 23:59:00';
	  }

	  if(isset($data['search'])){
		  $search=utf8_decode($data['search']);
	  }


	  if($this->input->post('search')){
	  	$search=$this->input->post('search');
	  }



	  if($this->input->post('fecha_inicio')){
	  		$fecha_inicio=$this->input->post('fecha_inicio').' 00:00:00';
			$fecha_fin=$this->input->post('fecha_fin').' 23:59:00';
	  }


	  if(strlen($fecha_inicio) >0 && strlen($fecha_fin)>0){
			$this->db->where('stamp >',$fecha_inicio);
			$this->db->where('stamp <',$fecha_fin);
	  }

	  if(strlen($search)>0){
		  for ($i=0; $i <count($this->_dataForTable) ; $i++){
			  if($i==0){
				  $this->db->like($this->_dataForTable[$i], $search, 'both');
				  $this->db->where('stamp >',$fecha_inicio);
				  $this->db->where('stamp <',$fecha_fin);
			  }else{
				  $this->db->or_like($this->_dataForTable[$i], $search, 'both');
				  $this->db->where('stamp >',$fecha_inicio);
				  $this->db->where('stamp <',$fecha_fin);
			  }
		  }
	  }


	    if($limit && $offset){
	      $this->db->limit($offset,$limit);
	    }
	    if($sort){
	      $this->db->order_by($sort,$sortOrder);
	    }else{
	      $this->db->order_by($this->_primary_key,'DESC');
	    }


	    //$this->db->limit($limit);
	    //$this->db->group_by(array($this->_primary_key,'stamp'));
	    $query= $this->db->get($this->_tablename,$limit,$offset);

	    if($query->num_rows()>0){
        $i=0;
	      foreach ($query->result() as  $value) {
	        $arrayJson['rows'][$i]['id']=$value->id;
	        $arrayJson['rows'][$i]['stamp']=$value->stamp;
	        $arrayJson['rows'][$i]['userMonedero']=$value->userMonedero;
	        $arrayJson['rows'][$i]['terminalMonedero']=$value->terminalMonedero;
	        $arrayJson['rows'][$i]['tipoTrx']=$value->tipoTrx;
	        $arrayJson['rows'][$i]['TIPO']=$value->TIPO;
	        $arrayJson['rows'][$i]['montoTotal']=$value->currency.' '.$value->montoTotal;
	        $arrayJson['rows'][$i]['formaPago']=$value->formaPago;
	        $arrayJson['rows'][$i]['cuenta']=$value->cuenta;
	        $arrayJson['rows'][$i]['email']=$value->email;
	        $arrayJson['rows'][$i]['descripcionTrx']=$value->descripcionTrx;
	        $arrayJson['rows'][$i]['terminalId']=$value->terminalId;
	        $arrayJson['rows'][$i]['MAPA']='<a onclick="ShowPointmap(\'https://maps.googleapis.com/maps/api/staticmap?center='.$value->lat.','.$value->lon.'&zoom=15&size=600x400&maptype=roadmap&markers=color:red%7Clabel:C%7C'.$value->lat.','.$value->lon.'&key=AIzaSyCjTI0FdJCGl26y2XJdFNZwP0LDYj2DL_A\')" href="#">MAPA</a>';
          $i++;
	      }
	    }else{
			$arrayJson['rows'][$i]['id']='';
			$arrayJson['rows'][$i]['stamp']='';
			$arrayJson['rows'][$i]['userMonedero']='';
			$arrayJson['rows'][$i]['terminalMonedero']='';
			$arrayJson['rows'][$i]['tipoTrx']='';
			$arrayJson['rows'][$i]['TIPO']='';
			$arrayJson['rows'][$i]['montoTotal']='';
			$arrayJson['rows'][$i]['formaPago']='';
			$arrayJson['rows'][$i]['cuenta']='';
			$arrayJson['rows'][$i]['email']='';
			$arrayJson['rows'][$i]['descripcionTrx']='';
			$arrayJson['rows'][$i]['terminalId']='';
			$arrayJson['rows'][$i]['MAPA']='';
		}
	  	$arrayJson['total']=$query->num_rows();
	    return $arrayJson;
	}

}

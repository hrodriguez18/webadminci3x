<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class AdminCalendarios_m extends MY_Model {
 	public $_table_name = 'esb.grupoDias';
	protected $_primary_key = 'id';
	protected $_primary_filter = 'intval';
	protected $_order_by = 'id';
	protected $_timestamps = FALSE;


	public $rulesGrupoDias=array('description'=>array('field'=>'description','label'=>'Descripción','rules'=>'trim|required'));

	public $template = array(
        'table_open'            => '<table class="table table-striped table-hover table-condensed table-bordered">',

        'thead_open'            => '<thead>',
        'thead_close'           => '</thead>',

        'heading_row_start'     => '<tr class"table_for_calendar">',
        'heading_row_end'       => '</tr>',
        'heading_cell_start'    => '<th>',
        'heading_cell_end'      => '</th>',

        'tbody_open'            => '<tbody>',
        'tbody_close'           => '</tbody>',

        'row_start'             => '<tr class"table_for_calendar">',
        'row_end'               => '</tr>',
        'cell_start'            => '<td>',
        'cell_end'              => '</td>',

        'row_alt_start'         => '<tr class"table_for_calendar">',
        'row_alt_end'           => '</tr>',
        'cell_alt_start'        => '<td>',
        'cell_alt_end'          => '</td>',

        'table_close'           => '</table>'
	);




	public function TableGrupoDias(){
		$this->table->set_template($this->template);
		$this->db->select('*');
		$query=$this->db->get('esb.GrupoDias');
		$tabla=array();
		if($query->num_rows()>0){
			foreach ($query->result() as $value) {
				$this->table->add_row($value->id,$value->description,$value->status,'<button id="'.$value->id.'" class="btn btn-xs btn-info" onclick="EditarGrupoDias('.$value->id.')"><i class="fa fa-edit "></i></button>  <button id="'.$value->id.'" class="btn btn-xs btn-danger"><i class="fa fa-remove "></i></button>');
			}
		}
		$this->table->set_heading('ID', 'Descripción', 'Status','Acciones');
		return $this->table->generate();
	}

	public function TableHorarios(){
		$this->table->set_template($this->template);
		$this->db->select('horarios.id,horarios.description,horarios.startSchedule,horarios.endSchedule,horarios.status');
		$this->db->select('grupoDias.description as GRUPO');
		$this->db->join('esb.grupoDias','horarios.grupoDiasId=grupoDias.id','left');
		$query=$this->db->get('esb.horarios');
		$tabla=array();
		if($query->num_rows()>0){
			foreach ($query->result() as $value) {
				$this->table->add_row($value->id,$value->description,$value->startSchedule,$value->endSchedule,$value->GRUPO,$value->status,'<button id="'.$value->id.'"  class="btn btn-xs btn-info"><i class="fa fa-edit "></i></button>  <button id="'.$value->id.'" class="btn btn-xs btn-danger"><i class="fa fa-remove "></i></button>');
			}
		}
		$this->table->set_heading('ID', 'Descripción','Inicio','Fin', 'Grupo','Status','Acciones');
		return $this->table->generate();
	}

	public function SaveGrupo($data){
		$this->db->insert('esb.grupoDias',$data);
		return $this->db->insert_id();
	}

	public function idFromDay($value){

		$this->db->select('id');
		$this->db->where('value',$value);
		$query=$this->db->get('esb.dias');
		$id='';
		foreach ($query->result() as $value) {
			$id=$value->id;
		}
		return $id;

	}

	public function PopulateTableGrupoDias(){
		return $this->get();

	}


	public function PopulateTableRelGrupoDias($grupo,$dia){
		$data=array('diasId'=>$dia,'grupoDiasId'=>$grupo);
		$this->db->insert('esb.relGrupoDias',$data);
		return TRUE;
	}

	public function Opt_GrupoDias(){
		$this->db->select('*');
		$query=$this->db->get('esb.grupoDias');
		$opt=array();
		if($query->num_rows()>0){
			foreach ($query->result() as  $value) {
				$opt[$value->id]=$value->description;
			}

		}
		return $opt;
	}

	public function FormNewHorario(){

		$this->load->helper('form');

		$description=array('name'=>'description','id'=>'horario_description','class'=>'form-control','value'=>'','type'=>'text','placeholder'=>'Descripción');
		$description['value']=set_value('description','',true);

		$hora_inicio=array('name'=>'startSchedule','id'=>'startSchedule','class'=>'form-control','value'=>'','type'=>'time');
		$hora_inicio['value']=set_value('description',date('H:i'),true);

		$hora_fin=array('name'=>'endSchedule','id'=>'endSchedule','class'=>'form-control','value'=>'','type'=>'time');
		$hora_fin['value']=set_value('endSchedule',date('H:i'),true);

		$status=array('A'=>'ACTIVO','I'=>'INACTIVO','0'=>'  ---SELECT---  ');


		$selector_grupo_dias=$this->Opt_GrupoDias();

		$selector_grupo_dias['default']='  ---SELECT---  ';

		$attt_form=array('id'=>'form_horarios');
		$attr_for_labels=array('class'=>'label label-warning');



		$form=form_open('#',$attt_form);
		$form.='<div class="form-group" id="horario_description_form_group">'.form_label('Descripción','description',$attr_for_labels).form_input($description).form_error('description').'</div>';

		$form.='<div class=form-group><div class="row"><div class="col-lg-6">'.form_label('Hora de Inicio','startSchedule',$attr_for_labels).form_input($hora_inicio).form_error('startSchedule').'</div><div class="col-lg-6">'.form_label('Hora de Fin','hora_fin',$attr_for_labels).form_input($hora_fin).form_error('hora_fin').'</div></div></div>';



		$form.='<div class="form-group">'.form_label('Estado','status',$attr_for_labels).form_dropdown('status',$status,set_value('status','0',true),'class="form-control"').form_error('status').'</div>';

		$form.='<div class="form-group">'.form_label('Grupo Dias','grupoDias',$attr_for_labels).form_dropdown('grupoDiasId',$selector_grupo_dias,set_value('grupoDiasId','default',true),'class="form-control" id="grupoDiasId"').form_error('grupoDiasId').'</div>';

		return $form;



	}

	public function InsertInfo($data,$table){
		$this->db->insert($table,$data);
	}


	public function FormNewGrupoDias(){
		return $this->load->view($this->config->item("theme")."/forms_modal/form_adminCalendarios_NewGrupoDias",'',true);
	}

	public function LimpiarRelGrupoDias($id){
		$this->db->where('grupoDiasId',$id);
		$this->db->delete('esb.relGrupoDias');
		return true;
	}

	public function GetDiasDisponibles($id){
		$this->db->select('diasId');
		$this->db->where('grupoDiasId',$id);
		return $this->db->get('esb.relGrupoDias')->result();
	}

	public function ConstruirDiasCheckBoxs($dias){

		$this->db->select('*');
		$query=$this->db->get('esb.dias');
		$html='';
		foreach ($query->result() as $value) {
			if(in_array($value->id, $dias)){
				$html.='<div class="checkbox checkbox-inline">
                    <input id="'.$value->value.'" checked="true" type="checkbox" value="'.$value->value.'" name="dia[]">
                    <label for="'.$value->value.'">
                   	'.$value->description.'
                    </label>
                </div>';
			}else{
				$html.='<div class="checkbox checkbox-inline">
                    <input id="'.$value->value.'" type="checkbox" value="'.$value->value.'" name="dia[]">
                    <label for="'.$value->value.'">
                   	'.$value->description.'
                    </label></div>';
			}
		}
		return $html;
	}

	public function UpdateGrupo($data,$id){
		$this->db->where('id',$id);
		$this->db->update('esb.grupoDias',$data);
	}



}
?>

<form action="#" id="form_horarios">
	<input type="hidden" name="id" value="<?php echo $id_horario ?>">
	<div class="form-group">
		<label for="horario">Nombre de Horario</label>
		<input type="text" required="" class="form-control" placeholder="Horario" value="<?php echo $description ?>" name="nombre_horario">
		<?php if(isset($fail)){echo $fail;} ?>
	</div>
	<div class="form-group">
		<label for="excepciones">
			Excepción
		</label>
		<?php echo $excepciones; ?>
	</div>
	<div class="form-group">

		<table class="table">
			<thead>
			<tr>

				<th>Día</th>
				<th>Hora Inicio</th>
				<th>Hora Fin</th>
				<th>Hab.</th>
			</tr>
			</thead>
			<tbody id="bodytable">

			<?php echo $table_horario; ?>

			</tbody>
		</table>
	</div>
	<p class="alert alert-info"><?php echo $this->lang->line('valid_time'); ?></p>
</form>

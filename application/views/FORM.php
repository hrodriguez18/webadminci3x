<form action="#" id="form_horarios">
	<div class="form-group">
		<label for="horario">Nombre de Horario</label>
		<input type="text" class="form-control" placeholder="Horario" name="nombre_horario">
		<?php if(isset($fail)){echo $fail;} ?>
	</div>
	<div class="form-group">
		<label for="excepciones">
			Excepción
		</label>
		<?php if(isset($excepciones)){echo $excepciones;} ?>
	</div>
	<div class="form-group">
		<table class="table">
			<thead>
			<tr>

				<th>Día</th>
				<th>Hora Inicio</th>
				<th>Hora Fin</th>
				<th>Hab.</th>
			</tr>
			</thead>
			<tbody id="bodytable">
			<tr>

				<td>Lunes</td>
				<td><input type="time"  value="00:00" name="hora_inicio[]" class="form-control"></td>
				<td><input type="time"  value="00:00" name="hora_fin[]" class="form-control"></td>
				<td><input type="checkbox" name="dias[]" value="L" /></td>

			</tr>
			<tr>

				<td>Martes</td>
				<td><input type="time" value="00:00" name="hora_inicio[]" class="form-control"></td>
				<td><input type="time" value="00:00" name="hora_fin[]" class="form-control"></td>
				<td><input type="checkbox" name="dias[]" value="M" /></td>
			</tr>
			<tr>

				<td>Miercoles</td>
				<td><input type="time" value="00:00" name="hora_inicio[]" class="form-control"></td>
				<td><input type="time" value="00:00" name="hora_fin[]" class="form-control"></td>
				<td><input type="checkbox" name="dias[]" value="W" /></td>
			</tr>
			<tr>

				<td>Jueves</td>
				<td><input type="time" value="00:00" name="hora_inicio[]" class="form-control"></td>
				<td><input type="time" value="00:00" name="hora_fin[]" class="form-control"></td>
				<td><input type="checkbox" name="dias[]" value="J" /></td>
			</tr>
			<tr>

				<td>Viernes</td>
				<td><input type="time" value="00:00" name="hora_inicio[]" class="form-control" ></td>
				<td><input type="time" value="00:00" name="hora_fin[]"  class="form-control"></td>
				<td><input type="checkbox" name="dias[]" value="V" /></td>
			</tr>
			<tr>

				<td>Sábado</td>
				<td><input type="time" value="00:00" name="hora_inicio[]" class="form-control"></td>
				<td><input type="time" value="00:00" name="hora_fin[]" class="form-control"></td>
				<td><input type="checkbox" name="dias[]" value="S" /></td>
			</tr>
			<tr>

				<td>Domingo</td>
				<td><input type="time" value="00:00" name="hora_inicio[]" class="form-control"></td>
				<td><input type="time" value="00:00" name="hora_fin[]" class="form-control"></td>
				<td><input type="checkbox" name="dias[]" value="D" /></td>
			</tr>
			</tbody>
		</table>
	</div>
	<p class="alert alert-info"><?php echo $this->lang->line('valid_time'); ?></p>
</form>

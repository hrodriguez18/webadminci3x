<html lang="en">
<head>
	<meta charset="utf-8" />
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url() ?>assets/icons/ic_launcher.png">
	<link rel="icon" type="image/png" sizes="96x96" href="<?php echo base_url() ?>assets/icons/ic_launcher.png">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title><?php echo $this->lang->line('titleAdmin'); ?></title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
     <!-- Bootstrap core CSS     -->
    <link href="<?php echo base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet" />



     <!-- MpayCenter CSS     -->
    <link href="<?php echo base_url() ?>assets/css/mpaycenter.css" rel="stylesheet" />

    <!--  Paper Dashboard core CSS   -->

    <link href="<?php echo base_url() ?>assets/css/paper-dashboard.css" rel="stylesheet"/>



    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
   <link href='https://fonts.googleapis.com/css?family=Muli:400,300' rel='stylesheet' type='text/css'>
   <link href="<?php echo base_url() ?>assets/css/themify-icons.css" rel="stylesheet">

</head>

<body>
	<div class="wrapper">
	    <div class="sidebar" data-background-color="brown" data-active-color="danger">
	    <!--
			Tip 1: you can change the color of the sidebar's background using: data-background-color="white | brown"
			Tip 2: you can change the color of the active button using the data-active-color="primary | info | success | warning | danger"
		-->
			<div class="logo">
				<a href="<?php echo base_url() ?>Admin" class="simple-text logo-mini">
					AD
				</a>

				<a href="#" class="simple-text logo-normal">
					<?php if(isset($titleMenu)){
						echo $titleMenu;
					}else{
						echo $this->lang->line('titleAdminCAP');
					} ?>
				</a>
			</div>
	    	<div class="sidebar-wrapper">
				<div class="user">
					<div class="photo">
						<img src="<?php echo base_url(); ?>assets/img/faces/user.svg">
	        </div>
	                <div class="info">
	                    <a data-toggle="collapse" href="#collapseExample" class="collapsed">
	                        <span>
								<?php $nombre=$this->session->userdata('name');
										if($nombre){
											echo $nombre;
										}
								 ?>
		                        <b class="caret"></b>
							</span>
	                    </a>
						<div class="clearfix"></div>

	                    <div class="collapse" id="collapseExample">
	                        <ul class="nav">
	                            <li>
									<a href="<?php echo base_url() ?>Perfil">
										<span class="sidebar-mini">P</span>
										<span class="sidebar-normal"><?php echo $this->lang->line('profile'); ?></span>
									</a>
								</li>
	                            <!--<li>
									<a href="#edit">
										<span class="sidebar-mini">Ep</span>
										<span class="sidebar-normal">Edit Profile</span>
									</a>
								</li>-->
	                            <li>
									<a href="<?php echo base_url() ?>Logout">
										<span class="sidebar-mini">C</span>
										<span class="sidebar-normal"><?php echo $this->lang->line('logout'); ?></span>
									</a>
								</li>
	                        </ul>
	                    </div>
	                </div>
	            </div>
	            <ul class="nav">
	              <?php $this->load->view('paper/menupaper/menu_cnb') ?>
	            </ul>
	    	</div>
	    </div>

	    <div class="main-panel">
			<nav class="navbar navbar-default">
	            <div class="container-fluid">
					<div class="navbar-minimize">
						<button id="minimizeSidebar" class="btn btn-fill btn-icon"><i class="ti-more-alt"></i></button>
					</div>

					<!-- AQUI SE CARGA EL NAVBAR O MENU SUPERIOR -->
					<?php echo $navbar_menu; ?>

	               <!-- <div class="collapse navbar-collapse">

	                    <ul class="nav navbar-nav navbar-right">

	                        <li>

	                        </li>

	                    </ul>
	                </div>-->
	            </div>
	        </nav>
			<!--AQUI SE CARGA SIEMPRE EL CONTENIDO DE LA VISTA-->

			<div class="content">
				<div class="container-fluid">
					<div class="row">

			            <div class="col-lg-12">
			                <div class="card ">
			                    <div class="card-header">
			                        <div class="row">
			                            <div class="col-lg-12">
			                                <h4 class="card-title"><?php echo $this->lang->line('customerSupport'); ?></h4>

			                            </div>
			                        </div>
			                    </div>

													<style media="screen">
													.progress {
															height:1.5em !important; /* needs to be an absolute measurement unless one of it's parent has an absolute height */
														}
													</style>

			                    <div class="card-content">
														<?php $message=$this->session->flashdata('message');
																	if($message){
																		echo $message;
																	}
														 ?>
                            <?php if(isset($form)){echo $form;} ?>


			                    </div>

			                    <div class="card-footer">
															<div class="" id="respuestadeToken">

															</div>
			                        <hr>
			                        <div class="stats">
																<input type="hidden" id="input_phone" name="phone" value="<?php echo $phone ?>">
																<input type="hidden" name="name" id="input_name" value="<?php echo $name; ?>">
																<input type="hidden" name="terminalId" id="input_terminalId" value="<?php echo $terminalId; ?>">
																<button type="button" role="button " class="btn btn-success" onclick="ActualizarData()">Actualizar</button>
																<button type="button" role="button " class="btn btn-danger" onclick="EnviarToken()">Enviar Token</button>
																<button type="button" role="button " class="btn btn-warning" onclick="ResetPIN()">Reiniciar PIN</button>
															</div>
			                    </div>
			                </div>
			            </div>
			        </div>
				</div>
			</div>

	         <!--------------------------------------------------------->
            <footer class="footer">
                <div class="container-fluid">

                </div>

            </footer>
	    </div>
	</div>
<?php $this->load->view('paper/footers/footer'); ?>
	<script src="<?php echo base_url() ?>assets/js/bootstrap-selectpicker.js"></script>
</body>
<script type="text/javascript">

	function EnviarToken(){
		var phone = $("#input_phone").val();
		var name = $("#input_name").val();
		var terminalId=$("#input_terminalId").val();
		var count=0;
		$("#respuestadeToken").html('<div class="progress"><div id="Mybar" class="progress-bar progress-bar-success active progress-bar-striped" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:'+count+'%">'+count+'%</div></div>')
		var intervalo=setInterval(function(){
			if(count<=100){
					$("#Mybar").attr('aria-valuenow',count);
					$("#Mybar").css('width',count+'%');
					$("#Mybar").html(count+' %');
			}
			count+=5
		}
		,100
	)
		$.post("<?php echo base_url(); ?>"+"SupportCNB/SetToken",{
			name:name,
			phone:phone,
			terminalId:terminalId,
		},function(data){
			if(data=='ok'){
					clearInterval(intervalo);
					$("#respuestadeToken").html('<p class="alert alert-success">Token Enviado...</p>');
			}else{
				$("#respuestadeToken").html(data);
			}
		});
}


	function ResetPIN(){
		var phone = $("#input_phone").val();
		var name = $("#input_name").val();
		var terminalId=$("#input_terminalId").val();
		var count=0;
		$("#respuestadeToken").html('<div class="progress"><div id="Mybar" class="progress-bar progress-bar-success active progress-bar-striped" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:'+count+'%">'+count+'%</div></div>')
		var intervalo=setInterval(function(){
				if(count<=100){
					$("#Mybar").attr('aria-valuenow',count);
					$("#Mybar").css('width',count+'%');
					$("#Mybar").html(count+' %');
				}
				count+=5
			}
			,100
		)
		$.post("<?php echo base_url(); ?>"+"SupportCNB/ResetPIN",{
			name:name,
			phone:phone,
			terminalId:terminalId,
		},function(data){
			if(data=='ok'){
				clearInterval(intervalo);
				$("#respuestadeToken").html('<p class="alert alert-success">PIN Enviado...</p>');
			}else{
				$("#respuestadeToken").html(data);
			}
		});
	}

	$('#idioma_lang').on('change', function(){
		var lan = this.value;
		var dominio='https://secure.prestopago.com/CI3/Admin/';
		$.post(dominio+'changeLang',{lang:lan},function(data){
			location.reload();
		});
	})

	function ActualizarData(){
		var formData = new FormData(document.getElementById("form_page"));
		$.ajax({
				url: "<?php echo base_url(); ?>AdminTerminales/"+'UpdateData',
				type: "post",
				dataType: "html",
				data: formData,
				cache: false,
				contentType: false,
				processData: false
		})
			.done(function(res){

				if(res!='ok'){
					$("#respuestadeToken").html('<p class="alert alert-danger">Información no se actualizó</p>');

				}else{
					$("#respuestadeToken").html('<p class="alert alert-success">Información Actualizada</p>');

			}
		});
	}




</script>

</html>

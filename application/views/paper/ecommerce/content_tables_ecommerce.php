<div class="content">
	<div class="container-fluid">
		<div class="row">
            <div class="col-lg-12">
                <div class="card ">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-lg-12">
                                <h4 class="card-title"><?php if(isset($title)){
                                  echo $title;
                                } ?></h4>
                            </div>
                        </div>

                    </div>

                    <div class="card-content">
                 		<div class="table-responsive">
                      <div class="toolbar">

                        <button class="btn btn-danger btn-sm">PDF <i class="ti-file"></i></span></button>
                        <button class="btn btn-primary btn-sm">XLS <i class="ti-file"></i></button>
                      </div>
                      <table id="table_ecommerce" class="table" >

                      </table>
                 		</div>
                    </div>

                    <div class="card-footer">
                        <hr>
                        <div class="stats">

                        </div>
                    </div>
                </div>
            </div>
        </div>
	</div>
</div>


<!-- Modal -->
<div class="modal fade" id="ModalApp" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header" >
				 <button type="button" class="close" data-dismiss="modal">&times;</button>
				 <h4 class="modal-title">FAQs</h4>
			 </div>
			 <div class="modal-body" id="modal_Content">

			 </div>
			 <div class="modal-footer">
				 <button type="button" class="btn btn-wd btn-fill btn-primary" onclick='Save()'>Guardar</button>
			 </div>

		</div>
	</div>
</div>

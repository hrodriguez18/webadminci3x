<div class="content">
	<div class="container-fluid">

		<div class="row" id="counters">
            <div class="col-lg-3 col-sm-6">
                <div class="card">
                    <div class="card-content">
                        <div class="row">
                            <div class="col-xs-5">
                                <div class="icon-big icon-warning text-center">
                                    <i class="ti-server"></i>
                                </div>
                            </div>
                            <div class="col-xs-7">
                                <div class="numbers">
																	<p><?php echo $this->lang->line('TD'); ?></p>

                                    <p>
                                    <?php
                                    	if(isset($transaccionesDiarias)){
                                    		echo $transaccionesDiarias;
                                    	}
                                     ?>
                                     </p>
                                </div>
                            </div>
                        </div>
                    </div>
					<div class="card-footer">
						<hr>
						<div class="stats">
						</div>
					</div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="card">
                    <div class="card-content">
                        <div class="row">
                            <div class="col-xs-5">
                                <div class="icon-big icon-success text-center">
                                    <i class="ti-wallet"></i>
                                </div>
                            </div>
                            <div class="col-xs-7">
                                <div class="numbers">
                                    <p><?php echo $this->lang->line('dailysales'); ?></p>
                                    <p>
                                    $<?php
                                    	if(isset($ventasDiarias)){
                                    		echo $ventasDiarias;
                                    	}else{
                                    		echo '0.00';
                                    	}
                                     ?>
                                     </p>
                                </div>
                            </div>
                        </div>
                    </div>
					<div class="card-footer">
						<hr>
						<div class="stats">

						</div>
					</div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="card">
                    <div class="card-content">
                        <div class="row">
                            <div class="col-xs-5">
                                <div class="icon-big icon-danger text-center">
                                    <i class="ti-pie-chart"></i>
                                </div>
                            </div>
                            <div class="col-xs-7">
                                <div class="numbers">
                                    <p><?php echo $this->lang->line('avgTicket'); ?></p>
                                    <p>
                                    $<?php
                                    	if(isset($ticketPromedio) ){
																				echo $ticketPromedio;
																			}
                                     ?>
                                     </p>
                                </div>
                            </div>
                        </div>
                    </div>
					<div class="card-footer">
						<hr>
						<div class="stats">

						</div>
					</div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="card">
                    <div class="card-content">
                        <div class="row">
                            <div class="col-xs-5">
                                <div class="icon-big icon-info text-center">
                                    <i class="ti-info"></i>
                                </div>
                            </div>
                            <div class="col-xs-7">
                                <div class="numbers">
                                    <p><?php echo $this->lang->line('payment'); ?></p>
                                    <p>
                                    <?php
                                    	if(isset($topMarca)){
                                    		echo $topMarca;
                                    	}
                                     ?>
                                     </p>
                                </div>
                            </div>
                        </div>
                    </div>
					<div class="card-footer">
						<hr>
						<div class="stats">

						</div>
					</div>
                </div>
            </div>
	                </div>
		<div class="row">
            <div class="col-lg-12">
                <div class="card ">
                    <div class="card-header">
                        <h4 class="card-title" id="anio_actual">
                            <?php echo date("Y"); ?> <?php
														if(!isset($totalVentas)){
															$totalVentas='$0.00';
														}
														echo $this->lang->line('sales').' : $. '.$totalVentas; ?></h4>
                        <p class="category"> <?php echo $this->lang->line('products_impuestos'); ?> </p>
                    </div>
                    <div class="card-content">
                    	<div id="myfirstchart" style="height: 250px;"></div>
                    </div>
                    <div class="card-footer">
                        <hr>
                        <div class="stats">

                        </div>
                    </div>
                </div>
            </div>


	   </div>

	</div>
</div>

<script type="text/javascript">
servidor='https://secure.prestopago.com/CI3/EcommercePaper/';

$("#myfirstchart").ready(function(){
	SetGraphic();
});


function SetGraphic(){


$.post(servidor+"Graphics",{},function(data){
			var months = ['J', 'F', 'M', 'A', 'M', 'J', 'J', 'A',
		 'S', 'O', 'N', 'D'];
		//alert(data)
		var mpayChart= new Morris.Line({
				element: 'myfirstchart',
				data: [],
				xkey: 'y',
				ykeys: ['b','a'],
				labels: ['Ventas','Transacciones'],
				xLabelFormat: function (x) {return months[x.getMonth()];}
			});

			mpayChart.setData($.parseJSON(data));
});
//mpayChart.setData(info);
}

</script>

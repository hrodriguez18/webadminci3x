
            <div class="col-lg-3 col-sm-6">
                <div class="card">
                    <div class="card-content">
                        <div class="row">
                            <div class="col-xs-5">
                                <div class="icon-big icon-warning text-center">
                                    <i class="ti-server"></i>
                                </div>
                            </div>
                            <div class="col-xs-7">
                                <div class="numbers">
                                    <p><?php echo $this->lang->line('logTx') ?></p>
                                    <p>
                                    <?php
                                    	if(isset($cuenta)){
                                    		echo $cuenta;
                                    	}
                                     ?>
                                     </p>
                                </div>
                            </div>
                        </div>
                    </div>
					<div class="card-footer">
						<hr>
						<div class="stats">

						</div>
					</div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="card">
                    <div class="card-content">
                        <div class="row">
                            <div class="col-xs-5">
                                <div class="icon-big icon-success text-center">
                                    <i class="ti-wallet"></i>
                                </div>
                            </div>
                            <div class="col-xs-7">
                                <div class="numbers">
                                    <p><?php echo $this->lang->line('sales') ?></p>
                                    <p>
                                    $<?php
                                    	if(isset($suma)){
                                    		echo $suma;
                                    	}else{
                                    		echo '0.00';
                                    	}
                                     ?>
                                     </p>
                                </div>
                            </div>
                        </div>
                    </div>
					<div class="card-footer">
						<hr>
						<div class="stats">

						</div>
					</div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="card">
                    <div class="card-content">
                        <div class="row">
                            <div class="col-xs-5">
                                <div class="icon-big icon-danger text-center">
                                    <i class="ti-pie-chart"></i>
                                </div>
                            </div>
                            <div class="col-xs-7">
                                <div class="numbers">
                                    <p><?php echo $this->lang->line('average') ?></p>

                                    <p>
                                    $<?php
                                    	if(isset($cuenta) and intval($cuenta)>0){
                                    		echo number_format(floatval($suma/$cuenta),2);
                                    	}else{
                                    		echo '0.00';
                                    	}
                                     ?>
                                     </p>
                                </div>
                            </div>
                        </div>
                    </div>
					<div class="card-footer">
						<hr>
						<div class="stats">

						</div>
					</div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="card">
                    <div class="card-content">
                        <div class="row">
                            <div class="col-xs-5">
                                <div class="icon-big icon-info text-center">
                                    <i class="ti-info"></i>
                                </div>
                            </div>
                            <div class="col-xs-7">
                                <div class="numbers">
                                    <p><?php echo $this->lang->line('returns') ?></p>
                                    <p>

                                        <?php
                                        if(isset($devoluciones)){
                                            echo $devoluciones;
                                        }
                                     ?>
                                    </p>

                                </div>




                            </div>
                        </div>
                    </div>
					<div class="card-footer">
						<hr>
						<div class="stats">

						</div>
					</div>
                </div>
            </div>

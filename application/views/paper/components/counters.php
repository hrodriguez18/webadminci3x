<div class="row" id="counters">
	<div class="col-lg-2 col-sm-6">
		<div class="card">
			<div class="card-content">
				<div class="row">
					<div class="col-xs-12">
						<div class="icon-big icon-warning text-center">
							<i class="ti-server"></i>
						</div>
					</div>
					<div class="col-xs-12 text-center">
						<div class="text-center">
							<p><?php echo $this->lang->line('transactions'); ?></p>
							<p>
								<?php
								if(isset($transacciones)){
									echo $transacciones;
								}
								?>
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-lg-2 col-sm-6">
		<div class="card">
			<div class="card-content">
				<div class="row">
					<div class="col-xs-12">
						<div class="icon-big icon-success text-center">
							<i class="fas fa-credit-card"></i>
						</div>
					</div>
					<div class="col-xs-12 text-center">
						<div class="text-center">
							<p><?php echo $this->lang->line('credit');?></p>
							<p>
								$ <?php
								if(isset($credito)){
									echo $credito;
								}else{
									echo '0.00';
								}
								?>
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-lg-4 col-sm-6">
		<div class="card">
			<div class="card-content">
				<div class="row">
					<div class="col-xs-12">
						<div class="icon-big icon-primary text-center">
							<i class="fas fa-balance-scale"></i>
						</div>
					</div>
					<div class="col-xs-12 text-center">
						<div class="text-center">
							<p><?php echo $this->lang->line('balance'); ?></p>
							<p>
								$ <?php
								if(isset($balance)){
									echo $balance;
								}else{
									echo '0.00';
								}
								?>
							</p>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
	<div class="col-lg-2 col-sm-6">
		<div class="card">
			<div class="card-content">
				<div class="row">
					<div class="col-xs-12">
						<div class="icon-big icon-danger text-center">
							<i class="fas fa-coins"></i>
						</div>
					</div>
					<div class="col-xs-12 text-center">
						<div class="text-center">
							<p><?php echo $this->lang->line('debit');?></p>
							<p>
								$ <?php
								if(isset($debito)){
									echo $debito;
								}else{
									echo '0.00';
								}
								?>
							</p>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
	<div class="col-lg-2 col-sm-6">
		<div class="card">
			<div class="card-content">
				<div class="row">
					<div class="col-xs-12">
						<div class="icon-big icon-info text-center">
							<i class="ti-info"></i>
						</div>
					</div>
					<div class="col-xs-12 text-center">
						<div class="text-center">
							<p><?php if(isset($nameLastKpi)){echo $nameLastKpi;}else{echo 'Devoluciones';} ?></p>
							<p>
								<?php
								if(isset($devoluciones)){
									echo $devoluciones;
								}
								?>
							</p>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>

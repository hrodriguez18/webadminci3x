<html lang="en">
<head>
	<meta charset="utf-8" />
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url() ?>assets/icons/ic_launcher.png">
	<link rel="icon" type="image/png" sizes="96x96" href="<?php echo base_url() ?>assets/icons/ic_launcher.png">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title><?php echo $this->lang->line('titleAdmin'); ?></title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />

		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">

     <!-- Bootstrap core CSS     -->
    <link href="<?php echo base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?php echo base_url() ?>assets/js/bootstrapTable/dist/bootstrap-table.css" rel="stylesheet" />

     <!-- MpayCenter CSS     -->
    <link href="<?php echo base_url() ?>assets/css/mpaycenter.css" rel="stylesheet" />

    <!--  Paper Dashboard core CSS   -->

    <link href="<?php echo base_url() ?>assets/css/paper-dashboard.css" rel="stylesheet"/>
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
   <link href='https://fonts.googleapis.com/css?family=Muli:400,300' rel='stylesheet' type='text/css'>
   <link href="<?php echo base_url() ?>assets/css/themify-icons.css" rel="stylesheet">

</head>

<body>
	<div class="wrapper">
	    <div class="sidebar" data-background-color="brown" data-active-color="danger">
	    <!--
			Tip 1: you can change the color of the sidebar's background using: data-background-color="white | brown"
			Tip 2: you can change the color of the active button using the data-active-color="primary | info | success | warning | danger"
		-->
			<div class="logo">
				<a href="<?php echo base_url() ?>Admin" class="simple-text logo-mini">
					AD
				</a>

				<a href="#" class="simple-text logo-normal">
					<?php if(isset($titleMenu)){
						echo $titleMenu;
					}else{
						echo $this->lang->line('titleAdminCAP');
					} ?>
				</a>
			</div>
	    	<div class="sidebar-wrapper">
				<div class="user">
					<div class="photo">
						<img src="<?php echo base_url(); ?>assets/img/faces/user.svg">
	        </div>
	                <div class="info">
	                    <a data-toggle="collapse" href="#collapseExample" class="collapsed">
	                        <span>
								<?php $nombre=$this->session->userdata('name');
										if($nombre){
											echo $nombre;
										}
								 ?>
		                        <b class="caret"></b>
							</span>
	                    </a>
						<div class="clearfix"></div>

	                    <div class="collapse" id="collapseExample">
	                        <ul class="nav">
	                            <li>
									<a href="<?php echo base_url() ?>Perfil">
										<span class="sidebar-mini"><i class="fas fa-user-circle"></i></span>
										<span class="sidebar-normal"><?php echo $this->lang->line('profile'); ?></span>
									</a>
								</li>
	                            <!--<li>
									<a href="#edit">
										<span class="sidebar-mini">Ep</span>
										<span class="sidebar-normal">Edit Profile</span>
									</a>
								</li>-->
	                            <li>
									<a href="<?php echo base_url() ?>Logout">
										<span class="sidebar-mini"><i class="fas fa-sign-out-alt"></i></span>
										<span class="sidebar-normal"><?php echo $this->lang->line('logout'); ?></span>
									</a>
								</li>
	                        </ul>
	                    </div>
	                </div>
	            </div>
	            <ul class="nav">
	               <?php $this->load->view('paper/menupaper/menu_admin') ?>
	            </ul>
	    	</div>
	    </div>

	    <div class="main-panel">
			<nav class="navbar navbar-default">
	            <div class="container-fluid">
					<div class="navbar-minimize">
						<button id="minimizeSidebar" class="btn btn-fill btn-icon"><i class="ti-more-alt"></i></button>
					</div>

					<!-- AQUI SE CARGA EL NAVBAR O MENU SUPERIOR -->
					<?php if(isset($navbar_menu)){
						echo $navbar_menu;
					} ?>
	               <!-- <div class="collapse navbar-collapse">

	                    <ul class="nav navbar-nav navbar-right">
	                        <li>
	                        </li>
	                    </ul>
	                </div>-->
	            </div>
	        </nav>

			<!--AQUI SE CARGA SIEMPRE EL CONTENIDO DE LA VISTA-->

      <div class="content">
      	<div class="container-fluid">
      		<div class="row">
                  <div class="col-lg-12">
                      <div class="card ">
                          <div class="card-header">
                              <div class="row">
                                  <div class="col-lg-12">
                                      <h4 class="card-title"><?php if(isset($title)){
                                        echo $title;
                                      } ?></h4>
																			<hr>
                                  </div>
                              </div>

                          </div>
                          <div class="card-content">


                            <div class="toolbar">
															<form class="" id="wpt" action="<?php echo base_url() ?>Winpt_C/DownWin" method="post">
																	<div class="row">
																		<div class="col-xs-4">
																			<div class="form-group">
																				<label for="">Inicio</label>
																				<input type="date" id="finicio" class="form-control" name="inicio" value="<?php echo date('Y-m-d'); ?>">
																			</div>
																		</div>

																		<div class="col-xs-4">
																			<div class="form-group">
																				<label for="">Fin</label>
																				<input type="date" id="ffin" class="form-control" name="fin" value="<?php echo date('Y-m-d'); ?>">
																			</div>
																		</div>

																		<div class="col-xs-2">
																			<div class="form-group">
																				<label for="">Descargar</label>
																				<button type="button" class="btn btn-info btn-sm" onclick="DownWpt()">
		                                    Descargar WINPT
		                                  	</button>
																			</div>
																		</div>
																	</div>
																<input type="hidden" id="ftipoTrx" name="tipoTrx" value="">
															</form>


								<!--<form class="" id="wptCnb" action="<?php echo base_url() ?>Winpt_C/DownWinCnb" method="post">
									<div class="row">
										<div class="col-xs-4">
											<div class="form-group">
												<label for="">Inicio</label>
												<input type="date" id="finicio" class="form-control" name="inicio" value="<?php echo date('Y-m-d'); ?>">
											</div>
										</div>

										<div class="col-xs-4">
											<div class="form-group">
												<label for="">Fin</label>
												<input type="date" id="ffin" class="form-control" name="fin" value="<?php echo date('Y-m-d'); ?>">
											</div>
										</div>

										<div class="col-xs-2">
											<div class="form-group">
												<label for="">Descargar</label>
												<button type="button" class="btn btn-success btn-sm" onclick="DownWptCnb()">
													Descargar WINPT CNB
												</button>
											</div>
										</div>
									</div>
									<input type="hidden" id="ftipoTrx" name="tipoTrx" value="">
								</form>-->

                              <div class="btn-toolbar" role="toolbar">




                              </div>

                            </div>
                            <table id="tabla" class="table" >
                            </table>
                          </div>
                          <div class="card-footer">
                              <hr>
                              <div class="stats">

                              </div>
                          </div>
                      </div>
                  </div>
              </div>
      	</div>
      </div>

	         <!--------------------------------------------------------->
            <footer class="footer">
                <div class="container-fluid">
                </div>
            </footer>
	    </div>
	</div>
  <!--   Core JS Files. Extra: TouchPunch for touch library inside jquery-ui.min.js   -->
  	<script src="<?php echo base_url()?>assets/js/jquery-3.1.1.min.js" type="text/javascript"></script>
  	<script src="<?php echo base_url()?>assets/js/jquery-ui.min.js" type="text/javascript"></script>
  	<script src="<?php echo base_url()?>assets/js/perfect-scrollbar.min.js" type="text/javascript"></script>
  	<script src="<?php echo base_url()?>assets/js/bootstrap.min.js" type="text/javascript"></script>
      	<!-- Promise Library for SweetAlert2 working on IE -->
  	<script src="<?php echo base_url()?>assets/js/es6-promise-auto.min.js"></script>
  		<!-- Sweet Alert 2 plugin -->
  	<script src="<?php echo base_url()?>assets/js/sweetalert2.js"></script>
      <!-- Wizard Plugin    -->
  	<script src="<?php echo base_url()?>assets/js/jquery.bootstrap.wizard.min.js"></script>

  	<!--  Bootstrap Table Plugin    -->
  	<script src="<?php echo base_url()?>assets/js/bootstrapTable/dist/bootstrap-table.js"></script>

  	<!-- Paper Dashboard PRO Core javascript and methods for Demo purpose -->
  	<script src="<?php echo base_url()?>assets/js/paper-dashboard.js"></script>

  		<!--  Switch and Tags Input Plugins -->
  	<script src="<?php echo base_url()?>assets/js/bootstrap-switch-tags.js"></script>

	<script src="<?php echo base_url() ?>assets/js/bootstrap-selectpicker.js">

	</script>

  <?php if($this->session->userdata('lang')=='es'){
    echo '<script src="'.base_url().'assets/js/bootstrapTable/dist/locale/bootstrap-table-es-MX.js"></script>';
  }else{
    echo '<script src="'.base_url().'assets/js/bootstrapTable/dist/locale/bootstrap-table-en-US.js"></script>';
  } ?>


	<form  id="form_dispersion" action="<?php echo base_url() ?>Dispersion/DeleteTabledispersion" method="post">
		<input type="hidden" value="1" name="none">
	</form>

	<form  id="form_Apply" action="<?php echo base_url() ?>Dispersion/ApplyDepositos" method="post">
		<input type="hidden" value="1" name="none">
	</form>

</body>

<script type="text/javascript">


	$('#idioma_lang').on('change', function(){
		var lan = this.value;
		var dominio="<?php echo base_url().$controllerServer ?>/";
		$.post(dominio+'changeLang',{lang:lan},function(data){
			location.reload();
		});
	})


	function DownWpt(){
		$("#wpt").submit();
	}

	function DownWptCnb(){

		$("#wptCnb").submit();
	}


</script>




</html>

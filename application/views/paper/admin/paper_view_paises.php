<div class="content" id="content_paises" >
	<div class="container-fluid">
		<div class="row">
            <div class="col-lg-12">
                <div class="card ">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-lg-12">
                                <h4 class="card-title"><?php echo $this->lang->line('country'); ?></h4>

                            </div>
                        </div>

                    </div>

                    <div class="card-content">
                 		<div class="table-responsive">
                 			<?php if(isset($tablePaises)){
                 				echo $tablePaises;
                 			} ?>
                 		</div>
                    </div>

                    <div class="card-footer">
                        <hr>
                        <div class="stats">

                        </div>
                    </div>
                </div>
            </div>
        </div>
	</div>
</div>
  <!-- Modal -->
  <div class="modal fade" id="ModalPais" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content" id="modal_Content">

      </div>
    </div>
</div>

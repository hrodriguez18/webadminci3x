<script async defer
		src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCH3gEliBYytt3Sx_flWixHPf7GauF3OP4&v=3&">
</script>

<div class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-xs-12">
				<?php if(isset($form_ventas)){
					echo $form_ventas;
				} ?>
			</div>
		</div>

		<div class="row" id="counters">
            <div class="col-lg-3 col-sm-6">
                <div class="card">
                    <div class="card-content">
                        <div class="row">
                            <div class="col-xs-5">
                                <div class="icon-big icon-warning text-center">
                                    <i class="ti-server"></i>
                                </div>
                            </div>
                            <div class="col-xs-7">
                                <div class="numbers">
                                    <p><?php echo $this->lang->line('transactions'); ?></p>
                                    <p>
                                    <?php
                                    	if(isset($info_ventas)){
                                    		echo $info_ventas['cuenta'];
                                    	}
                                     ?>
                                     </p>
                                </div>
                            </div>
                        </div>
                    </div>
					<div class="card-footer">
						<hr>
						<div class="stats">
						</div>
					</div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="card">
                    <div class="card-content">
                        <div class="row">
                            <div class="col-xs-5">
                                <div class="icon-big icon-success text-center">
                                    <i class="ti-wallet"></i>
                                </div>
                            </div>
                            <div class="col-xs-7">
                                <div class="numbers">
                                    <p><?php echo $this->lang->line('sales'); ?></p>
                                    <p>
                                    $<?php
                                    	if(isset($info_ventas)){
                                    		echo $info_ventas['suma'];
                                    	}else{
                                    		echo '0.00';
                                    	}
                                     ?>
                                     </p>
                                </div>
                            </div>
                        </div>
                    </div>
					<div class="card-footer">
						<hr>
						<div class="stats">

						</div>
					</div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="card">
                    <div class="card-content">
                        <div class="row">
                            <div class="col-xs-5">
                                <div class="icon-big icon-danger text-center">
                                    <i class="ti-pie-chart"></i>
                                </div>
                            </div>
                            <div class="col-xs-7">
                                <div class="numbers">
                                    <p><?php echo $this->lang->line('average'); ?></p>
                                    <p>
                                    $<?php
                                    	if(isset($info_ventas) and intval($info_ventas['cuenta'])>0){
                                    		echo number_format(floatval($info_ventas['suma'])/intval($info_ventas['cuenta']),2);
                                    	}else{
                                    		echo '0.00';
                                    	}
                                     ?>
                                     </p>
                                </div>
                            </div>
                        </div>
                    </div>
					<div class="card-footer">
						<hr>
						<div class="stats">

						</div>
					</div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="card">
                    <div class="card-content">
                        <div class="row">
                            <div class="col-xs-5">
                                <div class="icon-big icon-info text-center">
                                    <i class="ti-info"></i>
                                </div>
                            </div>
                            <div class="col-xs-7">
                                <div class="numbers">
                                    <p><?php echo $this->lang->line('returns'); ?></p>
                                    <p>
                                    <?php
                                    	if(isset($info_ventas)){
                                    		echo $info_ventas['devoluciones'];
                                    	}
                                     ?>
                                     </p>
                                </div>
                            </div>
                        </div>
                    </div>
					<div class="card-footer">
						<hr>
						<div class="stats">

						</div>
					</div>
                </div>
            </div>
	                </div>
		<div class="row">
            <div class="col-lg-12">
                <div class="card ">
                    <div class="card-header">
                        <h4 class="card-title" id="anio_actual">
                            <?php echo date("Y"); ?> <?php echo $this->lang->line('sales'); ?></h4>
                        <p class="category"> <?php echo $this->lang->line('products_impuestos'); ?> </p>
                    </div>
                    <div class="card-content">
                    	<div id="myfirstchart" style="height: 250px;"></div>
                    </div>
                    <div class="card-footer">
                        <hr>
                        <div class="stats">

                        </div>
                    </div>
                </div>
            </div>


	   </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="card ">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-lg-12">
                                <h4 class="card-title"><?php echo $this->lang->line('maps'); ?></h4>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4">
                                <?php if(isset($form_maps)){
                                    echo $form_maps;
                                    } ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <p class="category"> <?php  echo $this->lang->line('DescriptionofMAP'); ?> </p>
                            </div>

                        </div>





                    </div>
                    <div class="card-content">

                        <div id="map" style="width: 100%; height:500px">


                        </div>



                    </div>
                    <div class="card-footer">
                        <hr>
                        <div class="stats">
                            <i class="ti-check"></i> <?php $this->lang->line('maps'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
	</div>
</div>


<!--   Core JS Files. Extra: TouchPunch for touch library inside jquery-ui.min.js   -->
<script src="<?php echo base_url()?>assets/js/jquery-3.1.1.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>assets/js/jquery-ui.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>assets/js/perfect-scrollbar.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>assets/js/bootstrap.min.js" type="text/javascript"></script>



<!-- Promise Library for SweetAlert2 working on IE -->
<script src="<?php echo base_url()?>assets/js/es6-promise-auto.min.js"></script>

<!-- Sweet Alert 2 plugin -->
<script src="<?php echo base_url()?>assets/js/sweetalert2.js"></script>


<!-- Wizard Plugin    -->
<script src="<?php echo base_url()?>assets/js/jquery.bootstrap.wizard.min.js"></script>

<!--  Bootstrap Table Plugin    -->
<script src="<?php echo base_url()?>assets/js/bootstrap-table.js"></script>

<!--  Plugin for DataTables.net  -->
<script src="<?php echo base_url()?>assets/js/jquery.datatables.js"></script>



<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/fixedheader/3.1.3/js/dataTables.fixedHeader.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.1/js/dataTables.responsive.min.js"></script>



<!-- Paper Dashboard PRO Core javascript and methods for Demo purpose -->
<script src="<?php echo base_url()?>assets/js/paper-dashboard.js"></script>

<!--  Switch and Tags Input Plugins -->
<script src="<?php echo base_url()?>assets/js/bootstrap-switch-tags.js"></script>


<script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js"></script>

<!-- Promise Library for SweetAlert2 working on IE -->
<script src="<?php echo base_url()?>assets/js/es6-promise-auto.min.js"></script>

<!-- Paper Dashboard PRO DEMO methods, don't include it in your project! -->
<script src="<?php echo base_url()?>assets/js/demo.js"></script>



<?php if($this->session->userdata('lang')=='es'){
	echo '<script src="'.base_url().'assets/js/es/mpaycenter.js"></script>';
}else{
	echo '<script src="'.base_url().'assets/js/en/mpaycenter.js"></script>';
} ?>


<script>
	var map;
	var puntos;
	var servidor="https://secure.prestopago.com/CI3/admin/";

	var markers=[];
	function initMap(){


		var operador=$("#operador_id").val();

		$.post(servidor+"SearchPoints",{operador:operador,},function(data){
			puntos=$.parseJSON(data);
			map = new google.maps.Map(document.getElementById('map'), {
				center:{lat: 9.9907, lng: -79.499},
				zoom:5.8,
				gestureHandling:'cooperative'

			});
			var largeInfowindow = new google.maps.InfoWindow();
			for(var i=0; i<puntos.length; i++){
				var position=puntos[i].location;
				var title=puntos[i].title;
				var pinColor = (puntos[i].color).slice(1);
				var pinImage = new google.maps.MarkerImage("https://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|" + pinColor,
					new google.maps.Size(21, 34),
					new google.maps.Point(0,0),
					new google.maps.Point(10, 34));
				//alert(puntos.length)

				var marker = new google.maps.Marker({
					position:position,
					map:map,
					title:title,
					icon:pinImage,
					animation:google.maps.Animation.DROP,
					id:i
				});
				markers.push(marker);


				marker.addListener('click', function() {
					populateInfoWindow(this, largeInfowindow);
				});


			}
		});

		//constructor creates a new map - only center and zoom, are required
		//el constructor crea un nuevo mapa -solo el centrado y el zoom solo requeridos.

	}

	function setMapOnAll(map) {
		for (var i = 0; i < markers.length; i++) {
			markers[i].setMap(map);
		}
	}

	function clearMarkers() {
		setMapOnAll(null);
	}

	function deleteMarkers() {
		clearMarkers();
		markers = [];
	}


	function SetPoints(){
		var operador=$("#operador_id").val();

		$.post(servidor+"SearchPoints",{operador:operador,},function(data){
			puntos=$.parseJSON(data);
			deleteMarkers();
			var largeInfowindow = new google.maps.InfoWindow();
			for(var i=0; i<puntos.length; i++){
				var position=puntos[i].location;
				var title=puntos[i].title;
				var pinColor = (puntos[i].color).slice(1);
				var pinImage = new google.maps.MarkerImage("https://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|" + pinColor,
					new google.maps.Size(21, 34),
					new google.maps.Point(0,0),
					new google.maps.Point(10, 34));
				//alert(puntos.length)

				var marker = new google.maps.Marker({
					position:position,
					map:map,
					title:title,
					icon:pinImage,
					animation:google.maps.Animation.DROP,
					id:i
				});
				markers.push(marker);


				marker.addListener('click', function() {
					populateInfoWindow(this, largeInfowindow);
				});


			}
		});


	}

	function populateInfoWindow(marker, infowindow){
		if(infowindow.marker != marker){
			infowindow.marker=marker;
			infowindow.setContent('<div class="alert alert-info"><strong>'+marker.title+'</strong></div>');
			infowindow.open(map,marker);

			infowindow.addListener('closeclick', function(){
				infowindow.setMarker(null);
			});
		}
	}

</script>

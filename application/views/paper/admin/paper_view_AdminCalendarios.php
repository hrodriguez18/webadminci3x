<div class="content">
	<div class="container-fluid">
		<div class="row">
            <div class="col-lg-12">
                <div class="card ">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-lg-12">
                                <h4 class="card-title"><?php if(isset($title)){
                                  echo $title;
                                } ?></h4>

                            </div>  
                        </div>
                        
                    </div>

                    <style>
                      
                      .table_for_calendar{
                        padding:5px !important;
                      }

                      tr{
                        padding: none;

                      }
                      td,th{
                        padding: 3px !important;
                        margin: none;
                      }
                    </style>


                    <div class="card-content">
                      <hr>
                      <div class="row">
                        <div class="col-lg-12">
                          <p class="label label-info">Grupo Dias</p>


                          
                          <button class="btn btn-info" onclick="NewGrupo()">Nuevo Grupo</button>
                          
                          
                          <div class="table-responsive" id="table_grupo_dias">
                              <?php if(isset($TableGrupoDias)){
                                    echo $TableGrupoDias;
                                  } ?>
                          </div>
                        </div>
                      </div>
                      <hr>
                      <div class="row">
                        <div class="col-lg-12">
                          <p class="label label-warning">Horarios de Atención</p>
                          
                          <button class="btn btn-warning" onclick="MostraModalHorario()">Nuevo Horario</button>
                      
                          <div class="table-responsive" id="table_horarios">
                              <?php if(isset($TableHorarios)){
                                echo $TableHorarios;
                              } ?>
                          </div>
                        </div>
                      </div>
                   		
                    </div>

                    <div class="card-footer">
                        <hr>
                        <div class="stats">
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
	</div>
</div>

  <!-- Modal -->
  <div class="modal fade" id="modal_de_respuesta" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content" id="modal_Content">
          <div class="modal-header" >
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Respuesta</h4>
          </div>
          <div class="modal-body" id="content_modal_de_respuesta">
            
          </div>
          <div class="modal-footer">
            <button class="btn btn-success btn-sm"  onclick="OcultarModalRespuesta()">O.K</button>
          </div>
      </div>
    </div>
</div>
  <!-- Modal -->
  <div class="modal fade" id="ModalHorario" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content" id="modal_Content">
          <div class="modal-header" >
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Horarios</h4>
          </div>
          <div class="modal-body" id="contentModalHorario">
              
          </div>
          <div class="modal-footer">
            <button class="btn btn-success btn-sm"  onclick="SendInfoHorario()">Guardar</button>
          </div>
      </div>
    </div>
</div>

<!-- Modal -->
  <div class="modal fade" id="ModalNewGrupo" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content" id="ModalNewGrupo">
          <div class="modal-header" >
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Grupo</h4>
          </div>
          <div class="modal-body" id="modal_body_new_group">
            <form action="" id="form_grupo">
              <div class="form-group" id="grupo_desc">
                <label for="description" class="label label-info">Descripción</label>
                
                <input type="text" class="form-control" name="description" id="description" placeholder="Ingresar Descripción de Grupo">
                <hr>
              </div>
              <div class="form-group">
                <label for="" class="label label-info">Seleccionar Días</label>
                <br>
                <div class="checkbox checkbox-inline">
                    <input id="D" type="checkbox" value="D" name="dia[]">
                    <label for="D">
                    Domingo
                    </label>
                </div>
                
                <div class="checkbox checkbox-inline">
                    <input id="L" type="checkbox" value="L" name="dia[]">
                    <label for="L">
                    Lunes
                    </label>
                </div>
                <div class="checkbox checkbox-inline">
                    <input id="M" type="checkbox" value="M" name="dia[]">
                    <label for="M">
                    Martes
                    </label>
                </div>
                <div class="checkbox checkbox-inline">
                    <input id="W" type="checkbox" value="W" name="dia[]">
                    <label for="W">
                    Miercoles
                    </label>
                </div>

                <div class="checkbox checkbox-inline">
                    <input id="J" type="checkbox" value="J" name="dia[]">
                    <label for="J">
                    Jueves
                    </label>
                </div>

                

              </div>

              <div class="form-group">
                
                <div class="checkbox checkbox-inline">
                    <input id="V" type="checkbox" value="V" name="dia[]">
                    <label for="V">
                    Viernes
                    </label>
                </div>
                <div class="checkbox checkbox-inline">
                    <input id="S" type="checkbox" value="S" name="dia[]">
                    <label for="S">
                    Sábado
                    </label>
                </div>
                <hr>
                
              </div>

              <div class="form-group">
                <label for="status" class="label label-info">
                  Estado
                </label>
                <select name="status" id="status" class="selectpicker">
                  <option value="A">Activo</option>
                  <option value="I">Inactivo</option>
                </select>
              </div> 
            </form>
          </div>
          <div class="modal-footer">
            <button class="btn btn-success btn-sm"  onclick="EnviarNuevoGrupo()">Guardar</button>
          </div>
         
      </div>
    </div>
</div>

<script>
  var servidor = "<?php echo base_url(); ?>AdminCalendarios/";
  function NewGrupo(){
    $("#ModalNewGrupo").modal('show');
  }

  function EnviarNuevoGrupo(){
    var description=$("#description").val();

    if(description==''){
      $("#grupo_desc").addClass('has-error')
    }else{
      var formData = new FormData(document.getElementById("form_grupo"));
      $("#ModalNewGrupo").modal('hide');
      document.getElementById("form_grupo").reset();
      $("#content_modal_de_respuesta").html('<div class="loader"></div>');
      $('#modal_de_respuesta').modal('show');
      $.ajax({
          url: servidor+'SaveGrupo', 
          type: "post",
          dataType: "html",
          data: formData,
          cache: false,
          contentType: false,
          processData: false
      })
        .done(function(res){
           $("#content_modal_de_respuesta").html(res);
           UpdateTable();
           
        });

    }
    
    
  }

  function OcultarModalRespuesta(){
    $("#modal_de_respuesta").modal('hide');
  }

  function UpdateTable(){
    $.post(servidor+"UpdateTableGrupo",{},function(data){
      $("#table_grupo_dias").html(data);
    })
    $.post(servidor+"UpdateTableHorario",{},function(data){
      $("#table_horarios").html(data);
    })
  }


  function MostraModalHorario(){
    PopulateSelectorGrupoDias();
    $("#ModalHorario").modal('show');
  }

  function PopulateSelectorGrupoDias(){
    $.post(servidor+"FormNewHorario",{},function(data){
      $("#contentModalHorario").html(data);
    })
  }

  function SendInfoHorario(){

    var description=$("#horario_description").val();
    var status=$("#horario_status").val();
    var id_grupo_dia=$("#grupoDias").val();

    if(description==''){
      $("#horario_description_form_group").addClass('has-error')
    }else{
      var formData = new FormData(document.getElementById("form_horarios"));
      $("#ModalHorario").modal('hide');
      document.getElementById("form_horarios").reset();
      $("#content_modal_de_respuesta").html('<div class="loader"></div>');
      $('#modal_de_respuesta').modal('show');
      $.ajax({
          url: servidor+'SaveDataHorario', 
          type: "post",
          dataType: "html",
          data: formData,
          cache: false,
          contentType: false,
          processData: false
      })
        .done(function(res){
           $("#content_modal_de_respuesta").html(res);
           UpdateTable();
           
        });

    }

  }
</script>






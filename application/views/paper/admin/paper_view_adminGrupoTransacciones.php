<html lang="en">
<head>
	<meta charset="utf-8" />
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url() ?>assets/icons/ic_launcher.png">
	<link rel="icon" type="image/png" sizes="96x96" href="<?php echo base_url() ?>assets/icons/ic_launcher.png">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title><?php echo $this->lang->line('titleAdmin'); ?></title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


     <!-- Bootstrap core CSS     -->
    <link href="<?php echo base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet" />


<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
     <!-- MpayCenter CSS     -->
    <link href="<?php echo base_url() ?>assets/css/mpaycenter.css" rel="stylesheet" />

    <!--  Paper Dashboard core CSS   -->

    <link href="<?php echo base_url() ?>assets/css/paper-dashboard.css" rel="stylesheet"/>



    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
   <link href='https://fonts.googleapis.com/css?family=Muli:400,300' rel='stylesheet' type='text/css'>
   <link href="<?php echo base_url() ?>assets/css/themify-icons.css" rel="stylesheet">

</head>

<body>
	<div class="wrapper">
	    <div class="sidebar" data-background-color="brown" data-active-color="danger">
	    <!--
			Tip 1: you can change the color of the sidebar's background using: data-background-color="white | brown"
			Tip 2: you can change the color of the active button using the data-active-color="primary | info | success | warning | danger"
		-->
			<div class="logo">
				<a href="<?php echo base_url() ?>Admin" class="simple-text logo-mini">
					AD
				</a>

				<a href="#" class="simple-text logo-normal">
					<?php if(isset($titleMenu)){
						echo $titleMenu;
					}else{
						echo $this->lang->line('titleAdminCAP');
					} ?>
				</a>
			</div>
	    	<div class="sidebar-wrapper">
				<div class="user">
					<div class="photo">
						<img src="<?php echo base_url(); ?>assets/img/faces/user.svg">
	        </div>
	                <div class="info">


	                    <a data-toggle="collapse" href="#collapseExample" class="collapsed">
	                        <span>
								<?php $nombre=$this->session->userdata('name');
									if($nombre){
										echo $nombre;
									}
								 ?>
		                        <b class="caret"></b>
							</span>
	                    </a>
						<div class="clearfix"></div>

	                    <div class="collapse" id="collapseExample">
	                        <ul class="nav">
	                            <li>
									<a href="<?php echo base_url() ?>Perfil">
										<span class="sidebar-mini">P</span>
										<span class="sidebar-normal"><?php echo $this->lang->line('profile'); ?></span>
									</a>
								</li>
	                            <!--<li>
									<a href="#edit">
										<span class="sidebar-mini">Ep</span>
										<span class="sidebar-normal">Edit Profile</span>
									</a>
								</li>-->
	                            <li>
									<a href="<?php echo base_url() ?>Logout">
										<span class="sidebar-mini">C</span>
										<span class="sidebar-normal"><?php echo $this->lang->line('logout'); ?></span>
									</a>
								</li>
	                        </ul>
	                    </div>
	                </div>
	            </div>
	            <ul class="nav">
	              <?php $this->load->view('paper/menupaper/menu_admin'); ?>
	            </ul>
	    	</div>
	    </div>

	    <div class="main-panel">
			<nav class="navbar navbar-default">
	            <div class="container-fluid">
					<div class="navbar-minimize">
						<button id="minimizeSidebar" class="btn btn-fill btn-icon"><i class="ti-more-alt"></i></button>
					</div>

					<!-- AQUI SE CARGA EL NAVBAR O MENU SUPERIOR -->
					<?php echo $navbar_menu; ?>

	               <!-- <div class="collapse navbar-collapse">

	                    <ul class="nav navbar-nav navbar-right">

	                        <li>

	                        </li>

	                    </ul>
	                </div>-->
	            </div>
	        </nav>
			<!--AQUI SE CARGA SIEMPRE EL CONTENIDO DE LA VISTA-->

			<div class="content">
				<div class="container-fluid">
					<div class="row">

			            <div class="col-lg-12">
			                <div class="card ">
			                    <div class="card-header">
			                        <div class="row">
			                            <div class="col-lg-12">
			                                <h4 class="card-title"><?php echo $this->lang->line('TransactionsGroup'); ?></h4>
			                            </div>
			                        </div>

			                    </div>

			                    <div class="card-content">
			                 		<div class="toolbar">
														<button class="btn-success btn btn-sm" onclick="InsertData()"><?php echo $this->lang->line('btn-add'); ?></button>
												  <!--  <button class="btn btn-danger btn-sm">PDF <i class="ti-file"></i></span></button>
												    <button class="btn btn-primary btn-sm">XLS <i class="ti-file"></i></button>
									--></div>
			                 		<div class="table-responsive">
			                 			<table id="tableGrupoTransacciones">

			                 			</table>
			                 		</div>
			                    </div>

			                    <div class="card-footer">
			                        <hr>
			                        <div class="stats">

			                        </div>
			                    </div>
			                </div>
			            </div>


			        </div>


				</div>
			</div>
			<!-- Modal -->
			  <div class="modal fade" id="ModalApp" role="dialog">
			    <div class="modal-dialog modal-lg">
			      <!-- Modal content-->
			      <div class="modal-content" id="">
			        <div class="modal-header" >
					    <button type="button" class="close" data-dismiss="modal">&times;</button>
					    <h4 class="modal-title">Añadir Información</h4>
					  </div>
					  <div class="modal-body" id="modal_Content">

					  </div>
					  <div class="modal-footer">
					    <button type="button" class="btn btn-wd btn-fill btn-primary" onclick='SaveData()'>Guardar</button>
					  </div>
			      </div>
			    </div>
			</div>
	         <!--------------------------------------------------------->
            <footer class="footer">
                <div class="container-fluid">

                </div>
                <?php $this->load->view('paper/footers/footer'); ?>
            </footer>
	    </div>
	</div>

	<script src="<?php echo base_url() ?>assets/js/bootstrap-selectpicker.js"></script>


	<?php if($this->session->userdata('lang')=='es'){
		echo '<script src="'.base_url().'assets/js/es/tableGrupoTransacciones.js"></script>';
	}else{
		echo '<script src="'.base_url().'assets/js/en/tableGrupoTransacciones.js"></script>';
	} ?>

</body>
<script type="text/javascript">

	$('#idioma_lang').on('change', function(){
		var lan = this.value;
		var dominio='https://secure.prestopago.com/CI3/Admin/';
		$.post(dominio+'changeLang',{lang:lan},function(data){
			location.reload();
		});
	})
</script>

</html>

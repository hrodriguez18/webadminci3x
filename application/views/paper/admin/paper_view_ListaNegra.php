<div class="content">
	<div class="container-fluid">
		<div class="row">

            <div class="col-lg-12">
                <div class="card ">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-lg-12">
                                <h4 class="card-title"><?php if(isset($title)){
                                  echo $title;
                                } ?></h4>

                            </div>
                        </div>

                    </div>

                    <div class="card-content">
                 		<div class="table-responsive">
                 			<?php if(isset($table)){
                 				echo $table;
                 			} ?>
                 		</div>
                    </div>

                    <div class="card-footer">
                        <hr>
                        <div class="stats">

                        </div>
                    </div>
                </div>
            </div>
        </div>
	</div>
</div>

<!-- Modal -->
	<div class="modal fade" id="ModalApp" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content" id="">
				<div class="modal-header" >
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Añadir Información</h4>
			</div>
			<div class="modal-body" id="modal_Content">

			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-wd btn-fill btn-primary" onclick='SaveData()'>Guardar</button>
			</div>
			</div>
		</div>
</div>


<div class="modal fade" id="ModalBatch" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content" id="">
      <div class="modal-header" >
      <button type="button" class="close" data-dismiss="modal">&times;</button>
      <h4 class="modal-title">Subir CSV</h4>
    </div>
    <div class="modal-body" id="form_csv">

    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-wd btn-fill btn-primary" onclick='subirBatch()'>Subir</button>
    </div>
    </div>
  </div>
</div>

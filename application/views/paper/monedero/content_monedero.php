<script async defer
		src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCH3gEliBYytt3Sx_flWixHPf7GauF3OP4&v=3&">
</script>


<div class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-xs-12">
				<?php if(isset($formforCounters)){
					echo $formforCounters;
				} ?>
			</div>
		</div>

		<div id="rowCounter">
			<?php
				if(isset($contadores)){echo $contadores;}
			?>

		</div>

		<div class="row">
			<div class="col-lg-12">
				<div class="card ">
					<div class="card-header">
						<h4 class="card-title" id="anio_actual">
							<?php echo date("Y"); ?> <?php echo $this->lang->line('Monedero'); ?></h4>
						<p class="category"> <?php echo $this->lang->line('products_impuestos'); ?> </p>
					</div>
					<div class="card-content">
						<div id="myfirstchart" style="height: 250px;"></div>
					</div>
					<div class="card-footer">
						<hr>
						<div class="stats">

						</div>
					</div>
				</div>
			</div>


		</div>
		<div class="row">
			<div class="col-lg-12">
				<div class="card ">
					<div class="card-header">
						<div class="row">
							<div class="col-lg-12">
								<h4 class="card-title"><?php echo $this->lang->line('maps'); ?></h4>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-12">
								<p class="category"> <?php  echo $this->lang->line('DescriptionofMAP'); ?> </p>
							</div>

						</div>





					</div>
					<div class="card-content">

						<div id="map" style="width: 100%; height:500px">


						</div>

						<!--   Core JS Files. Extra: TouchPunch for touch library inside jquery-ui.min.js   -->
						<script src="<?php echo base_url()?>assets/js/jquery-3.1.1.min.js" type="text/javascript"></script>
						<script src="<?php echo base_url()?>assets/js/jquery-ui.min.js" type="text/javascript"></script>
						<script src="<?php echo base_url()?>assets/js/perfect-scrollbar.min.js" type="text/javascript"></script>
						<script src="<?php echo base_url()?>assets/js/bootstrap.min.js" type="text/javascript"></script>



						<!-- Promise Library for SweetAlert2 working on IE -->
						<script src="<?php echo base_url()?>assets/js/es6-promise-auto.min.js"></script>

						<!-- Sweet Alert 2 plugin -->
						<script src="<?php echo base_url()?>assets/js/sweetalert2.js"></script>


						<!-- Wizard Plugin    -->
						<script src="<?php echo base_url()?>assets/js/jquery.bootstrap.wizard.min.js"></script>

						<!--  Bootstrap Table Plugin    -->
						<script src="<?php echo base_url()?>assets/js/bootstrap-table.js"></script>

						<!--  Plugin for DataTables.net  -->
						<script src="<?php echo base_url()?>assets/js/jquery.datatables.js"></script>



						<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
						<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
						<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
						<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
						<script type="text/javascript" src="https://cdn.datatables.net/fixedheader/3.1.3/js/dataTables.fixedHeader.min.js"></script>
						<script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.1/js/dataTables.responsive.min.js"></script>



						<script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
						<script src="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js"></script>



						<!-- Paper Dashboard PRO Core javascript and methods for Demo purpose -->
						<script src="<?php echo base_url()?>assets/js/paper-dashboard.js"></script>

						<!--  Switch and Tags Input Plugins -->
						<script src="<?php echo base_url()?>assets/js/bootstrap-switch-tags.js"></script>



						<!-- Promise Library for SweetAlert2 working on IE -->
						<script src="<?php echo base_url()?>assets/js/es6-promise-auto.min.js"></script>

						<!-- Paper Dashboard PRO DEMO methods, don't include it in your project! -->
						<script src="<?php echo base_url()?>assets/js/demo.js"></script>






						<script>
							var months = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto',
								'Septiemb.', 'Octubre', 'Nov.', 'Dic.'];


							var lineChart = new Morris.Line({
								// ID of the element in which to draw the chart.
								element: 'myfirstchart',
								// Chart data records -- each entry in this array corresponds to a point on
								// the chart.



								// The name of the data record attribute that contains x-values.
								xkey: 'mes',
								// A list of names of data record attributes that contain y-values.
								ykeys: ['transacciones','credito','debito'],

								// Labels for the ykeys -- will be displayed when you hover over the
								// chart.
								labels: ['Transacciones','Créditos $ ','Débitos $ '],
								hideHover: true,

								xLabelFormat: function (x) {return months[x.getMonth()];},

								pointFillColors:['white'],
								pointStrokeColors: ['black'],

								lineColors:["#33d38c","#1fa4e4","#eeb029"],
								lineWidth:3,
								resize:false,
								smooth:true,
							});



							initMap();

							UpdateKPI();



							var puntos;
							var servidor="<?php echo base_url().$controlador?>";

							var markers=[];


							var map;


							function initMap(){
								map = new google.maps.Map(document.getElementById('map'), {
									center:{lat: 9.9907, lng: -79.499},
									zoom:5.8,
									gestureHandling:'cooperative'
								});
							}

							function setMapOnAll(map) {
								for (var i = 0; i < markers.length; i++) {
									markers[i].setMap(map);
								}
							}

							function clearMarkers() {
								setMapOnAll(null);
							}

							function deleteMarkers() {
								clearMarkers();
								markers = [];
							}


							function SetPoints(){

								var year=$("#anio").val();
								var month=$("#mes").val();
								var country=$("#pais").val();

								$.post(servidor+"/MAPA",{year:year,mes:month,pais:country,},function(data){
									puntos=$.parseJSON(data);

									deleteMarkers();

									var largeInfowindow = new google.maps.InfoWindow();
									for(var i=0; i<puntos.length; i++){
										var position=puntos[i].location;
										var title=puntos[i].title;
										var description=puntos[i].description;
										var pinColor = puntos[i].color;
										//var pinColor = '1fa4e4';
										var pinImage = new google.maps.MarkerImage("https://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|" + pinColor,
											new google.maps.Size(21, 34),
											new google.maps.Point(0,0),
											new google.maps.Point(10, 34));
										//alert(puntos.length)

										var marker = new google.maps.Marker({
											position:position,
											map:map,
											title:title,
											description:description,
											icon:pinImage,
											animation:google.maps.Animation.DROP,
											id:i
										});
										markers.push(marker);


										marker.addListener('click', function() {
											populateInfoWindow(this, largeInfowindow);
										});



									}
								});


							}

							function populateInfoWindow(marker, infowindow){
								if(infowindow.marker != marker){
									infowindow.marker=marker;
									infowindow.setContent(marker.description);
									infowindow.open(map,marker);

									infowindow.addListener('closeclick', function(){
										infowindow.setMarker(null);
									});
								}
							}


							function UpdateKPI(){
								var year=$("#anio").val();
								var month=$("#mes").val();
								var country=$("#pais").val();
								rutaServer="<?php echo base_url().$rutaUpdateKPI;  ?>"
								$.post(rutaServer,{
									year:year,
									month:month,
									country:country,
								},function(data){
									$("#rowCounter").html(data);
								});

								SetGraph();

							}


							function SetGraph(){
								var localServer="<?php echo base_url().$rutaSetGraph; ?>";
								var year=$("#anio").val();
								var pais=$("#pais").val();

								$.post(localServer,{year:year,pais:pais,},function(data){
									lineChart.setData($.parseJSON(data));
								});

								SetPoints();


							}

							SetPoints();


						</script>

					</div>
					<div class="card-footer">
						<hr>
						<div class="stats">
							<i class="ti-check"></i> <?php $this->lang->line('maps'); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


</div>

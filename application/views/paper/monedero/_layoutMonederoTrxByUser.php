<html lang="en">
<head>
	<meta charset="utf-8" />
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url() ?>assets/icons/ic_launcher.png">
	<link rel="icon" type="image/png" sizes="96x96" href="<?php echo base_url() ?>assets/icons/ic_launcher.png">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title><?php echo $this->lang->line('titleAdmin'); ?></title>

<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
<meta name="viewport" content="width=device-width" />

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">

<!-- Bootstrap core CSS     -->
<link href="<?php echo base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet" />
<link href="<?php echo base_url() ?>assets/js/bootstrapTable/dist/bootstrap-table.css" rel="stylesheet" />

<!-- MpayCenter CSS     -->
<link href="<?php echo base_url() ?>assets/css/mpaycenter.css" rel="stylesheet" />

<!--  Paper Dashboard core CSS   -->

<link href="<?php echo base_url() ?>assets/css/paper-dashboard.css" rel="stylesheet"/>



<link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
<link href='https://fonts.googleapis.com/css?family=Muli:400,300' rel='stylesheet' type='text/css'>
<link href="<?php echo base_url() ?>assets/css/themify-icons.css" rel="stylesheet">
</head>

<body>
<div class="wrapper">
	<div class="sidebar" data-background-color="brown" data-active-color="danger">
		<!--
            Tip 1: you can change the color of the sidebar's background using: data-background-color="white | brown"
            Tip 2: you can change the color of the active button using the data-active-color="primary | info | success | warning | danger"
        -->
		<div class="logo">
			<a href="<?php echo base_url() ?>Admin" class="simple-text logo-mini">
				AD
			</a>

			<a href="#" class="simple-text logo-normal">
				<?php if(isset($titleMenu)){
					echo $titleMenu;
				}else{
					echo $this->lang->line('titleAdminCAP');
				} ?>
			</a>
		</div>
		<div class="sidebar-wrapper">
			<div class="user">
				<div class="photo">
					<img src="<?php echo base_url(); ?>assets/img/faces/user.svg">
				</div>
				<div class="info">
					<a data-toggle="collapse" href="#collapseExample" class="collapsed">
	                        <span>
								<?php $nombre=$this->session->userdata('name');
								if($nombre){
									echo $nombre;
								}
								?>
								<b class="caret"></b>
							</span>
					</a>
					<div class="clearfix"></div>

					<div class="collapse" id="collapseExample">
						<ul class="nav">
							<li>
								<a href="<?php echo base_url() ?>Perfil">
									<span class="sidebar-mini"><i class="fas fa-user-circle"></i></span>
									<span class="sidebar-normal"><?php echo $this->lang->line('profile'); ?></span>
								</a>
							</li>
							<!--<li>
                                <a href="#edit">
                                    <span class="sidebar-mini">Ep</span>
                                    <span class="sidebar-normal">Edit Profile</span>
                                </a>
                            </li>-->
							<li>
								<a href="<?php echo base_url() ?>Logout">
									<span class="sidebar-mini"><i class="fas fa-sign-out-alt"></i></span>
									<span class="sidebar-normal"><?php echo $this->lang->line('logout'); ?></span>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<ul class="nav">
				<?php $this->load->view('paper/menupaper/menu_monedero') ?>
			</ul>
		</div>
	</div>

	<div class="main-panel">
		<nav class="navbar navbar-default">
			<div class="container-fluid">
				<div class="navbar-minimize">
					<button id="minimizeSidebar" class="btn btn-fill btn-icon"><i class="ti-more-alt"></i></button>
				</div>

				<!-- AQUI SE CARGA EL NAVBAR O MENU SUPERIOR -->
				<?php if(isset($navbar_menu)){
					echo $navbar_menu;
				} ?>
				<!-- <div class="collapse navbar-collapse">

                     <ul class="nav navbar-nav navbar-right">

                         <li>

                         </li>

                     </ul>
                 </div>-->
			</div>
		</nav>

		<!--AQUI SE CARGA SIEMPRE EL CONTENIDO DE LA VISTA-->

		<div class="content">
			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-12">
						<div class="card ">
							<div class="card-header">
								<div class="row">
									<div class="col-lg-12">
										<h4 class="card-title"><?php if(isset($title)){
												echo $title;
											} ?></h4>
									</div>
								</div>

							</div>

							<div class="card-content">

								<div class="table-responsive">
									<div class="toolbar">
										<form action="" class="form-inline">
											<button class="btn btn-danger " data-toggle="tooltip" title="Descargar Usuarios" type="button" onclick="ExportToPdf()"><i class="fas fa-file-pdf fa-lg"></i></button>
											<button class="btn btn-success " data-toggle="tooltip" title="Descargar Usuarios" type="button" onclick="ExportToExcel()"><i class="fas fa-file-excel-o fa-lg"></i></button>

										</form>
									</div>
									<table id="tabla" class="table" >

									</table>
								</div>
							</div>

							<script>
								$(document).ready(function(){
									$('[data-toggle="tooltip"]').tooltip();
								});
							</script>

							<div class="card-footer">
								<p>Buscar por fecha de registro</p>
								<form action="" class="form-inline">
									<label for=""><?php echo $this->lang->line('since'); ?></label>
									<input type="date" id="inicio" class="form-control" value="<?php echo date('Y-m-d'); ?>" />
									<label for=""><?php echo $this->lang->line('to'); ?></label>
									<input type="date" id="fin" class="form-control"  value="<?php echo date('Y-m-d'); ?>"/>
									<button class="btn btn-info " type="button" onclick="SearchFunction()"><i class="fas fa-search-plus fa-lg"></i></button>
								</form>
								<hr>
								<div class="stats">
									<?php $errors=$this->session->flashdata('errors'); if($errors){echo $errors;}?>
								</div>

								<div id="setInfo"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- Modal -->
		<div class="modal fade" id="ModalApp" role="dialog">
			<div class="modal-dialog <?php if(isset($sizeModal)){echo $sizeModal;} ?>">
				<!-- Modal content-->
				<div class="modal-content" >
					<div class="modal-header" >
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title"><?php if(isset($title_form)){echo $title_form;} ?></h4>
					</div>
					<div class="modal-body" id="modalFormContent">

					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-xs btn-primary" onclick='Save()'>Guardar</button>
					</div>
				</div>
			</div>
		</div>


		<!-- Modal -->
		<div class="modal fade" id="ModalFormCreateMonedero" role="dialog">
			<div class="modal-dialog">
				<!-- Modal content-->
				<div class="modal-content" >
					<div class="modal-header" >
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title"><?php if(isset($title_form)){echo $title_form;} ?></h4>
					</div>
					<div class="modal-body" id="modalFormContent">
						<form id="FormCreateUserMonedero" action="<?php echo base_url()?>MonederoPaper/CreateMonedero" enctype="multipart/form-data" method="post">
								<div class="form-group">
									<label for="">Subir Archivo</label>
									<input type="file" name="monederos" class="form-control">
								</div>
						</form>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-xs btn-fill btn-success" onclick='SubirFilebatch()'>Cargar</button>
					</div>
				</div>
			</div>
		</div>

		<!-- Modal -->
		<div class="modal fade" id="ModalMAP" role="dialog">
			<div class="modal-dialog">
				<!-- Modal content-->
				<div class="modal-content" >
					<div class="modal-header" >
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="text-info text-lef"><?php echo $this->lang->line('geolocation'); ?></h4>
					</div>
					<div class="modal-body" id="modalmapContent">

						<img src="" id="iframeMap" class="img-responsive img-rounded"/>
					</div>
				</div>
			</div>
		</div>
		<!--------------------------------------------------------->
		<footer class="footer">
			<div class="container-fluid">

			</div>
		</footer>
	</div>
</div>
<!--   Core JS Files. Extra: TouchPunch for touch library inside jquery-ui.min.js   -->
<script src="<?php echo base_url()?>assets/js/jquery-3.1.1.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>assets/js/jquery-ui.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>assets/js/perfect-scrollbar.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>assets/js/bootstrap.min.js" type="text/javascript"></script>
<!-- Promise Library for SweetAlert2 working on IE -->
<script src="<?php echo base_url()?>assets/js/es6-promise-auto.min.js"></script>
<!-- Sweet Alert 2 plugin -->
<script src="<?php echo base_url()?>assets/js/sweetalert2.js"></script>
<!-- Wizard Plugin    -->
<script src="<?php echo base_url()?>assets/js/jquery.bootstrap.wizard.min.js"></script>

<!--  Bootstrap Table Plugin    -->
<script src="<?php echo base_url()?>assets/js/bootstrapTable/dist/bootstrap-table.js"></script>

<!-- Paper Dashboard PRO Core javascript and methods for Demo purpose -->
<script src="<?php echo base_url()?>assets/js/paper-dashboard.js"></script>

<!--  Switch and Tags Input Plugins -->
<script src="<?php echo base_url()?>assets/js/bootstrap-switch-tags.js"></script>

<script src="<?php echo base_url() ?>assets/js/bootstrap-selectpicker.js">

</script>

<?php if($this->session->userdata('lang')=='es'){
	echo '<script src="'.base_url().'assets/js/bootstrapTable/dist/locale/bootstrap-table-es-MX.js"></script>';
}else{
	echo '<script src="'.base_url().'assets/js/bootstrapTable/dist/locale/bootstrap-table-en-US.js"></script>';
} ?>

<style media="screen">
	.progress {
		height:1.5em !important; /* needs to be an absolute measurement unless one of it's parent has an absolute height */
	}
</style>


</body>

<script type="text/javascript">

	<?php if(isset($myScript)){
		echo $myScript;
	} ?>

	$('#idioma_lang').on('change', function(){
		var lan = this.value;
		var dominio="<?php echo base_url().$controllerServer ?>/";
		$.post(dominio+'changeLang',{lang:lan},function(data){
			location.reload();
		});
	})


	function SendToken(){

		$("#setInfo").html('<div class="progress"><div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:100%">Enviando Sms...</div></div>');
		var servidorToken="<?php echo base_url().'MonederoPaper/MassiveSendWelcome'?>";
		$.post(servidorToken,{},function(data){
			if(data!=''){
				$("#setInfo").html(data);
			}
		});
	}

	function ShowFormLoad(){
		$("#ModalFormCreateMonedero").modal('show');
	}

	function SubirFilebatch(){

		$("#FormCreateUserMonedero").submit();
	}


</script>


<script type="text/javascript">



	var servidor="<?php echo base_url().$controllerServer ?>/";
	var rutaForm="<?php echo $rutaForm; ?>";
	var $table = $('#tabla');

	function VerSaldo(cuenta){
		$.post(servidor+'Saldo',{cuenta:cuenta},function(data){
			console.log(data);
		})
	}


	$table.ready(function(){
		$table.bootstrapTable({
			toolbar: ".toolbar",
			classes:'table table-hover table-no-bordered table-condensed',
			clickToSelect: false,
			sortClass:'success',
			showRefresh: true,
			paginationHAlign:'right',
			sidePagination:'server',
			search: true,
			iconsPrefix:'btn-xs',
			rowStyle:function rowStyle(row, index) {
				return {
					classes: '',
					css: {}
				};
			},
			height:450,
			detailView:false,
			iconSize:'',
			escape:false,
			radio:true,
			checkbox:true,
			striped:false,
			searchOnEnterKey:true,
			showToggle: false,
			showColumns: true,
			locale:'<?php echo $localeForTable; ?>',
			pagination: true,
			searchAlign: 'right',
			refresh:{silent:true},
			pageList: [50,100,200,500],
			pageSize:50,
			paginationVAlign:'bottom',
			silentSort:true,
			showPaginationSwitch:false,
			url:servidor+'<?php if(isset($populate)){echo $populate;} ?>',
			method:'post',
			contentType:'application/json',
			dataType:'json',
			queryParams: function (p) {
				return { 'limit':p.limit,
					'offset':p.offset,
					'sort':p.sort,
					'sortOrder':p.order,
					'search':p.search,
					'inicio':$("#inicio").val(),
					'fin': $("#fin").val()
				};
			},
			queryParamsType:'limit',

			columns:[
				<?php echo $columns; ?>

			],

			formatShowingRows: function(pageFrom, pageTo, totalRows){
				//do nothing here, we don't want to show the text "showing x of y from..."
			},
			formatRecordsPerPage: function(pageNumber){
				return pageNumber + " rows visible";
			},
			icons: {
				refresh: 'fas fa-sync-alt',
				toggle: 'fas fa-list-ol',
				columns: 'fas fa-columns',
				detailOpen: 'fas fa-plus-circle',
				detailClose: 'fas fa-minus-circle'
			},



		});
		//activate the tooltips after the data table is initialized
		$('[rel="tooltip"]').tooltip();

		$(window).resize(function () {
			$table.bootstrapTable('resetView');
		});

		window.operateEvents = {

			'click .edit': function (e, value, row, index) {
				$.post(rutaForm,{id:row['id']},function(data){
					$("#modalFormContent").html(data);
					$("#ModalApp").modal('show');
				});
			},
			'click .remove': function (e, value, row, index) {
				info = JSON.stringify(row);
				swal({
					title: '<?php echo $this->lang->line('are_you_sure'); ?>',
					text: '<?php echo $this->lang->line('advertencia_eliminar'); ?>',
					type: 'warning',

					showCancelButton: true,
					confirmButtonText: '<?php echo $this->lang->line('yes_delete_it'); ?>',
					cancelButtonText: '<?php echo $this->lang->line('no_sure'); ?>',
					confirmButtonClass: "btn btn-success btn-fill",
					cancelButtonClass: "btn btn-danger btn-fill",
					buttonsStyling: false
				}).then(function(){
					$.post(servidor+"delete",{id:row['id'],},function(data){
						swal({
							title: '<?php echo $this->lang->line('Attention'); ?>',
							html: data,
							type: 'success',
							confirmButtonClass: "btn btn-success btn-fill",
							buttonsStyling: false
						})

					})
					$table.bootstrapTable('refresh');
				}, function(dismiss) {
					// dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
					if (dismiss === 'cancel') {
						swal({
							title: '<?php echo $this->lang->line('cancelled'); ?>',
							text: '<?php echo $this->lang->line('element_safe'); ?>',
							type: 'error',
							confirmButtonClass: "btn btn-info btn-fill",
							buttonsStyling: true
						})
					}
				})

			}
		};

	});



	function operateFormatter(value, row, index) {
		return ['<a rel="tooltip" title="Editar" class="btn btn-simple btn-sm btn-info  table-action edit" href="javascript:void(0)">',
			'<i class="fas fa-edit"></i>',
			'</a>',
			'<a rel="tooltip" title="Eliminar" class="btn btn-simple btn-sm btn-danger  table-action remove" href="javascript:void(0)">',
			'<i class="fas fa-trash"></i>',
			'</a>'].join('   ');
	}

	function InsertData(){

		$.post(servidor+'formUsers',{},function(data){
			if(data!=''){
				$("#modalFormContent").html(data);
			}
			$("#ModalApp").modal('show');
		});
	}

	function CantidadTransacciones(value, row, index){

		id=value;
		cantidad=0;
		servidor="<?php echo base_url().'MonederoPaper/CantidadTransacciones'?>";
		 $.post(servidor,{id:id,},function(data){
			return data
		});
	}

	function CantidadLogin(value, row, index){

		id=value;
		cantidad=0;
		servidor="<?php echo base_url().'MonederoPaper/CantidadLogin'?>";
		$.post(servidor,{id:id,},function(data){
			return data
		});
	}

	function changeToYesOrNot(value, row, index){
		if(value=='1'){
			return "SI"
		}

		return "NO"
	}

	function Save(){
		var formData = new FormData(document.getElementById("form_page"));
		$("#modalFormContent").html('<div class="loader"></div>');
		$.ajax({
			url: servidor+'save',
			type: "post",
			dataType: "html",
			data: formData,
			cache: false,
			contentType: false,
			processData: false
		})
			.done(function(res){
				if(res!='ok'){
					$("#modalFormContent").html(res);
				}else{
					$table.bootstrapTable('refresh');
					$("#ModalApp").modal('hide');
					swal({
						title: '<?php echo $this->lang->line('Attention'); ?>',
						html: '<p class="alert alert-success"><strong><?php echo $this->lang->line('insertMessageSuccess'); ?></strong></p>',
						type: 'success',
						confirmButtonClass: "btn btn-success btn-fill",
						buttonsStyling: false
					});
				}
			});
	}



	function ExportToPdf(){
		$("#finicio").val($("#inicio").val());
		$("#ffin").val($("#fin").val());
		$("#search").val($(".input-").val());
		$("#form_to_export").submit();
	}

	function ExportToExcel(){
		$("#finicio_e").val($("#inicio").val());
		$("#ffin_e").val($("#fin").val());
		$("#search_e").val($(".input-").val());
		$("#form_to_excel").submit();
	}

	function SearchFunction(){
		var inicio=$("#inicio").val();
		var fin = $("#fin").val();
		var search = $(".input-").val();
		$table.bootstrapTable('showLoading');
		$.post(servidor+"<?php if(isset($populate)){echo $populate;} ?>",{
			inicio:inicio,
			fin:fin,
			search:search,
		},function(data){
			var obj= JSON.parse(data);
			$table.bootstrapTable('hideLoading');
			$table.bootstrapTable('load', obj);
		})
	}

	function ShowPointmap(ruta){
		$("#iframeMap").attr('src',ruta);
		$("#ModalMAP").modal('show');

	}

</script>

<form class="" id="form_to_export" action="<?php if(isset($rutaToExport)){echo $rutaToExport;} ?>" method="post">
	<input type="hidden" id="finicio" name="inicio" value="">
	<input type="hidden" id="ffin" name="fin" value="">
	<input type="hidden" id="search" name="search" value="">
</form>

<form class="" id="form_to_excel" action="<?php if(isset($rutaToExportExcel)){echo $rutaToExportExcel;} ?>" method="post">
	<input type="hidden" id="finicio_e" name="inicio" value="">
	<input type="hidden" id="ffin_e" name="fin" value="">
	<input type="hidden" id="search_e" name="search" value="">
</form>




</html>


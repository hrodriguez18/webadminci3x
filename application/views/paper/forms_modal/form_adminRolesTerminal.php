 <div class="modal-header" >
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h4 class="modal-title"><?php if(isset($title_form)){echo $title_form;} ?></h4>
  </div>
  <div class="modal-body">
    <form action="" id="form_page">
      <div class="form-group">
       <?php if(isset($form)){
        echo $form;
       } ?>
      </div>
    </form>

    <div class="row">

      <div class="col-lg-12">
        <div class="col-lg-6 " >
          <p class="text-success">Roles Actuales</p>
          <ul id="l2" class="list-group list-group-sortable-connected group-actuales">
            <?php if(isset($permisosActuales)){
              echo $permisosActuales;
            } ?>
          </ul>
        </div>
        <div class="col-lg-6 ">
          <p class="text-warning">Roles Disponibles</p>
          <ul id="l2" class="list-group list-group-sortable-connected group-disponibles">
            <?php if(isset($PermisosDisponibles)){
              echo $PermisosDisponibles;
            } ?>

          </ul>
        </div>
      </div>
      <div class="col-lg-12">
        <p class="alert alert-info"><?php echo $this->lang->line('withoutpermission'); ?></p>
      </div>
    </div>

    <script>
      $("#l1,#l2").sortable(
      {

        connectWith:'.connected',

      }
    );

      function EnviarDataSotable(){
        var i=0;
        var data = [];

        var descripcion=$("#descripcion").val();
        var status=$("#status").val();

        $(".group-actuales li").each(function(){

          data[i]=this.id
          i++;
        })
        $("#modal_Content").html('<div class="loader"></div>');
        $.post('https://secure.prestopago.com/CI3/adminRolesTerminal/SaveData',{info:data,descripcion:descripcion,status:status,},function(data){

            if(data!='Todo bien!'){
              $("#modal_Content").html(data)
            }else{
              $('#table_adminRolesTerminal').bootstrapTable('refresh');
                $("#ModalApp").modal('hide');
                swal({
                          title: 'Dato Insertado',
                          text: data,
                          type: 'success',
                          confirmButtonClass: "btn btn-success btn-fill",
                          buttonsStyling: false
                       });
            }

        })
      }





    </script>


  </div>
  <div class="modal-footer">
    <button type="button" class="btn btn-wd btn-fill btn-primary" onclick='EnviarDataSotable()'>Guardar</button>
  </div>

 <div class="modal-header" >
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h4 class="modal-title">Editar Procesador - Biller</h4>
  </div>
  <div class="modal-body">
    <form action="" id="form_page_update">
      <div class="form-group">
        <input type="hidden" value="<?php echo $Cod_Biller; ?>" name='Cod_Biller'>
      </div>
      <div class="form-group">
        <?php echo form_error('Descripcion'); ?>
        <input type="text" class="form-control" placeholder="Nombre de Procesador" name="Descripcion" id="Descripcion" value="<?php echo $Descripcion; ?>">
      </div>
      
    </form>
    
    
  </div>
  <div class="modal-footer">
    <button type="button" class="btn btn-wd btn-fill btn-primary" onclick='UpdateData()'>Actualizar</button>
  </div>
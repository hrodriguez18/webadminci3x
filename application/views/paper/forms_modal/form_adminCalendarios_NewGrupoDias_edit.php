<form action="" id="form_page_update">
  <input type="hidden" value="<?php echo $id; ?>" name="id">
              <div class="form-group" id="grupo_desc">
                <label for="description" class="label label-info">Descripción</label>
                
                <input type="text" class="form-control" name="description" id="description" placeholder="Ingresar Descripción de Grupo" value="<?php if(isset($description)){
                  echo $description;
                } ?>">
                <?php echo form_error('description'); ?>
                <hr>
              </div>
              <div class="form-group">
                <?php if(isset($chk)){
                  echo $chk;
                } ?>
                <hr>
                
              </div>
              <div class="form-group">
                <label for="status" class="label label-info">
                  Estado
                </label>

                <?php 

                  $status=array('A'=>'Activo','I'=>'Inactivo');
                  echo form_dropdown('status',$status,$status_value,'class="form-control"');
                 ?>
                
              </div> 
              <div class="form-group">
                  <button class="btn btn-success" onclick="UpdateDataGrupoDias()">Actualizar</button>
              </div>
            </form>
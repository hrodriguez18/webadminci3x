 <div class="modal-header" >
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h4 class="modal-title">Editar Bin</h4>
  </div>
  <div class="modal-body">
    <form action="" id="form_page_update">
      <div class="form-group">
       <?php if(isset($form)){
        echo $form;
       } ?>
      </div>
      
    </form>
    
    
  </div>
  <div class="modal-footer">
    <button type="button" class="btn btn-wd btn-fill btn-primary" onclick='UpdateData()'>Actualizar</button>
  </div>
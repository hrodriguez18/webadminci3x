<?php echo form_open('UPD');

  $data_session['userid']=$this->session->userdata('userid');
  $data_session['username']=$this->session->userdata('username');
  $data_session['email']=$this->session->userdata('email');
  $data_session['name']=$this->session->userdata('name');
?>
<input type="hidden" name="userid" id="userid" value="<?php echo set_value('userid',$data_session['userid'],true);?>">
<div class="row">
  <div class="col-lg-4"><div class="form-group">
    <label for="username" class="label label-info"><?php echo $this->lang->line('username');?></label>
    <input type="text" class="form-control" name="username" id="username" value="<?php echo set_value('username',$data_session['username'],true)?>" placeholder="Username" readonly>
  </div></div>
  <div class="col-lg-4"><div class="form-group">
    <label for="name" class="label label-info"><?php echo $this->lang->line('name');?></label>
    <input type="text" class="form-control" name="name" id="name" value="<?php echo set_value('name',$data_session['name'],true)?>" placeholder="Nombre" readonly>
  </div></div>
  <div class="col-lg-4"><div class="form-group">
    <label for="email" class="label label-info"><?php echo $this->lang->line('email');?></label>
    <input type="text" class="form-control" name="email" id="email" value="<?php echo set_value('email',$data_session['email'],true)?>" placeholder="Email" readonly>
  </div></div>
  <div class="col-lg-12"><div class="form-group">
    <div class="alert alert-info">
		<?php echo $this->lang->line('infoChangePassword');?>
    </div>
  </div></div>
  <div class="col-lg-4"><div class="form-group">
    <label for="password" class="label label-info"><?php echo $this->lang->line('oldPassword');?></label>
    <input type="password" class="form-control" name="password" id="password" value="" placeholder="<?php echo $this->lang->line('oldPassword_placeholder');?>" >
    <?php echo form_error('password'); ?>
  </div></div>
  <div class="col-lg-4"><div class="form-group">
    <label for="newPassword" class="label label-info"><?php echo $this->lang->line('newPassword');?></label>
    <input type="password" class="form-control" name="newPassword" id="newPassword" value="" placeholder="<?php echo $this->lang->line('newPassword_placeholder');?>" >
    <?php echo form_error('newPassword'); ?>
  </div></div>
  <div class="col-lg-4">
    <div class="form-group">
      <label for="newPasswordConfirm" class="label label-info"><?php echo $this->lang->line('confirm');?></label>
      <input type="password" class="form-control" name="newPasswordConfirm" id="newPasswordConfirm" value="" placeholder="<?php echo $this->lang->line('confirm_placeholder');?>" >
      <?php echo form_error('newPasswordConfirm'); ?>
    </div>
  </div>
  <div class="col-lg-12">
    <div class="form-group">
      <input type="submit" class="btn btn-info" value="<?php echo $this->lang->line('update');?>">
    </div>
  </div>
</div>

<?php echo form_close(); ?>

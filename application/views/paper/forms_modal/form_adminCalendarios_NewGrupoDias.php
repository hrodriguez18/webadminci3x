<form action="" id="form_page">
              <div class="form-group" id="grupo_desc">
                <label for="description" class="label label-info">Descripción</label>
                
                <input type="text" class="form-control" name="description" id="description" placeholder="Ingresar Descripción de Grupo" value="<?php echo set_value('description','',true); ?>">
                <?php echo form_error('description'); ?>
                <hr>
              </div>
              <div class="form-group">
                <label for="" class="label label-info">Seleccionar Días</label>
                <br>
                <div class="checkbox checkbox-inline">
                    <input id="D" type="checkbox" value="D" name="dia[]">
                    <label for="D">
                    Domingo
                    </label>
                </div>
                
                <div class="checkbox checkbox-inline">
                    <input id="L" type="checkbox" value="L" name="dia[]">
                    <label for="L">
                    Lunes
                    </label>
                </div>
                <div class="checkbox checkbox-inline">
                    <input id="M" type="checkbox" value="M" name="dia[]">
                    <label for="M">
                    Martes
                    </label>
                </div>
                <div class="checkbox checkbox-inline">
                    <input id="W" type="checkbox" value="W" name="dia[]">
                    <label for="W">
                    Miercoles
                    </label>
                </div>

                <div class="checkbox checkbox-inline">
                    <input id="J" type="checkbox" value="J" name="dia[]">
                    <label for="J">
                    Jueves
                    </label>
                </div>
              </div>

              <div class="form-group">
                
                <div class="checkbox checkbox-inline">
                    <input id="V" type="checkbox" value="V" name="dia[]">
                    <label for="V">
                    Viernes
                    </label>
                </div>
                <div class="checkbox checkbox-inline">
                    <input id="S" type="checkbox" value="S" name="dia[]">
                    <label for="S">
                    Sábado
                    </label>
                </div>
                <hr>
                
              </div>
              <div class="form-group">
                <label for="status" class="label label-info">
                  Estado
                </label>
                <select name="status" id="status" class="form-control">
                  <option value="A">Activo</option>
                  <option value="I">Inactivo</option>
                </select>
              </div> 
              <div class="form-group">
                  <button class="btn btn-success" onclick="SaveData()">Guardar</button>
              </div>
            </form>
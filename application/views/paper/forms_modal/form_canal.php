 <div class="modal-header" >
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h4 class="modal-title"><?php if(isset($title_form)){echo $title_form;} ?></h4>
  </div>
  <div class="modal-body">
    <form action="" id="form_page">
      <div class="form-group">
        <label for="canal"><?php echo $this->lang->line('channel'); ?></label>
        <input type="text" class="form-control" placeholder="Nombre de Canal" name="Canal" id="Canal" value="<?php echo set_value('Canal'); ?>">
        <?php echo form_error('Canal'); ?>
      </div>

    </form>


  </div>
  <div class="modal-footer">
    <button type="button" class="btn btn-wd btn-fill btn-primary" onclick='SaveData()'>Guardar</button>
  </div>

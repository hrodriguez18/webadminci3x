 <div class="modal-header" >
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h4 class="modal-title">Editar Canal</h4>
  </div>
  <div class="modal-body">
    <form action="" id="form_page_update">
      <div class="form-group">
        <input type="hidden" value="<?php echo $Id; ?>" name='Id'>
      </div>
      <div class="form-group">

        <label for="canal"><?php echo $this->lang->line('channel'); ?></label>
        <input type="text" class="form-control" placeholder="Nombre de Canal" name="Canal" id="Canal" value="<?php echo $Canal; ?>">
        <?php echo form_error('Canal'); ?>
      </div>

    </form>


  </div>
  <div class="modal-footer">
    <button type="button" class="btn btn-wd btn-fill btn-primary" onclick='UpdateData()'>Actualizar</button>
  </div>

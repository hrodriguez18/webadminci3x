 <div class="modal-header" >
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h4 class="modal-title">Moneda</h4>
  </div>
  <div class="modal-body">
    <div class="form-group">

      <label for=""><?php echo $this->lang->line('cod_money'); ?></label>
      <input type="text" class="form-control" placeholder="Codigo de Moneda" name="Cod_Moneda" id="Cod_Moneda" value="<?php echo $Cod_Moneda; ?>" readonly="true">
      <?php echo form_error('Cod_Moneda'); ?>
    </div>
    <div class="form-group">

      <label for=""><?php echo $this->lang->line('description'); ?></label>
      <textarea name="Descripcion" id="Descripcion" style="resize: none" class="form-control" placeholder="Descripcion de Moneda" rows="5"><?php echo $Descripcion; ?></textarea>
      <?php echo form_error('Descripcion'); ?>
    </div>
    <div class="form-group">

      <label for=""><?php echo $this->lang->line('label'); ?></label>
      <input type="text" placeholder="Etiqueta" name="DescripcionCorta" class="form-control" id="descorta" value="<?php echo $DescripcionCorta; ?>">
      <?php echo form_error('DescripcionCorta'); ?>
    </div>
  </div>
  <div class="modal-footer">
    <button type="button" class="btn btn-wd btn-fill btn-primary" onclick='UpdateMoneda()'>Actualizar</button>
  </div>

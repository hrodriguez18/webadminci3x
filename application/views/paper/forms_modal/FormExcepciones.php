<form action="#" id="form_page">
	<?php
		$id=array('name'=>'id','id'=>'id','value'=>'','type'=>'hidden');
		$description=array('name'=>'description','id'=>'description','value'=>'','placeholder'=>'Descripción','class'=>'form-control');
		$status=array('A'=>'Activo','I'=>'Inactivo');

		$id['value']=set_value('id','',true);
		$description['value']=set_value('description','',true);
		echo form_input($id);
	?>

	<div class="row">
		<div class="col-lg-12">
			<div class="form-group">
				<label for="description" class="label label-warning">Descripción</label>
				<?php echo form_input($description).form_error('description'); ?>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="form-group">
				<label for="description" class="label label-warning">Status</label>
				<?php echo form_dropdown('status',$status,set_value('status','A',true),'class="form-control"'); ?>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-10">
			<div class="form-group">
				<button class="btn btn-success" type="button" onclick="AddDay()">Añadir Día</button>
				<button class="btn btn-danger" type="button" onclick="RemoveDate()">Limpiar Todo</button>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div >
				<style>
					.table_date>td,th{
						margin:5px auto !important;
					}
					th{
						padding: 2px !important;
					}
					td{
						padding: 2px !important;
					}
				</style>
				<table id="table_date" class="table table-striped table-hover table-condensed table-bordered">
					<thead>
						<tr>
							<th>Excepeciones de Fechas</th>
						</tr>
					</thead>
					<tbody id="content_day_form">

					</tbody>
				</table>
			</div>
		</div>
	</div>
	<p class="alert alert-info"><?php echo $this->lang->line('infoExcep');?></p>
</form>

<script>
	function AddDay(){
		$("#content_day_form").append('<tr><td><input type="date" name="day[]" class="form-control"/></td></tr>');
	}

	function RemoveDate(){

		$('#content_day_form').html('');
	}
</script>

 <div class="modal-header" >
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h4 class="modal-title">Editar Bines Bloqueados (<?php if(isset($idGrupo)){
      echo $idGrupo;
    } ?>).</h4>
  </div>
  <input type="hidden" name="idGrupo" id="idGrupo" value="<?php echo $idGrupo; ?>">
  <div class="modal-body">
    <div class="row">
      <div class="col-lg-12">
           <div class="form-group">
             <label for="descripcion">Descripcion</label>
             <?php echo form_error('descripcion'); ?>
             <input type="text" class="form-control" placeholder="Descripcion de Bloqueo" name="descripcion" id="descripcion" value="<?php echo $descripcion; ?>" readonly="true">
          </div>
      </div>
    </div>

    <div class="row">
      <div class="col-lg-12">
           <div class="form-group">
             <label for="descripcion">Grupo</label>
             <?php echo form_error('grupo'); ?>
             <input type="text" class="form-control" placeholder="Nombre de Grupo" name="grupo" id="grupo" value="<?php echo $GRUPO; ?>" readonly="true">
          </div>
      </div>
    </div>

    <div class="row">

      <div class="col-lg-12">
        <div class="col-lg-6 " >
          <p class="text-danger">Bin Bloqueado</p>
          <ul id="l1" class="list-group list-group-sortable-connected group-block">
            <?php if(isset($ListabinesBloqueados)){
              echo $ListabinesBloqueados;
            } ?>

          </ul>
        </div>
        <div class="col-lg-6 ">
          <p class="text-success">Bin Disponible</p>
          <ul id="l2" class="list-group list-group-sortable-connected group-stock">
            <?php if(isset($ListabinesDesbloqueados)){
              echo $ListabinesDesbloqueados;
            } ?>

          </ul>
        </div>

      </div>

      <div class="col-lg-12">
        <p class="alert alert-info"><?php echo $this->lang->line('empty_bines'); ?></p>
      </div>

    </div>

    <script>
      $("#l1,#l2").sortable(
      {

        connectWith:'.connected',

      }
    );

      function ActualizarBinTerminal(){
        var i=0;
        var data = [];

        var descripcion=$("#descripcion").val();
        var nombreGrupo=$("#grupo").val();
        var idgrupo=$("#idGrupo").val();


        $(".group-block li").each(function(){

          data[i]=this.id
          i++;
        })
        $("#modal_Content").html('<div class="loader"></div>');
        $.post('https://secure.prestopago.com/CI3/AdminBinTerminal/UpdateData',{info:data,descripcion:descripcion,grupo:nombreGrupo,idGrupo:idgrupo},function(data){

            if(data!='Todo bien!'){
              $("#modal_Content").html(data)
            }else{
              $('#table_binTerminal').bootstrapTable('refresh');
                $("#ModalApp").modal('hide');
                swal({
                          title: 'Dato Insertado',
                          text: data,
                          type: 'success',
                          confirmButtonClass: "btn btn-success btn-fill",
                          buttonsStyling: false
                       });
            }

        })
      }





    </script>



  </div>
  <div class="modal-footer">
    <button type="button" class="btn btn-wd btn-fill btn-primary" onclick='ActualizarBinTerminal()'>Actualizar</button>
  </div>

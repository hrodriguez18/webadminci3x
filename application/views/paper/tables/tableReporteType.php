<div class="toolbar">
    <!--<button class="btn btn-success btn-sm" onclick="InsertData()" ><?php echo $this->lang->line('btn-add'); ?></button>-->
    <button class="btn btn-danger " onclick="ExportToPdf()"><i class="fas fa-file-pdf"></i></span></button>
    <button class="btn btn-primary "><i class="fas fa-file-excel"></i></button>
</div>
  <form class="form-inline" action="<?php echo base_url() ?>CnbPaper/ExportToPdfByType" id="form_search_cnbTipo" method="post">
    <div class="row">
      <div class="col-lg-3"><div class="form-group">
        <label for="">Fecha Inicial</label>
        <input type="date" class="form-control" name="fecha_inicio" value="<?php echo date('Y-m-d'); ?>">
      </div></div>
      <div class="col-lg-3"><div class="form-group">
        <label for="">Fecha Final</label>
        <input type="date" class="form-control" name="fecha_fin" value="<?php echo date('Y-m-d'); ?>">
      </div></div>
      <div class="col-lg-3"><div class="form-group">
        <label for="">Tipo</label>
        <select class="form-control" name="tipo" id="tipo">
          <option value="Debito CNB">Débito CNB</option>
          <option value="Credito CNB">Crédito CNB</option>
          <option value="Credito CNB(Compra)">Crédito CNB(Compra)</option>
        </select>
      </div></div>
      <div class="col-lg-1">
        <div class="form-group">
          <label for="">Buscar</label>
          <button type="button" role="button" onclick="UpdateDataByDate()" class="btn btn-info" name="button"><i class="fas fa-search"></i></button>
        </div>
      </div>
    </div>
  </form>



<table id="tableCnbByType" class="table" >

</table>
<script type="text/javascript">
  function ExportToPdf(){
    $("#form_search_cnbTipo").submit();
  }
</script>

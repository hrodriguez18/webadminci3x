<div class="toolbar">
    <button class="btn-success btn btn-sm" onclick="insertNew()"><?php echo $this->lang->line('btn-add'); ?></button>
    <!--<button class="btn btn-danger btn-sm">PDF <i class="ti-file"></i></span></button>
    <button class="btn btn-primary btn-sm">XLS <i class="ti-file"></i></button>
--></div>
<table id="table_Horario" class="table" >

</table>

  <!-- Modal -->
  <div class="modal fade" id="ModalApp" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content" >
      	<div class="modal-header">
      		<button type="button" class="close" data-dismiss="modal">&times;</button>
      		<h4 class="modal-title">Horario</h4>
      	</div>
      	<div class="modal-body" id="modal_Content">

      	</div>
      	<div class="modal-footer">
      		<button class="btn btn-success" onclick="ValidarHorarios()">Guardar</button>
      	</div>
      </div>
    </div>
</div>


<script>
  function ValidarHorarios(){
     var formData = new FormData(document.getElementById("form_horarios"));

    $("#modal_Content").html('<div class="loader"></div>');
    $.ajax({
        url: servidor+'SaveHorario',
        type: "post",
        dataType: "html",
        data: formData,
        cache: false,
        contentType: false,
        processData: false
    })
      .done(function(res){

        if(res!='Todo bien!'){
          $("#modal_Content").html(res)
        }else{
          $table.bootstrapTable('refresh');
          $("#ModalApp").modal('hide');
          swal({
                    title: 'Dato Insertado',
                    text: res,
                    type: 'success',
                    confirmButtonClass: "btn btn-success btn-fill",
                    buttonsStyling: false
                 });
        }
      });
  }
</script>

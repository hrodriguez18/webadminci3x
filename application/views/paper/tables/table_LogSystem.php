<div class="toolbar">
<!--  <button class="btn-success btn btn-sm" onclick="insertNew()"><?php echo $this->lang->line('btn-add'); ?></button>
-->  <button class="btn btn-danger btn-sm" onclick="ExportToPdf()">PDF <i class="ti-file"></i></span></button>
 <!-- <button class="btn btn-primary btn-sm">XLS <i class="ti-file"></i></button>-->
</div>

<form class="form-inline" action="<?php echo base_url() ?>LogControl/Export" id="form_pdf" method="post">
  <div class="row">
    <div class="col-lg-3"><div class="form-group">
      <label for="">Fecha Inicial</label>
      <input type="date" class="form-control" name="fecha_inicio" id="field_fecha_inicio" value="<?php echo date('Y-m-d'); ?>">
    </div></div>
    <div class="col-lg-3"><div class="form-group">
      <label for="">Fecha Final</label>
      <input type="date" class="form-control" id="field_fecha_fin"  name="fecha_fin" value="<?php echo date('Y-m-d'); ?>">
    </div></div>
    <div class="col-lg-1">
      <div class="form-group">

        <label for="">Buscar</label>
        <button type="button" role="button" onclick="UpdateDataByDate()" class="btn btn-info btn-sm" name="button"><i class="ti-search"></i></button>
      </div>
    </div>
  </div>
</form>
<table id="table_LogSystem" class="table" >

</table>
<script type="text/javascript">
  function ExportToPdf(){
    $("#form_pdf").attr('action',"<?php echo base_url() ?>LogControl/Export");
    $("#form_pdf").submit();
  }

  function UpdateDataByDate(){
	  var inicio=$("#field_fecha_inicio").val();
	  var fin=$("#field_fecha_fin").val();
	  $table.bootstrapTable('refresh',{query:{fecha_inicio:inicio,fecha_fin:fin}});
  }
</script>

<!--var dataForm = new FormData(document.getElementById("form_pdf"));
$.ajax({
url:"<?php echo base_url()?>LogControl/populateTable",
type:"post",
dataType:"html",
data:dataForm,
cache:false,
contentType:false,
processData:false
}).done(function(res){
$table.bootstrapTable('load',JSON.parse(res));
});-->

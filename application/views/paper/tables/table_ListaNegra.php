<div class="toolbar">
 <button class="btn-success btn btn-sm" onclick="InsertData()"><?php echo $this->lang->line('btn-add'); ?></button>

 <button class="btn btn-danger" onclick="ExportToPdf()" data-toggle="tooltip" title="Exportar PDF" data-placement="bottom"><i class="fas fa-file-pdf fa-lg"></i></span> </button>
  <!--<button class="btn btn-primary" data-toggle="tooltip" title="Exportar XLS" data-placement="bottom"> <i class="fas fa-file-excel fa-lg"></i></button>-->
   <button class="btn-info btn" data-toggle="tooltip" title="Subir .csv" data-placement="bottom" onclick="formCsv()"><i class="fas fa-cloud-upload-alt fa-lg"></i></span></button>
</div>

<form class="form-inline" action="<?php echo base_url() ?>ListaNegra_c/Export" id="form_pdf" method="post">
  <input type="hidden" name="nada" value="">
</form>
<table id="tableListaNegra" class="table" >

</table>
<script type="text/javascript">
  function ExportToPdf(){
    $("#form_pdf").submit();
  }

  function formCsv(){
    var data='<form id="formbkl"><div class="form-group"><label>Archivo .csv</label><input class="form-control" name="csv" value="" type="file"/></div></form>'
    $("#form_csv").html(data);
    $("#ModalBatch").modal('show');
  }

  function subirBatch(){
    var formData = new FormData(document.getElementById("formbkl"));

  	$("#form_csv").html('<div class="loader"></div>');
  	$.ajax({

  	    url: servidor+'populateBatch',
  	    type: "post",
  	    dataType: "html",
  	    data: formData,
  	    cache: false,
  	    contentType: false,
  	    processData: false
  	})
      .done(function(res){
      	if(res!=''){
      		$("#form_csv").html(res+'<form id="formbkl"><div class="form-group"><label>Archivo .csv</label><input class="form-control" name="csv" value="" type="file"/></div></form>');
      	}else{
      		$table.bootstrapTable('refresh');
      		$("#ModalBatch").modal('hide');
      		swal({
  	                title: 'Inserción Completada',
  	                text: res,
  	                type: 'success',
  	                confirmButtonClass: "btn btn-success btn-fill",
  	                buttonsStyling: false
  	             });
      	}
      });
  }

</script>

<div class="alert alert-warning">
  <p><strong>Para cargar el archivo con datos de lista negra, debe seguir con los siguientes parametros:</strong></p>
  <p><strong>|TELEFONO| , |CEDULA| , |ESTADO(A,I)| , |EMAIL|. El formato debe ser .csv delimitado por comas(,).</strong></p>
</div>

<div class="toolbar">
    <button class="btn-success btn btn-sm" onclick="insertNew()"><?php echo $this->lang->line('btn-add'); ?></button>
    <button class="btn btn-danger btn-sm" onclick="ExportToPdf()">PDF <i class="ti-file"></i></span></button>
    <!--<button class="btn btn-primary btn-sm">XLS <i class="ti-file"></i></button>-->
</div>
<form class="form-inline" action="<?php echo base_url() ?>AdminOperadores/Export" id="formToExport" method="post">
<input type="hidden" name="id" value="<?php echo $this->session->userdata('userid'); ?>">
</form>
<table id="table_operadores" class="table" >

</table>

<script type="text/javascript">
  function ExportToPdf(){
    $("#formToExport").submit();
  }
</script>

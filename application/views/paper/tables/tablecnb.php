<div class="toolbar">
    <!--<button class="btn btn-success btn-sm" onclick="InsertData()" ><?php echo $this->lang->line('btn-add'); ?></button>-->
    <button class="btn btn-danger btn-sm" onclick="ExportToPdf()">PDF <i class="ti-file"></i></span></button>
    <button class="btn btn-primary btn-sm">XLS <i class="ti-file"></i></button>

</div>
  <form class="form-inline" action="<?php echo base_url() ?>ExportPdf" id="form_search_cnb" method="post">
    <div class="row">
      <div class="col-lg-3"><div class="form-group">
        <label for="">Fecha Inicial</label>
        <input type="date" class="form-control" name="fecha_inicio" value="<?php echo date('Y-m-d'); ?>">
      </div></div>
      <div class="col-lg-3"><div class="form-group">
        <label for="">Fecha Final</label>
        <input type="date" class="form-control" name="fecha_fin" value="<?php echo date('Y-m-d'); ?>">
      </div></div>
      <div class="col-lg-1">
        <div class="form-group">

          <label for="">Buscar</label>
          <button type="button" role="button" onclick="UpdateDataByDate()" class="btn btn-info" name="button"><i class="fas fa-search"></i></button>
        </div>
      </div>
    </div>
  </form>



<table id="table_cnb" class="table" >

</table>
<script type="text/javascript">
  function ExportToPdf(){
    $("#form_search_cnb").submit();
  }


</script>

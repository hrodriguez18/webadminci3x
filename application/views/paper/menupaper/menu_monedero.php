<li>
	<a  href="<?php echo base_url() ?>Wallet/Users">
		<i class="fas fa-wallet"></i>
		<p><?php echo $this->lang->line('walletUsers'); ?></p>
	</a>
</li>
<li>
	<a  href="<?php echo base_url() ?>Wallet/CountTrxUsers">
		<i class="fas fa-info-circle"></i>
		<p><?php echo $this->lang->line('detailTrxByUsers'); ?></p>
	</a>
</li>
<li >
	<a  href="<?php echo base_url() ?>Wallet/Transacciones">
		<i class="fas fa-layer-group"></i>
		<p><?php echo $this->lang->line('logTx'); ?></p>
	</a>
</li>
<li >
	<a  href="<?php echo base_url() ?>Wallet/EstadosDeCuentas">
		<i class="fas fa-calculator"></i>
		<p><?php echo $this->lang->line('accountStatus'); ?></p>
	</a>
</li>

<li >
	<a  href="<?php echo base_url() ?>Wallet/ComisionMonedero">
		<i class="fas fa-money-bill-alt"></i>
		<p><?php echo $this->lang->line('commissionWallet'); ?></p>
	</a>
</li>

<?php
	echo '<li >
		<a  href="'.base_url().'SupportWallet">
			<i class="fas fa-user-cog"></i>
			<p>'.$this->lang->line('customerSupport').'</p>
		</a>
	</li>';
?>

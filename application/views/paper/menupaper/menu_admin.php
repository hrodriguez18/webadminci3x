

<?php if(array_key_exists (8,$this->mis_permisos)){
	echo '<li  class"active">
		<a  href="'. base_url().'Admin/Monedas" >
			<i class="fas fa-dollar-sign"></i>
			<p>'.$this->lang->line('currency').'</p>
		</a>
	</li>';
}


if(array_key_exists (7,$this->mis_permisos)){
	echo '<li >
		<a  href="'.base_url().'Admin/Paises">
			<i class="fas fa-globe-americas"></i>
			<p>'.$this->lang->line('country').'</p>
		</a>
	</li>';}

	if(array_key_exists (14,$this->mis_permisos)){
	echo '<li >
		<a  href="'.base_url().'Admin/Canales">
			<i class="fas fa-archive"></i>
			<p>'.$this->lang->line('Channels').'</p>
		</a>
	</li>';}

if(array_key_exists (9,$this->mis_permisos)){
	echo '<li >
		<a  href="'.base_url().'Admin/Procesadores">
			<i class="fas fa-store-alt"></i>
			<p>'.$this->lang->line('biller').'</p>
		</a>
	</li>';
}


if(array_key_exists (24,$this->mis_permisos)){
	echo '<li >
		<a  href="'.base_url().'Admin/Merchants">
			<i class="fas fa-dolly-flatbed"></i>
			<p>'.$this->lang->line('merchants').'</p>
		</a>
	</li>';

}


if(array_key_exists (1,$this->mis_permisos)){
	echo '<li >
		<a  href="'.base_url().'Admin/Operadores">
			<i class="fas fa-hand-holding-usd"></i>
			<p>'.$this->lang->line('operators').'</p>
		</a>
	</li>';
}


if(array_key_exists (2,$this->mis_permisos)){
	echo '<li >
		<a  href="'. base_url().'Admin/Agencias">
			<i class="fas fa-industry"></i>
			<p>'.$this->lang->line('agency').'</p>
		</a>
	</li>';
}


if(array_key_exists (145,$this->mis_permisos)){
	echo '<li >
		<a  href="'. base_url().'ListaNegra">
			<i class="fas fa-lock"></i>
			<p>'.$this->lang->line('blacklist').'</p>
		</a>
	</li>';
}

if(array_key_exists (158,$this->mis_permisos)){
	echo '<li >
		<a  href="'. base_url().'Contrato">
			<i class="fas fa-file-contract"></i>
			<p>'.$this->lang->line('contrato').'</p>
		</a>
	</li>';
}

if(array_key_exists (158,$this->mis_permisos)){
	echo '<li >
		<a  href="'. base_url().'Faq">
			<i class="fas fa-question-circle"></i>
			<p>'.$this->lang->line('faq').'</p>
		</a>
	</li>';
}


if(array_key_exists (164,$this->mis_permisos)){
	echo '<li >
		<a  href="'. base_url().'ActividadComercial">
			<i class="fas fa-building"></i>
			<p>'.$this->lang->line('actividadComercial').'</p>
		</a>
	</li>';
}




if(array_key_exists (157,$this->mis_permisos)){
	echo '<li >
		<a  href="'. base_url().'WINPT">
			<i class="fas fa-money-check-alt"></i>
			<p>'.$this->lang->line('WINPT').'</p>
		</a>
	</li>';
}

//==================================================================================


if(array_key_exists (167,$this->mis_permisos) || array_key_exists (178,$this->mis_permisos)){
	echo '<li >
		<a  data-toggle="collapse" href="#depositos">
			<i class="fas fa-retweet"></i>
			<p>'.$this->lang->line('depositos_retiros').' <b class="caret"></b></p>
		</a>
		<div class="collapse" id="depositos">
		
		
			<ul class="nav">';


	if(array_key_exists (178,$this->mis_permisos)){
		echo '<li >
		<a  href="'. base_url().'Dispersion">
			<i class="fas fa-exchange-alt"></i>
			<p>'.$this->lang->line('depositos').'</p>
		</a>
	</li>';
	}

	if(array_key_exists (183,$this->mis_permisos)){
		echo '<li >
		<a  href="'. base_url().'Retiros">
			<i class="fas fa-exchange-alt"></i>
			<p>'.$this->lang->line('retiros').' Billetera</p>
		</a>
	</li>';
	}

	if(array_key_exists (167,$this->mis_permisos)){
		echo '<li >
		<a  href="'. base_url().'DepositosCnb">
			<i class="fas fa-exchange-alt"></i>
			<p>'.$this->lang->line('depositoscnb').'</p>
		</a>
	</li>';
	}

	if(array_key_exists (168,$this->mis_permisos)){
		echo '<li >
		<a  href="'. base_url().'RetirosCnb">
			<i class="fas fa-exchange-alt"></i>
			<p>'.$this->lang->line('retiros').' CNB</p>
		</a>
	</li>';
	}






	echo '</ul></div></li>';

}

//=======================================================================================


if(array_key_exists (208,$this->mis_permisos)){
	echo '<li >
		<a  href="'. base_url().'LimitesMonedero">
			<i class="fab fa-google-wallet"></i>
			<p>'.$this->lang->line('limitesMonedero').'</p>
		</a>
	</li>';
}

//=======================================================================================


if(array_key_exists (79,$this->mis_permisos)){

	echo '<li >
		<a  data-toggle="collapse" href="#monetizacion">
			<i class="fas fa-coins"></i>
			<p>'.$this->lang->line('monetization').' <b class="caret"></b></p>
		</a>
		<div class="collapse" id="monetizacion">
			<ul class="nav">';
			if(array_key_exists (80,$this->mis_permisos)){
				echo '<li>
					<a href="'.base_url().'Admin/Transacciones">
						<span class="sidebar-mini">TR</span>
						<span class="sidebar-normal">'.$this->lang->line('transactions').'</span>
					</a>
				</li>';
			}

			if(array_key_exists (84,$this->mis_permisos)){
				echo '<li>
					<a href="'.base_url().'Admin/Montos">
						<span class="sidebar-mini">PC</span>
						<span class="sidebar-normal">'.$this->lang->line('commissions').'</span>
					</a>
				</li>';
			}

			if(array_key_exists (88,$this->mis_permisos)){
				echo '<li>
					<a href="'.base_url().'Admin/LimitesTransacciones">
						<span class="sidebar-mini">RT</span>
						<span class="sidebar-normal">'.$this->lang->line('TransactionsLimits').'</span>
					</a>
				</li>';
			}

			if(array_key_exists (91,$this->mis_permisos)){
				echo '<li>
						<a href="'.base_url().'Admin/GrupoTransacciones">
							<span class="sidebar-mini">GT</span>
							<span class="sidebar-normal">'.$this->lang->line('TransactionsGroup').'</span>
						</a>
					</li>';
			}

			echo '</ul></div></li>';

}



if(array_key_exists (78,$this->mis_permisos)){
	echo '<li >
		<a  data-toggle="collapse" href="#Cal">
			<i class="fas fa-calendar-alt"></i>
			<p>'.$this->lang->line('scheduling').' <b class="caret"></b></p>
		</a>
		<div class="collapse" id="Cal">
			<ul class="nav">';

			if(array_key_exists (96,$this->mis_permisos)){
				echo '<li>
					<a href="'.base_url().'Admin/Excepcion">
						<span class="sidebar-mini">ED</span>
						<span class="sidebar-normal">'.$this->lang->line('exception').'</span>
					</a>
				</li>';
			}

			if(array_key_exists (99,$this->mis_permisos)){
				echo '<li>
					<a href="'.base_url().'Admin/Horarios">
						<span class="sidebar-mini">CA</span>
						<span class="sidebar-normal">'.$this->lang->line('schedule').'</span>
					</a>
				</li>';
			}

			echo '</ul></div></li>';
}


if(array_key_exists (194,$this->mis_permisos)){
	echo '<li >
		<a  data-toggle="collapse" href="#TerminalItems">
			<i class="fas fa-tablet-alt"></i>
			<p>'.$this->lang->line('terminal').' <b class="caret"></b></p>
		</a>
		<div class="collapse" id="TerminalItems">
			<ul class="nav">';

			if(array_key_exists (194,$this->mis_permisos)){
				echo '<li>
					<a href="'.base_url().'Admin/Terminales">
						<span class="sidebar-mini">TE</span>
						<span class="sidebar-normal">'.$this->lang->line('agenteCorresponsal').'</span>
					</a>
				</li>';
			}

			if(array_key_exists (28,$this->mis_permisos)){
				echo '<li>
					<a href="'.base_url().'Admin/Bines">
						<span class="sidebar-mini">BI</span>
						<span class="sidebar-normal">'.$this->lang->line('bines').'</span>
					</a>
				</li>';
			}

			if(array_key_exists (107,$this->mis_permisos)){
				echo '<li>
					<a href="'.base_url().'Admin/BinTerminal">
						<span class="sidebar-mini">GB</span>
						<span class="sidebar-normal">'.$this->lang->line('grupoBines').'</span>
					</a>
				</li>';
			}

			if(array_key_exists (25,$this->mis_permisos)){
				echo '<li>
					<a href="'.base_url().'Admin/Limites">
						<span class="sidebar-mini">LM</span>
						<span class="sidebar-normal">'.$this->lang->line('limits').'</span>
					</a>
				</li>';
			}

			if(array_key_exists (115,$this->mis_permisos)){
				echo '<li>
							<a href="'.base_url().'Admin/UsersTerminales">
								<span class="sidebar-mini">UT</span>
								<span class="sidebar-normal">'.$this->lang->line('userterminal').'</span>
							</a>
						</li>';
			}

			if(array_key_exists (111,$this->mis_permisos)){
				echo '<li>
					<a href="'.base_url().'Admin/RolesTerminal">
						<span class="sidebar-mini">RO</span>
						<span class="sidebar-normal">'.$this->lang->line('rolesterminal').'</span>
					</a>
				</li>';
			}



		echo '</ul></div></li>';
}


if(array_key_exists (10,$this->mis_permisos)){
	echo '<li >
		<a  data-toggle="collapse" href="#usersItems">
			<i class="ti-user"></i>
			<p>'.$this->lang->line('userssystem').'<b class="caret"></b></p>
		</a>
		<div class="collapse" id="usersItems">
			<ul class="nav">';

			if(array_key_exists (10,$this->mis_permisos)){
				echo '<li>
					<a href="'.base_url().'Users">
						<span class="sidebar-mini">US</span>
						<span class="sidebar-normal">'.$this->lang->line('userssystem').'</span>
					</a>
				</li>';
			}

			if(array_key_exists (4,$this->mis_permisos)){
				echo '<li>
					<a href="'.base_url().'RolesSistema">
						<span class="sidebar-mini">RL</span>
						<span class="sidebar-normal">'.$this->lang->line('roles').'</span>
					</a>
				</li>';
			}

			echo '</ul></div></li>';

			if(array_key_exists (74,$this->mis_permisos)){
				echo '<li >
					<a  href="'.base_url().'LogControl">
						<i class="fas fa-eye"></i>
						<p>'.$this->lang->line('logs').'</p>
					</a>
				</li>';
			}

			if(array_key_exists (149,$this->mis_permisos)){
				echo '<li >
					<a  href="'.base_url().'Email">
						<i class="fas fa-envelope"></i>
						<p>'.$this->lang->line('email').'</p>
					</a>
				</li>';
			}
}

 ?>
 <hr>


			<!--<li>
				<a href="<?php echo base_url() ?>Admin/GrupoBines">
					<span class="sidebar-mini">GB</span>
					<span class="sidebar-normal">Grupo Bines</span>
				</a>
			</li>-->


			<!--<li>
				<a href="<?php echo base_url();?>Admin/PermisosTerminal">
					<span class="sidebar-mini">PE</span>
					<span class="sidebar-normal">Permisos</span>
				</a>
			</li>-->

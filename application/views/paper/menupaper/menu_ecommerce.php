<?php

echo '<li >
				<a  data-toggle="collapse" href="#ReportesItems">
					<i class="ti-bar-chart"></i>
					<p>'.$this->lang->line('reports').'<b class="caret"></b></p>
				</a>
			<div class="collapse" id="ReportesItems">
				<ul class="nav">';


		if(in_array('Ver Reporte  Diario',$this->mis_permisos)){
			echo '<li>
				<a href="'.base_url().'Ecommerce/Diario">
					<span class="sidebar-mini">DI</span>
					<span class="sidebar-normal">'.$this->lang->line('dreport1').'</span>
				</a>
			</li>';
		}

		if(in_array('Ver Reporte Semanal',$this->mis_permisos)){
			echo '<li>
				<a href="'.base_url().'Ecommerce/Semanal">
					<span class="sidebar-mini">WE</span>
					<span class="sidebar-normal">'.$this->lang->line('wreport1').'</span>
				</a>
			</li>';
		}


		if(in_array('Ver Reporte  Mensual',$this->mis_permisos)){
			echo '<li>
				<a href="'.base_url().'Ecommerce/Mensual">
					<span class="sidebar-mini">SE</span>
					<span class="sidebar-normal">'.$this->lang->line('mreport1').'</span>
				</a>
			</li>';
		}

		if(in_array('Ver Reporte Anual',$this->mis_permisos)){
			echo '<li>
				<a href="'.base_url().'Ecommerce/Anual">
					<span class="sidebar-mini">AN</span>
					<span class="sidebar-normal">'.$this->lang->line('areport1').'</span>
				</a>
			</li>';
		}

		/*if(in_array('Ver Reporte  Personalizado',$this->mis_permisos)){
			echo '<li>
				<a href="'.base_url().'Ecommerce/Personalizado">
					<span class="sidebar-mini">PE</span>
					<span class="sidebar-normal">'.$this->lang->line('creport1').'</span>
				</a>
			</li>';
		}*/
		echo '</ul></div></li>';

?>

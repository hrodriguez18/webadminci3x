<!--<li>
	<a  href="#">
		<i class="ti-money"></i>
		<p>Asignación de Límites</p>
	</a>
</li>-->
<?php
	if(in_array('Ver Reportes CNB',$this->mis_permisos)){
	echo '<li >
	<a  href="'.base_url().'CNB/Reportes">
		<i class="fas fa-receipt"></i>
		<p>'.$this->lang->line('reportTrx').'</p>
	</a>
</li>';
 }

?>


<li >
	<a  href="<?php echo base_url(); ?>CNB/CNBInstalados">
		<i class="fas fa-tasks"></i>
		<p><?php echo $this->lang->line('cnbinstalados'); ?></p>
	</a>
</li>

<li >
	<a  href="<?php echo base_url(); ?>CNB/ReportesType">
		<i class="fas fa-sitemap"></i>
		<p><?php echo $this->lang->line('reportbytype'); ?></p>
	</a>
</li>

<li >
	<a  href="<?php echo base_url(); ?>CNB/Ingresos">
		<i class="fas fa-exchange-alt"></i>
		<p><?php echo $this->lang->line('incomereports'); ?></p>
	</a>
</li>

<li >
	<a  href="<?php echo base_url(); ?>CNB/Comisiones">
		<i class="fas fa-coins"></i>
		<p><?php echo $this->lang->line('commissionreports'); ?></p>
	</a>
</li>

<li >
	<a  href="<?php echo base_url(); ?>CNB/EstadodeCuentas">
		<i class="fas fa-calculator"></i>
		<p><?php echo $this->lang->line('accountstatements'); ?></p>
	</a>
</li>

<?php if(in_array('Soporte CNB',$this->mis_permisos)){
	echo '<li >
		<a  href="'. base_url().'SupportCNB">
			<i class="fas fa-user-cog"></i>
			<p>'.$this->lang->line('customerSupport').'</p>
		</a>
	</li>';
} ?>

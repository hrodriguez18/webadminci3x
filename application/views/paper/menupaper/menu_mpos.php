<!--<li >
	<a  href="#">
		<i class="ti-comment-alt"></i>
		<p>Mensajes Push</p>
	</a>
</li>-->

<?php
		if(in_array('Ver FAQ',$this->mis_permisos)){
			echo '<li >
				<a  href="'.base_url().'MPOS/FAQS">
					<i class="ti-info"></i>
					<p>FAQ</p>
				</a>
			</li>';
		}

		if(in_array('Ver Accessos MPOS',$this->mis_permisos)){
			echo '<li >
				<a  href="'.base_url().'MPOS/Accesos">
					<i class="ti-alert"></i>
					<p>'.$this->lang->line('logLogin').'</p>
				</a>
			</li>';
		}

		if(in_array('Ver Transacciones MPOS',$this->mis_permisos)){
			echo '<li >
				<a  href="'.base_url().'MPOS/Transacciones">
					<i class="ti-harddrives"></i>
					<p>'.$this->lang->line('transactions').'</p>
				</a>
			</li>';
		}

		if(in_array('Ver Alertas MPOS',$this->mis_permisos)){
			echo '<li >
				<a  href="'.base_url().'MPOS/Alertas">
					<i class="ti-alert"></i>
					<p>'.$this->lang->line('alertas').'</p>
				</a>
			</li>';
		}


 ?>



<!--
<li >
	<a  data-toggle="collapse" href="#ReportesItems">
		<i class="ti-mobile"></i>
		<p>Reportes <b class="caret"></b></p>
	</a>
	<div class="collapse" id="ReportesItems">
		<ul class="nav">
			<li>
				<a href="#">
					<span class="sidebar-mini">AL</span>
					<span class="sidebar-normal">Alertas</span>
				</a>
			</li>
			<li>
				<a href="#">
					<span class="sidebar-mini">LO</span>
					<span class="sidebar-normal">Logs</span>
				</a>
			</li>

        </ul>
	</div>
</li>


<li >
	<a  href="#">
		<i class="ti-shield"></i>
		<p>Límites</p>
	</a>
</li>-->

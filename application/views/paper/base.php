<html lang="en">
<head>
	<meta charset="utf-8" />
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url() ?>assets/icons/ic_launcher.png">
	<link rel="icon" type="image/png" sizes="96x96" href="<?php echo base_url() ?>assets/icons/ic_launcher.png">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title><?php echo $this->lang->line('titleAdmin'); ?></title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />

		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">

     <!-- Bootstrap core CSS     -->
    <link href="<?php echo base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet" />


     <!-- MpayCenter CSS     -->
    <link href="<?php echo base_url() ?>assets/css/mpaycenter.css" rel="stylesheet" />

    <!--  Paper Dashboard core CSS   -->

    <link href="<?php echo base_url() ?>assets/css/paper-dashboard.css" rel="stylesheet"/>
	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css">

    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
   <link href='https://fonts.googleapis.com/css?family=Muli:400,300' rel='stylesheet' type='text/css'>
   <link href="<?php echo base_url() ?>assets/css/themify-icons.css" rel="stylesheet">
   <!--<link href="<?php echo base_url() ?>assets/iconMap/dist/css/map-icons.css" rel="stylesheet">-->
</head>

<body>

	<style>
		.map-icon-label .map-icon {
			font-size:18px;
			color:white;
			line-height:48px;
			text-align:center;
		}

		.map-icon {
			z-index: unset;

		}
	</style>
	<div class="wrapper">
	    <div class="sidebar" data-background-color="brown" data-active-color="danger">
	    <!--
			Tip 1: you can change the color of the sidebar's background using: data-background-color="white | brown"
			Tip 2: you can change the color of the active button using the data-active-color="primary | info | success | warning | danger"
		-->
			<div class="logo">
				<a href="<?php echo base_url() ?>Admin" class="simple-text logo-mini">
					AD
				</a>

				<a href="#" class="simple-text logo-normal">
					<?php if(isset($titleMenu)){
						echo $titleMenu;
					}else{
						echo $this->lang->line('titleAdminCAP');
					} ?>
				</a>
			</div>
	    	<div class="sidebar-wrapper">
				<div class="user">
					<div class="photo">
						<img src="<?php echo base_url(); ?>assets/img/faces/user.svg">
	        </div>
	                <div class="info">
	                    <a data-toggle="collapse" href="#collapseExample" class="collapsed">
	                        <span>

								<?php $nombre=$this->session->userdata('name');
										if($nombre){
											echo $nombre;
										}
								 ?>
		                        <b class="caret"></b>
							</span>
	                    </a>
						<div class="clearfix"></div>

	                    <div class="collapse" id="collapseExample">
	                        <ul class="nav">
	                            <li>
									<a href="<?php echo base_url() ?>Perfil">
										<span class="sidebar-mini"><i class="fas fa-user-circle"></i></span>
										<span class="sidebar-normal"><?php echo $this->lang->line('profile'); ?></span>
									</a>
								</li>
	                            <!--<li>
									<a href="#edit">
										<span class="sidebar-mini">Ep</span>
										<span class="sidebar-normal">Edit Profile</span>
									</a>
								</li>-->
	                            <li>
									<a href="<?php echo base_url() ?>Logout">
										<span class="sidebar-mini"><i class="fas fa-sign-out-alt"></i></span>
										<span class="sidebar-normal"><?php echo $this->lang->line('logout'); ?></span>
									</a>
								</li>
	                        </ul>
	                    </div>
	                </div>
	            </div>
	            <ul class="nav">
	               <?php if(isset($sidebar_menu)){
	               	echo $sidebar_menu;
	               } ?>
	            </ul>
	    	</div>
	    </div>

	    <div class="main-panel">
			<nav class="navbar navbar-default">
	            <div class="container-fluid">
					<div class="navbar-minimize">
						<button id="minimizeSidebar" class="btn btn-fill btn-icon"><i class="ti-more-alt"></i></button>
					</div>

					<!-- AQUI SE CARGA EL NAVBAR O MENU SUPERIOR -->
					<?php if(isset($navbar_menu)){
						echo $navbar_menu;
					} ?>





	               <!-- <div class="collapse navbar-collapse">

	                    <ul class="nav navbar-nav navbar-right">

	                        <li>

	                        </li>

	                    </ul>
	                </div>-->
	            </div>
	        </nav>




			<!--AQUI SE CARGA SIEMPRE EL CONTENIDO DE LA VISTA-->
	        <?php
	        	if(isset($content_view))
	        	{
		        	echo $content_view;
		        }
	         ?>
	         <!--------------------------------------------------------->
            <footer class="footer">
                <div class="container-fluid">

                </div>
            </footer>
	    </div>
	</div>
	<?php if(isset($footer)){
		echo $footer;
	} ?>




</body>

<script src="<?php echo base_url() ?>assets/js/bootstrap-selectpicker.js">

</script>

<script type="text/javascript">

	$('#idioma_lang').on('change', function(){
		var lan = this.value;
		var dominio='https://secure.prestopago.com/CI3/Admin/';
		$.post(dominio+'changeLang',{lang:lan},function(data){
			location.reload();
		});
	})

	var sesion=<?php echo $this->session->userdata('expire_session');?>+"000";


	setInterval(function(){
		var fecha=(new Date()).getTime();
		if(sesion < fecha){
			window.location.replace("<?php echo base_url(); ?>");
		}else{
			console.log("Aún no es Hora de Cerrar la Sesión");
		}
	},930000);


</script>







</html>

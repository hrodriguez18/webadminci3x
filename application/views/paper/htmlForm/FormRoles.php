<form action="#" method="post" id="formRolesSistema">
  <div class="row">
  <div class="col-lg-12">
       <div class="form-group">
         <label for="descripcion">Descripcion</label>
         <?php echo form_error('descripcion'); ?>
         <input type="text" class="form-control" placeholder="Descripcion de Rol" name="descripcion" id="descripcion" value="<?php echo set_value('descripcion'); ?>">
      </div>
  </div>
</div>

<div class="row">
  <div class="col-lg-12">
       <div class="form-group">
         <label for="descripcion">Estado</label>
         <?php $estado_array=array('A'=>'Activo','I'=>'Inactivo'); ?>
         <?php echo form_dropdown('status', $estado_array, 'A','class="form-control" id="status"'); ?>

      </div>
  </div>
</div>

<div class="row">

  <div class="col-lg-12">
    <div class="col-lg-6 " >
      <p class="text-success">Permisos Activos</p>
      <ul id="l1" class="list-group list-group-sortable-connected group-RolesActivos">

      </ul>
    </div>
    <div class="col-lg-6 ">
      <p class="text-warning">Permisos Disponible</p>
      <ul id="l2" class="list-group list-group-sortable-connected group-RolesDisponibles">
        <?php if(isset($ListaPermisos)){
          echo $ListaPermisos;
        } ?>

      </ul>
    </div>
  </div>
</div>
<p class="alert alert-info"><?php echo $this->lang->line('withoutpermission'); ?></p>
</form>

<script>
  $("#l1,#l2").sortable(
  {

    connectWith:'.connected',

  }
);

  function EnviarData(){
    var i=0;
    var data = [];

    var descripcion=$("#descripcion").val();
    var status=$("#status").val();

    $(".group-RolesActivos li").each(function(){

      data[i]=this.id
      i++;
    })
    $("#modal_Content").html('<div class="loader"></div>');
    $.post('https://secure.prestopago.com/CI3/RolesSistema/Save',{info:data,descripcion:descripcion,status:status,},function(data){

        if(data!='Todo bien!'){
          $("#modal_Content").html(data)
        }else{
          $('#TableRolesSistema').bootstrapTable('refresh');
            $("#ModalApp").modal('hide');
            swal({
                      title: 'Dato Insertado',
                      text: data,
                      type: 'success',
                      confirmButtonClass: "btn btn-success btn-fill",
                      buttonsStyling: false
                   });
        }

    })
  }
</script>

<form action="#" id="form_grupo_trans" method="post">
	<?php echo $this->session->flashdata('error');?>
	<input type="hidden" name="id" value="<?php if(isset($id)){echo $id;} ?>">
	<div class="form-group">
		<label for="description">Descripción</label>
		<input type="text" name="description" placeholder="Descripción" class="form-control" value="<?php if(isset($description)){ echo $description;} ?>">
		<?php echo form_error('description'); ?>

	</div>
	<div class="form-group">
		<label for="status">Estado</label>
		<?php $optStatus=array('A'=>'Activo','I'=>'Incativo'); ?>
		<?php
			$stat='';
			if(isset($status)){$stat=$status;}

			?>
		<?php echo form_dropdown('status', $optStatus, $stat,'class="form-control"'); ?>
	</div>
	<hr>
	<?php if(isset($error)){ echo $error;} ?>
	<div class="form-group">
		<h5><?php echo $this->lang->line('add_delete'); ?></h5>
		<button class="btn btn-warning" type="button" onclick="AddRelTrans()">+</button>
		<button class="btn btn-danger" type="button" onclick="RemoveLast()">-</button>
	</div>
	<hr>

	<?php if(isset($ListaGrupoTrx)){echo $ListaGrupoTrx; }?>

</form>
<script>
	function AddRelTrans(){
		servidor="https://secure.prestopago.com/CI3/AdminGrupoTransacciones/";
		var formData = new FormData(document.getElementById("form_grupo_trans"));
		$.ajax({
		    url: servidor+'GetLineTransaction',
		    type:"post",
		    dataType: "html",
		    data: formData,
		    cache: false,
		    contentType: false,
		    processData: false,
		})
	    .done(function(res){

	    	if(res!=''){
	    		$("#contentTrans").append(res);
	    	}
	    });

	}

	function RemoveLast(){
		$("#contentTrans").children(".row:last").remove();
	}
</script>

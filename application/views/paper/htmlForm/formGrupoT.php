<form action="#" id="form_grupo_trans" method="post">

	<div class="form-group">
		<label for="description">Descripción</label>
		<input type="text" name="description" placeholder="Descripción" class="form-control" value="<?php set_value('description'); ?>">
		<?php echo form_error('description'); ?>
	</div>
	<div class="form-group">
		<label for="status">Estado</label>
		<select name="status" id="" class="form-control">
			<option value="A">Activo</option>
			<option value="I">Incativo</option>
		</select>
	</div>
	<hr>
	<div class="form-group">
		<h5><?php echo $this->lang->line('add_delete'); ?></h5>
		<button class="btn btn-warning btn-sm" type="button" onclick="AddRelTrans()">+</button>
		<button class="btn btn-danger btn-sm" type="button" onclick="RemoveLast()">-</button>
	</div>
	<div class="form-group">

	</div>
	<hr>
	<div class="form-group" id="contentTrans">

	</div>

</form>
<script>
	function AddRelTrans(){
		servidor="https://secure.prestopago.com/CI3/AdminGrupoTransacciones/";
		var formData = new FormData(document.getElementById("form_grupo_trans"));
		$.ajax({
		    url: servidor+'GetLineTransaction',
		    type:"post",
		    dataType: "html",
		    data: formData,
		    cache: false,
		    contentType: false,
		    processData: false,
		})
	    .done(function(res){

	    	if(res!=''){
	    		$("#contentTrans").append(res);
	    	}
	    });

	}

	function RemoveLast(){
		$("#contentTrans").children(".row:last").remove();
	}
</script>

<!--   Core JS Files. Extra: TouchPunch for touch library inside jquery-ui.min.js   -->
	<script src="<?php echo base_url()?>assets/js/jquery-3.1.1.min.js" type="text/javascript"></script>
	<script src="<?php echo base_url()?>assets/js/jquery-ui.min.js" type="text/javascript"></script>
	<script src="<?php echo base_url()?>assets/js/perfect-scrollbar.min.js" type="text/javascript"></script>
	<script src="<?php echo base_url()?>assets/js/bootstrap.min.js" type="text/javascript"></script>

    	<!-- Promise Library for SweetAlert2 working on IE -->
	<script src="<?php echo base_url()?>assets/js/es6-promise-auto.min.js"></script>

		<!-- Sweet Alert 2 plugin -->
	<script src="<?php echo base_url()?>assets/js/sweetalert2.js"></script>


    <!-- Wizard Plugin    -->
	<script src="<?php echo base_url()?>assets/js/jquery.bootstrap.wizard.min.js"></script>

	<!--  Bootstrap Table Plugin    -->
	<script src="<?php echo base_url()?>assets/js/bootstrap-table.js"></script>

	<!--  Plugin for DataTables.net  -->
	<script src="<?php echo base_url()?>assets/js/jquery.datatables.js"></script>



	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/fixedheader/3.1.3/js/dataTables.fixedHeader.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.1/js/dataTables.responsive.min.js"></script>



	<!-- Paper Dashboard PRO Core javascript and methods for Demo purpose -->
	<script src="<?php echo base_url()?>assets/js/paper-dashboard.js"></script>

		<!--  Switch and Tags Input Plugins -->
	<script src="<?php echo base_url()?>assets/js/bootstrap-switch-tags.js"></script>

	<?php if($this->session->userdata('lang')=='es'){
		echo '<script src="'.base_url().'assets/js/es/UsersTerminal.js"></script>';
	}else{
		echo '<script src="'.base_url().'assets/js/en/UsersTerminal.js"></script>';
	} ?>

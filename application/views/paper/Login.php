<html lang="en">
<head>

    <meta charset="utf-8" />
    <link rel="icon" type="image/png" sizes="96x96" href="<?php echo base_url() ?>assets/icons/ic_launcher.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <title>Mpay Center</title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


     <!-- Bootstrap core CSS     -->
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>assets/css/mpaycenter.css" rel="stylesheet"/>

    <!--  Paper Dashboard core CSS    -->
    <link href="<?php echo base_url(); ?>assets/css/paper-dashboard.css" rel="stylesheet"/>

    <!--  Fonts and icons     -->
  <!--  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">-->
  <!--  <link href='https://fonts.googleapis.com/css?family=Muli:400,300' rel='stylesheet' type='text/css'>-->
  <!--  <link href="<?php echo base_url(); ?>assets/css/themify-icons.css" rel="stylesheet">-->
</head>

<body>
    <div class="wrapper wrapper-full-page">
        <div class="full-page login-page" data-color="transparent" data-image="<?php echo base_url(); ?>assets/icons/bgBnp.svg">
        <!--   you can change the color of the filter page using: data-color="blue | azure | green | orange | red | purple" -->
            <div class="content">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4 col-sm-6 col-md-offset-4 col-sm-offset-3">
                            <form method="post" action="<?php echo base_url() ?>Iniciar">
                                <div class="card" data-background="color" data-color="blue">
                                    <div class="card-header">
                                        <h3 class="card-title">Credenciales</h3>
                                        <br>
                                        <?php
                                            //var_dump($this->session->flashdata('_message'));
                                            $_message=$this->session->flashdata('_message');
                                            if(isset($_message)){
                                                echo $_message;
                                            }

                                            $blocked=$this->session->flashdata('blocked');
                                            if($blocked){
                                            	echo $blocked;
											}
                                        ?>
                                    </div>
                                    <div class="card-content">
                                        <div class="form-group">
                                            <label>Usuario</label>
                                            <input type="text" placeholder="Ingresar Usuario" class="form-control input-no-border" name="username" value="<?php echo set_value('username'); ?>">
                                            <?php echo form_error('username'); ?>
                                        </div>
                                        <div class="form-group">
                                            <label>Contraseña</label>
                                            <input type="password" placeholder="Ingresar Contraseña" class="form-control input-no-border" name="password" value="<?php echo set_value('password'); ?>" >
                                            <?php echo form_error('password'); ?>
                                        </div>
                                    </div>
                                    <div class="card-footer text-center">
                                        <button type="submit" class="btn btn-fill btn-wd ">Iniciar sesión</button>

                                        <div class="forgot">
                                            <a href="<?php echo base_url(); ?>ForgotPass">¿Olvidaste tu contraseña?</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <footer class="footer footer-transparent">
                <div class="container">
                    <div class="copyright">
                        &copy; <script>document.write(new Date().getFullYear())</script> - Mpay Center</a>
                    </div>
                </div>
            </footer>
        </div>
    </div>



</body>

    <!--   Core JS Files. Extra: TouchPunch for touch library inside jquery-ui.min.js   -->
    <script src="<?php echo base_url(); ?>assets/js/jquery-3.1.1.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery-ui.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/js/perfect-scrollbar.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js" type="text/javascript"></script>

    <!--  Forms Validations Plugin-->
    <script src="<?php echo base_url(); ?>assets/js/jquery.validate.min.js"></script>

    <!-- Promise Library for SweetAlert2 working on IE -->
    <script src="<?php echo base_url(); ?>assets/js/es6-promise-auto.min.js"></script>

    <!--  Plugin for Date Time Picker and Full Calendar Plugin
    <script src="<?php echo base_url(); ?>assets/js/moment.min.js"></script>
-->
    <!--  Date Time Picker Plugin is included in this js file -->


    <!--  Switch and Tags Input Plugins -->
    <script src="<?php echo base_url(); ?>assets/js/bootstrap-switch-tags.js"></script>



    <!-- Paper Dashboard PRO Core javascript and methods for Demo purpose -->
    <script src="<?php echo base_url(); ?>assets/js/paper-dashboard.js"></script>

    <!-- Paper Dashboard PRO DEMO methods, don't include it in your project! -->

    <script src="<?php echo base_url(); ?>assets/js/demo.js"></script>

	<script type="text/javascript">
        $().ready(function(){
            demo.checkFullPageBackgroundImage();

            setTimeout(function(){
                // after 1000 ms we add the class animated to the login/register card
                $('.card').removeClass('card-hidden');
            }, 700)
        });
	</script>

</html>

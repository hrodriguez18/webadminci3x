<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_SuperModel extends CI_Model {

//VARIABLES PARA HACER LAS CONSULTAS A LA BASE DE DATOS
	protected $_tablename='';
	protected $_primary_key='id';
	protected $_primary_filter='intval';
	protected $_order_by='id';


//VARIABLES QUE SE NECESITAN PARA GENERAR EL FORMULARIO
	public  $_rules=array();
	public $_inputs=array();
	public $documentationForForm='';
	protected $_timestamps=FALSE;
	protected $_form='';
	protected $_form_group_class='';
	protected $_attr_submit_button='';
	protected $_class_label='';
//ESTA RUTA LE PERMITE A LA VISTA SABER HACIA DONDE BUSCAR EL ECHO DEL FORMULARIO
	protected $_ruta='';

	//RELACION PARA GENERAR LA TABLA DE ESTA CLASE
	protected $_headingTable=array();
	//usado para buscar los datos de la db
	protected $_datatable=array();
	protected $_joinForBootstrapTable=array();
	//usado para mostrar los datos en la tabla e incluir los btn actions
	protected $_dataForTable=array();
	protected $_jointable=array();
	//para generar los informes en PDF
	public $_TableToExport='';
	public $_footer_ContentToExport='';
	public $_pdfTitle='';
	public $_pdfDescription='';
	public $_headersTable=array();
	public $_selectToExport=array();
	public $_orderName='';
	public $_order='';
	public $_join=array();
	public $_dateVariable='';



	protected $template = array(
        'table_open'            => '<table class="table table-responsive" style="border-left: 1px solid black;border-bottom: 1px solid black;border-top: 1px solid black;">',

        'thead_open'            => '<thead class="">',
        'thead_close'           => '</thead>',

        'heading_row_start'     => '<tr>',
        'heading_row_end'       => '</tr>',
        'heading_cell_start'    => '<th style="color: #000000;font-weight: bolder;border-bottom:1px solid black;border-right: 1px solid black;
											text-align: center;white-space: nowrap;">',
        'heading_cell_end'      => '</th>',

        'tbody_open'            => '<tbody>',
        'tbody_close'           => '</tbody>',

        'row_start'             => '<tr>',
        'row_end'               => '</tr>',
        'cell_start'            => '<td nowrap style="color: #000000;margin: 5px auto;border-right: 1px solid black;
 													border-bottom: 1px solid black;text-align: left;text-align: center;
 													white-space: nowrap;">',
        'cell_end'              => '</td>',

        'row_alt_start'         => '<tr>',
        'row_alt_end'           => '</tr>',
        'cell_alt_start'        => '<td nowrap style="color: #000000;margin: 5px auto;border-right: 1px solid black;
 													border-bottom: 1px solid black;text-align: left;
 													text-align: center;white-space: nowrap;
 													background-color: #beffbb;">',
        'cell_alt_end'          => '</td>',

        'table_close'           => '</table>'
	);



	public function __construct(){
		parent::__construct();
		//Do your magic here
		date_default_timezone_set('America/Panama');


	}

	public function get( $id = NULL, $single = FALSE, $limit = NULL, $offset = NULL, $num_rows = FALSE ) {
		if ($id != NULL) {
			$filter = $this->_primary_filter;
			$id = $filter($id);
			$this->db->where($this->_primary_key, $id);
			$method = 'row';
		} elseif($single == TRUE ) {
			$method = 'row';
		} elseif($num_rows == TRUE) {
			$method = 'num_rows';
		} else {
			$method = 'result';
		}


		$this->db->order_by($this->_order_by,'ASC');

		return $this->db->get($this->_tablename, $limit, $offset)->$method();
	}

	public function get_by($where, $single = FALSE, $limit = NULL, $offset = NULL, $num_rows = FALSE ){
		$this->db->where($where);
		return $this->get(NULL, $single, $limit, $offset, $num_rows);
	}

	public function save($data, $id = NULL){
		//set timestamps
		// if ($this->_timestamps == TRUE) {
		// 	$now = date('Y-m-d H:i:s');
		// 	$id || $data['created'] = $now;
		// 	$data['modified'] = $now;
		// }
		//insert
		if ($id === NULL) {
			!isset($data[$this->_primary_key]) || $data[$this->_primary_key] = NULL;
			$this->db->set($data);
			$this->db->insert($this->_tablename);
			$id = $this->db->insert_id();
		}
		//update
		else {
			$filter = $this->_primary_filter;
			$id = $filter($id);
			$this->db->set($data);
			$this->db->where($this->_primary_key, $id);
			$this->db->update($this->_tablename);
		}
		return $id;
	}

	public function delete($id){
		$filter = $this->_primary_filter;
		$id = $filter($id);
		if (!$id) {
			return FALSE;
		}
		$this->db->where($this->_primary_key, $id);
		$this->db->limit(1);
		if($this->db->delete($this->_tablename)){
			return TRUE;
		}else{
			if($this->db->_error_number()==23000){
				echo '<p class="label label-danger">'.$this->lang->line('error_foreignkey').' ('.$id.')</p><p class="label label-danger">'.$this->lang->line('message_of_error_23000').'</p>';
			}
			return FALSE;
		}
	}

	public function optGenerate($table,$optValue,$description,$label){
		$descripcion='';
		$opt=array();
		if($this->lang->line($label)!=''){
			$descripcion=$this->lang->line('please_select').' '.$this->lang->line($label);
		}else{
			$descripcion=$this->lang->line('please_select').' '.$label;
		}
		$opt['']=$descripcion;
		$this->db->select('*');
		$this->db->order_by($description,'ASC');
		$query=$this->db->get($table);

		foreach ($query->result() as $value){

			$opt[$value->$optValue]=$value->$description;
		}


		return $opt;
	}

	public function form($size='lg',$id=NULL){
		$infoTable=array();
		$valueSelector='';

		if($id!=NULL){
			$infoTable=$this->get($id,TRUE,$num_rows = TRUE);
			if(property_exists($infoTable, 'password')){
				$infoTable->password='';
			}
		}


		$this->_form=form_open($this->_ruta.'Save','id="form_page"').'<div class="row">';
		$field='';
		foreach ($this->_inputs as $key ){
			$name=$key['name'];

			if(isset($key['label'])){
				$label=trim($key['label']," ");
				if($this->lang->line($label)){
					$key['label']=$this->lang->line($label);
				}
			}
			if(isset($key['placeholder'])){
				$placeholder=trim($key['placeholder']," ");
				if($this->lang->line($placeholder)){
					$key['placeholder']=$this->lang->line($placeholder);
				}
			}

			if($infoTable){
				if($name!='send'){
					if(isset($infoTable->$name)){
						$key['value']=$infoTable->$name;
						//$key['default']=$infoTable->$name;
					}
					$this->_attr_submit_button='class="btn btn-danger"';
				}
				$key['value_btn']='Actualizar';
				if($key['type']=='selector'){
					$valueSelector=$infoTable->$name;
				}
			}else{
				$key['value']=set_value($key['name'],'',true);
				if($key['type']=='time'){
					$key['value']=set_value($key['name'],date('H:i'),true);
				}
				if($key['type']=='date'){
					$key['value']=set_value($key['name'],date('Y-m-d'),true);
				}
			}
			$error='';
			$has_error='';
			if(form_error($key['name'])!=''){
				//$error=form_error($key['name']);
				$error='';
				$has_error='has-error';
			}

			if($key['type']=='hidden'){
				$field=form_input($key);
			}
			if($key['type']=='text'){
				$field='<div class="'.$this->_form_group_class.$has_error.'">'.form_label($key['label'],$key['name'],$this->_class_label).form_input($key).$error.'</div>';
			}

			if($key['type']=='password'){
				$field='<div class="'.$this->_form_group_class.$has_error.'">'.form_label($key['label'],$key['name'],$this->_class_label).form_input($key).$error.'</div>';
			}
			if($key['type']=='email'){
				$field='<div class="'.$this->_form_group_class.$has_error.'">'.form_label($key['label'],$key['name'],$this->_class_label).form_input($key).$error.'</div>';
			}
			if($key['type']=='number'){
				$field='<div class="'.$this->_form_group_class.$has_error.'">'.form_label($key['label'],$key['name']).form_input($key).$error.'</div>';
			}
			if($key['type']=='textarea'){
				$field='<div class="'.$this->_form_group_class.$has_error.'">'.form_label($key['label'],$key['name'],$this->_class_label).form_textarea($key).$error.'</div>';
			}
			if($key['type']=='selector'){
				$table=$key['table_rel'];
				$optValue=$key['opt'];
				$description=$key['description'];
				$id=$key['id'];
				$opt=$this->optGenerate($table,$optValue,$description,$key['label']);
				if(isset($key['jquery'])){
					$jquery=$key['jquery'];
				}else{
					$jquery='';
				}
				$field='<div class="'.$this->_form_group_class.$has_error.'">'.form_label($key['label'],$key['name'],$this->_class_label).form_dropdown($key['name'], $opt,set_value($key['name'],$valueSelector,true),'class="'.$key['class'].' " id="'.$id.'" '.$jquery).$error.'</div>';
			}

			if($key['type']=='time'){
				$field='<div class="'.$this->_form_group_class.'">'.form_label($key['label'],$key['name'],$this->_class_label).form_input($key).'</div>';
			}

			if($key['type']=='date'){
				$field='<div class="'.$this->_form_group_class.$has_error.'">'.form_label($key['label'],$key['name'],$this->_class_label).form_input($key).$error.'</div>';
			}

			if($key['type']=='checkbox'){

				$table=$key['table'];
				$field_value=$key['field_value'];
				$name=$key['name'];
				$field_label=$key['field_label'];

				$field=$this->GenerateCheckBox($table,$field_value,$name,$field_label);
			}

			if($key['type']=='submit'){
				$field=form_submit($key['name'],$key['value_btn'],$this->_attr_submit_button);
			}

			if($size=='lg'){
				if($key['type']=='submit' || $key['type']=='textarea'){
					$this->_form.='<div class="col-lg-12">'.$field.'</div>';
				}else{
					$this->_form.='<div class="col-lg-4">'.$field.'</div>';
				}

			}
			if($size=='md'){

				if($key['type'=='submit'] || $key['type']=='textarea'){
					$this->_form.='<div class="col-lg-12">'.$field.'</div>';
				}else{
					$this->_form.='<div class="col-lg-6">'.$field.'</div>';
				}

			}
			if($size=='sm'){

				if($key['type']=='submit' ){
					$this->_form.='<div class="col-lg-12">'.$field.'</div>';
				}else{
					$this->_form.='<div class="col-lg-12">'.$field.'</div>';
				}

			}

		}
		$this->_form.=form_close().'</div>';
		//$this->documentationForForm;

		if(validation_errors()){
			$this->_form.='<hr>'.validation_errors('<div ><ul><li class="text-danger">', '</li></ul></div>');
		}

		return $this->_form;
		//return $valueSelector;
	}


	public function Table(){
		$_id=$this->_primary_key;
		$this->table->set_template($this->template);
		$headingArray=array();

		if(count($this->_headingTable)>0){
			$this->table->set_heading($this->_headingTable);
		}
		if(count($this->_datatable)>0){
			foreach ($this->_datatable as $value){
				$this->db->select($value);
			}
		}

		if(count($this->_jointable)>0){
			foreach ($this->_jointable as $key) {
				$this->db->join($key['table_join'],$key['table_rel'],'left');
			}
		}

		$this->db->order_by($this->_order_by,'ASC');

		$query=$this->db->get($this->_tablename);
		$i=1;
		foreach ($query->result() as $key ){
			$dataArray=array();
			array_push($dataArray, $i);
			$i++;
			foreach ($this->_dataForTable as &$value){
				array_push($dataArray, $key->$value);
			}
			$btn_actions='<a href="'.base_url().$this->_ruta.'Edit/'.$key->$_id.'" class="btn btn-sm btn-info"><i class="fa fa-edit"></i></a> <a href="'.base_url().$this->_ruta.'Delete/'.$key->$_id.'" class="btn btn-sm btn-danger"><i class="fa fa-eraser"></i></a>';
			array_push($dataArray, $btn_actions);
			$this->table->add_row($dataArray);

		}

		return $this->table->generate();

	}

	public function GenerateCheckBox($table,$field_value,$name,$field_label){
		$this->db->select('*');
		$query=$this->db->get($table);
		$field='';
		foreach ($query->result() as  $value){
			$field.='<div class="form-group"><div class="checkbox checkbox-inline">'.'<input id="'.$value->$field_value.'" " type="checkbox" value="'.$value->$field_value.'" name="'.$name.'[]"><label for="'.$value->$field_value.'">'.$value->$field_label.'</label></div></div>';
		}

		return $field;

	}


	public function generar() {
			$this->load->library('Pdf');
			$pdf = new Pdf('L', 'mm', 'A4', true, 'UTF-8', false);
			$pdf->SetCreator(PDF_CREATOR);
			$pdf->SetAuthor('hrodriguez');
			$pdf->SetTitle('REPORT');
			$pdf->SetSubject('REPORTE NBP');
			$pdf->SetKeywords('Reporte');

// datos por defecto de cabecera, se pueden modificar en el archivo tcpdf_config_alt.php de libraries/config

			$logo='/logobnp.jpg';
			$pdf->SetHeaderData($logo, PDF_HEADER_LOGO_WIDTH, strtoupper($this->_pdfTitle), strtoupper($this->_pdfDescription) . ' | GENERADO POR: '.strtoupper($this->session->userdata('name')).'. | FECHA DE CREACION : '.date('d-m-Y H:i:s') , array(0, 64, 255), array(0, 64, 128));
			$pdf->setFooterData($tc = array(0, 64, 0), $lc = array(0, 64, 128));

// datos por defecto de cabecera, se pueden modificar en el archivo tcpdf_config.php de libraries/config
			$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
			$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
// se pueden modificar en el archivo tcpdf_config.php de libraries/config
			$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
// se pueden modificar en el archivo tcpdf_config.php de libraries/config
			$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
			$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
			$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
// se pueden modificar en el archivo tcpdf_config.php de libraries/config
			$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
//relación utilizada para ajustar la conversión de los píxeles
			$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
// ---------------------------------------------------------
// establecer el modo de fuente por defecto
			$pdf->setFontSubsetting(true);

// Establecer el tipo de letra

//Si tienes que imprimir carácteres ASCII estándar, puede utilizar las fuentes básicas como
// Helvetica para reducir el tamaño del archivo.
			$pdf->SetFont('freemono', '', 12, '', true);

// Añadir una página
// Este método tiene varias opciones, consulta la documentación para más información.
			$pdf->AddPage('L', 'A3');
//fijar efecto de sombra en el texto
			$pdf->setTextShadow(array('enabled' => true, 'depth_w' => 0.2, 'depth_h' => 0.2, 'color' => array(196, 196, 196), 'opacity' => 1, 'blend_mode' => 'Normal'));
			//preparamos y maquetamos el contenido a crear
			//$html = $this->load->view('reports/testTable','',true);
// Imprimimos el texto con writeHTMLCell()
			$pdf->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $this->_TableToExport.$this->_footer_ContentToExport, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = '', $autopadding = true);
// ---------------------------------------------------------
// Cerrar el documento PDF y preparamos la salida
// Este método tiene varias opciones, consulte la documentación para más información.
			$nombre_archivo = utf8_decode('REPORT_'.time().'.pdf');
			ob_end_clean();
			$this->load->helper('download');
			header('Content-Disposition: attachment; filename="'.$nombre_archivo.'"');
			$data= $pdf->Output($nombre_archivo,'D');
			force_download($nombre_archivo, $data);
	}

	public function exportToPdf(){

		if($this->input->post('fecha_inicio')){
			$fecha_inicio=$this->input->post('fecha_inicio');
		}
		if($this->input->post('fecha_fin')){
			$fecha_fin=$this->input->post('fecha_fin');
		}

		if(count($this->_selectToExport)>0){
			for ($i=0; $i < count($this->_selectToExport); $i++) {
				$this->db->select($this->_selectToExport[$i]);
			}
		}else{
			$this->db->select('*');
		}
		if($this->_orderName!='' && $this->_order!=''){
			$this->db->order_by($this->_orderName,$this->_order);
		}

		if(isset($fecha_inicio)  && isset($fecha_fin)){
			$fecha_inicio.=' 00:00:00';
			$fecha_fin.=' 23:59:00';
			$this->db->where($this->_dateVariable.' >',$fecha_inicio);
			$this->db->where($this->_dateVariable.' <',$fecha_fin);
		}

		if(count($this->_join) > 0){

			$this->db->join($this->_join['table'],$this->_join['relation'],$this->_join['type']);
		}
		$query=$this->db->get($this->_tablename)->result_array();
		$this->table->set_heading($this->_headersTable);
		$this->_TableToExport=$this->table->generate($query);
		$this->generar();
	}

	public function populate($limit=NULL,$offset=NULL,$search=NULL,$sort=NULL,$sortOrder=NULL){
			$arrayJson=array();
	    $arrayJson['total']=$this->db->count_all($this->_tablename);

			if(count($this->_datatable)>0){
				for ($i=0; $i <count($this->_datatable) ; $i++) {
					$this->db->select($this->_datatable[$i]);
				}
				if(count($this->_joinForBootstrapTable)>0){
					foreach ($this->_joinForBootstrapTable as $key) {
						$this->db->join($key['table_join'],$key['table_rel'],'left');
					}

				}

			}else{
				$this->db->select('*');
			}

			if($search != ''){
				for ($i=0; $i <count($this->_dataForTable) ; $i++){
					if($i==0){
						$this->db->like($this->_dataForTable[$i], $search, 'both');
					}else{
						$this->db->or_like($this->_dataForTable[$i], $search, 'both');
					}
				}
			}

	    if($limit && $offset){
	      $this->db->limit($offset,$limit);
	    }
	    if($sort){
	      $this->db->order_by($sort,$sortOrder);
	    }else{
	      $this->db->order_by($this->_primary_key,'DESC');
	    }
	    //$this->db->limit($limit);
	    //$this->db->group_by(array($this->_primary_key,'stamp'));
	    $query= $this->db->get($this->_tablename,$limit,$offset);

	    if($query->num_rows()>0){
	      foreach ($query->result() as $key => $value) {
	        $arrayJson['rows'][$key]=$value;
	      }
	    }
	    return $arrayJson;
	}



}






/* End of file MY_SuperModel.php */
/* Location: ./application/core/MY_SuperModel.php */

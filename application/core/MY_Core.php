<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Core extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
  }

  public function index()
  {
      echo "Saludos de desde MY_Core";
  }

}

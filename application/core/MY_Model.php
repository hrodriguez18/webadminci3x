<?php
class MY_Model extends CI_Model {

	protected $_table_name = '';
	protected $_primary_key = 'id';
	protected $_primary_filter = 'intval';
	protected $_order_by = '';
	public $rules = array();
	protected $_timestamps = FALSE;
	protected $idioma='spanish';
	public $_TableToExport='';
	public $_footer_ContentToExport='';
	public $_pdfTitle='';
	public $_pdfDescription='';


	function __construct() {
		parent::__construct();
        $this->lang->load('configuration',$this->idioma);
	}

	public function get($id = NULL, $single = FALSE){

		if ($id != NULL) {
			$filter = $this->_primary_filter;
			$id = $filter($id);
			$this->db->where($this->_primary_key, $id);
			$method = 'row';
		}
		elseif($single == TRUE) {
			$method = 'row';
		}
		else {
			$method = 'result';
		}

		if (!count($this->db->order_by($this->_order_by))) {
			$this->db->order_by($this->_order_by);
		}
		return $this->db->get($this->_table_name)->$method();
	}

	public function get_by($where, $single = FALSE){
		$this->db->where($where);
		return $this->get(NULL, $single);
	}

	public function save($data, $id = NULL){

		// Set timestamps
		if ($this->_timestamps == TRUE) {
			$now = date('Y-m-d H:i:s');
			$id || $data['created'] = $now;
			$data['modified'] = $now;
		}

		// Insert
		if ($id === NULL) {
			//!isset($data[$this->_primary_key]) || $data[$this->_primary_key] = NULL;
			$this->db->set($data);
			$this->db->insert($this->_table_name);
			$id = $this->db->insert_id();
		}
		// Update
		else {
			$filter = $this->_primary_filter;
			$id = $filter($id);
			$this->db->set($data);
			$this->db->where($this->_primary_key, $id);
			$this->db->update($this->_table_name);
		}

		return $id;
	}

	public function delete($id){
		$filter = $this->_primary_filter;
		$id = $filter($id);

		if (!$id) {
			return FALSE;
		}
		$this->db->where($this->_primary_key, $id);
		$this->db->limit(1);
		$this->db->delete($this->_table_name);
		return TRUE;
	}

	public function generar() {
			$this->load->library('Pdf');
			$pdf = new Pdf('L', 'mm', 'A4', true, 'UTF-8', false);
			$pdf->SetCreator(PDF_CREATOR);
			$pdf->SetAuthor('hrodriguez');
			$pdf->SetTitle('REPORT');
			$pdf->SetSubject('REPORTE NBP');
			$pdf->SetKeywords('Reporte');

// datos por defecto de cabecera, se pueden modificar en el archivo tcpdf_config_alt.php de libraries/config

			$logo='/logobnp.jpg';
			$pdf->SetHeaderData($logo, PDF_HEADER_LOGO_WIDTH, strtoupper($this->_pdfTitle), strtoupper($this->_pdfDescription) . ' | GENERADO POR: '.strtoupper($this->session->userdata('name')).'. | FECHA DE CREACION : '.date('d-m-Y H:i:s') , array(0, 64, 255), array(0, 64, 128));
			$pdf->setFooterData($tc = array(0, 64, 0), $lc = array(0, 64, 128));

// datos por defecto de cabecera, se pueden modificar en el archivo tcpdf_config.php de libraries/config
			$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
			$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
// se pueden modificar en el archivo tcpdf_config.php de libraries/config
			$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
// se pueden modificar en el archivo tcpdf_config.php de libraries/config
			$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
			$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
			$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
// se pueden modificar en el archivo tcpdf_config.php de libraries/config
			$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
//relación utilizada para ajustar la conversión de los píxeles
			$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
// ---------------------------------------------------------
// establecer el modo de fuente por defecto
			$pdf->setFontSubsetting(true);

// Establecer el tipo de letra

//Si tienes que imprimir carácteres ASCII estándar, puede utilizar las fuentes básicas como
// Helvetica para reducir el tamaño del archivo.
			$pdf->SetFont('freemono', '', 12, '', true);

// Añadir una página
// Este método tiene varias opciones, consulta la documentación para más información.
			$pdf->AddPage('L', 'A3');
//fijar efecto de sombra en el texto
			$pdf->setTextShadow(array('enabled' => true, 'depth_w' => 0.2, 'depth_h' => 0.2, 'color' => array(196, 196, 196), 'opacity' => 1, 'blend_mode' => 'Normal'));
			//preparamos y maquetamos el contenido a crear
			//$html = $this->load->view('reports/testTable','',true);
// Imprimimos el texto con writeHTMLCell()
			$pdf->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $this->_TableToExport.$this->_footer_ContentToExport, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = '', $autopadding = true);
// ---------------------------------------------------------
// Cerrar el documento PDF y preparamos la salida
// Este método tiene varias opciones, consulte la documentación para más información.
			$nombre_archivo = utf8_decode('REPORT_'.time().'.pdf');
			ob_end_clean();
			$this->load->helper('download');
			header('Content-Disposition: attachment; filename="'.$nombre_archivo.'"');
			$data= $pdf->Output($nombre_archivo,'D');
			force_download($nombre_archivo, $data);
	}
}

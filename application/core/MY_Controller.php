<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller{

	public $permisos=array();
	public $mis_permisos=array();

	public $permiso_crear=0;
	public $permiso_actualizar=0;
	public $permiso_eliminar=0;
	public $permiso_ver=0;
	public $_lang='';


	//
	//parametros para la tabla, $controllerServer= controlador donde apuntaran las funciones de la tabla
	//$datatable=arreglo de datos para identificar las columnas(ver formato en controlador)
	/*
		$this->dataTable[0]=array('some_option'=>'some_value');
	*/

	//dato para locale table
	public $localeForTable='';
	//variable para exportar tabla;
	public $rutaToExport='';
	//el value puede o no estar en el diccionario de idiomas
	public $mymodel='';
	public $controlerServer='Admin';
	public $dataTable=array();


	//====================================================================

	//Ruta del formulario para insertar o editar uno nuevo.
	public $rutarForm='';

	//Tamaño del FORMULARIO
	public $formSize='lg';

	public function __construct(){

        parent::__construct();
        if(!$this->session->userdata('is_logged')){
			echo '<p class="alert alert-danger">Tu sesión caducó</p>';
        	redirect('Login','refresh');
       	}
		$this->load->model($this->mymodel,'modelo');
		$this->rutaForm=base_url().$this->router->fetch_class().'/form';
		$this->load->model('logUsers');

		$this->_lang=$this->session->userdata('lang');

		if($this->_lang=='es'){
			$this->lang->load('spanish','spanish');
			$this->localeForTable='es-MX';
		}else{
			$this->lang->load('english','english');
			$this->localeForTable='en-US';
		}

		$this->mis_permisos=$this->PermisosMenu();
		$controller=$this->router->fetch_class();
		$method=$this->router->fetch_method();

		$excepciones=array(
			'Logout','populate','populateTable','Populate','PopulateTable','changeLang',
			'menu_admin','menu_monedero','menu_cnb','menu_mpos','menu_ecommerce','getColumnsForTable',
			'isEmail','diferente_de_default','withoutSignal','existeCuenta','esCuentaNueva',
			'isUniqueEmail','YaExisteActvidadComercial','UpdateKPI','ReloadTableMoneda',
			'UpdateGraphic','MAPA','CargarCsv','Modal','EditarElemento','FormPais','populateTablePais',
			'EditarPais','Form','EditData','Export','form','Editar','Edit','export','populateOperadores',
			'ExportOperadores','populateReportType','exportType','populateReporteIngresos',
			'exportIngresos','populateReport','populateTableEstados','DescargarSaldo','Search',
			'Perfil','PasswordReset','populateUserMonedero','formUsers',
			'exportUsersMonedero','GetLineTransaction','CantidadTransacciones','CantidadLogin','MposPaper',
			'populateComisiones','exportcomission','PopulateDetalleTrxByUsers','exportUsersMonederoByTrxPdf',
			'exportUsersMonederoByTrxExcel','populateComisiones','exportcomissionMonedero','optAgencias'

		);

		if(!in_array($method,$excepciones) && !in_array($controller,$excepciones)){
			$this->tienesPermiso();
		}
    }

    public function tienesPermiso(){
		$controller=$this->router->fetch_class();
		$method=$this->router->fetch_method();
		$i=0;
		$permisos=$this->session->userdata('permisos');
		foreach ($permisos as $permiso){
			if(in_array($controller,$permiso) && in_array($method,$permiso)){
				$i=1;
				break;
			}else{
				$i=0;
			}
		}

		if($i==0){
			$datalog['userid']=$this->session->userdata('userid');
			$datalog['action']='Eliminar Monedas No Permitido';
			$datalog['status']='No Completado';
			$datalog['category']='Borrar';
			$datalog['username']=$this->session->userdata('username');
			$datalog['name']=$this->session->userdata('name');
			$datalog['ip']=$this->input->ip_address();
			$this->logUsers->save($datalog);

			if(strpos($method,'index') !== FALSE){
				echo $this->load->view('paper/stop','',true);
			}else{
				echo '<p class="alert alert-danger">Lo sentimos, usted NO tiene suficientes privilegios para realizar esta accion, Favor Contacte al Administrador del Sistema. </p>';
				echo '<p class="alert alert-danger">Controlador : '.$controller.'</p>';
				echo '<p class="alert alert-danger">Metodo : '.$method.'</p>';
			}

			//echo '<p class="alert alert-danger">Controlador : '.$controller.'</p>';
			//echo '<p class="alert alert-danger">Metodo : '.$method.'</p>';
			exit();
		}
		$datalog['userid']=$this->session->userdata('userid');
		$datalog['action']=$this->router->fetch_class().' -> '.$this->router->fetch_method().'-> Permitido Redireccionado';
		$datalog['status']='Completado';
		$datalog['category']='Editar';
		$datalog['username']=$this->session->userdata('username');
		$datalog['name']=$this->session->userdata('name');
		$datalog['ip']=$this->input->ip_address();
		$this->logUsers->save($datalog);

		return FALSE;
	}

	public function PermisosMenu(){
		$permisos=$this->session->userdata('permisos');
		$misPermisos=array();
		foreach ($permisos as $permiso){
			array_push($misPermisos,$permiso);
		}
		$arreglo=array();
		foreach ($misPermisos as $value){
			$arreglo[$value['Id']]=$value['Descripcion'];
		}
		return $arreglo;
	}



	public $navbar_header=array("administration"=>"Admin","mPOS"=>"MPOS","CNB"=>"CNB","Monedero"=>"Monedero","eCommerce"=>"Ecommerce",'idioma'=>'
						<select name="idioma" id="idioma_lang" class="selectpicker" data-style="btn-info " >
							<option value="es">ES</option>
							<option value="en">EN</option>
						</select>
					');

	public function navbar_header_menu(){

		$html='<div class="navbar-header"><button type="button" class="navbar-toggle"><span class="sr-only">Toggle navigation</span><span class="icon-bar bar1"></span><span class="icon-bar bar2"></span><span class="icon-bar bar3"></span></button>';
		if($this->session->userdata('lang')=='es'){
			$this->navbar_header['idioma']='
								<select name="idioma" id="idioma_lang" class="selectpicker" data-style="btn-info " >
									<option value="es" selected>ES</option>
									<option value="en">EN</option>
								</select>
							';
		}else{
			$this->navbar_header['idioma']='
								<select name="idioma" id="idioma_lang" class="selectpicker" data-style="btn-info " >
									<option value="es" >ES</option>
									<option value="en" selected>EN</option>
								</select>
							';
		}

		foreach ($this->navbar_header as $key => $value){

			if($key!='idioma')
			{
				$html.='<a class="navbar-brand separator" href="'.base_url().$value.'">'.$this->lang->line($key).'</a>';
			}else
			{
				$html.='<a class="navbar-brand" href="#"><div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">'.$value.'</div></a>';
			}

		}
		$html.='</div>';
		return $html;
	}

	public function menu_admin(){
		return $this->load->view($this->config->item("theme")."/menupaper/menu_admin","",true);
	}

	public function menu_monedero(){
		return $this->load->view($this->config->item("theme")."/menupaper/menu_monedero","",true);
	}

	public function menu_cnb(){
		return $this->load->view($this->config->item("theme")."/menupaper/menu_cnb","",true);
	}

	public function menu_mpos(){
		return $this->load->view($this->config->item("theme")."/menupaper/menu_mpos","",true);
	}

	public function menu_ecommerce(){
		return $this->load->view($this->config->item("theme")."/menupaper/menu_ecommerce","",true);
	}

	public function changeLang(){
		$lang=$this->input->post('lang');
		$this->session->set_userdata('lang',$lang);
	}

	public function populateTable(){
		//
		$data = json_decode(file_get_contents('php://input'), true);

		$limit=NULL;
		$offset=NULL;
		$search=NULL;
		$sort=NULL;
		$sortOrder=NULL;
		if(isset($data['limit'])){
			$limit= $data['limit'];
		}

		if(isset($data['offset'])){
			$offset= $data['offset'];
		}

		if(isset($data['search'])){
			$search=utf8_decode($data['search']);
		}

		if(isset($data['sort'])){
			$sort=$data['sort'];
		}

		if(isset($data['sortOrder'])){
			$sortOrder=$data['sortOrder'];
		}

		echo json_encode($this->modelo->populate($limit,$offset,$search,$sort,$sortOrder));

}

	public function newForm(){
  	echo $this->modelo->form('sm');
  }

	public function form(){
		if($this->input->post('id')){
			$id=$this->input->post('id');
		}else{
			$id=NULL;
		}
		echo $this->modelo->form($this->formSize,$id);
	}

  public function Editar(){
  	$obj=$this->input->post('id');
  	echo $this->modelo->form('sm',$obj);
  }

	public function save(){
		$id=NULL;
		$dataToInsert=array();

		if($this->input->post($this->modelo->_primary_key) != ''){
			$id=$this->input->post($this->modelo->_primary_key);
			foreach ($this->modelo->_inputs as $key => $value){
					$key=$key;
					$dataToInsert[$key]=$this->input->post($key);

			}
			unset($dataToInsert[$this->modelo->_primary_key]);
			$reglas=$this->modelo->_rules;
			foreach ($reglas as $key => $value) {
				if(strpos($value['rules'],'unique')){
					$reglas[$key]['rules']='trim|required|max_length[80]';
				}
				if($value['field']=='password'){
					$reglas[$key]['rules']='trim|max_length[80]';
				}
				if($value['field']=='email'){
					$reglas[$key]['rules']='trim|max_length[80]|callback_isEmail';
				}
			}
			$this->form_validation->set_rules($reglas);
			$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
			//var_dump($dataToInsert);
			if($this->form_validation->run()==TRUE){
				if(array_key_exists('password',$dataToInsert)){
					if($dataToInsert['password']!=''){
						$dataToInsert['password']=$this->encrypt->encode($dataToInsert['password']);
					}else{
						unset($dataToInsert['password']);
					}
					//$dataToInsert['password']=='prueba que si existe';
				}
				$this->modelo->save($dataToInsert,$id);
				echo 'ok';
			}else{
				echo $this->modelo->form($this->formSize,$id);
			}
		}else{
			foreach ($this->modelo->_inputs as $key => $value) {
					$dataToInsert[$key]=$this->input->post($key);
					if($dataToInsert[$key]==''){
						unset($dataToInsert[$key]);
					}
			}
			unset($dataToInsert[$this->modelo->_primary_key]);
			$this->form_validation->set_rules($this->modelo->_rules);
			$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
			//var_dump($dataToInsert);
			if($this->form_validation->run()==TRUE){

				if(array_key_exists('password',$dataToInsert)){
					$dataToInsert['password']=$this->encrypt->encode($dataToInsert['password']);
					//$dataToInsert['password']=='prueba que si existe';
				}
				$this->modelo->save($dataToInsert);
				echo 'ok';
			}else{
				echo $this->modelo->form($this->formSize);
			}
		}
	}

	public function getColumnsForTable(){
  	$columns='';
  	for($i=0; $i<count($this->dataTable); $i++) {
  		$arreglo=$this->dataTable[$i];
  		$columns.='{';
  		foreach ($arreglo as $key => $value) {
  			if($value=='true' || $value == 'false'){
  				$columns.=$key.':'.$value.',';
  			}else{
					//Aqui se hace la modificacion del lenguaje
					if($key=='title'){
						$value=$this->lang->line($value);
					}
  				$columns.=$key.':"'.$value.'",';
  			}
  		}
  		$columns.='},';
  	}
  	return $columns;
  }

	public function delete(){
		$key=$this->modelo->_primary_key;
		if($this->input->post($key)){
			$id=$this->input->post($key);
		}else{
			$id=$this->input->post('id');
		}
		if($this->modelo->delete($id)){
			echo $this->lang->line('error_delete').$id;
		}
	}

	public function isEmail($str){
			if(preg_match('/^[_a-z0-9-]+(.[_a-z0-9-]+)*@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,4})$/',$str)){
				return TRUE;
			}else{
				$this->form_validation->set_message('isEmail', $this->lang->line('isnt_email'));
				return FALSE;
			}
	}

	public function diferente_de_default($str){
		if($str==''){
				$this->form_validation->set_message('diferente_de_default',$this->lang->line('mustChoose'));
				return FALSE;
		}
		return TRUE;
	}

	public function withoutSignal($str){
		if(preg_match('/^\d+$/', $str)){
			return TRUE;
		}else{
			$this->form_validation->set_message('withoutSignal',$this->lang->line('without'));
			return FALSE;
		}
	}

	public function existeCuenta($str){
		$this->db->select('usuariosMonedero.id');
		$this->db->where('cuentaMonedero',$str);
		$query=$this->db->get('esb.usuariosMonedero');
		if($query->num_rows()>0){
			return FALSE;
		}

		return TRUE;
	}

	public function esCuentaNueva($cedula,$telefono){
		$this->db->select('usuariosMonedero.id');
		$this->db->where('cedula',$cedula);
		$this->db->or_where('phone',$telefono);
		$cedulas=$this->db->get('esb.usuariosMonedero');
		if($cedulas->num_rows()>0){
			return FALSE;
		}

		return TRUE;
	}

	/**
	 * @param $str
	 * @return bool
	 */
	public function isUniqueEmail($str){


		$this->db->select('email');
		$this->db->where('email',$str);
		$query=$this->db->get($this->modelo->_tablename);

		if($query->num_rows()>0){
			$this->form_validation->set_message('isUniqueEmail',$this->lang->line('uniqueEmail'));
			return FALSE;
		}
		return TRUE;
	}

	public function noContiene($str){
		$palabrasProhibidas=array('BNP','contraseña','password','Password123','password123');

		foreach ($palabrasProhibidas as $palabrasProhibida) {
			$pos = preg_match("/$palabrasProhibida/i", $str);
			if($pos){
				$this->form_validation->set_message('noContiene', 'La contaseña no puede contener la palabra : '.$palabrasProhibida);
				return FALSE;
			}
		}
		return TRUE;
	}

	public function noContenerNombre($str){
		$nombre=$this->input->post('name');
		$username=$this->input->post('username');

		$eva1=preg_match("/$nombre/i",$str);
		$eva2=preg_match("/$username/i",$str);

		if($eva2){
			$this->form_validation->set_message('noContenerNombre', 'La contaseña no puede contener el Nombre de Usuario : '.$str);
			return FALSE;
		}

		if($eva1){
			$this->form_validation->set_message('noContenerNombre', 'La contaseña no puede contener Nombre : '.$str);
			return FALSE;
		}

		return TRUE;
	}




}

?>

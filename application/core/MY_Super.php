<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Super extends CI_Controller {
	protected $model='';
	public $navbar_header=array("Administración"=>"Admin","mPOS"=>"MPOS","Monedero"=>"Monedero","CNB"=>"CNB","eCommerce"=>"Ecommerce",'idioma'=>'
						<select name="idioma" id="idioma" class="selectpicker" data-style="btn-info " >
							<option value="es">ES</option>
							<option value="en">EN</option>
						</select>
					');

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}

	public function index()
	{
		echo "Hola mundo";
	}

	public function navbar_header_menu(){

		$html='<div class="navbar-header"><button type="button" class="navbar-toggle"><span class="sr-only">Toggle navigation</span><span class="icon-bar bar1"></span><span class="icon-bar bar2"></span><span class="icon-bar bar3"></span></button>';
		foreach ($this->navbar_header as $key => $value) {

			if($key!='idioma')
			{
				$html.='<a class="navbar-brand separator" href="'.base_url().$value.'">'.$key.'</a>';
			}else
			{
				$html.='<a class="navbar-brand" href="#"><div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">'.$value.'</div></a>';
			}
		}
		$html.='</div>';
		return $html;
	}


}

/* End of file mY_SuperController.php */
/* Location: ./application/controllers/mY_SuperController.php */

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

$route['translate_uri_dashes'] = FALSE;


$route['default_controller'] = "Login";
$route['404_override'] = 'E_404';


/* End of file routes.php */
/* Location: ./application/config/routes.php */

//ADMINISTRACION
$route['Admin']="Admin/index";

$route['Admin/Monedas']='Admin/Monedas';
$route['Admin/Paises']='Admin/Paises';
$route['Admin/Canales']='AdminCanales/index';
$route['Admin/Procesadores']='AdminProcesadores/index';
$route['Admin/Merchants']='AdminMerchants/index';
$route['Admin/Operadores']='AdminOperadores/index';
$route['Admin/Agencias']='AdminAgencias/index';
$route['Admin/Terminales']='AdminTerminales/index';
$route['Admin/Bines']='AdminBine/index';
$route['Admin/GrupoBines']='AdminGrupoBine/index';
$route['Admin/BinTerminal']='AdminBinTerminal/index';
$route['Admin/Limites']='AdminLimites/index';
$route['Admin/RolesTerminal']='AdminRolesTerminal/index';
$route['Admin/PermisosTerminal']='AdminPermisosTerminal/index';
$route['Admin/UsersTerminales']='AdminUsersTerminal/index';
$route['Admin/Excepcion']='AdminExcepcion/index';
$route['Admin/Calendarios']='AdminCalendarios/index';
$route['Admin/Horarios']='AdminHorarios/index';


$route['Admin/Transacciones']='AdminTransacciones/index';
$route['Admin/Montos']='AdminMontos/index';
$route['Admin/LimitesTransacciones']='AdminLimitesTransacciones/index';
$route['Admin/GrupoTransacciones']='AdminGrupoTransacciones/index';


$route['UPD']='Perfil/UpdatePerfil';


$route['Users']='AdminSystem/index';





//Billetera
$route['Monedero']="MonederoPaper/index";
$route['Monedero']="MonederoPaper/index";
$route['Wallet/Users']='MonederoPaper/Users';
$route['Wallet/ComisionMonedero']='MonederoPaper/ComisionMonedero';
$route['Wallet/CountTrxUsers']='MonederoPaper/DetalleTrxByUsers';
$route['Monedero/Accesos']='MonederoPaper/Accesos';
$route['Wallet/Transacciones']='MonederoTransacciones_c/index';
$route['Monedero/Alertas']='MonederoPaper/Alertas';
$route['Wallet/EstadosDeCuentas']='MonederoEstados/index';
$route['Wallet/AtencionCliente']='MonederoSupport/index';
$route['CreandoMonederos']='MonederoSupport/CreateMonedero';
$route['ActividadComercial']='ActividadesComerciales/index';
$route['Wallet/SaldoPdf']='MonederoEstados/DescargarSaldo';
//CNB
$route['CNB']="CnbPaper/index";

$route['CNB/Reportes']='CnbPaper/Reportes';
$route['CNB/ReportesType']='CnbPaper/ReportesType';
$route['CNB/Ingresos']='CnbPaper/Ingresos';
$route['CNB/Comisiones']='CnbPaper/Comisiones';
$route['CNB/EstadodeCuentas']='CnbPaper/EstadodeCuentas';
$route['CNB/CNBInstalados']='CnbPaper/CNBInstalados';

//mPOS

$route['MPOS']="MposPaper/index";
$route['MPOS/FAQS']='MposPaper/faqs';
$route['MPOS/Accesos']='MposPaper/Accesos';
$route['MPOS/Transacciones']='MposPaper/Transacciones';
$route['MPOS/Alertas']='MposPaper/Alertas';

//ECOMMERCE


$route['Ecommerce']="EcommercePaper/index";
$route['Ecommerce/Diario']="EcommercePaper/rdiario";
$route['Ecommerce/Semanal']="EcommercePaper/rsemanal";
$route['Ecommerce/Mensual']="EcommercePaper/rmensual";
$route['Ecommerce/Anual']="EcommercePaper/ranual";
$route['Ecommerce/Personalizado']="EcommercePaper/rpersonalizado";

//LOGIN
$route['Iniciar']='Login/Log_in';

$route['Logout']='Admin/Logout';


$route['ListaNegra']='ListaNegra_c/index';

$route['Email']='Email/index';


$route['ForgotPass']='PasswordReset/index';
$route['ResetPass']='PasswordReset/ResetPass';
$route['ChangePass']='PasswordReset/ChangePass';
$route['InsertPass']='PasswordReset/InsertPass';

$route['Dispersion']='Dispersion/index';
$route['WINPT']='Winpt_C/index';

$route['helpcnb']='termscnb/Ayuda';
$route['helpmonedero']='termsmonedero/Ayuda';


$route['faqcnb']='FaqsView/FaqsCnb';
$route['faqbilletera']='FaqsView/FaqsBilletera';

?>

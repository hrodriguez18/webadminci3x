<?php
	$lang['settings'] = 'Configuraci&oacute;n';
	$lang['logout'] = 'Salir';
	$lang['home'] = 'Inicio';
	$lang['change_password'] = 'Debe cambiar su contrase&ntilde;a para continuar utilizando el sitio';
	$lang['agencies'] = 'Agencias';
	$lang['agency'] = 'Agencia';
	$lang['operators'] = 'Operadores';
	$lang['operator'] = 'Operador';
	$lang['terminals'] = 'Terminales';
	$lang['country'] = 'Pa&iacute;ses';
	$lang['currency'] = 'Monedas';
	$lang['billers'] = 'Procesadores';
	$lang['users'] = 'Usuarios';
	$lang['logLogin'] = 'Accesos';
	$lang['logTx'] = 'Transacciones';
	$lang['description'] = 'Descripci&oacute;n';
	$lang['status'] = 'Estado';
	$lang['statusAuth'] = 'Estado Autorizador';
	$lang['address'] = 'Direcci&oacute;n';
	$lang['lastname'] = 'Apellido';
	$lang['fname'] = 'Primer Nombre';
	$lang['sname'] = 'Segundo Nombre';
	$lang['flastname'] = 'Primer Apellido';
	$lang['slastname'] = 'Segundo Apellido';
	$lang['username'] = 'Usuario';
	$lang['password'] = 'Contrase&ntilde;a';
	$lang['email'] = 'Correo';
	$lang['name'] = 'Nombre';
	$lang['date'] = 'Fecha';
	$lang['codRespuesta'] = 'C&oacute;digo de Respuesta';
	$lang['imei'] = 'UUID Tel&eacute;fono';
	$lang['autorization'] = 'Autorizaci&oacute;n';
	$lang['amount'] = 'Monto';
	$lang['geo'] = 'Georeferencia';
	$lang['voucher'] = 'Tiquete';
	$lang['roles'] = 'Roles';
	$lang['category'] = 'Categor&iacute;a';
	$lang['permission'] = 'Permisos';
	$lang['rolepermission'] = 'Roles/Permisos';
	$lang['nopermission'] = 'No tiene permisos asignados para ver esta secci&oacute;n';
	$lang['phone'] = 'Tel&eacute;fono';
	$lang['sim'] = 'UUID Tel&eacute;fono';
	$lang['code'] = 'Codigo Confirmaci&oacute;n';
	$lang['swipe'] = 'ID de Dispositivo';
	$lang['user_exists'] = 'El usuario ingresado ya existe';
	$lang['validate_limite'] = 'El valor del l&iacute;mite no puede ser menor que cero';
	$lang['validate_direccion'] = 'El campo de direcci&oacute;n no puede contener eñes o tildes';
	$lang['moneda_exists'] = 'El c&oacute;digo de moneda ingresado ya existe';
	$lang['pais_exists'] = 'El codigo de pa&iacute;s ingresado ya existe';
	$lang['current_login_date'] = 'Fecha de Acceso';
	$lang['current_login_ip'] = 'IP de Acceso';
	$lang['last_login_ip'] = '&Uacute;ltima IP de Acceso';
	$lang['countryCode'] = 'C&oacute;digo de Pa&iacute;s';
	$lang['currencyCode'] = 'C&oacute;digo de Moneda';
	$lang['canal'] = 'Canales';
	$lang['contrato'] = 'Mensajes Push';
	$lang['faq'] = 'FAQ';
	$lang['titulo'] = 'T&iacute;tulo';
	$lang['selectOp'] = 'Seleccione Operador';
	$lang['flag'] = 'Bandera';
	$lang['ene'] = 'Enero';
	$lang['feb'] = 'Febrero';
	$lang['mar'] = 'Marzo';
	$lang['abr'] = 'Abril';
	$lang['may'] = 'Mayo';
	$lang['jun'] = 'Junio';
	$lang['jul'] = 'Julio';
	$lang['ago'] = 'Agosto';
	$lang['sep'] = 'Septiembre';
	$lang['oct'] = 'Octubre';
	$lang['nov'] = 'Noviembre';
	$lang['dic'] = 'Diciembre';
	$lang['month'] = 'Mes';
	$lang['country1'] = 'Pa&iacute;s';
	$lang['canal1'] = 'Canal';
	$lang['aut'] = 'Autorizador';
	$lang['all'] = 'Todos';
	$lang['dashboard'] = 'Dashboard Mobile POS - Pagos Móviles';
	$lang['sales'] = 'Ventas';
	$lang['sale'] = 'Venta';
	$lang['quantity'] = 'Cantidad';
	$lang['prom'] = 'Venta Promedio';
	$lang['reference'] = 'Referencia';
	$lang['commerce'] = 'Comercio';
	$lang['cardno'] = 'No. Tarjeta';
	$lang['tip'] = 'Propina';
	$lang['totalPay'] = 'Total a Pagar';
	$lang['dateFilter'] = 'Fecha de Consulta';
	$lang['filter'] = 'Filtrar';
	$lang['merchantId1'] = 'Merchant Id 1';
	$lang['merchantId2'] = 'Merchant Id 2';
	$lang['limite'] = 'Limite';
	$lang['customerNumber'] = 'Customer Number';
	$lang['dbaNumber'] = 'EPX DBA / ACP User';
	$lang['terminalNumber'] = 'EPX Terminal / ACP TParty';
	$lang['descriptionMer'] = 'EPX Descripci&oacute;n / ACP Password';
	$lang['merchantId'] = 'Merchant ID';
	$lang['merchants'] = 'Merchants';
	$lang['biller'] = 'Procesador';
	$lang['limiteTx'] = 'Limite de monto por Tx';
	$lang['limiteDia'] = 'Limite de monto por D&iacute;a';
	$lang['limiteMes'] = 'Limite de monto por Mes';
	$lang['txDia'] = 'Limite de Tx por D&iacute;a';
	$lang['disponibleDia'] = 'Disponible del D&iacute;a';
	$lang['disponibleMes'] = 'Disponible del Mes';
	$lang['disponibleTx'] = 'Disponible de Tx';
	$lang['limites'] = 'Limites';
	$lang['grupoBines'] = 'Grupo de Bines bloqueados';
	$lang['bines'] = 'Bines';
	$lang['year'] = 'Año';
	$lang['Lat1'] = 'L&iacute;mite de latitud inicial';
	$lang['Lat2'] = 'L&iacute;mite de latitud final';
	$lang['Lon1'] = 'L&iacute;mite de longitud inicial';
	$lang['Lon2'] = 'L&iacute;mite de longitud final';
	$lang['alertas'] = 'Alertas';
	$lang['save'] = 'Guardar';
	$lang['monitorLimites'] = 'Monitor l&iacute;mite';
	$lang['shortDescription'] = 'Descripci&oacute;n corta';
	$lang['cuenta'] = 'Numero de cuenta';
	$lang['token'] = 'Token';
	$lang['actividadComercial'] = 'Actividad comercial';
	$lang['wizard'] = 'Crear Token';
	$lang['usuarioCad'] = 'Nuevo Usuario';
	$lang['logweb'] = 'Log web';
	$lang['logprocesador'] = 'Log procesador';
	$lang['logccb'] = 'Log WS CCB';
	$lang['logs'] = 'Logs';
	$lang['directorio'] = 'Directorio';
	$lang['resetPass'] = 'Reset';
	$lang['resetPassword'] = 'Reset de contrase&ntilde;a';
	$lang['statusPassword'] = 'Estado de contrase&ntilde;a';
	$lang['askReset'] = 'Esta seguro que desea reiniciar la contrase&ntilde;a?';
	$lang['tipoDoc'] = 'Tipo de documento';
	$lang['documento'] = 'Documento';
	$lang['deleteError'] = 'La informaci&oacute;n no puede ser eliminada, porque existen dependencias con la misma.';
	$lang['passwordRepeat'] = 'No es posible ingresar la contrase&ntilde;a actual';
	//ECOMMERCE
	$lang['dashboard'] = 'Dashboard';
	$lang['reports'] = 'Reportes';
	$lang['dreport'] = 'Diario';
	$lang['wreport'] = 'Semanal';
	$lang['mreport'] = 'Mensual';
	$lang['areport'] = 'Anual';
	$lang['creport'] = 'Personalizado';
	$lang['ireport'] = 'Incidencias';
	$lang['dreport1'] = 'Reporte diario';
	$lang['wreport1'] = 'Reporte semanal';
	$lang['mreport1'] = 'Reporte mensual';
	$lang['areport1'] = 'Reporte anual';
	$lang['creport1'] = 'Reporte personalizado';
	$lang['ireport1'] = 'Incidencias';
	$lang['administration'] = 'Administracion';
	$lang['users'] = 'Usuarios';
	$lang['password'] = 'Contrase&ntilde;a';
	$lang['roles'] = 'Roles';
	$lang['timestamp'] = 'Fecha';
	$lang['transtype'] = 'Tipo';
	$lang['apiname'] = 'Comercio';
	$lang['amount'] = 'Monto';
	$lang['status'] = 'Estado';
	$lang['textresponse'] = 'Resultado';
	$lang['codedescription'] = 'Descripci&oacute;n';
	$lang['description'] = 'Descripci&oacute;n';
	$lang['username'] = 'Usuario';
	$lang['name'] = 'Nombre';
	$lang['email'] = 'Correo';
	$lang['date'] = 'Fecha';
	$lang['nopermission'] = 'No tiene permisos asignados para ver esta secci&oacute;n';
	$lang['user_exists'] = 'El usuario ingresado ya existe';
	$lang['current_login_date'] = 'Fecha de Acceso';
	$lang['current_login_ip'] = 'IP de Acceso';
	$lang['last_login_ip'] = '&Uacute;ltima IP de Acceso';
	$lang['resetPass'] = 'Reset';
	$lang['resetPassword'] = 'Reset de contrase&ntilde;a';
	$lang['statusPassword'] = 'Estado de contrase&ntilde;a';
	$lang['askReset'] = 'Esta seguro que desea reiniciar la contrase&ntilde;a?';
	$lang['tipoDoc'] = 'Tipo de documento';
	$lang['deleteError'] = 'La informaci&oacute;n no puede ser eliminada, porque existen dependencias con la misma.';
	$lang['passwordRepeat'] = 'No es posible ingresar la contrase&ntilde;a actual';
	$lang['transacciones24'] = 'transacciones las ultimas 24hr.';
	$lang['transacciones'] = 'TRANSACCIONES DIARIAS';
	$lang['dailysales'] = 'VENTAS DIARIAS';
	$lang['totalsalestoday'] = 'Total de ventas de hoy';
	$lang['avgTicket'] = 'TICKET PROMEDIO';
	$lang['uravgticket'] = 'El ticket promedio es';
	$lang['payment'] = 'TOP DE MARCAS';
	$lang['toppayment'] = 'La marca mas usada en compras es ';
	$lang['totalSales'] = 'TOTAL DE VENTAS';
	$lang['uhave'] = 'Tienes';
	$lang['salesin'] = 'en ventas y';
	$lang['transacciones1'] = 'transacciones.';
	$lang['lastestsales'] = 'Ultimas Ventas';
	$lang['ticket'] = 'Ticket';
	$lang['trader'] = 'Comercio';
	$lang['sales'] = 'VENTAS';
	$lang['quantity'] = 'Cantidad';
	$lang['operator'] = 'Operadores';
	$lang['agency'] = 'Agencias';
	$lang['address'] = 'Direccion';
	$lang['country'] = 'Pais';
	$lang['currency'] = 'Moneda';
	$lang['currencyCode'] = 'Codigo de moneda';
	$lang['shortDescription'] = 'Descripci&oacuten corta';
	$lang['countryCode'] = 'Codigo de pais';
	$lang['flag'] = 'Bandera';
	$lang['lat1'] = 'Latitud 1';
	$lang['lat2'] = 'Latitud 2';
	$lang['lon1'] = 'Longitud 1';
	$lang['lon2'] = 'Longitud 2';
	$lang['key'] = 'Llave de autorizaci&oacuten';
	$lang['cart'] = 'Carrito de compras';
	$lang['filter'] = 'Filtrar';
	$lang['mailSent'] = 'Se ha enviado un correo de confirmaci&oacuten';
	$lang['mailError'] = 'Ocurrio un problema con el correo, intente de nuevo';
	$lang['limiteTx'] = 'Limite de monto por Tx';
	$lang['limiteDia'] = 'Limite de monto por D&iacute;a';
	$lang['limiteMes'] = 'Limite de monto por Mes';
	$lang['monitorLimites'] = 'Monitor l&iacute;mite';
	$lang['disponibleDia'] = 'Disponible del D&iacute;a';
	$lang['disponibleMes'] = 'Disponible del Mes';
	$lang['disponibleTx'] = 'Disponible de Tx';
	$lang['limiteMontoTx'] = 'Monto limite por Tx';
	$lang['sale'] = 'Sale';
	$lang['auth'] = 'Authorization';
	$lang['capture'] = 'Capture';
	$lang['void'] = 'Void';
	$lang['refund'] = 'Refund';
	$lang['payments'] = 'POS Web';
	$lang['ccard'] = 'Numero de tarjeta de credito';
	$lang['expdate'] = 'Fecha de vencimiento';
	$lang['cvv'] = 'CVV/CVV2';
	$lang['zip'] = 'ZIP';
	$lang['saleT'] = 'Transacci&oacute;n de venta';
	$lang['authT'] = 'Transacci&oacute;n de autorizaci&oacute;n';
	$lang['captureT'] = 'Transacci&oacute;n de captura';
	$lang['voidT'] = 'Transacci&oacute;n void';
	$lang['refundT'] = 'Transacci&oacute;n de reversa';
	$lang['transid'] = 'Id de Transacci&oacute;n';
	$lang['saleAuthTime'] = 'Limite de tiempo para ventas duplicadas (segundos)';
	$lang['refundTime'] = 'Limite de tiempo para reversas duplicadas (segundos)';
	$lang['grupoBines'] = 'Grupo de Bines bloqueados';
	$lang['bines'] = 'Bines';
	$lang['blBines'] = 'Lista negra de BIN';
	$lang['company'] = 'Empresa';
	$lang['fname'] = 'Nombre';
	$lang['lname'] = 'Apellido';
	$lang['addr1'] = 'Direcci&oacute;n 1';
	$lang['addr2'] = 'Direcci&oacute;n 2';
	$lang['city'] = 'Ciudad';
	$lang['state'] = 'Estado';
	$lang['phone'] = 'Telefono';
	$lang['fax'] = 'Fax';
	$lang['authorize'] = 'Autorizar';
	$lang['txapproved'] = 'Transacci&oacute;n Aprobada';
	$lang['reference'] = 'Referencia';
	$lang['authorization'] = 'Autorizaci&oacute;n';
	$lang['commerce'] = 'Comercio';
	$lang['addr'] = 'Direcci&oacute;n';
	$lang['cardno'] = 'No. Tarjeta';
	$lang['salesre'] = 'Venta';
	$lang['totalpay'] = 'Total pagado';
	$lang['receipt'] = 'Recibo de pago';
	$lang['sendReceipt'] = 'Enviar comprobante';
	$lang['mailSuccess'] = 'El correo se envio satisfactoriamente!';
	$lang['mailNoSuccess'] = 'Ocurrio un problema enviando el correo!';
	$lang['purchaseCurrencyCode'] = 'Currency';
	$lang['purchaseAmount'] = 'Amount';
	$lang['authorizationCode'] = 'Auth Code';
	$lang['authorizationResponse'] = 'Auth Response';
	$lang['errorCode'] = 'Error';
	$lang['errorMessage'] = 'Error Message';
	$lang['acquirerId'] = 'Acquirer Id';
	$lang['idCommerce'] = 'Commerce Id';
	$lang['mcc'] = 'MCC';
	$lang['language'] = 'Language';
	$lang['userCommerce'] = 'User Commerce';
	$lang['programmingLanguage'] = 'Programming Lang';
	$lang['commerceAssociated'] = 'Commerce Asoc';
	$lang['claveSecretaAlg'] = 'Secret Key';
	$lang['userCodePayme'] = 'User Code PM';
	$lang['responseLink'] = 'Response Link';
	$lang['operador'] = 'Merchant';
	//CNB
	$lang['depositos'] = 'Depositos';
	$lang['retiros'] = 'Retiros';
	$lang['pagos'] = 'Pagos';
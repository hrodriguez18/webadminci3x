<?php

$lang['login'] = 'Entrar';
$lang['username'] = 'Usuario';
$lang['password'] = 'Contrase&ntilde;a';
$lang['password_recover'] = '&iquest;Olvid&oacute; su contrase&ntilde;a?';
$lang['login_error_user'] = 'Usuario o contrase&ntilde;a no v&aacute;lida.';
$lang['login_error_disabled'] = 'Usuario deshabilitado.  Contacte a su administrador.';
$lang['locked_error'] = 'Contrase&ntilde;a no v&aacute;lida.';
$lang['logout'] = 'Ha salido del sistema satisfactoriamente.';
$lang['locked'] = 'Bloqueo de pantalla';
$lang['exit'] = 'Salir del sistema';

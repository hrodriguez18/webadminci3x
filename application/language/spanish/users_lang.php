<?php

$lang['users'] = 'Usuarios';
$lang['name'] = 'Nombre';
$lang['email'] = 'Email';
$lang['username'] = 'Nombre de usuario';
$lang['type'] = 'Tipo';
$lang['status'] = 'Estado';
$lang['users_search'] = 'B&uacute;squeda de usuarios';
$lang['users_edit'] = 'Edici&oacute;n de usuarios';
$lang['operator'] = 'Operador';
$lang['agency'] = 'Agencia';
$lang['terminal'] = 'Terminal';
$lang['biller'] = 'Distribuidor';
$lang['readonly'] = 'S&oacute;lo lectura';
$lang['vterminal'] = 'Terminal de pago';
$lang['change_password'] = 'Cambiar contrase&ntilde;a';
$lang['password'] = 'Contrase&ntilde;a';
$lang['confirm'] = 'Confirmar';
$lang['user_exists'] = 'El usuario ya existe en el sistema';

/*Estados*/
$lang['ENABLED'] = 'Habilitado';
$lang['CHANGE_PASSWORD'] = 'Cambiar password';
$lang['BLOCKED'] = 'Bloqueado';
$lang['BLOCKED_ERROR'] = 'Bloqueado por error';

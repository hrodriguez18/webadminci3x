<?php

$lang['settings'] = 'Configuraci&oacute;n';
$lang['personal_information'] = 'Informaci&oacute;n personal';
$lang['contact_information'] = 'Informaci&oacute;n de contacto';
$lang['change_password'] = 'Cambiar su contrase&ntilde;a';
$lang['last_login_date'] = '&Uacute;ltima fecha de acceso';
$lang['last_change_pass'] = 'Fecha de actualizacion de contrase&ntilde;a';
$lang['last_login_ip'] = '&Uacute;ltima IP de acceso';
$lang['name'] = 'Nombre';
$lang['email'] = 'Email';
$lang['password'] = 'Contrase&ntilde;a';
$lang['confirm'] = 'Confirmar';
$lang['passValidation'] = 'La contrase&ntilde;a ingresada no cumple con el formato. Debe contener por lo menos una letra minuscula, una mayuscula y un numero';
$lang['passwordRepeat'] = 'No es posible ingresar la contrase&ntilde;a actual';
$lang['username'] = 'Usuario';

$lang['type']='Tipo';
$lang['operationid']='Id Operación';
$lang['agencyid']='Agencia';
$lang['terminalid']='Terminal ID';
$lang['rol_id']='Rol';
$lang['category_id']='Categoría';
$lang['status']='Estado';
$lang['fEntrance']='fEntrance';

$lang['already_user']="Lo sentimos, ya existe este Usuario";
$lang['already_email']="Lo sentimos, ya existe este Correo";
$lang['validate_option']="Favor escoger una opcion válida";
$lang['TitleRolesTerminal']='Datos Roles Terminal';

$lang['descripcion']='Descripción';
$lang['roles']='Roles';
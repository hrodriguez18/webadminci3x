<?php

$lang['settings'] = 'Configuraci&oacute;n';
$lang['logout'] = 'Salir';
$lang['home'] = 'Inicio';
$lang['btn-add']='Agregar';
$lang['change_password'] = 'Debe cambiar su contrase&ntilde;a para continuar utilizando el sitio';
$lang['agencies'] = 'Agencias';
$lang['agency'] = 'Agencia Corresponsal';
$lang['please_select'] = 'Favor Seleccione';
$lang['account']='Cuenta Bancaria';
$lang['mustChoose']='Debe elegir una opción para %s';
$lang['infoExcep']='Para poder guardar debe al menos ingresar una fecha';
$lang['valid_time']='Para guardar debe al menos ingresar una horario válido';
$lang['withoutpermission']='No se permiten roles sin permisos';
$lang['empty_bines']='No se permiten bines vacíos';
$lang['cnbUser']='Usuario CNB';
$lang['accountBNP']='Cuenta BN';
$lang['tipotrx']='Tipo de Transacción';
$lang['chooseFile']='Seleccionar Archivo';
$lang['formDepositos']='Formulario';
$lang['success']='Ha sido ingresado exitosamente';
$lang['info_upload_dispersion']='Para subir una dispersion ("Depositos Masivos"), sólo se permite formato .CSV y los datos se deben ingresar en el siguiente orden | NOMBRE | CEDULA | TELEFONO | CORREO | MONTO |,  sin cabeceras.';
$lang['cod_money']='Código Moneda';
$lang['label']='Etiqueta';
$lang['dispersion_deleted']='Se ha eliminado correctamente la tabla de Depósitos';

$lang['limitesMonedero']='Límites Billetera ';

$lang['codigoMpay']='Código de transacción';

$lang['lastTransaction']='Última Transacción';

$lang['limiteRetiroMes']='L. Retiro Mensual';
$lang['limiteDepositoMes']='Limite Maximo de cuenta';
$lang['comisionFijaTrx']='Comisión Fija Trx';
$lang['comisionPorcentualTrx']='Comisión %  Trx';

$lang['without']='El campo %s no debe contener signos o guiones ';
$lang['cedula_ya_existe']='Esta identificación ya existe';
$lang['required']='Este campo es necesario';
$lang['geolocation']='Geolocalización ';
$lang['deposito']='Depósitos';
$lang['WINPT']='WINPT';


$lang['NoTrx']='Trx financieras';
$lang['NoLogin']='Trx no financieras';


//=============================================================
//Billetera
$lang['walletUsers']='Usuarios Billetera';
$lang['sexo']='Sexo';
$lang['actions']='Acciones';
$lang['insertMessageSuccess']='Información Insertada!';
$lang['Attention']='Atención';
$lang['are_you_sure']='¿Estas Seguro?';
$lang['yes_delete_it']='Sí, Eliminar';
$lang['no_sure']='No estoy seguro';
$lang['cancelled']='Cancelado';
$lang['accountStatus']='Estados de Cuenta';
$lang['Ver']='Ver';
$lang['download']='Descargar';
$lang['load']='Cargar';
$lang['apply']='Aplicar';
$lang['btn_down']="<i class='fas fa-download'></i>";
$lang['btn_view']="<i class='far fa-eye'></i>";
$lang['element_safe']='El elemento está a salvo!';
$lang['server']='Servidor';
$lang['port']='Puerto';
$lang['method']='Method';
$lang['functions']='Función';
$lang['message_function_unique']='Ya existe un correo realizando esta función';
$lang['message_email_unique']='Este correo ya ha sido registrado';
$lang['isnt_email']='No es un correo válido';
$lang['documentation_email']='<p class="alert alert-warning"><strong>Para actualizar la contraseña sólo llena el campo, si no, puedes dejarlo vacío.</strong></p>';
$lang['advertencia_eliminar']='Estas a punto de eliminar un elemento de esta tabla...';
//=============================================================

//REPORTES CNB;
$lang['accountstatements']='Estado de Cuenta';
$lang['reportbytype']='Transacciones por tipo';
$lang['incomereports']='Reporte de Ingreso';
$lang['commissionreports']='Reporte de Comisiones';
$lang['walletAcount']='Cuenta Billetera';
$lang['subsidy']='Tipo de Subsidio';
$lang['created_at']='Fecha de Registro';
$lang['imei_m']='IMEI';
$lang['methodPay']='Forma Pago';

$lang['Comercialactivity']='Actividad Comercial';
$lang['report']='Reporte';
$lang['reportTrx']='Reporte Trx';
$lang['cnbinstalados']='CNB Instalados';
$lang['to']='Hasta : ';
$lang['since']='Desde : ';
$lang['color']='Color';

//=====================================================
$lang['userssystem'] = 'Usuarios de Sistema';
$lang['rolesterminal'] = 'Roles Corresponsal';
$lang['add_delete']='Agregar / Eliminar';
$lang['maps']='Mapas';
$lang['blacklist']='Lista Negra';
$lang['titleAdmin'] = 'Administración';
$lang['titleAdminCAP'] = 'ADMINISTRACIÓN';
$lang['operator'] = 'Comercio Facturador';
$lang['messageSupport']='Ningún Cliente Encontrado';
$lang['customerSupport'] = 'Atención a Cliente';
$lang['terminals'] = 'Terminales';
$lang['error_expression']='El campo de contraseña debe tener al menos: <ul> <li> (1) caracter mayuscula </li> <li> (1) carcater númerico </li><li> (8) carcateres mínimos </li><li> (16) carcateres máximos </li></ul>';
$lang['Description_Support']='<p class="alert alert-info"><strong>Para Modificar parametros de usuario primero debe completar el siguiente formulario, y luego verificar que se trata del usuario correcto.</strong></p>';
$lang['currency'] = 'Monedas';
$lang['billers'] = 'Procesadores';
$lang['users'] = 'Usuarios';
$lang['logLogin'] = 'Accesos';
$lang['logTx'] = 'Transacciones';
$lang['description'] = 'Descripci&oacute;n';
$lang['status'] = 'Estado';
$lang['statusAuth'] = 'Estado Autorizador';
$lang['address'] = 'Direcci&oacute;n';
$lang['lastname'] = 'Apellido';
$lang['fname'] = 'Primer Nombre';
$lang['sname'] = 'Segundo Nombre';
$lang['flastname'] = 'Primer Apellido';
$lang['slastname'] = 'Segundo Apellido';
$lang['username'] = 'Usuario';

$lang['email'] = 'Correo';
$lang['name'] = 'Nombre';
$lang['date'] = 'Fecha';
$lang['delete']='Rechazar';
$lang['error_delete']='Se ha eliminado el elemento con ID : ';
$lang['Birthdate']='Fecha de Nacimiento';
$lang['codRespuesta'] = 'C&oacute;digo de Respuesta';
$lang['imei'] = 'UUID Tel&eacute;fono';
$lang['autorization'] = 'Autorizaci&oacute;n';
$lang['amount'] = 'Monto';
$lang['geo'] = 'Georeferencia';
$lang['error_foreignkey']='No es posible eliminar este elemento : ';
$lang['voucher'] = 'Tiquete';
$lang['message_of_error_23000']='Podria relacionarse con otra tabla';
$lang['roles'] = 'Roles';
$lang['category'] = 'Categor&iacute;a';
$lang['permission'] = 'Permisos';
$lang['rolepermission'] = 'Roles/Permisos';
$lang['nopermission'] = 'No tiene permisos asignados para ver esta secci&oacute;n';
$lang['phone'] = 'Tel&eacute;fono';
$lang['sim'] = 'UUID Tel&eacute;fono';
$lang['code'] = 'Codigo Confirmaci&oacute;n';
$lang['swipe'] = 'ID de Dispositivo';
$lang['saldo']='Balance';
$lang['user_exists'] = 'El usuario ingresado ya existe';
$lang['validate_limite'] = 'El valor del l&iacute;mite no puede ser menor que cero';
$lang['validate_direccion'] = 'El campo de direcci&oacute;n no puede contener eñes o tildes';
$lang['moneda_exists'] = 'El c&oacute;digo de moneda ingresado ya existe';
$lang['pais_exists'] = 'El codigo de pa&iacute;s ingresado ya existe';
$lang['current_login_date'] = 'Fecha de Acceso';
$lang['current_login_ip'] = 'IP de Acceso';
$lang['last_login_ip'] = '&Uacute;ltima IP de Acceso';
$lang['countryCode'] = 'C&oacute;digo de Pa&iacute;s';
$lang['currencyCode'] = 'C&oacute;digo de Moneda';
$lang['canal'] = 'Canales';
$lang['contrato'] = 'Términos y Condiciones';
$lang['faq'] = 'FAQ';
$lang['tipoFaq'] = 'Tipo de FAQ';
$lang['titulo'] = 'T&iacute;tulo';
$lang['TitleFaqsCnb'] = 'Preguntas Frecuentes CNB';
$lang['TitleFaqsBilletera'] = 'Preguntas Frecuentes Billetera';
$lang['selectOp'] = 'Seleccione Operador';
$lang['flag'] = 'Bandera';
$lang['ene'] = 'Enero';
$lang['feb'] = 'Febrero';
$lang['mar'] = 'Marzo';
$lang['abr'] = 'Abril';
$lang['may'] = 'Mayo';
$lang['jun'] = 'Junio';
$lang['jul'] = 'Julio';
$lang['ago'] = 'Agosto';
$lang['sep'] = 'Septiembre';
$lang['oct'] = 'Octubre';
$lang['nov'] = 'Noviembre';
$lang['dic'] = 'Diciembre';
$lang['month'] = 'Mes';
$lang['country1'] = 'Pa&iacute;s';
$lang['canal1'] = 'Canal';
$lang['aut'] = 'Autorizador';
$lang['all'] = 'Todos';
$lang['dashboard'] = 'Dashboard Mobile POS - Pagos Móviles';
$lang['sales'] = 'Ventas';
$lang['sale'] = 'Venta';
$lang['average']='Promedio';
$lang['returns']='Devoluciones';
$lang['quantity'] = 'Cantidad';
$lang['prom'] = 'Venta Promedio';
$lang['reference'] = 'Referencia';
$lang['commerce'] = 'Comercio';
$lang['cardno'] = 'No. Tarjeta';
$lang['tip'] = 'Propina';
$lang['totalPay'] = 'Total a Pagar';
$lang['dateFilter'] = 'Fecha de Consulta';
$lang['filter'] = 'Filtrar';
$lang['merchantId1'] = 'Merchant Id 1';
$lang['merchantId2'] = 'Merchant Id 2';
$lang['limite'] = 'Limite';
$lang['schedule'] = 'Horarios';
$lang['exception'] = 'Excepciones';
$lang['scheduling'] = 'Calendarización';
$lang['customerNumber'] = 'Customer Number';
$lang['dbaNumber'] = 'EPX DBA / ACP User';
$lang['terminalNumber'] = 'EPX Terminal / ACP TParty';
$lang['descriptionMer'] = 'EPX Descripci&oacute;n / ACP Password';
$lang['merchantId'] = 'Merchant ID';
$lang['merchants'] = 'Merchants';
$lang['biller'] = 'Procesador';
$lang['limiteTx'] = 'Limite de monto por Tx';
$lang['limiteDia'] = 'Limite de monto por D&iacute;a';
$lang['limiteMes'] = 'Limite de monto por Mes';
$lang['txDia'] = 'Limite de Tx por D&iacute;a';
$lang['disponibleDia'] = 'Disponible del D&iacute;a';
$lang['disponibleMes'] = 'Disponible del Mes';
$lang['disponibleTx'] = 'Disponible de Tx';
$lang['limites'] = 'Limites';
$lang['grupoBines'] = 'Grupo de Bines bloqueados';
$lang['bines'] = 'Bines';
$lang['year'] = 'Año';
$lang['Channels'] = 'Canales';
$lang['limits'] = 'Límites';
$lang['channel']='Canal';

$lang['commissionaccount']='Cuenta Comisión';
$lang['commissions']='Precios Y Comisiones';
$lang['commission']='Comisión';
$lang['monetization'] = 'Parámetros Transac.';
$lang['TransactionsLimits']='Rangos Transacción';
$lang['TransactionsGroup'] = 'Grupo Transacciones';
$lang['Lat1'] = 'L&iacute;mite de latitud inicial';
$lang['Lat2'] = 'L&iacute;mite de latitud final';
$lang['Lon1'] = 'L&iacute;mite de longitud inicial';
$lang['Lon2'] = 'L&iacute;mite de longitud final';
$lang['userterminal'] = 'Usuarios Corresponsal';
$lang['save'] = 'Guardar';
$lang['monitorLimites'] = 'Monitor l&iacute;mite';
$lang['shortDescription'] = 'Descripci&oacute;n corta';
$lang['cuenta'] = 'Numero de cuenta';
$lang['token'] = 'Token';
$lang['actividadComercial'] = 'Actividad comercial';
$lang['wizard'] = 'Crear Token';
$lang['usuarioCad'] = 'Nuevo Usuario';
$lang['logweb'] = 'Log web';
$lang['logprocesador'] = 'Log procesador';
$lang['logccb'] = 'Log WS CCB';
$lang['directorio'] = 'Directorio';
$lang['resetPass'] = 'Reset';
$lang['resetPassword'] = 'Reset de contrase&ntilde;a';
$lang['statusPassword'] = 'Estado de contrase&ntilde;a';
$lang['askReset'] = 'Esta seguro que desea reiniciar la contrase&ntilde;a?';
$lang['tipoDoc'] = 'Tipo de documento';
$lang['documento'] = 'Documento de identificación';
$lang['deleteError'] = 'La informaci&oacute;n no puede ser eliminada, porque existen dependencias con la misma.';
$lang['passwordRepeat'] = 'No es posible ingresar la contrase&ntilde;a actual';
//ECOMMERCE
$lang['dashboard'] = 'Dashboard';
$lang['reports'] = 'Reportes';
$lang['dreport'] = 'Diario';
$lang['wreport'] = 'Semanal';
$lang['mreport'] = 'Mensual';
$lang['areport'] = 'Anual';
$lang['creport'] = 'Personalizado';
$lang['ireport'] = 'Incidencias';
$lang['dreport1'] = 'Reporte diario';
$lang['wreport1'] = 'Reporte semanal';
$lang['mreport1'] = 'Reporte mensual';
$lang['areport1'] = 'Reporte anual';
$lang['creport1'] = 'Reporte personalizado';
$lang['ireport1'] = 'Incidencias';
$lang['administration'] = 'Administración';
$lang['users'] = 'Usuarios';
$lang['mPOS']='mPOS';
$lang['CNB']='CNB';
$lang['Monedero']='Billetera';
$lang['eCommerce']='eCommerce';
$lang['roles'] = 'Roles';
$lang['timestamp'] = 'Fecha';
$lang['transtype'] = 'Tipo';
$lang['apiname'] = 'Comercio';
$lang['amount'] = 'Monto';
$lang['status'] = 'Estado';
$lang['texto'] = 'Texto';
$lang['textresponse'] = 'Resultado';
$lang['codedescription'] = 'Descripci&oacute;n';
$lang['description'] = 'Descripci&oacute;n';
$lang['username'] = 'Usuario';
$lang['name'] = 'Nombre';
$lang['email'] = 'Correo';
$lang['date'] = 'Fecha';
$lang['nopermission'] = 'No tiene permisos asignados para ver esta secci&oacute;n';
$lang['user_exists'] = 'El usuario ingresado ya existe';
$lang['current_login_date'] = 'Fecha de Acceso';
$lang['current_login_ip'] = 'IP de Acceso';
$lang['last_login_ip'] = '&Uacute;ltima IP de Acceso';
$lang['resetPass'] = 'Reset';
$lang['resetPassword'] = 'Reset de contrase&ntilde;a';
$lang['statusPassword'] = 'Estado de contrase&ntilde;a';
$lang['askReset'] = 'Esta seguro que desea reiniciar la contrase&ntilde;a?';
$lang['tipoDoc'] = 'Tipo de documento';
$lang['deleteError'] = 'La informaci&oacute;n no puede ser eliminada, porque existen dependencias con la misma.';
$lang['passwordRepeat'] = 'No es posible ingresar la contrase&ntilde;a actual';
$lang['transacciones24'] = 'transacciones las ultimas 24hr.';
$lang['TD'] = 'TRANSA. DIARIAS';
$lang['dailysales'] = 'VENTAS DIARIAS';
$lang['totalsalestoday'] = 'Total de ventas de hoy';
$lang['avgTicket'] = 'TICKET PROMEDIO';
$lang['uravgticket'] = 'El ticket promedio es';
$lang['payment'] = 'TOP DE MARCAS';
$lang['toppayment'] = 'La marca mas usada en compras es ';
$lang['totalSales'] = 'TOTAL DE VENTAS';
$lang['uhave'] = 'Tienes';
$lang['salesin'] = 'en ventas y';
$lang['transacciones1'] = 'transacciones.';
$lang['lastestsales'] = 'Ultimas Ventas';
$lang['ticket'] = 'Ticket';
$lang['trader'] = 'Comercio';
$lang['sales'] = 'Ventas';
$lang['quantity'] = 'Cantidad';

$lang['lastTransaction']='Última Trx';
$lang['dateLastTrx']='Fecha última Trx';

$lang['detailTrxByUsers']='Detalle Trx Por Usuario';

$lang['NumTrx']='No. Trx';

$lang['commissionWallet']='Comisiones Billetera';

$lang['address'] = 'Dirección';
$lang['country'] = 'Pais';
$lang['currency'] = 'Moneda';
$lang['currencyCode'] = 'Codigo de moneda';
$lang['shortDescription'] = 'Descripci&oacuten corta';
$lang['countryCode'] = 'Codigo de pais';
$lang['latitud'] = 'Latitud';
$lang['longitud'] = 'Longitud';
$lang['flag'] = 'Bandera';
$lang['lat1'] = 'Latitud 1';
$lang['lat2'] = 'Latitud 2';
$lang['lon1'] = 'Longitud 1';
$lang['lon2'] = 'Longitud 2';
$lang['key'] = 'Llave de autorizaci&oacuten';
$lang['cart'] = 'Carrito de compras';
$lang['filter'] = 'Filtrar';
$lang['mailSent'] = 'Se ha enviado un correo de confirmaci&oacuten';
$lang['mailError'] = 'Ocurrio un problema con el correo, intente de nuevo';
$lang['limiteTx'] = 'Limite de monto por Tx';
$lang['limiteDia'] = 'Limite de monto por D&iacute;a';
$lang['limiteMes'] = 'Limite de monto por Mes';
$lang['monitorLimites'] = 'Monitor l&iacute;mite';
$lang['disponibleDia'] = 'Disponible del D&iacute;a';
$lang['disponibleMes'] = 'Disponible del Mes';
$lang['disponibleTx'] = 'Disponible de Tx';
$lang['limiteMontoTx'] = 'Monto limite por Tx';
$lang['sale'] = 'Sale';
$lang['auth'] = 'Authorization';
$lang['capture'] = 'Capture';
$lang['void'] = 'Void';
$lang['refund'] = 'Refund';


$lang['credit'] = 'Crédito';
$lang['debit']='Débito';
$lang['devolution']='Devoluciones';
$lang['balance']='Balance';




$lang['payments'] = 'POS Web';
$lang['ccard'] = 'Numero de tarjeta de credito';
$lang['expdate'] = 'Fecha de vencimiento';
$lang['cvv'] = 'CVV/CVV2';
$lang['zip'] = 'ZIP';
$lang['saleT'] = 'Transacci&oacute;n de venta';
$lang['authT'] = 'Transacci&oacute;n de autorizaci&oacute;n';
$lang['captureT'] = 'Transacci&oacute;n de captura';
$lang['voidT'] = 'Transacci&oacute;n void';
$lang['refundT'] = 'Transacci&oacute;n de reversa';
$lang['transid'] = 'Id de Transacci&oacute;n';
$lang['saleAuthTime'] = 'Limite de tiempo para ventas duplicadas (segundos)';
$lang['refundTime'] = 'Limite de tiempo para reversas duplicadas (segundos)';
$lang['grupoBines'] = 'Grupo de Bines bloqueados';
$lang['bines'] = 'Bines';
$lang['blBines'] = 'Lista negra de BIN';
$lang['company'] = 'Empresa';
$lang['fname'] = 'Nombre';
$lang['lname'] = 'Apellido';
$lang['addr1'] = 'Direcci&oacute;n 1';
$lang['addr2'] = 'Direcci&oacute;n 2';
$lang['city'] = 'Ciudad';
$lang['state'] = 'Estado';
$lang['phone'] = 'Teléfono';
$lang['fax'] = 'Fax';
$lang['authorize'] = 'Autorizar';
$lang['txapproved'] = 'Transacci&oacute;n Aprobada';
$lang['reference'] = 'Referencia';
$lang['authorization'] = 'Autorizaci&oacute;n';
$lang['commerce'] = 'Comercio';
$lang['addr'] = 'Direcci&oacute;n';
$lang['cardno'] = 'No. Tarjeta';
$lang['salesre'] = 'Venta';
$lang['cedula']='Cédula / RUC';
$lang['tokenEnviado']='Token Enviado';
$lang['up']='Subir';
$lang['totalpay'] = 'Total pagado';
$lang['receipt'] = 'Recibo de pago';
$lang['sendReceipt'] = 'Enviar comprobante';
$lang['mailSuccess'] = 'El correo se envio satisfactoriamente!';
$lang['mailNoSuccess'] = 'Ocurrio un problema enviando el correo!';
$lang['purchaseCurrencyCode'] = 'Currency';
$lang['products_impuestos']='Todos los productos incluyen impuestos';
$lang['purchaseAmount'] = 'Amount';
$lang['DescriptionofMAP']='Descripción de MAPA';
$lang['authorizationCode'] = 'Auth Code';
$lang['authorizationResponse'] = 'Auth Response';
$lang['errorCode'] = 'Error';
$lang['errorMessage'] = 'Error Message';
$lang['acquirerId'] = 'Acquirer Id';
$lang['idCommerce'] = 'Commerce Id';
$lang['mcc'] = 'MCC';
$lang['language'] = 'Language';
$lang['userCommerce'] = 'User Commerce';
$lang['programmingLanguage'] = 'Programming Lang';
$lang['commerceAssociated'] = 'Commerce Asoc';
$lang['claveSecretaAlg'] = 'Secret Key';
$lang['userCodePayme'] = 'User Code PM';
$lang['responseLink'] = 'Response Link';
$lang['operador'] = 'Merchant';
//CNB
$lang['depositos'] = 'Depositos';
$lang['depositoscnb'] = 'Depositos CNB';
$lang['retiros'] = 'Retiros';
$lang['pagos'] = 'Pagos';

$lang['depositos'] = 'Depositos Billetera';

$lang['depositos_retiros']='Depositos / Retiros';

$lang['settings'] = 'Configuraci&oacute;n';
$lang['personal_information'] = 'Informaci&oacute;n personal';
$lang['contact_information'] = 'Informaci&oacute;n de contacto';
$lang['change_password'] = 'Cambiar su contrase&ntilde;a';
$lang['last_login_date'] = '&Uacute;ltima fecha de acceso';
$lang['last_change_pass'] = 'Fecha de actualizacion de contrase&ntilde;a';
$lang['last_login_ip'] = '&Uacute;ltima IP de acceso';
$lang['name'] = 'Nombre';
$lang['cnbOperador']='Comercio CNB';


$lang['comisionCNB']='Comisión CNB';
$lang['comisionBanco']='Comisión Banco';

$lang['password'] = 'Contrase&ntilde;a';
$lang['confirm'] = 'Confirmar';
$lang['passValidation'] = 'La contrase&ntilde;a ingresada no cumple con el formato. Debe contener por lo menos una letra minuscula, una mayuscula y un numero';
$lang['passwordRepeat'] = 'No es posible ingresar la contrase&ntilde;a actual';
$lang['username'] = 'Usuario';

$lang['type']='Tipo';
$lang['operationid']='Id Operación';

$lang['terminalid']='Terminal ID';
$lang['rol_id']='Rol';
$lang['category_id']='Categoría';
$lang['status']='Estado';
$lang['fEntrance']='fEntrance';

$lang['already_user']="Lo sentimos, ya existe este Usuario";
$lang['already_email']="Lo sentimos, ya existe este Correo";
$lang['validate_option']="Favor escoger una opcion válida";
$lang['TitleRolesTerminal']='Datos Roles Terminal';

$lang['uniqueEmail']='El Campo %s debe ser único';

$lang['descripcion']='Descripción';
$lang['passwordConfirm']='Confirmar Contraseña';
$lang['roles']='Roles';

$lang['users'] = 'Usuarios';
$lang['name'] = 'Nombre';

$lang['username'] = 'Nombre de usuario';
$lang['type'] = 'Tipo';
$lang['status'] = 'Estado';
$lang['btn_search']='Buscar';
$lang['users_search'] = 'B&uacute;squeda de usuarios';
$lang['users_edit'] = 'Edici&oacute;n de usuarios';


$lang['terminal'] = 'Terminal';
$lang['biller'] = 'Procesador';
$lang['readonly'] = 'S&oacute;lo lectura';
$lang['vterminal'] = 'Terminal de pago';
$lang['change_password'] = 'Cambiar contrase&ntilde;a';
$lang['password'] = 'Contrase&ntilde;a';
$lang['confirm'] = 'Confirmar';
$lang['user_exists'] = 'El usuario ya existe en el sistema';

/*Estados*/
$lang['ENABLED'] = 'Habilitado';
$lang['CHANGE_PASSWORD'] = 'Cambiar password';
$lang['BLOCKED'] = 'Bloqueado';
$lang['BLOCKED_ERROR'] = 'Bloqueado por error';



$lang['your_account'] = 'Su cuenta';
$lang['block'] = 'Bloquear';
$lang['settings'] = 'Configuraci&oacute;n';
$lang['logout'] = 'Salir';
$lang['list'] = 'Listado';
$lang['new'] = 'Crear';
$lang['home'] = 'Inicio';
$lang['users'] = 'Usuarios';
$lang['configuration'] = 'Configuraci&oacute;n';
$lang['status'] = 'Estados';
$lang['billers'] = 'Procesadores';
$lang['merchants'] = 'Merchants';
$lang['limites'] = 'Limites';
$lang['operators'] = 'Comercio Corresponsal';
$lang['agencies'] = 'Agencias';
$lang['terminals'] = 'Terminales';
$lang['pos'] = 'POS';
$lang['transactions'] = 'Transacciones';
$lang['terminal'] = 'Terminal';
$lang['users'] = 'Usuarios';
$lang['logLogin'] = 'Accesos';
$lang['logTx'] = 'Transacciones';
$lang['actividadComercial'] = 'Actividad Comercial';
$lang['btn-upload']='Cargar desde archivo';
$lang['currency'] = 'Monedas';
$lang['description'] = 'Descripci&oacute;n';
$lang['status'] = 'Estado';
$lang['roles'] = 'Roles';
$lang['category'] = 'Categor&iacute;a';
$lang['permission'] = 'Permisos';
$lang['rolepermission'] = 'Roles/Permisos';
$lang['canal'] = 'Canales';
$lang['contrato'] = 'Contrato';
$lang['faq'] = 'FAQ';
$lang['titulo'] = 'Titulo';
$lang['alertas'] = 'Alertas';
$lang['monitorLimites'] = 'Monitor l&iacute;mite';
$lang['wizard'] = 'Crear Token';
$lang['logweb'] = 'Log web';
$lang['logprocesador'] = 'Log procesador';
$lang['logccb'] = 'Log WS CCB';
$lang['logs'] = 'Logs de auditoria';

$lang['login'] = 'Entrar';
$lang['username'] = 'Usuario';
$lang['password_recover'] = '&iquest;Olvid&oacute; su contrase&ntilde;a?';
$lang['login_error_user'] = 'Usuario o contrase&ntilde;a no v&aacute;lida.';
$lang['login_error_disabled'] = 'Usuario deshabilitado.  Contacte a su administrador.';
$lang['locked_error'] = 'Contrase&ntilde;a no v&aacute;lida.';
$lang['logout'] = 'Salir';
$lang['locked'] = 'Bloqueo de pantalla';
$lang['exit'] = 'Salir del sistema';
$lang['profile'] = 'Perfil';
$lang['file'] = 'Archivo';
$lang['ID']='ID';
$lang['infoChangePassword']='<p>Para Cambiar Contraseña, debe ingresar <strong>la contraseña Antigua</strong>, Luego crear una nueva y confirmarla.</p>';
$lang['oldPassword']='Contraseña Antigua';
$lang['newPassword']='Nueva Contraseña';
$lang['update']='Actualizar';
$lang['agenteCorresponsal']='Agente Corresponsal';

//placeholders

$lang['oldPassword_placeholder']='Contraseña Antigua';
$lang['newPassword_placeholder']='Nueva Contraseña';
$lang['update_placeholder']='Actualizar';
$lang['confirm_placeholder']='Confirmar Nueva Contraseña';
$lang['errorEmptyForm']='El formulario no puede estar vacío';

?>

<?php

$lang['settings'] = 'Settings';
$lang['personal_information'] = 'Personal Information';
$lang['contact_information'] = 'Contact Information';
$lang['change_password'] = 'Change Password';
$lang['username'] = 'Username';
$lang['last_login_date'] = 'Last Login Date';
$lang['last_change_pass'] = 'Password update';
$lang['last_login_ip'] = 'Last Login IP';
$lang['name'] = 'Name';
$lang['email'] = 'Email';
$lang['password'] = 'Password';
$lang['confirm'] = 'Confirm';
$lang['passValidation'] = 'The password you entered does not comply with the format. It must contain at least one lowercase letter, an uppercase and a number';
$lang['passwordRepeat'] = 'Is not allowed to use the actual password';
$lang['type']='Type';

$lang['operationid']='Operation Id';
$lang['agencyid']='Agency';
$lang['terminalid']='Terminal ID';
$lang['category_id']='Category';
$lang['status']='Status';
$lang['fEntrance']='fEntrance';


$lang['already_user']="Sorry, this user already exists";
$lang['already_email']="Sorry, this email already exists";
$lang['validate_option']="Please, select a valid option'";
$lang['TitleRolesTerminal']='Data Terminal Rol';
$lang['descripcion']='Description';
$lang['roles']='Rol';
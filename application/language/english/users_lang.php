<?php
	$lang['users'] = 'Users';
	$lang['name'] = 'Name';
	$lang['email'] = 'Email';
	$lang['username'] = 'Username';
	$lang['type'] = 'Type';
	$lang['status'] = 'Status';
	$lang['users_search'] = 'Users Search';
	$lang['users_edit'] = 'Users Edit';
	$lang['operator'] = 'Operatoror';
	$lang['agency'] = 'Agency';
	$lang['terminal'] = 'Terminal';
	$lang['biller'] = 'Biller';
	$lang['readonly'] = 'S&oacute;lo lectura';
	$lang['vterminal'] = 'Terminal de pago';
	$lang['change_password'] = 'Change Password';
	$lang['password'] = 'Password';
	$lang['confirm'] = 'Confirm';
	$lang['user_exists'] = 'User Already Exists';

	/*Estados*/
	$lang['ENABLED'] = 'Enabled';
	$lang['CHANGE_PASSWORD'] = 'Change Password';
	$lang['BLOCKED'] = 'Blocked';
	$lang['BLOCKED_ERROR'] = 'Blocked Error';

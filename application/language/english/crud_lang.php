<?php
	$lang['-SEARCH_FORM-'] = 'Seach';
	$lang['-LIST_FORM-'] = 'List';
	$lang['-EDIT_FORM-'] = 'Edit';
	$lang['-CREATE_FORM-'] = 'Create';
	$lang['-CHANGE-'] = 'Change';
	$lang['-CREATE-'] = 'Create';
	$lang['-CHANGEOK-'] = 'Change OK';
	$lang['-CHANGENO-'] = 'Change Not OK';
	$lang['-CREATEOK-'] = 'Created OK';
	$lang['-SEARCH-'] = 'Search';
	$lang['-ALL-'] = 'All';
	$lang['-FIRST-'] = 'Firts';
	$lang['-LAST-'] = 'Last';
	$lang['-NO_RESULTS-'] = 'No Results';
	$lang['-EXCEL-'] = 'Excel';
	$lang['-BACK-'] = 'Back';
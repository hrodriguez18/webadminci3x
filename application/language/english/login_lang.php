<?php

$lang['login'] = 'Login';
$lang['username'] = 'Username';
$lang['password'] = 'Password';
$lang['password_recover'] = 'Forgot your password?';
$lang['login_error_user'] = 'Invalid User or Password';
$lang['login_error_disabled'] = 'Disabled User, contact your administrator';
$lang['locked_error'] = 'Invalid Password';
$lang['logout'] = 'Successful Logout';
$lang['locked'] = 'User Locked';
$lang['exit'] = 'Logout';
$lang['dashboard'] = 'Mobile POS Dashboard - Pagos Móviles';

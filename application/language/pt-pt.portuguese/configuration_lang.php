<?php

$lang['settings'] = 'Configuração';
$lang['personal_information'] = 'Informações Pessoais';
$lang['contact_information'] = 'Informações de Contato';
$lang['change_password'] = 'Alterar sua senha';
$lang['username'] = 'Usu&aacute;rio';
$lang['last_login_date'] = '&Uacute;ltima data de acesso';
$lang['last_login_ip'] = '&Uacute;ltima acesso IP';
$lang['name'] = 'Nome';
$lang['email'] = 'Correio';
$lang['password'] = 'Senha';
$lang['confirm'] = 'Confirmar';

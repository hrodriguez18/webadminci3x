<?php

$lang['login'] = 'Entrar';
$lang['username'] = 'Usu&aacute;rio';
$lang['password'] = 'Senha';
$lang['password_recover'] = '&iquest;Esqueceu sua senha?';
$lang['login_error_user'] = 'Nome de usu&atilde;rio ou senha inv&atilde;lida';
$lang['login_error_disabled'] = 'Usu&aacute;rio desativado. Contacte o seu administrador.';
$lang['locked_error'] = 'Senha Inv&aacute;lida';
$lang['logout'] = 'Ele saiu com êxito o sistema';
$lang['locked'] = 'Bloqueio de tela';
$lang['exit'] = 'Sair';

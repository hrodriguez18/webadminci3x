<?php
	$lang['your_account'] = 'Sua Conta';
	$lang['block'] = 'Bloco';
	$lang['settings'] = 'Definições';
	$lang['logout'] = 'Sair';
	$lang['list'] = 'Listagem';
	$lang['new'] = 'Criar';
	$lang['home'] = 'Iniciação';
	$lang['users'] = 'Usuarios';
	$lang['configuration'] = 'Configuração';
	$lang['status'] = 'Estados';
	$lang['billers'] = 'Processadores';
	$lang['merchants'] = 'Comerciantes';
	$lang['limites'] = 'Limites';
	$lang['operators'] = 'Operadores';
	$lang['agencies'] = 'Agências';
	$lang['terminals'] = 'Terminais';
	$lang['pos'] = 'POS';
	$lang['transactions'] = 'Negociações';
	$lang['terminal'] = 'Terminal';
	$lang['users'] = 'Usuarios';
	$lang['logLogin'] = 'Acesso';
	$lang['logTx'] = 'Negociações';
	$lang['country'] = 'Pa&iacute;ses';
	$lang['currency'] = 'Moedas';
	$lang['description'] = 'Sescrição';
	$lang['status'] = 'Estado';
	$lang['roles'] = 'Roles';
	$lang['category'] = 'Categoria';
	$lang['permission'] = 'Autorizações';
	$lang['rolepermission'] = 'Roles/Autorizações';
	$lang['canal'] = 'Canais';
	$lang['contrato'] = 'Contrato';
	$lang['faq'] = 'FAQ';
	$lang['titulo'] = 'T&iacute;tulo';
	$lang['alertas'] = 'Alertas';
	$lang['monitorLimites'] = 'Limites de monitor';
<?php

$lang['users'] = 'Usu&aacute;rios';
$lang['name'] = 'Nome';
$lang['email'] = 'Correio';
$lang['username'] = 'Nome de usu&aacute;rio';
$lang['type'] = 'Tipo';
$lang['status'] = 'Estado';
$lang['users_search'] = 'Procura do Usu&aacute;rio';
$lang['users_edit'] = 'Edici&oacute;n de Usu&aacute;rios';
$lang['operator'] = 'Operador';
$lang['agency'] = 'Agência';
$lang['terminal'] = 'Terminal';
$lang['change_password'] = 'Alterar a senha';
$lang['password'] = 'Senha';
$lang['confirm'] = 'Confirmar';
$lang['user_exists'] = 'O usuário já existe no sistema';

/*Estados*/
$lang['ENABLED'] = 'Ativado';
$lang['CHANGE_PASSWORD'] = 'Alterar a senha';
$lang['BLOCKED'] = 'Trancado';
$lang['BLOCKED_ERROR'] = 'Bloqueado por engano';

<?php

$lang['-SEARCH_FORM-'] = 'B&uacute;squeda';
$lang['-LIST_FORM-'] = 'Listado';
$lang['-EDIT_FORM-'] = 'Edici&oacute;n';
$lang['-CREATE_FORM-'] = 'Crear';
$lang['-CHANGE-'] = 'Cambiar';
$lang['-CREATE-'] = 'Crear';
$lang['-CHANGEOK-'] = 'Informaci&oacute;n modificada de forma correcta';
$lang['-CHANGENO-'] = 'No se pudo modificar la informaci&oacute;n';
$lang['-CREATEOK-'] = 'Elemento creado de forma correcta';
$lang['-SEARCH-'] = 'Buscar';
$lang['-ALL-'] = 'Todos';
$lang['-FIRST-'] = 'Primero';
$lang['-LAST-'] = '&Uacute;ltimo';
$lang['-NO_RESULTS-'] = 'No existen resultados';
$lang['-EXCEL-'] = 'Excel';
$lang['-BACK-'] = 'Atr&aacute;s';
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class AdminBine extends MY_Controller{

	public $permiso_crear=65;
	public $permiso_actualizar=66;
	public $permiso_eliminar=67;
	public $permiso_ver=28;

	public $contenido=array();
	protected $dataForm=array('title_form'=>'Datos Bine');


	public function __construct(){
        parent::__construct();
        $this->load->model('bines_m');

    }

    public function index(){

    	$this->contenido['table']=$this->load->view("paper/tables/table_bines",'',true);
    	$this->contenido['title']=$this->lang->line('bines');
		$datos["content_view"]=$this->load->view("paper/admin/paper_view_bines",$this->contenido,true);
		$datos['navbar_menu']=$this->navbar_header_menu();
		$datos['sidebar_menu']=$this->menu_admin();
		$datos['footer']=$this->load->view("paper/footers/footer_bines",'',true);
		$this->load->view("paper/base",$datos);

    }
    public function populateTable(){
    	echo json_encode($this->bines_m->get());
    }

    public function Form(){
    	$this->dataForm['form']=$this->bines_m->Form();
    	echo $this->load->view("paper/forms_modal/form_bines",$this->dataForm,true);
    }

    public function saveData(){
		$this->form_validation->set_rules($this->bines_m->rules);
		$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
		if($this->form_validation->run()==TRUE){
            $data['bine']=$this->input->post('bine');
            $data['descripcion']=$this->input->post('descripcion');
            $data['status']=$this->input->post('status');
			$this->bines_m->save($data);
			echo "Todo bien!";
		}else{
			$this->dataForm['form']=$this->bines_m->Form();
    		echo $this->load->view("paper/forms_modal/form_bines",$this->dataForm,true);
		}
	}

	public function EditData(){
		$id=$this->input->post('Id');
		$data_by_id=$this->bines_m->get($id,true);
		$dataForm['form']=$this->bines_m->FormForEdit($data_by_id);
        $dataForm['id_bine']=$id;
		echo $this->load->view("paper/forms_modal/form_bines_edit",$dataForm,true);
		//var_dump($id);

	}

	public function UpdateData(){
		$this->form_validation->set_rules($this->bines_m->rules);
		$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
		if($this->form_validation->run()==TRUE){
            $data['bine']=$this->input->post('bine');
            $data['descripcion']=$this->input->post('descripcion');
            $data['status']=$this->input->post('status');
			$this->bines_m->save($data,$this->input->post('id'));
			echo 'Todo bien!';
		}else{
			$id=$this->input->post('Id');
			$data_by_id=$this->bines_m->get($id,true);
			$dataForm['form']=$this->bines_m->FormForEdit($data_by_id);
            $dataForm['id_bine']=$id;
			echo $this->load->view("paper/forms_modal/form_bines_edit",$dataForm,true);

		}
	}

	public function DeleteData(){
		$id=$this->input->post('id');
		$this->bines_m->delete($id);
		echo '<p class="label label-success">'.$this->lang->line('error_delete').'<strong>'.$id.'</strong></p>';
	}

	public function diferente_de_0($str) {
        if ($str == '0')
        {
            $this->form_validation->set_message('diferente_de_0', 'Elige una opcion Válida');
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }





}
?>

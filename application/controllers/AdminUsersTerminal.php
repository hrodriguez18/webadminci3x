<?php if ( ! defined('BASEPATH')) exit('No direct scriptccessllowed');

class AdminUsersTerminal extends MY_Controller{

	public $permiso_crear=116;
	public $permiso_actualizar=117;
	public $permiso_eliminar=118;
	public $permiso_ver=115;
	public $mymodel='UsersTerminal_m';

	public $contenido=array();
	protected $dataForm=array('title_form'=>'Usuario de Terminales');


	public $localeForTable='es-MX';
	//variable para exportar tabla;
	public $rutaToExport='';
	//el value puede o no estar en el diccionario de idiomas

	public $controlerServer='AdminUsersTerminal';
	public $dataTable=array();


	public function __construct(){
        parent::__construct();
        $this->load->model('UsersTerminal_m');
    }

    public function index(){

		$this->dataTable[0]=array('field'=>'id','title'=>'ID','sortable'=>'true','visible'=>'false','align'=>'left');
		$this->dataTable[1]=array('field'=>'name','title'=>'name','sortable'=>'true','visible'=>'true','align'=>'left');
		$this->dataTable[2]=array('field'=>'lastname','title'=>'lastname','sortable'=>'true','visible'=>'true','align'=>'left');
		$this->dataTable[3]=array('field'=>'email','title'=>'phone','sortable'=>'true','visible'=>'true','align'=>'left');
		$this->dataTable[4]=array('field'=>'status','title'=>'status','sortable'=>'true','visible'=>'true','align'=>'left');
		$this->dataTable[5]=array('field'=>'rol','title'=>'rol_id','sortable'=>'true','visible'=>'true','align'=>'left');
		$this->dataTable[6]=array('field'=>'fnacimiento','title'=>'Birthdate','sortable'=>'true','visible'=>'true','align'=>'left');
		$this->dataTable[7]=array('field'=>'actions','title'=>'actions','class'=>'td-actions text-left','align'=>'left','events'=>'operateEvents','formatter'=>'operateFormatter');
		$data['titleMenu']=$this->lang->line('titleAdmin');
		$data['navbar_menu']=$this->navbar_header_menu();
		$data['controllerServer']=$this->controlerServer;
		$data['columns']=$this->getColumnsForTable();
		$data['rutaForm']=$this->rutaForm;
		$data['title_form']=$this->lang->line('userterminal');
		$data['sizeModal']='modal-lg';
		$data['documentation']='';
		$data['title']=$this->lang->line('userterminal');
		$data['rutaToExport']='';
		$data['localeForTable']=$this->localeForTable;
		$this->load->view('paper/admin/_layoutEmail',$data);

    }

    public function populateTable(){
		$this->load->model('populateModel');

		$users=new populateModel();

		$users->_tablename='esb.usuarios';
		$users->_primary_key='usuarios.id';
		$users->_selection=array(
			'usuarios.id',
			'usuarios.email',
			'usuarios.name',
			'usuarios.lastname',
			'usuarios.email',
			'usuarios.fnacimiento',
			'usuarios.status',
			'usuarios.phone1',
			'rolesTerminal.descripcion as rol'
		);
		$users->_camposBusqueda=array(
			'usuarios.email',
			'usuarios.name',
			'usuarios.lastname',
			'usuarios.fnacimiento'
		);
		$users->_search='';
		$users->_joinRelation=array(
			array(
				'table'=>'esb.rolesTerminal',
				'where_equals'=>'usuarios.rolId=rolesTerminal.id',
				'option'=>'left'
			)
		);



    	echo $users->populate_join();
    }

	public function save(){
		$id=NULL;
		$dataToInsert=array();

		if($this->input->post($this->modelo->_primary_key) != ''){
			$id=$this->input->post($this->modelo->_primary_key);
			foreach ($this->modelo->_inputs as $key => $value){
				$key=$key;
				$dataToInsert[$key]=$this->input->post($key);

			}
			unset($dataToInsert[$this->modelo->_primary_key]);
			$reglas=$this->modelo->_rules;
			foreach ($reglas as $key => $value) {
				if(strpos($value['rules'],'unique')){
					$reglas[$key]['rules']='trim|required|max_length[80]';
				}
				if($value['field']=='password'){
					$reglas[$key]['rules']='trim|max_length[80]';
				}
				if($value['field']=='email'){
					$reglas[$key]['rules']='trim|max_length[80]|callback_isEmail';
				}
			}
			$this->form_validation->set_rules($reglas);
			$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
			//var_dump($dataToInsert);
			if($this->form_validation->run()==TRUE){
				if(array_key_exists('password',$dataToInsert)){
					if($dataToInsert['password']!=''){
						$dataToInsert['password']=$this->encrypt->encode($dataToInsert['password']);
					}else{
						unset($dataToInsert['password']);
					}
					//$dataToInsert['password']=='prueba que si existe';
				}
				$dataToInsert['phone1']=$this->input->post('email');
				$this->modelo->save($dataToInsert,$id);
				echo 'ok';
			}else{
				echo $this->modelo->form($this->formSize,$id);
			}
		}else{
			foreach ($this->modelo->_inputs as $key => $value) {
				$dataToInsert[$key]=$this->input->post($key);
				if($dataToInsert[$key]==''){
					unset($dataToInsert[$key]);
				}
			}
			unset($dataToInsert[$this->modelo->_primary_key]);
			$this->form_validation->set_rules($this->modelo->_rules);
			$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
			//var_dump($dataToInsert);
			if($this->form_validation->run()==TRUE){

				if(array_key_exists('password',$dataToInsert)){
					$dataToInsert['password']=$this->encrypt->encode($dataToInsert['password']);
					//$dataToInsert['password']=='prueba que si existe';
				}
				$dataToInsert['phone1']=$this->input->post('email');
				$this->modelo->save($dataToInsert);
				echo 'ok';
			}else{
				echo $this->modelo->form($this->formSize);
			}
		}
	}



}
?>

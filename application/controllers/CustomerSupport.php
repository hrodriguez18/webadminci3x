<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CustomerSupport extends MY_Controller{

  public $permiso_crear=0;
	public $permiso_actualizar=0;
	public $permiso_eliminar=0;
	public $permiso_ver=143;

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
  }

  function index()
  {
    $datos['navbar_menu']=$this->navbar_header_menu();
		$this->load->view('paper/admin/customerSupport',$datos);
  }

  public function BuscarCliente(){
    $message='<p class="alert alert-warning"><strong>'.$this->lang->line('messageSupport').'</strong></p>';
    $this->session->set_flashdata('message',$message);
    redirect('CustomerSupport','refresh');
  }

}

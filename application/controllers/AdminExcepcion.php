<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AdminExcepcion extends MY_Controller {

	public $permiso_crear=120;
	public $permiso_actualizar=97;
	public $permiso_eliminar=98;
	public $permiso_ver=96;

public function __construct()
	{
		parent::__construct();
		$this->load->model('ExcepcionModel');
	}

	public function index()
	{
		$datos['navbar_menu']=$this->navbar_header_menu();
		$datos['title']=$this->lang->line('exception');
		$this->load->view('paper/admin/_AdminLayoutExcepciones',$datos);
	}

	public function populateTable(){

		echo json_encode($this->ExcepcionModel->DataTable());
	}

	public function Form(){
		echo $this->load->view('paper/forms_modal/FormExcepciones','',true);
	}

	public function saveData(){

		$description=array('description'=>array('field'=>'description','label'=>'Description','rules'=>'trim|required'));

		$this->form_validation->set_rules($description);
		$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
		if($this->form_validation->run()==TRUE){

			if($this->input->post('id')!=''){
				$idGrupo=$this->input->post('id');

				if($this->input->post('day')){
					$dias=$this->input->post('day');
					$data['description']=$this->input->post('description');
					$data['status']=$this->input->post('status');

					$this->ExcepcionModel->UpdateGrupoExcepciones($data,$idGrupo);

					$this->ExcepcionModel->LimpiarExcepcionDeDias($idGrupo);

					foreach ($dias as $value){

						$info=array('grupoExcepcionesId'=>$idGrupo,'fechaExcepcion'=>$value);
						$this->db->insert('esb.relGrupoExcepcionesDias',$info);

					}

				echo 'Todo bien!';
				}else{
					echo $this->load->view('paper/forms_modal/FormExcepciones','',true);
				}


			}else{

				if($this->input->post('day')){
					$dias=$this->input->post('day');
					$data['description']=$this->input->post('description');
					$data['status']=$this->input->post('status');
					$idGrupoExcepcion=$this->ExcepcionModel->SaveGrupoExcepciones($data);
					foreach ($dias as $value){

						$info=array('grupoExcepcionesId'=>$idGrupoExcepcion,'fechaExcepcion'=>$value);
						$this->db->insert('esb.relGrupoExcepcionesDias',$info);
					}

					echo 'Todo bien!';
				}else{
					echo $this->load->view('paper/forms_modal/FormExcepciones','',true);
				}
			}
		}else{
			echo $this->load->view('paper/forms_modal/FormExcepciones','',true);
		}
	}



	public function EditData(){
		$id=$this->input->post('id');
		$data=$this->ExcepcionModel->get($id);
		$info['description']=$data->description;
		$info['status']=$data->status;
		$info['id']=$data->id;
		$info['inputs_day']=$this->ExcepcionModel->TraerInputsDays($id);
		echo $this->load->view('paper/forms_modal/FormExcepciones_edit',$info,true);
	}

	public function DeleteData(){
		$id=$this->input->post('id');
		$this->ExcepcionModel->LimpiarExcepcionDeDias($id);
		$this->ExcepcionModel->delete($id);
		echo '<p class="label label-success">'.$this->lang->line('error_delete').'<strong>'.$id.'</strong></p>';

	}


}

/* End of file AdminExcepcion.php */
/* Location: ./application/controllers/AdminExcepcion.php */

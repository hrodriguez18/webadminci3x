<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SupportCNB extends MY_Controller{

  public $permiso_ver=154;

  public $mymodel='terminales_m';

  public function __construct()
  {

    parent::__construct();
    //Codeigniter : Write Less Do More
    $this->load->model('UsersTerminal_m');
  }

  public function index()
  {
    $data['titleMenu']=$this->lang->line('CNB');
    $data['navbar_menu']=$this->navbar_header_menu();
    $this->load->view('paper/Support/_layoutSupportCnb',$data);
  }

  public function Search(){
    $telefono=$this->input->post('telefono');
    $fecha=$this->input->post('fecha');
    $cedula=$this->input->post('cedula');
	$data=array();
    $this->db->select('*');
    $this->db->where('cedula',$cedula);
    $this->db->where('fnacimiento',$fecha);
	$this->db->limit(1);
	$usuarios=$this->db->get('esb.usuarios');
	$id=0;
	$phone='';
	if($usuarios->num_rows()>0){
		foreach($usuarios->result() as $user){
			$data['name']=$user->name.' '.$user->lastname;
			$id=$user->terminalId;
		}
		$this->db->select('*');
		$this->db->where('terminalPhone',$telefono);
		$terminal=$this->db->get('esb.Terminales');
		if($terminal->num_rows()>0){
			$phone=$telefono;

			$userCNB=new terminales_m();
			//$userMonedero->_inputs['submit']=array('id'=>array('type'=>'submit','value'=>'Actualizar','class'=>'btn btn-success'));
			$data['form']=$userCNB->form('lg',$id);
			$data['navbar_menu']=$this->navbar_header_menu();
			$data['phone']=$phone;
			$data['id']=$id;
			$data['terminalId']=$id;
			$data['titleMenu']=$this->lang->line('CNB');
			$this->load->view('paper/Support/_layoutResultCnb',$data);
		}else{
			$this->session->set_flashdata('message','<p class="alert alert-warning">No se encontró resultado...</p>');
			redirect('SupportCNB','refresh');
		}
	}else{
		$this->session->set_flashdata('message','<p class="alert alert-warning">No se encontró resultado...</p>');
		redirect('SupportCNB','refresh');
	}


  }


  public function SetToken(){
      $phone=$this->input->post('phone');
      $name=$this->input->post('name');
      $terminalId=$this->input->post('terminalId');
      $respuesta=$this->SendToken($phone,$name,$terminalId);
      if($respuesta->error==0){
        echo 'ok';
      }else{
        echo '<p class="alert alert-danger">Token no enviado, Hubo un error : <strong>'.$respuesta->content.'</strong></p>';
      }

  }


  private function SendToken($phone,$name,$terminalId){

	  $this->db->select('terminalPhone');
	  $this->db->where('ID_Usuario',$terminalId);
	  $terminal=$this->db->get('esb.Terminales');

	  foreach ($terminal->result() as $value){
		  $phone = $value->terminalPhone;
	  }

    $this->load->library('Sms');
    $numberSended=rand(111111,999999);
    $templateMsn='restablecerPass';
    $sms= new Sms();
    $sms->phone=$phone;
    $sms->personName=$name;
    $sms->templateMsn=$templateMsn;
    $sms->token=$numberSended;
		$response=$sms->send();

    if($response->error==1){
      $dataToinsert['error']=$response->error;
      $dataToinsert['content']=$response->content;
    }
    if($response->error==0){
      $dataToinsert['id']=$response->id;
      $dataToinsert['error']=$response->error;
      $dataToinsert['status_id']=$response->status_id;
      $dataToinsert['last_change']=$response->last_change;
      $dataToinsert['content']=$response->content;
    }
    $dataToinsert['tokenSended']=$this->encrypt->encode($numberSended);
    $this->db->insert('esb.responseApiSmsRest',$dataToinsert);

    $data['registerToken']=md5($numberSended);
    $this->db->where('ID_Usuario',$terminalId);
    $this->db->update('esb.Terminales',$data);

    return $response;

  }

  public function ResetPIN(){
	  $phone=$this->input->post('phone');
	  $name=$this->input->post('name');
	  $terminalId=$this->input->post('terminalId');
	  $respuesta=$this->SendPIN($phone,$name,$terminalId);
	  if($respuesta->error==0){
		  echo 'ok';
	  }else{
		  echo '<p class="alert alert-danger">PIN no enviado, Hubo un error : <strong>'.$respuesta->content.'</strong></p>';
	  }
	}

	private function SendPIN($phone,$name,$terminalId){

  		$this->db->select('terminalPhone');
  		$this->db->where('ID_Usuario',$terminalId);
  		$terminal=$this->db->get('esb.Terminales');

  		foreach ($terminal->result() as $value){
  			$phone = $value->terminalPhone;
		}
		$this->load->library('Sms');
		$numberSended=rand(111111,999999);
		$templateMsn='ResetPin';
		$sms= new Sms();
		$sms->phone=$phone;
		$sms->personName=$name;
		$sms->templateMsn=$templateMsn;
		$sms->token=$numberSended;
		$response=$sms->send();

		if($response->error==1){
			$dataToinsert['error']=$response->error;
			$dataToinsert['content']=$response->content;
		}
		if($response->error==0){
			$dataToinsert['id']=$response->id;
			$dataToinsert['error']=$response->error;
			$dataToinsert['status_id']=$response->status_id;
			$dataToinsert['last_change']=$response->last_change;
			$dataToinsert['content']=$response->content;
		}
		$dataToinsert['tokenSended']=$this->encrypt->encode($numberSended);
		$this->db->insert('esb.responseApiSmsRest',$dataToinsert);

		$data['password']=md5($numberSended);
		$this->db->where('terminalId',$terminalId);
		$this->db->update('esb.usuarios',$data);

		return $response;
	}




}

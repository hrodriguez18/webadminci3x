<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ListaNegra_c extends MY_Controller{

  public $permiso_crear=146;
	public $permiso_actualizar=147;
	public $permiso_eliminar=148;
	public $permiso_ver=145;

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
    $this->load->model('ListaNegra_m');
  }

  public function index(){

    $this->contenido['table']=$this->load->view("paper/tables/table_ListaNegra",'',true);
    $this->contenido['title']=$this->lang->line('blacklist');
    $datos["content_view"]=$this->load->view("paper/admin/paper_view_ListaNegra",$this->contenido,true);
    $datos['navbar_menu']=$this->navbar_header_menu();
    $datos['sidebar_menu']=$this->menu_admin();
    $datos['footer']=$this->load->view("paper/footers/footer_blackList",'',true);

    $this->load->view("paper/base",$datos);

  }
  public function populateTable(){
    echo json_encode($this->ListaNegra_m->get());
  }

  public function Form(){
    echo $this->ListaNegra_m->Form('sm');
  }

  public function Editar(){
    $id=$this->input->post('id');
    echo $this->ListaNegra_m->Form('sm',$id);
  }

  public function save(){
  	if(strlen($this->input->post('id'))>0){
		$rules=array(
			'telefono'=>array(
				'field'=>'telefono',
				'label'=>'Teléfono',
				'rules'=>'trim|required|max_length[100]|callback_withoutSignal|required'
			),
			'documento'=>array(
				'field'=>'documento',
				'label'=>'Cédula',
				'rules'=>'trim|required|max_length[100]'
			),
			'email'=>array(
				'field'=>'email',
				'label'=>'email',
				'rules'=>'trim|required|max_length[100]'
			),
			'status'=>array(
				'field'=>'status',
				'label'=>'status',
				'rules'=>'trim|required|callback_diferente_de_default'
			)
		);
  	}else{
  		$rules=$this->ListaNegra_m->_rules;
  	}
	$this->form_validation->set_rules($rules);
	$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
	if($this->form_validation->run()==TRUE){
		$data=array();
	  if($this->input->post('id') == ''){
		  foreach ($this->ListaNegra_m->_inputs as $key => $value) {
			$data[$key]=$this->input->post($key);
		  }
		  unset($data['id']);
		  $this->ListaNegra_m->save($data);
		  echo "Todo bien!";
	  }else{
		if($this->input->post('id') !=''){
		  foreach ($this->ListaNegra_m->_inputs as $key => $value) {
			$data[$key]=$this->input->post($key);
		  }
		  unset($data['id']);
		  if($this->ListaNegra_m->save($data,$this->input->post('id'))){
			echo "Todo bien!";
		  }else{
			echo "no se pudo actualizar";
		  }
		}
	  }
	}else{
		//echo "no pasa la validacion";
	  echo $this->ListaNegra_m->form('sm');
	}
  }

  public function Delete(){
    $id=$this->input->post('id');
    if($this->ListaNegra_m->delete($id)){
      echo '<p class="label label-success">'.$this->lang->line('error_delete').'<strong>'.$id.'</strong></p>';
    }
  }

  public function Export(){
    $this->ListaNegra_m->exportToPdf();
  }

  public function populateBatch(){
    $this->load->library('upload');
    $config['upload_path']='assets/uploads/files/listaNegra';
    $config['allowed_types'] = 'csv';
    $config['max_size'] = '2000';
    $this->upload->initialize($config);
    if($this->upload->do_upload('csv')){
      $file_info=$this->upload->data();
      $file = fopen($file_info['full_path'],"r");
      $i=0;
      $arreglo=array();
      $dataToInsert=array();
      $td=array('telefono','documento','status','email');
      while(!feof($file))
        {
          $linea = fgets($file);
          $arreglo=explode(',',$linea);
          for($k=0;$k<count($td);$k++){
            if(isset($arreglo[$k])){
            	if(strlen($arreglo[$k])){
					$dataToInsert[$i][$td[$k]]=$arreglo[$k];
				}
            }
          }
          $i++;
        }
        fclose($file);
      for ($i=0; $i < count($dataToInsert) ; $i++){
        $cedula=$dataToInsert[$i]['documento'];
        $telefono=$dataToInsert[$i]['telefono'];
        $email=$dataToInsert[$i]['email'];
        if($this->cedulaRepetida($cedula) && $this->telefonoUnico($telefono) && $this->CorreoUnico($email)){
          $this->ListaNegra_m->save($dataToInsert[$i]);
        }
      }
      unlink($file_info['full_path']);
      echo '';
    }else{
      echo '<p class="alert alert-danger">No fue posible cargar el archivo favor siga las instrucciones...</p>';
    }

  }

  public function cedulaRepetida($str){
      $this->db->select('id');
      $this->db->where('documento',$str);
      $query=$this->db->get('esb.blackList');
      if($query->num_rows()>0){
          $this->form_validation->set_message('cedulaRepetida',$this->lang->line('cedula_ya_existe'));
          return FALSE;
      }
      return TRUE;
  }

  public function telefonoUnico($str){
  	$this->db->select('*');
  	$this->db->where('telefono',$str);
  	$query=$this->db->get('esb.blacklist');
  	if($query->num_rows()>0){
  		$this->form_validation->set_message('telefonoUnico','El %s Debe ser único');
  		return FALSE;
	}
	return TRUE;

  }

  public function CorreoUnico($str){
	  $this->db->select('*');
	  $this->db->where('email',$str);
	  $query=$this->db->get('esb.blacklist');
	  if($query->num_rows()>0){
		  $this->form_validation->set_message('CorreoUnico','El %s Debe ser único');
		  return FALSE;
	  }
	  return TRUE;
  }

}

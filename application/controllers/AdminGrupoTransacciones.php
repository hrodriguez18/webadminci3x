<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AdminGrupoTransacciones extends MY_Controller {

	public $_vista='paper_view_adminGrupoTransacciones';

	public $permiso_crear=92;
	public $permiso_actualizar=93;
	public $permiso_eliminar=94;
	public $permiso_ver=91;

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		$this->load->model('GrupoTransaccionesModel');
	}

	public function index()
	{
		$data['navbar_menu']=$this->navbar_header_menu();
		$this->load->view('paper/admin/'.$this->_vista,$data);
	}

	public function PopulateTable(){
		echo json_encode($this->GrupoTransaccionesModel->PopulateTable());
	}

	public function Form(){
		echo $this->load->view('paper/htmlForm/formGrupoT','',true);
	}

	public function GetLineTransaction(){

		//var_dump($this->input->post());
		$data=array();
		$selectores=array();


		foreach ($this->input->post() as $key => $value) {
			$data[$key]=$value;
		}

		if(isset($data['trx'])){

			foreach ($data['trx'] as $value) {
				array_push($selectores, $value);
			}
			//var_dump($selectores);

			$optionT=$this->GrupoTransaccionesModel->OptTransacciones($selectores);
			$optMontos=$this->GrupoTransaccionesModel->OptMontos();
			$optLimites=$this->GrupoTransaccionesModel->OptLimites();
			$form='<div class="row">';
			$form.='<div class="col-lg-4"><div class="form-group">'.form_label('Transacción','trx').form_dropdown('trx[]', $optionT, 'default','class="form-control"').'</div></div>';
			$form.='<div class="col-lg-4"><div class="form-group">'.form_label('Precios y Comisiones','montos').form_dropdown('montos[]', $optMontos, 'default','class="form-control"').'</div></div>';
			$form.='<div class="col-lg-4"><div class="form-group">'.form_label('Rangos','limites').form_dropdown('limites[]', $optLimites, 'default','class="form-control"').'</div></div>';

			$form.='</div>';
			if(count($optionT)>0){
				echo $form;
			}
		}else{
			$vacio=array();
			$optionT=$this->GrupoTransaccionesModel->OptTransacciones($vacio);
			$optMontos=$this->GrupoTransaccionesModel->OptMontos();
			$optLimites=$this->GrupoTransaccionesModel->OptLimites();
			$form='<div class="row">';
			$form.='<div class="col-lg-4"><div class="form-group">'.form_label('Transacción','trx').form_dropdown('trx[]', $optionT, 'default','class="form-control"').'</div></div>';
			$form.='<div class="col-lg-4"><div class="form-group">'.form_label('Precios y Comisiones','montos').form_dropdown('montos[]', $optMontos, 'default','class="form-control"').'</div></div>';
			$form.='<div class="col-lg-4"><div class="form-group">'.form_label('Rangos','limites').form_dropdown('limites[]', $optLimites, 'default','class="form-control"').'</div></div>';

			$form.='</div>';

			echo $form;
		}
	}

	public function Save(){
		$data=array();
		$data['trx'][0]=NULL;
		$this->form_validation->set_rules($this->GrupoTransaccionesModel->_rules);
		$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
		if($this->form_validation->run()==TRUE){
			foreach ($this->input->post() as $key => $value) {
				$data[$key]=$value;
			}
			if(isset($data['id'])){
				if( $data['id'] !=''){
					$grupo['description']=$data['description'];
					$grupo['status']=$data['status'];
					$this->GrupoTransaccionesModel->save($grupo,$data['id']);
					$this->GrupoTransaccionesModel->CleanRelationGrupoTrxMontos($data['id']);
					if($data['trx'][0]!=NULL){

						for ($i=0; $i < count($data['trx']); $i++) {
							$rel['idTrx']=$data['trx'][$i];
							$rel['idMontoTrx']=$data['montos'][$i];
							$rel['idLimit']=$data['limites'][$i];
							$rel['idGrupoTrx']=$data['id'];
							$this->GrupoTransaccionesModel->SaveRelGrupoTrx($rel);
						}
						echo "Todo bien!";
					}else{
						$info['description']=$this->input->post('description');
						$info['id']=$this->input->post('id');
						$this->session->set_flashdata('error', '<p class="alert alert-danger">'.$this->lang->line('errorEmptyForm').'</p>');
						echo $this->load->view('paper/htmlForm/formGrupoT2',$info,true);
					}

				}else{
					$info['description']=$this->input->post('description');
					$info['error']='<p class="alert alert-danger">'.$this->lang->line('errorEmptyForm').'</p>';
					echo $this->load->view('paper/htmlForm/formGrupoT2',$info,true);
				}
			}else{
				$grupo['description']=$data['description'];
				$grupo['status']=$data['status'];
				$idgrupoTr=$this->GrupoTransaccionesModel->save($grupo);

				if(isset($data['trx'][0])){
					for ($i=0; $i < count($data['trx']); $i++) {
						$rel['idTrx']=$data['trx'][$i];
						$rel['idMontoTrx']=$data['montos'][$i];
						$rel['idLimit']=$data['limites'][$i];
						$rel['idGrupoTrx']=$idgrupoTr;
						$this->GrupoTransaccionesModel->SaveRelGrupoTrx($rel);
					}
						echo "Todo bien!";
				}else{
					$info['error']='<p class="alert alert-danger">'.$this->lang->line('errorEmptyForm').'</p>';
					$this->session->set_flashdata('error', '<p class="alert alert-danger">'.$this->lang->line('errorEmptyForm').'</p>');
					echo $this->load->view('paper/htmlForm/formGrupoT2',$info,true);
				}

			}
		}else{
			$info['error']='<p class="alert alert-danger">'.$this->lang->line('errorEmptyForm').'</p>';
			echo $this->load->view('paper/htmlForm/formGrupoT2',$info,true);
		}
	}

	public function Editar($id=''){
		if($id==''){
			$id=$this->input->post('id');
		}

		$query=$this->GrupoTransaccionesModel->get($id);

		//var_dump($query);

		foreach ($query as $key => $value){
			$infoGrupo[$key]=$value;
		}

		$infoGrupo['ListaGrupoTrx']=$this->GrupoTransaccionesModel->ListaGrupoTrx($infoGrupo['id']);

		//var_dump($infoGrupo);
		echo $this->load->view('paper/htmlForm/formGrupoT2',$infoGrupo,true);
	}


	public function Edit(){
		$id=$this->input->post('id');
		echo $this->GrupoTransaccionesModel->Form('sm',$id);
	}

	public function Delete(){
		$id=$this->input->post('id');
		$this->GrupoTransaccionesModel->CleanRelationGrupoTrxMontos($id);
		$this->GrupoTransaccionesModel->delete($id);
		echo '<p class="label label-success">'.$this->lang->line('error_delete').'<strong>'.$id.'</strong></p>';

	}

}

/* End of file AdminMontos.php */
/* Location: ./application/controllers/AdminMontos.php */

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Retiros extends MY_Controller{

  public $permiso_ver='169';

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
  }

  function index()
  {

    $this->dataTable[0]=array('field'=>'id','title'=>'ID','sortable'=>'false','visible'=>'false','align'=>'left');
		$this->dataTable[1]=array('field'=>'name','title'=>'name','sortable'=>'false','visible'=>'true','align'=>'left');
		$this->dataTable[2]=array('field'=>'cuentaMonedero','title'=>'account','sortable'=>'false','visible'=>'true','align'=>'left');
		$this->dataTable[3]=array('field'=>'monto','title'=>'amount','sortable'=>'false','visible'=>'true','align'=>'left');
		$this->dataTable[4]=array('field'=>'description','title'=>'description','sortable'=>'false','visible'=>'true','align'=>'left');

    $data['titleMenu']=$this->lang->line('retiros').' Monedero';
    $data['navbar_menu']=$this->navbar_header_menu();
    $data['controllerServer']='Retiros';
    $data['columns']=$this->getColumnsForTable();
    $data['rutaForm']=$this->rutaForm;
    $data['sizeModal']='modal-lg';
    $data['populate']='populate';
    $data['title']=$this->lang->line('retiros').' Monedero';
    $data['localeForTable']=$this->localeForTable;
    $this->load->view('paper/admin/_layoutRetirosMonedero',$data);
  }

  public function populate(){
    $this->load->model('populateModel');
    $tabla=new populateModel();
    $tabla->_tablename='esb.Retiros';
    $tabla->_primary_key='id';
    $tabla->_primary_filter='intval';
    $tabla->_order_by='id';
    $tabla->_order='DESC';
    $tabla->fecha_inicio='';
    $tabla->fecha_fin='';
    $tabla->_timestamps='';
    $tabla->_limit=0;
    $tabla->_search='';
    $tabla->_offset=0;
    $tabla->_selection=array(
                              'id',
                              'name',
                              'cuentaMonedero',
                              'monto',
                              'description'
                              );
    $tabla->_valueSearch='';
    $tabla->_searchingValue='';
    echo $tabla->populate();
  }

  public function SaveFile(){
    $name=$_FILES['FileDeposito']['name'];
    $config['upload_path'] = 'assets/uploads/files/depositos';
    $config['allowed_types'] = 'txt';
    $config['max_size'] = '5000';
    $this->load->library('upload');
    $this->upload->initialize($config);

    if ( ! $this->upload->do_upload('FileDeposito'))
	{
		$this->session->set_flashdata('errors','<p class="alert alert-danger">'.$this->upload->display_errors('','').'</p>');
		redirect(base_url().'Retiros','refresh');
	}
	else
	{
		$info=$this->upload->data();
		$finalMessage='';


		//carga de datos en la $tabla

		$info=$this->upload->data();
		$file = fopen($info['full_path'],"r");
		$i=0;

		$linea=array();


		if ($file) {
			while (($bufer = fgets($file, 4096)) !== false) {
				$linea[$i]=$bufer;
				$i++;
			}
			if (!feof($file)) {
				echo "Error: fallo inesperado de fgets()\n";
			}
			fclose($file);
		}

		$cuenta=array();
		$j=0;
		$cuentasQueNoExisten=0;
		$NoCuenta='';
		for ($k=1;$k<(count($linea)-1);$k++){
			$key=trim(substr($linea[$k],2,17));
			if(!$this->existeCuenta($key)){

				$cuenta[$j]['name']=trim(substr($linea[$k],18,30));
				$cuenta[$j]['cuentaMonedero']=$key;
				$cuenta[$j]['monto']=number_format(intval(substr($linea[$k],59,12))/100,'2','.','');
				$cuenta[$j]['description']=trim(substr($linea[$k],71,33));

				$cuenta[$j]['description']=trim(substr($linea[$k],71,33));

				$j++;
			}else{
				$cuentasQueNoExisten++;
				$NoCuenta.=' - '.$key;
			}
		}

		$montoAPagar=number_format(intval(substr($linea[0],10,23))/100,'2','.','');



		if($j>0){$this->db->insert_batch('esb.Retiros',$cuenta);}

		//var_dump($cuenta);

		unlink($info['full_path']);
		$finalMessage.='<br><p class="alert alert-success"><strong>'.$info['file_name'].'</strong> '.$this->lang->line('success').'<br>Total de Despositos Ingresados : <strong>'.$j.'.</strong><br> Monto Total En esta Carga :<strong> B/. '.$montoAPagar.'</strong></p>';
		$this->session->set_flashdata('errors',$finalMessage);
		redirect(base_url().'Retiros','refresh');
	}

  }

  public function cedulaRepetida($str){
      $this->db->select('id');
      $this->db->where('cedula',$str);
      $query=$this->db->get('esb.Retiros');
      if($query->num_rows()>0){
          $this->form_validation->set_message('cedulaRepetida',$this->lang->line('cedula_ya_existe'));
          return FALSE;
      }
      return TRUE;
  }

  public function DeleteTabledispersion(){
    //$this->db->delete('esb.Retiros');
    //$this->db->empty_table('esb.Retiros');
    $this->db->truncate('esb.Retiros');
    $this->session->set_flashdata('errors','<p class="alert alert-info">'.$this->lang->line('dispersion_deleted').'</p>');
    redirect('Retiros','refresh');
  }

  public function ApplyDepositos(){
	  ini_set('memory_limit', '1024M'); // or you could use 1G
	  ini_set('max_execution_time', '600'); // or you could use 1G
  	$cuentasNoEncotradas=array();
	  $usuarioNoEncontrado=0;
      $this->db->select('*');
      $Deposito=$this->db->get('esb.Retiros');
      if($Deposito->num_rows()>0){
      	$this->load->library('Sms');
      	$sms= new Sms();

        foreach ($Deposito->result() as  $infoDeposito) {
          $Transaccion=new stdClass();
          $this->db->select('*');
          $this->db->where('cuentaMonedero',$infoDeposito->cuentaMonedero);
          $this->db->limit(1);
          $datoUsuario=$this->db->get('esb.usuariosMonedero');


          if($datoUsuario->num_rows()>0){
            foreach($datoUsuario->result() as $infoUser) {
                  $Transaccion->UserMonedero=$infoUser->email;
                  $Transaccion->terminalMonedero=$infoUser->imei;
                  $Transaccion->terminalid=$infoUser->id;
                  $Transaccion->tipoTrx='98';
                  $Transaccion->montoTotal=$infoDeposito->monto;
                  $Transaccion->montoTrx=$infoDeposito->monto;
                  $Transaccion->formaPago='Débito';
                  $Transaccion->cuenta=$infoUser->cuentaMonedero;
                  $Transaccion->descripcionTrx='Débito';
                  $Transaccion->aux='-';
                  $Transaccion->stamp=date('Y-m-d H:i:s');
                  $Transaccion->lat='';
                  $Transaccion->lon='';
                  $Transaccion->cuentaOperativa='0001-0000-0000-00001';
                  $Transaccion->currency='USD';
                  //$Transaccion->cuenta1='0012-012309-112';
                  $Transaccion->cuenta1=$infoUser->cuentaMonedero;
                  $Transaccion->email='transferencias@mides.com';
                  $Transaccion->nombre='';
                  $this->db->insert('esb.transactionMonederoLog',$Transaccion);


				//================================================================================
				//$Transaccion->terminalid=$datosTerminal->ID_Usuario;
				//$Transaccion->terminalid=$datosTerminal->ID_Usuario;
				$Transaccion->tipoTrx='97';
				$Transaccion->montoTotal=$infoDeposito->monto;
				$Transaccion->montoTrx=$infoDeposito->monto;
				$Transaccion->formaPago='Crédito';
				$Transaccion->stamp=date('Y-m-d H:i:s');
				$Transaccion->cuenta='2353100100103007';
				$Transaccion->descripcionTrx='Crédito';
				$Transaccion->aux='+';
				$Transaccion->currency='USD';
				$Transaccion->lat='';
				$Transaccion->lon='';
				$Transaccion->email='';
				$Transaccion->nombre='';
				$Transaccion->cuenta1='2353100100103007';

				$this->db->insert('esb.transactionMonederoLog',$Transaccion);


				//=============================================================================

                  $this->db->where('cuentaMonedero',$infoDeposito->cuentaMonedero);
                  $this->db->delete('esb.Retiros');

					$sms->token=$infoDeposito->monto;
					$sms->phone=$infoUser->phone;
					$sms->personName=$infoUser->name.' '.$infoUser->lastname;
					$sms->templateMsn='retiro';
					$sms->send();

            }
          }else{
          	$usuarioNoEncontrado++;
          	$cuentasNoEncotradas[$usuarioNoEncontrado]=$infoDeposito->cuentaMonedero;
		  }
        }
        $strNoEncontras='0';
        if(count($usuarioNoEncontrado)>0){
			foreach ($cuentasNoEncotradas as $cuenta){
				$strNoEncontras.=$cuenta.',';
			}
		}
        $this->session->set_flashdata('errors','<p class="alert alert-success">Retiros Aplicados.<br>Cuentas No Encontradas: '.$strNoEncontras.'</p>');
        redirect('Retiros','refresh');

      }else{
        $this->session->set_flashdata('errors','<p class="alert alert-success">Nada para aplicar!</p>');
        redirect('Retiros','refresh');
      }


  }

}

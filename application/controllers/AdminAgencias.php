<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class AdminAgencias extends MY_Controller{

	public $contenido=array();

	protected $dataForm=array('title_form'=>'Datos Agencias');

	public $permiso_crear=42;
	public $permiso_actualizar=43;
	public $permiso_eliminar=44;
	public $permiso_ver=2;
	public $mymodel='Agency_m';

	public $localeForTable='es-MX';
  //variable para exportar tabla;
  public $rutaToExport='';
  //el value puede o no estar en el diccionario de idiomas

  public $controlerServer='AdminAgencias';
  public $dataTable=array();



	public function __construct(){
        parent::__construct();
				//se carga el model de agencias
        $this->load->model('agencias_m');
    }

    public function index(){

		$this->dataTable[0]=array('field'=>'id','title'=>'ID','sortable'=>'true','visible'=>'false','align'=>'left');
		$this->dataTable[1]=array('field'=>'Operador','title'=>'operator','sortable'=>'true','visible'=>'true','align'=>'left');
		$this->dataTable[2]=array('field'=>'Descripcion','title'=>'description','sortable'=>'true','visible'=>'true','align'=>'left');
		$this->dataTable[3]=array('field'=>'Estado','title'=>'status','sortable'=>'true','visible'=>'false','align'=>'left');
		$this->dataTable[4]=array('field'=>'Actividad','title'=>'Comercialactivity','sortable'=>'true','visible'=>'true','align'=>'left');
		$this->dataTable[5]=array('field'=>'telefono','title'=>'phone','sortable'=>'true','visible'=>'true','align'=>'left');
		$this->dataTable[6]=array('field'=>'cuenta','title'=>'account','sortable'=>'true','visible'=>'false','align'=>'left');
		$this->dataTable[7]=array('field'=>'documento','title'=>'documento','sortable'=>'true','visible'=>'false','align'=>'left');
		$this->dataTable[8]=array('field'=>'tipoDocumento','title'=>'tipoDoc','sortable'=>'true','visible'=>'false','align'=>'left');
		$this->dataTable[9]=array('field'=>'cuentaComision','title'=>'commissionaccount','sortable'=>'true','visible'=>'false','align'=>'left');
		$this->dataTable[10]=array('field'=>'actions','title'=>'actions','class'=>'td-actions text-left','align'=>'left','events'=>'operateEvents','formatter'=>'operateFormatter');
		$data['titleMenu']=$this->lang->line('titleAdmin');
		$data['navbar_menu']=$this->navbar_header_menu();
		$data['controllerServer']=$this->controlerServer;
		$data['columns']=$this->getColumnsForTable();
		$data['rutaForm']=$this->rutaForm;
		$data['title_form']=$this->lang->line('agency');
		$data['sizeModal']='modal-lg';
	    $data['documentation']='';
	    $data['title']=$this->lang->line('agency');
	    $data['rutaToExport']='';
	    $data['localeForTable']=$this->localeForTable;
	    $this->load->view('paper/admin/_layoutEmail',$data);

    }

	public function delete(){
		$id=$this->input->post('id');
		$codAgencia='';
		$dataTerminal=array();

		$this->db->select('*');
		$this->db->where('id',$id);
		$usuario=$this->db->get('esb.Agencias')->row_array();
		if($usuario){
			$codAgencia=$usuario['Cod_Agencia'];
		}
		if(strlen($codAgencia)>0){
			$this->db->select('Terminales.terminalPhone');
			$this->db->where('Cod_Agencia',$codAgencia);
			$dataTerminal=$this->db->get('esb.consultaSaldoMonedero')->row_array();
		}


		$this->load->library('Sms');
		//$numberSended=$token;
		$templateMsn='deleteUser';
		$sms= new Sms();
		$sms->phone=$dataTerminal['terminalPhone'];
		$sms->personName='Usuario';
		$sms->templateMsn=$templateMsn;
		//$sms->token=$numberSended;
		$sms->token='.';
		$response=$sms->send();
		$this->modelo->delete($id);
		echo '<p class="alert-success alert">Se ha eliminado la Agencia Con Id: '.$id.'</p>';
	}





}
?>

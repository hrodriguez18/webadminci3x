<?php if ( ! defined('BASEPATH')) exit('No direct scriptccessllowed');

class AdminSystem extends MY_Controller{

	public $permiso_crear=32;
	public $permiso_actualizar=33;
	public $permiso_eliminar=34;
	public $permiso_ver=10;

	public $contenido=array();
	protected $dataForm=array('title_form'=>'Datos Usuario del Sistema');


	public function __construct(){
        parent::__construct();
        $this->load->model('adminsystem_m');
    }

    public function index(){

    	$this->contenido['table']=$this->load->view("paper/tables/table_adminsystem",'',true);
    	$this->contenido['title']=$this->lang->line('userssystem');
		$datos["content_view"]=$this->load->view("paper/admin/paper_view_adminsystem",$this->contenido,true);
		$datos['navbar_menu']=$this->navbar_header_menu();
		$datos['sidebar_menu']=$this->menu_admin();
		$datos['footer']=$this->load->view("paper/footers/footer_adminsystem",'',true);

		$this->load->view("paper/base",$datos);

    }
    public function populateTable(){
    	echo json_encode($this->adminsystem_m->get());
    }

    public function Form(){
    	$this->dataForm['form']=$this->adminsystem_m->Form();
    	echo $this->load->view("paper/forms_modal/form_adminsystem",$this->dataForm,true);
    }

    public function saveData(){
		$this->form_validation->set_rules($this->adminsystem_m->rules);
		$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
		if($this->form_validation->run()==TRUE){
			$data=array();
			foreach ($this->input->post() as $key => $value) {
				if($key!='userid'){
					$data[$key]=$value;
				}
				if($key=='password'){
					$data[$key]=$this->encrypt->encode($value);
				}

			}
			$this->adminsystem_m->save($data);
			echo "Todo bien!";
		}else{
			$this->dataForm['form']=$this->adminsystem_m->Form();
    		echo $this->load->view("paper/forms_modal/form_adminsystem",$this->dataForm,true);
		}
	}

	public function EditData(){
		$id=$this->input->post('Id');
		$data_by_id=$this->adminsystem_m->get($id,true);

		$dataForm['form']=$this->adminsystem_m->Form($data_by_id);
		echo $this->load->view("paper/forms_modal/form_adminsystem_edit",$dataForm,true);
		//var_dump($id);

	}

	public function UpdateData(){
		$this->form_validation->set_rules($this->adminsystem_m->rules_update);
		$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
		if($this->form_validation->run()==TRUE){
			$data=array();
			foreach ($this->input->post() as $key => $value) {
				if($key!='userid'){
					$data[$key]=$value;
				}
				if($key=='password'){
					if($value!=''){
						$data[$key]=$this->encrypt->encode($value);
					}else{
						unset($data[$key]);
					}
				}
			}

			$this->adminsystem_m->save($data,$this->input->post('userid'));
			echo 'Todo bien!';
		}else{
			$id=$this->input->post('userid');
			$data_by_id=$this->adminsystem_m->get($id,true);
			$dataForm['form']=$this->adminsystem_m->Form($data_by_id);
			echo $this->load->view("paper/forms_modal/form_adminsystem_edit",$dataForm,true);

		}
	}

	public function DeleteData(){
		$id=$this->input->post('id');
		$this->adminsystem_m->delete($id);
		echo '<p class="label label-success">'.$this->lang->line('error_delete').'<strong>'.$id.'</strong></p>';

	}

	public function diferente_de_0($str){
        if ($str == '0')
        {
            $this->form_validation->set_message('diferente_de_0', lang('validate_option'));
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }




    public function usuario_unico($str){
    	$field='username';
    	$value=$str;
    	if ($this->adminsystem_m->countUserByData($field,$value)>0)
        {
            $this->form_validation->set_message('usuario_unico', lang('already_user'));
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }

    public function email_unico($str){
    	$field='email';
    	$value=$str;
    	if ($this->adminsystem_m->countUserByData($field,$value)>0)
        {
            $this->form_validation->set_message('email_unico', lang('already_email'));
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }

    public function regex_match($str){
			 	if(preg_match('/^(?=(?:.*\d){1})(?=(?:.*[A-Z]){1})(?=(?:.*[a-z]){1})\S{8,}$/',$str)){
					return TRUE;
				}else{
					$this->form_validation->set_message('regex_match', lang('error_expression'));
					return FALSE;
				}
		 }

}
?>

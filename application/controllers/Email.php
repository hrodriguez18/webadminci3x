<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Email extends MY_Controller{

  public $permisos=array();
  public $mis_permisos=array();

  public $permiso_crear=150;
  public $permiso_actualizar=142;
  public $permiso_eliminar=153;
  public $permiso_ver=149;
  //
  //parametros para la tabla, $controllerServer= controlador donde apuntaran las funciones de la tabla
  //$datatable=arreglo de datos para identificar las columnas(ver formato en controlador)
  /*
    $this->dataTable[0]=array('some_option'=>'some_value');
  */

  //dato para locale table
  public $localeForTable='es-MX';
  //variable para exportar tabla;
  public $rutaToExport='';
  //el value puede o no estar en el diccionario de idiomas
  public $mymodel='Email_m';
  public $controlerServer='Email';
  public $dataTable=array();


  //====================================================================

  //Ruta del formulario para insertar o editar uno nuevo.
  public $rutarForm='';

  //Tamaño del FORMULARIO
  public $formSize='lg';

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
  }

  function index()
  {

		$this->dataTable[0]=array('field'=>'email','title'=>'email','sortable'=>'true','visible'=>'true','align'=>'left');
		$this->dataTable[1]=array('field'=>'server','title'=>'server','sortable'=>'true','visible'=>'true','align'=>'left');
		$this->dataTable[2]=array('field'=>'port','title'=>'port','sortable'=>'true','visible'=>'true','align'=>'left');
		$this->dataTable[3]=array('field'=>'method','title'=>'method','sortable'=>'true','visible'=>'true','align'=>'left');
		$this->dataTable[4]=array('field'=>'status','title'=>'status','sortable'=>'true','visible'=>'true','align'=>'left');
		$this->dataTable[5]=array('field'=>'funcion','title'=>'functions','sortable'=>'true','visible'=>'true','align'=>'left');
	  	$this->dataTable[6]=array('field'=>'actions','title'=>'actions','class'=>'td-actions text-left','align'=>'left','events'=>'operateEvents','formatter'=>'operateFormatter');
		$data['titleMenu']=$this->lang->line('titleAdmin');
		$data['navbar_menu']=$this->navbar_header_menu();
		$data['controllerServer']=$this->controlerServer;
		$data['columns']=$this->getColumnsForTable();
		$data['rutaForm']=$this->rutaForm;
		$data['title_form']='Email';
		$data['sizeModal']='modal-lg';
    	$data['documentation']='';
    	$data['title']=$this->lang->line('email');
		$data['rutaToExport']='';
		$data['localeForTable']=$this->localeForTable;
		$this->load->view('paper/admin/_layoutEmail',$data);
  }

  public function uniqueEmail($str){
    if($this->modelo->countEmail($str)>0){
        $this->form_validation->set_message('uniqueEmail',$this->lang->line('message_email_unique'));
        return FALSE;
    }
    return TRUE;
  }

  public function uniqueFunction($str){
    if($this->modelo->countFunction($str)>0){
        $this->form_validation->set_message('uniqueFunction',$this->lang->line('message_function_unique'));
        return FALSE;
    }
    return TRUE;
  }



}

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class CnbPaper extends MY_Controller{

	public $permiso_ver=127;

	public function __construct(){
        parent::__construct();

        $this->load->model('cnbModel');
		$this->load->helper('date');
    }



	public function index(){
		$this->load->model('DashboardModel');
		$dashBoard=new DashboardModel();
		$year=date('Y');
		$mes=date('m');
		$pais='PAN';

		$lastDay=days_in_month($mes, $year);


		$fechaInicio=$year.'-'.$mes.'-01 00:00:00';
		$fechaFin=$year.'-'.$mes.'-'.$lastDay.' 23:59:00';



		$dashBoard->_tablename='esb.transactionLog';
		$dashBoard->_order_by='id';
		$dashBoard->_order='DESC';
		$dashBoard->_limit=0;
		$dashBoard->_timestamps='stamp';

		$dashBoard->_fechaInicio=$fechaInicio;
		$dashBoard->_fechaFin=$fechaFin;

		$datos_content['contadores']=$dashBoard->getCounters();
		$datos_content['formforCounters']=$dashBoard->FormSearchCountersCNB();
		$datos_content['rutaUpdateKPI']='CnbPaper/UpdateKPI';
		$datos_content['rutaSetGraph']='CnbPaper/UpdateGraphic';
		$datos_content['controlador']='CnbPaper';
		$datos_content['titleMenu']='CNB';


		$datos["content_view"]=$this->load->view("paper/cnb/content_cnb",$datos_content,true);
		$datos['navbar_menu']=$this->navbar_header_menu();
		$datos['sidebar_menu']=$this->load->view('paper/menupaper/menu_cnb','',true);

		$this->load->view("paper/base",$datos);
	}

	public function UpdateKPI(){



		$year=$this->input->post('year');
		$mes=$this->input->post('month');
		$operador=$this->input->post('operador');

		$lastDay=days_in_month($mes, $year);


		$fechaInicio=$year.'-'.$mes.'-01 00:00:00';
		$fechaFin=$year.'-'.$mes.'-'.$lastDay.' 23:59:00';


		$this->load->model('DashboardModel');
		$dashBoard=new DashboardModel();
		$dashBoard->_tablename='esb.transactionLog';
		$dashBoard->_order='DESC';
		$dashBoard->_order_by='id';
		$dashBoard->_limit=0;
		$dashBoard->_timestamps='stamp';

		$dashBoard->_fechaInicio=$fechaInicio;
		$dashBoard->_fechaFin=$fechaFin;

		$dashBoard->_especificField='cnbOperador';
		$dashBoard->_especificValue=$operador;

		echo $dashBoard->getCounters();

	}

	public function UpdateGraphic(){
		if($this->input->post('year')){
			$year=$this->input->post('year');
		}else{
			$year=2018;
		}

		if($this->input->post('operador')){
			$operador=$this->input->post('operador');
		}





		$this->load->model('DashboardModel');
		$dashBoard=new DashboardModel();
		$dashBoard->_tablename='esb.transactionLog';
		$dashBoard->_order='DESC';
		$dashBoard->_order_by='id';
		$dashBoard->_limit=0;
		$dashBoard->_timestamps='stamp';

		$dashBoard->_fechaInicio=$year.'-01-01 00:00:00';
		$dashBoard->_fechaFin=$year.'-12-31 23:59:00';

		$dashBoard->_especificField='cnbOperador';
		$dashBoard->_especificValue=$operador;



		$allData=json_decode($dashBoard->getData());

		$meses=array();
		for ($i=1;$i<=12;$i++){
			$dia='';
			if($i<10){
				$dia='0';
			}
			array_push($meses,$year.'-'.$dia.$i);
		}

		$transacciones=array();
		$debitos=array();
		$creditos=array();

		$dataGraphic=array();

		//se obtienen los array de Transacciones por mes
		for($mes=0; $mes<count($meses); $mes++){
			$count=0;
			foreach ($allData as $value){
				if(substr($value->stamp,0,7)==$meses[$mes]){
					$count++;
				}
			}
			array_push($transacciones,$count);

			$suma=0;
			foreach ($allData as $value){
				if(substr($value->stamp,0,7)==$meses[$mes] && $value->aux=='+'){
					$suma+=$value->montoTrx;
				}
			}
			array_push($creditos,number_format($suma,'2','.',''));

			$suma=0;
			foreach ($allData as $value){
				if(substr($value->stamp,0,7)==$meses[$mes] && $value->aux=='-'){
					$suma+=$value->montoTrx;
				}
			}
			array_push($debitos,number_format($suma,'2','.',''));
		}

		//echo var_dump($meses).'<br>';
		//echo var_dump($transacciones).'<br>';
		//echo var_dump($creditos).'<br>';
		//echo var_dump($debitos).'<br>';


		for ($k=0;$k<12;$k++){
			array_push($dataGraphic,array('mes'=>$meses[$k],'transacciones'=>$transacciones[$k],'credito'=>$creditos[$k],'debito'=>$debitos[$k]));
		}


		//var_dump($dataGraphic);

		echo json_encode($dataGraphic);



	}

	public function MAPA(){

		$this->load->helper('date');

		if($this->input->post('year')){
			$year=$this->input->post('year');
		}else{
			$year=2018;
		}

		if($this->input->post('operador')){
			$operador=$this->input->post('operador');
		}

		if($this->input->post('mes')){
			$mes=$this->input->post('mes');
		}else{
			$mes=date("m");
		}

		$lastDay=days_in_month($mes,$year);


		$this->load->model('DashboardModel');
		$dashBoard=new DashboardModel();
		$dashBoard->_tablename='esb.transactionLog';
		$dashBoard->_order='DESC';
		$dashBoard->_order_by='id';
		$dashBoard->_limit=0;
		$dashBoard->_timestamps='stamp';

		$dashBoard->_fieldsSelected=array(
			'transactionLog.id',
			'transactionLog.userMonedero',
			'transactionLog.terminalMonedero',
			'transactionLog.tipoTrx',
			'transactionLog.montoTotal',
			'transactionLog.montoTrx',
			'transactionLog.formaPago',
			'transactionLog.stamp',
			'transactionLog.cnbOperador',
			'transactionLog.cnbAgencia',
			'transactionLog.cnbUser',
			'transactionLog.cnbTerminal',
			'transactionLog.cuenta',
			'transactionLog.descripcionTrx',
			'transactionLog.aux',
			'transactionLog.agenciaCNBId',
			'transactionLog.cuentaComision',
			'transactionLog.comisionTotal',
			'transactionLog.currency',
			'transactionLog.terminalId',
			'transactionLog.lat',
			'transactionLog.lon',
			'transactionLog.email',
			'transactionLog.nombre',
			'Operadores.Color as color'
		);

		$dashBoard->_joins=array(
			array(
				'table'=>'esb.Operadores',
				'relation'=>'transactionLog.cnbOperador=Operadores.Descripcion',
				'type'=>'left'
			)
		);

		$dashBoard->_fechaInicio=$year.'-'.$mes.'-01 00:00:00';
		$dashBoard->_fechaFin=$year.'-'.$mes.'-'.$lastDay.' 23:59:00';

		$dashBoard->_especificField='cnbOperador';
		$dashBoard->_especificValue=$operador;

		$allData=json_decode($dashBoard->getData());
		$points=array();
		$i=0;
		foreach ($allData as $value){
			$points[$i]['location']=array('lat'=>floatval($value->lat),'lng'=>floatval($value->lon));
			$points[$i]['description']='<p class="text-info">Fecha: '.$value->stamp.'</p>';

			if($value->nombre != ''){
				$points[$i]['description'].='<p class="text-info">Usuario: '.$value->userMonedero.' ('.$value->nombre.')</p>';
			}else{
				$points[$i]['description'].='<p class="text-info">Usuario: '.$value->userMonedero.'</p>';
			}

			$points[$i]['description'].='<p class="text-info">Descripción: '.$value->descripcionTrx.'</p>';
			$points[$i]['description'].='<p class="text-info">Monto: '.$value->currency.' '.$value->montoTrx.'</p>';
			$points[$i]['description'].='<p class="text-info">Forma de Pago: '.$value->formaPago.'</p>';

			$points[$i]['title']=$value->descripcionTrx;

			$points[$i]['color']=str_replace('#','',$value->color);
			//$points[$i]['color']=$value->color;

			$i++;
		}

		echo json_encode($points);

	}


	public function searchByDate(){
		echo json_encode($this->cnbModel->searchByDate());
	}

	public function searchByDateAndType(){
		echo json_encode($this->cnbModel->searchByDateAndType());
	}

	public function ExportToPdfByType(){
		$this->cnbModel->ExportToPdfByType();
	}

	public function Reportes(){
		$this->dataTable[0]=array('field'=>'id','title'=>'ID','sortable'=>'true','visible'=>'false','align'=>'left');
		$this->dataTable[1]=array('field'=>'stamp','title'=>'date','sortable'=>'true','visible'=>'true','align'=>'left');
    	$this->dataTable[2]=array('field'=>'cnbOperador','title'=>'cnbOperador','sortable'=>'true','visible'=>'true','align'=>'left');
		$this->dataTable[3]=array('field'=>'cnbAgencia','title'=>'agency','sortable'=>'true','visible'=>'true','align'=>'left');
    	$this->dataTable[4]=array('field'=>'cnbUser','title'=>'cnbUser','sortable'=>'true','visible'=>'false','align'=>'left');

		$this->dataTable[5]=array('field'=>'descripcionTrx','title'=>'description','sortable'=>'false','visible'=>'true','align'=>'left');
		$this->dataTable[6]=array('field'=>'montoTotal','title'=>'amount','sortable'=>'false','visible'=>'true','align'=>'left');
		$this->dataTable[7]=array('field'=>'comisionTotal','title'=>'commissions','sortable'=>'false','visible'=>'true','align'=>'left');
		$this->dataTable[8]=array('field'=>'formaPago','title'=>'methodPay','sortable'=>'false','visible'=>'false','align'=>'left');
		$this->dataTable[9]=array('field'=>'cuentaBNP','title'=>'accountBNP','sortable'=>'false','visible'=>'false','align'=>'left');
		$this->dataTable[10]=array('field'=>'cuenta','title'=>'account','sortable'=>'false','visible'=>'false','align'=>'left');
		$this->dataTable[11]=array('field'=>'cuentaComision','title'=>'commissionaccount','sortable'=>'false','visible'=>'false','align'=>'left');
		$this->dataTable[12]=array('field'=>'mapa','title'=>'maps','sortable'=>'false','visible'=>'true','align'=>'left');
		$data['titleMenu']=$this->lang->line('CNB');
		$data['navbar_menu']=$this->navbar_header_menu();
		$data['controllerServer']='CnbPaper';
		$data['columns']=$this->getColumnsForTable();
		$data['rutaForm']=$this->rutaForm;
		$data['sizeModal']='modal-lg';
		$data['populate']='populateReport';
		$data['title']=$this->lang->line('reportTrx');
		$data['rutaToExport']=base_url().$this->router->fetch_class().'/export';
		$data['localeForTable']=$this->localeForTable;
		$this->load->view('paper/cnb/_layoutReport',$data);
	}

	public function populate(){
		echo json_encode($this->cnbModel->PopulateTable());
	}

	public function ReportesType(){
		$this->dataTable[0]=array('field'=>'id','title'=>'ID','sortable'=>'true','visible'=>'false','align'=>'left');
		$this->dataTable[1]=array('field'=>'stamp','title'=>'date','sortable'=>'true','visible'=>'true','align'=>'left');
    	$this->dataTable[2]=array('field'=>'cnbOperador','title'=>'cnbOperador','sortable'=>'true','visible'=>'true','align'=>'left');
		$this->dataTable[3]=array('field'=>'cnbAgencia','title'=>'agency','sortable'=>'true','visible'=>'true','align'=>'left');
    	$this->dataTable[4]=array('field'=>'cnbUser','title'=>'cnbUser','sortable'=>'true','visible'=>'false','align'=>'left');
		$this->dataTable[5]=array('field'=>'cnbTerminal','title'=>'terminal','sortable'=>'true','visible'=>'false','align'=>'left');
		$this->dataTable[6]=array('field'=>'descripcionTrx','title'=>'description','sortable'=>'false','visible'=>'true','align'=>'left');
		$this->dataTable[7]=array('field'=>'montoTotal','title'=>'amount','sortable'=>'false','visible'=>'true','align'=>'left');
		$this->dataTable[8]=array('field'=>'comisionTotal','title'=>'commission','sortable'=>'false','visible'=>'true','align'=>'left');
		$this->dataTable[9]=array('field'=>'formaPago','title'=>'methodPay','sortable'=>'false','visible'=>'false','align'=>'left');
		$this->dataTable[10]=array('field'=>'cuentaBNP','title'=>'accountBNP','sortable'=>'false','visible'=>'false','align'=>'left');
		$this->dataTable[11]=array('field'=>'cuenta','title'=>'account','sortable'=>'false','visible'=>'false','align'=>'left');
		$this->dataTable[12]=array('field'=>'cuentaComision','title'=>'commissionaccount','sortable'=>'false','visible'=>'false','align'=>'left');
		$this->dataTable[13]=array('field'=>'mapa','title'=>'maps','sortable'=>'false','visible'=>'true','align'=>'left');
    	$data['titleMenu']=$this->lang->line('CNB');
		$data['navbar_menu']=$this->navbar_header_menu();
	  	$data['controllerServer']='CnbPaper';
		$data['columns']=$this->getColumnsForTable();
		$data['rutaForm']=$this->rutaForm;
		$data['sizeModal']='modal-lg';
		$data['populate']='populateReportType';
    	$data['title']=$this->lang->line('reportbytype');
		$data['selectorTipo']=$this->tipoTrx();
		$data['rutaToExport']=base_url().$this->router->fetch_class().'/exportType';
		$data['localeForTable']=$this->localeForTable;
		$this->load->view('paper/cnb/_layoutReportTipo',$data);
	}

	public function Ingresos(){
	$this->dataTable[0]=array('field'=>'id','title'=>'ID','sortable'=>'true','visible'=>'false','align'=>'left');
	$this->dataTable[1]=array('field'=>'stamp','title'=>'date','sortable'=>'true','visible'=>'true','align'=>'left');
	$this->dataTable[2]=array('field'=>'cnbOperador','title'=>'cnbOperador','sortable'=>'true','visible'=>'true','align'=>'left');
	$this->dataTable[3]=array('field'=>'cnbAgencia','title'=>'agency','sortable'=>'true','visible'=>'true','align'=>'left');
	$this->dataTable[4]=array('field'=>'cnbUser','title'=>'cnbUser','sortable'=>'true','visible'=>'false','align'=>'left');
	$this->dataTable[5]=array('field'=>'descripcionTrx','title'=>'description','sortable'=>'false','visible'=>'true','align'=>'left');
	$this->dataTable[6]=array('field'=>'montoTotal','title'=>'amount','sortable'=>'false','visible'=>'true','align'=>'left');
	$this->dataTable[7]=array('field'=>'comisionTotal','title'=>'commission','sortable'=>'false','visible'=>'true','align'=>'left');
	$this->dataTable[8]=array('field'=>'formaPago','title'=>'methodPay','sortable'=>'false','visible'=>'false','align'=>'left');
	$this->dataTable[9]=array('field'=>'cuentaBNP','title'=>'accountBNP','sortable'=>'false','visible'=>'false','align'=>'left');
	$this->dataTable[10]=array('field'=>'cuenta','title'=>'account','sortable'=>'false','visible'=>'false','align'=>'left');
	$this->dataTable[11]=array('field'=>'cuentaComision','title'=>'commissionaccount','sortable'=>'false','visible'=>'false','align'=>'left');
	$this->dataTable[12]=array('field'=>'mapa','title'=>'maps','sortable'=>'false','visible'=>'true','align'=>'left');
	$data['titleMenu']=$this->lang->line('CNB');
	$data['navbar_menu']=$this->navbar_header_menu();
	$data['controllerServer']='CnbPaper';
	$data['columns']=$this->getColumnsForTable();
	$data['rutaForm']=$this->rutaForm;
	$data['sizeModal']='modal-lg';
	$data['populate']='populateReporteIngresos';
	$data['title']='Reporte de Ingresos';
	$data['rutaToExport']=base_url().$this->router->fetch_class().'/exportIngresos';
	$data['localeForTable']=$this->localeForTable;
	$this->load->view('paper/cnb/_layoutReport',$data);

	}

	public function Comisiones(){
		$this->dataTable[0]=array('field'=>'Operador','title'=>'cnbOperador','sortable'=>'true','visible'=>'true','align'=>'left');
		$this->dataTable[1]=array('field'=>'Agencia','title'=>'agency','sortable'=>'true','visible'=>'true','align'=>'left');
		$this->dataTable[2]=array('field'=>'cuentaComision','title'=>'commissionaccount','sortable'=>'true','visible'=>'true','align'=>'left');
		$this->dataTable[3]=array('field'=>'comisionCNB','title'=>'comisionCNB','sortable'=>'true','visible'=>'true','align'=>'left');
		$this->dataTable[4]=array('field'=>'comisionBanco','title'=>'comisionBanco','sortable'=>'true','visible'=>'true','align'=>'left');

		$data['titleMenu']=$this->lang->line('CNB');
		$data['navbar_menu']=$this->navbar_header_menu();
		$data['controllerServer']='CnbPaper';
		$data['columns']=$this->getColumnsForTable();
		$data['rutaForm']=$this->rutaForm;
		$data['sizeModal']='modal-lg';
		$data['populate']='populateComisiones';
		$data['title']='Reporte de Comisiones';
		$data['rutaToExport']=base_url().$this->router->fetch_class().'/exportcomission';
		$data['localeForTable']=$this->localeForTable;
		$this->load->view('paper/cnb/_layoutReportComisiones',$data);
	}

	public function populateComisiones(){
		$data = json_decode(file_get_contents('php://input'), true);
		$limit=NULL;
		$offset=NULL;
		$search=NULL;
		$sort=NULL;
		$sortOrder=NULL;
		$fecha_inicio=date('Y-m-d').' 00:00:00';
		$fecha_fin=date('Y-m-d').' 23:59:59';
		if(isset($data['limit'])){
			$limit= $data['limit'];
		}

		if(isset($data['offset'])){
			$offset= $data['offset'];
		}

		if(isset($data['search'])){
			$search=utf8_decode($data['search']);
		}

		if(isset($data['sort'])){
			$sort=$data['sort'];
		}

		if(isset($data['sortOrder'])){
			$sortOrder=$data['sortOrder'];
		}

		if(isset($data['inicio'])){
			$fecha_inicio=$data['inicio'];
		}

		if(isset($data['fin'])){
			$fecha_fin=$data['fin'];
		}


		if($this->input->post('inicio') && $this->input->post('fin')){
			$fecha_inicio=$this->input->post('inicio').' 00:00:00';
			$fecha_fin=$this->input->post('fin').' 23:59:59';
		}

		$this->load->model('populateModel');
		$report=new populateModel();
		$report->_tablename='esb.transactionLog';
		$report->_primary_key='id';
		$report->_primary_filter='intval';
		$report->_order_by='id';
		$report->_order='ASC';
		$report->fecha_inicio=$fecha_inicio;
		$report->fecha_fin=$fecha_fin;
		$report->_timestamps='stamp';
		$report->_limit=$limit;
		$report->_search=$search;
		$report->_offset=$offset;
		$report->_selection=array();
		$report->_isMap=FALSE;
		$report->_Comisiones=FALSE;
		echo $report->getComisionesCNB();
	}

	public function EstadodeCuentas(){
		$this->dataTable[0]=array('field'=>'Cod_Agencia','title'=>'ID','sortable'=>'true','visible'=>'false','align'=>'left');
		$this->dataTable[1]=array('field'=>'Descripcion','title'=>'name','sortable'=>'true','visible'=>'true','align'=>'left');
		$this->dataTable[2]=array('field'=>'telefono','title'=>'phone','sortable'=>'true','visible'=>'true','align'=>'left');
		$this->dataTable[3]=array('field'=>'cuenta','title'=>'account','sortable'=>'true','visible'=>'true','align'=>'left');
		$this->dataTable[4]=array('field'=>'saldo','title'=>'saldo','sortable'=>'false','visible'=>'true','align'=>'left');
		$this->dataTable[5]=array('field'=>'cuenta','title'=>'btn_down','sortable'=>'false','visible'=>'true',
			'formatter'=>'setLink','align'=>'left');
		$data['titleMenu']=$this->lang->line('CNB');
		$data['navbar_menu']=$this->navbar_header_menu();
		$data['controllerServer']='CnbPaper';
		$data['columns']=$this->getColumnsForTable();
		$data['rutaForm']=$this->rutaForm;
		$data['title_form']='CNB';
		$data['sizeModal']='modal-lg';
		$data['title']=$this->lang->line('accountstatements');
		$data['rutaToExport']=base_url().$this->router->fetch_class().'/exportUser';
		$data['localeForTable']=$this->localeForTable;
		$this->load->view('paper/cnb/_layoutEstados',$data);
	}


	public function DescargarSaldo(){

		$this->load->model('ExportToPdf');
		$reporte = new ExportToPdf();

		$reporte->_tablename='esb.Agencias';
		$reporte->_primary_key='Cod_Agencia';
		$reporte->_order_by='Agencias.Cod_Agencia';

		//$reporte->fecha_inicio=$this->input->post('fecha_inicio').' 00:00:00';
		//$reporte->fecha_fin=$this->input->post('fecha_fin').' 23:59:00';


		$reporte->_order='DESC';

		$reporte->_limit=1;

		//cuenta: 275fe7fcf7b5f84d

		$reporte->_especificFieldToSearch='Agencias.Cod_agencia';

		$reporte->_especificValueToSearch=$this->input->post('cuenta');


		$reporte->_camposBusqueda=array();

		$reporte->_selection=array(
			'Agencias.Cod_Agencia',
			'Agencias.Descripcion',
			'Agencias.telefono',
			'Agencias.cuenta',
			'consultaSaldoCNB.Saldo as saldo'
		);
		$reporte->_joinRelation=array(
			array(

				'table'=>'esb.consultaSaldoCNB',
				'where_equals'=>'Agencias.Cod_Agencia=consultaSaldoCNB.agenciaCNBId',
				'option'=>'left'
			)
		);



		$reporte->_filename='EstadoDeCuenta_'.time().'_.pdf';
		$reporte->_descriptionPdf='REPORTE DE ESTADO DE CUENTA DE CNB PRESENTE EN EL SISTEMA';
		$reporte->_headersTablePdf=array(
			'COD AGENCIA',
			'NOMBRE',
			'TELEFONO',
			'CUENTA',
			'SALDO'
		);
		$reporte->_template['table_open']= '<table border="0" style="
																	border-left: 1px solid black;
																	border-bottom: 1px solid black;
																	border-top: 1px solid black;
																	">';
		$reporte->_template['thead_open']= '<thead>';



		$reporte->_template['heading_row_start']= '<tr style="">';



		$reporte->_template['heading_cell_start']= '<th style="
																	color: #000000;
																	font-weight: bolder;
																	border-bottom:1px solid black;
																	border-right: 1px solid black;
																	text-align: center;
 																	white-space: nowrap;
																">';


		$reporte->_template['cell_start']= '<td nowrap style="
														
														color: #000000;
 														margin: 5px auto;
 														border-right: 1px solid black;
 														border-bottom: 1px solid black;
 														text-align: left;
 														text-align: center;
 														white-space: nowrap;
 														">';


		$reporte->_template['cell_alt_start']= '<td nowrap style="
														
														color: #000000;
 														margin: 5px auto;
 														border-right: 1px solid black;
 														border-bottom: 1px solid black;
 														text-align: left;
 														text-align: center;
 														white-space: nowrap;
 														background-color: #beffbb;
 														">';

		$reporte->getPdf();

	}

	public function populateTableEstados(){
		$this->load->model('populateModel');
		$users=new populateModel();
		$users->_tablename='esb.Agencias';
		$users->_primary_key='Cod_Agencia';
		$users->_order_by='Agencias.Cod_Agencia';
		$users->_Comisiones=FALSE;

		$users->_camposBusqueda=array(
			'Agencias.Cod_Agencia',
			'Agencias.Descripcion',
			'Agencias.telefono',
			'Agencias.cuenta'
		);

		$users->_selection=array(
			'Agencias.Cod_Agencia',
			'Agencias.Descripcion',
			'Agencias.telefono',
			'Agencias.cuenta',
			'consultaSaldoCNB.Saldo as saldo'

		);
		$users->_joinRelation=array(

			array(

				'table'=>'esb.consultaSaldoCNB',
				'where_equals'=>'Agencias.Cod_Agencia=consultaSaldoCNB.agenciaCNBId',
				'option'=>'left'
			)
		);


		echo $users->populate_join();

	}

	public function exportcomission(){
		$letter=array(
			'a','b','c','d','e','f','g','h','i'
		);
		$rows=array();

		$inicio=date('Y-m-d').' 00:00:00';
		$fin=date('Y-m-d').' 23:59:00';

		$search='';

		if($this->input->post('search')){
			$search=$this->input->post('search');
		}

		if($this->input->post('inicio') && $this->input->post('fin')){
			$inicio=$this->input->post('inicio').' 00:00:00';
			$fin=$this->input->post('fin').' 23:59:00';
		}

		$this->db->select('*');

		if(strlen($search)>0){
			$this->db->like('Descripcion',$search);
		}
		$operadores=$this->db->get('esb.Operadores')->result_array();

		foreach ($operadores as $operador){
			$this->db->select('Agencias.Descripcion,Agencias.cuentaComision');
			$this->db->where('Cod_Operador',$operador['Cod_Operador']);
			$Agencias=$this->db->get('esb.Agencias')->result_array();
			foreach ($Agencias as $agencia){
				$this->db->select('transactionLog.comisionTotal,transactionLog.montoComisionCNB');
				$this->db->where('cnbAgencia',$agencia['Descripcion']);
				if(strlen($inicio)>0){
					$this->db->where('stamp >',$inicio);
					$this->db->where('stamp <',$fin);
				}
				$comisiones=$this->db->get('esb.transactionLog')->result_array();
				$comisionCNB=0;
				$comisionbanco=0;
				foreach ($comisiones as $monto){
					$comisionbanco+=$monto['comisionTotal'];
					$comisionCNB+=$monto['montoComisionCNB'];
				}
				array_push($rows,array(

						'Operador'=>$operador['Descripcion'],
						'Agencia'=>$agencia['Descripcion'],
						'cuentaComision'=>$agencia['cuentaComision'],
						'comisionCNB'=>$comisionCNB,
						'comisionBanco'=>$comisionbanco
					)

				);
			}

		}

		$this->load->library('excel');
		$this->excel->setActiveSheetIndex(0);
		$this->excel->getActiveSheet()->setTitle('COMISIONES CNB');

		$countletter=0;
		foreach ($rows[0] as $key => $value){
			$this->excel->getActiveSheet()->setCellValue($letter[$countletter].'1',$key);
			$countletter++;
		}
		$fila=2;
		for($i=0;$i<count($rows);$i++){
			$countletter=0;
			foreach ($rows[$i] as  $value){
				$this->excel->getActiveSheet()->setCellValue($letter[$countletter].$fila,$value);
				$countletter++;
			}
			$fila++;
		}

		//Le ponemos un nombre al archivo que se va a generar.
		$archivo = time().'.xls';
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$archivo.'"');
		header('Cache-Control: max-age=0');
		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
		//Hacemos una salida al navegador con el archivo Excel.
		$objWriter->save('php://output');
	}

	public function exportType(){
		$columnSearch='';
		$columnValue='';
		if($this->input->post('tipoTrx')!='all'){
			$columnSearch='tipoTrx';
			$columnValue=$this->input->post('tipoTrx');

		}

		$reporteLog=new cnbModel();
		$reporteLog->_selectToExport=array(
																				'stamp',
																				'cnbOperador',
																				'cnbAgencia',
																				'cnbUser',
																				'descripcionTrx',
																				'currency',
																				'montoTotal',
																				'comisionTotal'

																			);
		$reporteLog->_dateVariable='stamp';
		$reporteLog->_join=array();
		$reporteLog->_order='DESC';
		$reporteLog->_headersTable=array(
																			'FECHA',
																		 	'CNB OPERADOR',
																		  'CNB AGENCIA',
																			'CNB USER',
																			'DESCRIPCION TRX',
																			'MONTO TRX',
																			'MONTO COMISION'
																		);
		$reporteLog->_orderName='stamp';

		$reporteLog->_columnSearch=$columnSearch;
		$reporteLog->_columnValue=$columnValue;

		$reporteLog->ReporteByType();

	}

	public function exportIngresos(){
		$reporteLog=new cnbModel();
		$reporteLog->_selectToExport=array(
																				'stamp',
																				'cnbOperador',
																				'cnbAgencia',
																				'cnbUser',
																				'cnbTerminal',
																				'descripcionTrx',
																				'currency',
																				'montoTotal',
																				'comisionTotal'

																			);

		$reporteLog->_dateVariable='stamp';
		$reporteLog->_join=array();
		$reporteLog->_order='DESC';
		$reporteLog->_headersTable=array(
																			'FECHA',
																		 	'CNB OPERADOR',
																		  'CNB AGENCIA',
																			'CNB USER',
																			'CNB TERMINAL',
																			'DESCRIPCION TRX',
																			'MONTO TRX',
																			'MONTO COMISION'
																		);
		$reporteLog->_orderName='stamp';

		$reporteLog->ReportesDeIngreso();

	}
	public function export(){
		$reporteLog=new cnbModel();
		$reporteLog->_selectToExport=array(
																				'stamp',
																				'cnbOperador',
																				'cnbAgencia',
																				'cnbUser',
																				'cnbTerminal',
																				'descripcionTrx',
																				'currency',
																				'montoTotal',
																				'comisionTotal'

																			);

		$reporteLog->_dateVariable='stamp';
		$reporteLog->_join=array();
		$reporteLog->_order='DESC';
		$reporteLog->_headersTable=array(
																			'FECHA',
																		 	'CNB OPERADOR',
																		  'CNB AGENCIA',
																			'CNB USER',
																			'CNB TERMINAL',
																			'DESCRIPCION TRX',
																			'MONTO TRX',
																			'MONTO COMISION'
																		);
		$reporteLog->_orderName='stamp';

		$reporteLog->Reporte();
	}

	public function populateReport(){
		$data = json_decode(file_get_contents('php://input'), true);
		$limit=NULL;
		$offset=NULL;
		$search=NULL;
		$sort=NULL;
		$sortOrder=NULL;
		if(isset($data['limit'])){
			$limit= $data['limit'];
		}else{
			if($this->input->post('limit')){
				$limit=$this->input->post('limit');
			}
		}

		if(isset($data['offset'])){
			$offset= $data['offset'];
		}

		if(isset($data['search'])){
			$search=utf8_decode($data['search']);
		}

		if(isset($data['sort'])){
			$sort=$data['sort'];
		}

		if(isset($data['sortOrder'])){
			$sortOrder=$data['sortOrder'];
		}
		if(isset($data['incio'])){
			$fecha_inicio=$data['inicio'].' 00:00:00';
			$fecha_fin=$data['fin'].' 23:59:59';
		}



			if($this->input->post('inicio') && $this->input->post('fin')){
				$fecha_inicio=$this->input->post('inicio').' 00:00:00';
				$fecha_fin=$this->input->post('fin').' 23:59:59';
			}else{
				$fecha_inicio=date('Y-m-d').' 00:00:00';
				$fecha_fin=date('Y-m-d').' 23:59:59';
			}


		if($this->input->post('limit')){
			$limit= $this->input->post('limit');
		}

		if($this->input->post('offset')){
			$offset= $this->input->post('offset');
		}

		if($this->input->post('search')){
			$search=$this->input->post('search');
		}


		$this->load->model('populateModel');
		$report=new populateModel();
		$report->_tablename='esb.transactionLog';
	    $report->_primary_key='id';
	    $report->_primary_filter='intval';
	    $report->_order_by='id';
	    $report->_order='ASC';
	    $report->fecha_inicio=$fecha_inicio;
	    $report->fecha_fin=$fecha_fin;
	    $report->_timestamps='stamp';
	    $report->_limit=$limit;
	    $report->_search=$search;
	    $report->_offset=$offset;
	    $report->_selection=array();
			$report->_isMap=TRUE;
			echo $report->populateTrx();
	}

	public function populateReportType(){
		$data = json_decode(file_get_contents('php://input'), true);
		$limit=NULL;
		$offset=NULL;
		$search=NULL;
		$sort=NULL;
		$sortOrder=NULL;
		$type=NULL;
		if(isset($data['limit'])){
			$limit= $data['limit'];
		}

		if(isset($data['offset'])){
			$offset= $data['offset'];
		}

		if(isset($data['search'])){
			$search=utf8_decode($data['search']);
		}

		if(isset($data['sort'])){
			$sort=$data['sort'];
		}

		if(isset($data['sortOrder'])){
			$sortOrder=$data['sortOrder'];
		}

			if($this->input->post('inicio') && $this->input->post('fin')){
				$fecha_inicio=$this->input->post('inicio').' 00:00:00';
				$fecha_fin=$this->input->post('fin').' 23:59:59';
			}else{
				$fecha_inicio=date('Y-m-d').' 00:00:00';
				$fecha_fin=date('Y-m-d').' 23:59:59';
			}
			if($this->input->post('tipoTrx')){
				$type=$this->input->post('tipoTrx');
			}

			$this->load->model('populateModel');
			$report=new populateModel();
			$report->_tablename='esb.transactionLog';
	    $report->_primary_key='id';
	    $report->_primary_filter='intval';
	    $report->_order_by='id';
	    $report->_order='ASC';
	    $report->fecha_inicio=$fecha_inicio;
	    $report->fecha_fin=$fecha_fin;
	    $report->_timestamps='stamp';
	    $report->_limit=$limit;
	    $report->_search=$search;
	    $report->_offset=$offset;
	    $report->_selection=array();
	    if($this->input->post('tipoTrx') !='all'){
			$report->_valueSearch='tipoTrx';
			$report->_searchingValue=$type;

		}
		$report->_isMap=TRUE;
	    echo $report->populate();
	}

	public function populateReporteIngresos(){
		$data = json_decode(file_get_contents('php://input'), true);
		$limit=NULL;
		$offset=NULL;
		$search=NULL;
		$sort=NULL;
		$sortOrder=NULL;
		$type=NULL;
		if(isset($data['limit'])){
			$limit= $data['limit'];
		}

		if(isset($data['offset'])){
			$offset= $data['offset'];
		}

		if(isset($data['search'])){
			$search=utf8_decode($data['search']);
		}

		if(isset($data['sort'])){
			$sort=$data['sort'];
		}

		if(isset($data['sortOrder'])){
			$sortOrder=$data['sortOrder'];
		}

			if($this->input->post('inicio') && $this->input->post('fin')){
				$fecha_inicio=$this->input->post('inicio').' 00:00:00';
				$fecha_fin=$this->input->post('fin').' 23:59:59';
			}else{
				$fecha_inicio=date('Y-m-d').' 00:00:00';
				$fecha_fin=date('Y-m-d').' 23:59:59';
			}
			if($this->input->post('tipoTrx')){
				$type=$this->input->post('tipoTrx');
			}

			$this->load->model('populateModel');
			$report=new populateModel();
			$report->_valueSearch='aux';
			$report->_searchingValue='+';
			$report->_tablename='esb.transactionLog';
	    $report->_primary_key='id';
	    $report->_primary_filter='intval';
	    $report->_order_by='id';
	    $report->_order='ASC';
	    $report->fecha_inicio=$fecha_inicio;
	    $report->fecha_fin=$fecha_fin;
	    $report->_timestamps='stamp';
	    $report->_limit=$limit;
	    $report->_search=$search;
	    $report->_offset=$offset;
	    $report->_selection=array(
	    	'transactionLog.userMonedero',
	    	'transactionLog.terminalMonedero',
	    	'transactionLog.tipoTrx',
	    	'transactionLog.montoTotal',
	    	'transactionLog.formaPago',
	    	'transactionLog.stamp',
	    	'transactionLog.cnbOperador',
	    	'transactionLog.cnbAgencia',
	    	'transactionLog.cnbUser',
	    	'transactionLog.cnbTerminal',
	    	'transactionLog.cuenta',
	    	'transactionLog.descripcionTrx',
	    	'transactionLog.cuentaBNP',
	    	'transactionLog.agenciaCNBId',
	    	'transactionLog.cuentaComision',
	    	'transactionLog.comisionTotal',
	    	'transactionLog.currency',
	    	'transactionLog.terminalId',
	    	'transactionLog.lat',
	    	'transactionLog.lon',
	    	'transactionLog.descripcionCompra',
	    	'transactionLog.cuenta1',
	    	'transactionLog.email',
	    	'transactionLog.nombre'

		);
		$report->_valueSearch='aux';
		$report->_searchingValue='+';
		$report->_isMap=TRUE;
		echo $report->populateIngresos();
	}

	public function tipoTrx(){
		$selectorTipoTrx='<select class="form-control" name="tipoTrx" id="tipoTrx">';
		$opciones='';
		$this->db->select('*');
		//$this->db->group_by(array('esb.transactionLog.tipoTrx','transactionLog.descripcionTrx'));
		$this->db->order_by('name','asc');
		$query=$this->db->get('esb.tipoTransaccionesCnb');
		foreach ($query->result() as $value) {
			$opciones.='<option value="'.$value->id.'">'.$value->name.'</option>';
		}
		$opciones.='<option value="all">Todas</option>';
		$selectorTipoTrx.=$opciones.'</select>';
		return $selectorTipoTrx;
	}

	public function CNBInstalados(){
		$this->dataTable[0]=array('field'=>'Cod_Operador','title'=>'ID','sortable'=>'true','visible'=>'false','align'=>'left');
		$this->dataTable[1]=array('field'=>'fechaRegistro','title'=>'date','sortable'=>'true','visible'=>'true','align'=>'left');
		$this->dataTable[2]=array('field'=>'Descripcion','title'=>'description','sortable'=>'true','visible'=>'true','align'=>'left');
		$this->dataTable[3]=array('field'=>'Estado','title'=>'status','sortable'=>'true','visible'=>'true','align'=>'left');
		$this->dataTable[4]=array('field'=>'Pais','title'=>'country','sortable'=>'true','visible'=>'true','align'=>'left');
		$this->dataTable[5]=array('field'=>'Color','title'=>'color','sortable'=>'true','visible'=>'true','align'=>'left','formatter'=>'setColor');
		$this->dataTable[6]=array('field'=>'cantidadTransacciones','title'=>'NoTrx','sortable'=>'true','visible'=>'true','align'=>'left');
		$this->dataTable[7]=array('field'=>'cantidadLogin','title'=>'NoLogin','sortable'=>'true','visible'=>'true','align'=>'left');
		$this->dataTable[8]=array('field'=>'canal','title'=>'channel','sortable'=>'true','visible'=>'true','align'=>'left');
		$data['titleMenu']='CNB';
		$data['navbar_menu']=$this->navbar_header_menu();
		$data['controllerServer']='CnbPaper';
		$data['columns']=$this->getColumnsForTable();
		$data['rutaForm']=$this->rutaForm;
		$data['title']=$this->lang->line('cnbinstalados');
		$data['sizeModal']='modal-lg';
		$data['documentation']='';
		$data['rutaToExport']='';
		$data['populate']='populateOperadores';
		$data['localeForTable']=$this->localeForTable;
		$this->load->view('paper/cnb/_layoutReporteCnbInstalados',$data);
	}

	public function populateOperadores(){

		$fecha_de_inicio=$this->input->post('inicio');
		$fecha_de_fin=$this->input->post('fin');


		$this->load->model('populateModel');
		$report=new populateModel();
		$report->_tablename='esb.Operadores';
		$report->_primary_key='Cod_Operador';
		$report->_primary_filter='intval';
		$report->_order_by='Cod_Operador';
		$report->_order='ASC';
		$report->_timestamps='fechaRegistro';

		$report->withCountCNB=TRUE;

		$report->fecha_fin=$fecha_de_fin;
		$report->fecha_inicio=$fecha_de_inicio;

		$report->_selection=array(
			'Operadores.Cod_Operador',
			'Operadores.fechaRegistro',
			'Operadores.Pais',
			'Operadores.Estado',
			'Operadores.Descripcion',
			'Operadores.Color',
			'Canales.Canal as canal',
		);

		$report->_camposBusqueda=array(
			'Operadores.Descripcion'
		);

		$report->_joinRelation=array(
			array(
				'table'=>'esb.Canales',
				'where_equals'=>'Operadores.canalId=Canales.Id',
				'option'=>'left'
			)
		);

		echo $report->populate_join();

	}

	public function ExportOperadores(){

		$fecha_de_inicio=$this->input->post('fecha_de_inicio');
		$fecha_de_fin=$this->input->post('fecha_de_fin');

		$this->load->model('ExportToPdf');
		$reporte = new ExportToPdf();

		$reporte->_tablename='esb.Operadores';
		$reporte->_primary_key='Cod_Operador';
		$reporte->_order_by='Operadores.Cod_Operador';

		$reporte->_timestamps='fechaRegistro';

		$reporte->fecha_inicio=$fecha_de_inicio;
		$reporte->fecha_fin=$fecha_de_fin;

		$reporte->withCountCNB=TRUE;


		$reporte->_selection=array(

			'Operadores.Cod_Operador',
			'Operadores.fechaRegistro',
			'Operadores.Pais',
			'Operadores.Estado',
			'Operadores.Descripcion',
			'Canales.Canal as canal',

		);
		$reporte->_joinRelation=array(

			array(

				'table'=>'esb.Canales',
				'where_equals'=>'Operadores.canalId=Canales.Id',
				'option'=>'left'
			)
		);

		$reporte->_filename='Operadores'.time().'_.pdf';
		$reporte->_descriptionPdf='REPORTE DE COMERCIO FACTURADOR CNB PRESENTES EN EL SISTEMA';
		$reporte->_headersTablePdf=array('COD_OPERADOR','FECHA DE REGISTRO','PAIS','ESTADO','DESCRIPCION','CANAL','TRX FINANCIERAS','TRX NO FINANCIERAS');
		$reporte->_template['table_open']= '<table border="0" style="
																	border-left: 1px solid black;
																	border-bottom: 1px solid black;
																	border-top: 1px solid black;
																	">';
		$reporte->_template['thead_open']= '<thead>';



		$reporte->_template['heading_row_start']= '<tr style="">';



		$reporte->_template['heading_cell_start']= '<th style="
																	color: #000000;
																	font-weight: bolder;
																	border-bottom:1px solid black;
																	border-right: 1px solid black;
																	text-align: center;
 																	white-space: nowrap;
																">';


		$reporte->_template['cell_start']= '<td nowrap style="
														
														color: #000000;
 														margin: 5px auto;
 														border-right: 1px solid black;
 														border-bottom: 1px solid black;
 														text-align: left;
 														text-align: center;
 														white-space: nowrap;
 														">';


		$reporte->_template['cell_alt_start']= '<td nowrap style="
														
														color: #000000;
 														margin: 5px auto;
 														border-right: 1px solid black;
 														border-bottom: 1px solid black;
 														text-align: left;
 														text-align: center;
 														white-space: nowrap;
 														background-color: #beffbb;
 														">';

		$reporte->getPdf();
	}



}

?>

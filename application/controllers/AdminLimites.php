<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class AdminLimites extends MY_Controller{

	public $permiso_crear=123;
	public $permiso_actualizar=124;
	public $permiso_eliminar=125;
	public $permiso_ver=25;

	public $contenido=array();
	protected $dataForm=array('title_form'=>'Datos Límites');


	public function __construct(){
        parent::__construct();
        $this->load->model('limites_m');

    }

    public function index(){

    	$this->contenido['table']=$this->load->view("paper/tables/table_limites",'',true);
    	$this->contenido['title']=$this->lang->line('limits');;
		$datos["content_view"]=$this->load->view("paper/admin/paper_view_limites",$this->contenido,true);
		$datos['navbar_menu']=$this->navbar_header_menu();
		$datos['sidebar_menu']=$this->menu_admin();
		$datos['footer']=$this->load->view("paper/footers/footer_limites",'',true);

		$this->load->view("paper/base",$datos);

    }
    public function populateTable(){
    	echo json_encode($this->limites_m->get());
    }

    public function Form(){
    	$this->dataForm['form']=$this->limites_m->Form();
    	echo $this->load->view("paper/forms_modal/form_limites",$this->dataForm,true);
    }

    public function saveData(){
		$this->form_validation->set_rules($this->limites_m->rules);
		$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
		if($this->form_validation->run()==TRUE){
			foreach ($this->input->post() as $key => $value) {
				$data[$key]=$value;
			}
			$this->limites_m->save($data);
			echo "Todo bien!";
		}else{
			$this->dataForm['form']=$this->limites_m->Form();
    		echo $this->load->view("paper/forms_modal/form_limites",$this->dataForm,true);
		}
	}

	public function EditData(){
		$id=$this->input->post('Id');
		$data_by_id=$this->limites_m->get($id,true);

		$dataForm['form']=$this->limites_m->FormForEdit($data_by_id);
		echo $this->load->view("paper/forms_modal/form_limites_edit",$dataForm,true);
		//var_dump($id);

	}

	public function UpdateData(){
		$this->form_validation->set_rules($this->limites_m->rules);
		$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
		if($this->form_validation->run()==TRUE){
			$data=array();
			foreach ($this->input->post() as $key => $value) {
				if($key!='id'){
					$data[$key]=$value;
				}
			}
			$this->limites_m->save($data,$this->input->post('id'));
			echo 'Todo bien!';
		}else{
			$id=$this->input->post('id');
			$data_by_id=$this->limites_m->get($id,true);
			$dataForm['form']=$this->limites_m->FormForEdit($data_by_id);
			echo $this->load->view("paper/forms_modal/form_limites_edit",$dataForm,true);

		}
	}

	public function DeleteData(){
		$id=$this->input->post('id');
		$this->limites_m->delete($id);
		echo '<p class="label label-success">'.$this->lang->line('error_delete').'<strong>'.$id.'</strong></p>';

	}

	public function diferente_de_0($str)
        {
            if ($str == '0')
            {
                $this->form_validation->set_message('diferente_de_0', 'Favor Seleccionar Una Opcion Válida');
                return FALSE;
            }
            else
            {
                return TRUE;
            }
        }

}
?>

<?php
/**
 * Created by PhpStorm.
 * User: hrodriguez
 * Date: 10/01/18
 * Time: 02:39 AM
 */


defined('BASEPATH') OR exit('No direct script access allowed');

class Faq extends MY_Controller {


	public $permiso_actualizar=61;
	public $permiso_ver=158;
	public $mymodel='Faq_m';
	public $controlerServer='Faq';
	public $formSize='sm';

	//dato para locale table
	public $localeForTable='es-MX';


	public function __construct()
	{
		parent::__construct();
		//Codeigniter : Write Less Do More
	}



	public function index()
	{
		$this->dataTable[0]=array('field'=>'Id','title'=>'ID','sortable'=>'true','visible'=>'false','align'=>'left');
		$this->dataTable[1]=array('field'=>'Titulo','title'=>'titulo','sortable'=>'true','visible'=>'true','align'=>'left');
		$this->dataTable[2]=array('field'=>'Texto','title'=>'texto','sortable'=>'true','visible'=>'true','align'=>'left');
		$this->dataTable[3]=array('field'=>'Estado','title'=>'status','sortable'=>'true','visible'=>'true','align'=>'left');
		$this->dataTable[4]=array('field'=>'tipo','title'=>'tipoFaq','sortable'=>'true','visible'=>'true','align'=>'left');
		$this->dataTable[5]=array('field'=>'actions','title'=>'actions','class'=>'td-actions text-left','align'=>'left','events'=>'operateEvents','formatter'=>'operateFormatter');
		$data['titleMenu']=$this->lang->line('titleAdmin');
		$data['navbar_menu']=$this->navbar_header_menu();
		$data['controllerServer']=$this->controlerServer;
		$data['columns']=$this->getColumnsForTable();
		$data['rutaForm']=$this->rutaForm;
		$data['title_form']=$this->lang->line('faq');
		$data['sizeModal']='modal-lg';
		$data['documentation']='';
		$data['title']=$this->lang->line('faq');
		$data['rutaToExport']='';
		$data['localeForTable']=$this->localeForTable;
		$this->load->view('paper/admin/vista_contrato',$data);
	}

	public function FaqsCnb(){


		$data['title']=$this->lang->line('TitleFaqsCnb');
		$data['fullText']=$this->PreguntasFrecuentesTipo('CNB');
		$this->load->view('Paper/Faqs',$data);

	}

	public function FaqsBilletera(){
		$data['title']=$this->lang->line('TitleFaqsBilletera');
		$data['fullText']=$this->PreguntasFrecuentesTipo('Billetera');
		$this->load->view('Paper/Faqs',$data);
	}


	private function PreguntasFrecuentesTipo($nameTipo){
		$tipo=0;
		$parrafos='';
		if(strlen($nameTipo)>0){
			if($nameTipo=='CNB'){
				$tipo=1;
			}
			if($nameTipo=='Billetera'){
				$tipo=2;
			}

		}
		$this->db->select('*');
		if($tipo>0){
			$this->db->where('tipo',$tipo);
		}
		$this->db->where('Estado','A');

		$query=$this->db->get('esb.FAQ');

		if($query->num_rows()>0){
			foreach ($query->result() as $item) {
				$parrafos.='<div><h4 class="text-center "><strong class="text-info">'.$item->Titulo.'</strong></h4><p><strong class="text-danger">Respuesta : </strong><br>'.$item->Texto.'</p></div>';
			}
		}

		return $parrafos;

	}

}

/* End of file Controllername.php */
?>

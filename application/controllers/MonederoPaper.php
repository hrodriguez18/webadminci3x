<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MonederoPaper extends MY_Controller{

//DATOS PARA LOS PERMISOS
	public $permiso_ver=126;
	public $permiso_crear=166;
	public $permiso_eliminar=165;
//DATOS PARA LA TABLA
	public $mymodel='Monedero_m';
	public $controlerServer='MonederoPaper';
	public $dataTable=array();
//Ruta para llamar al formulario;
	public $rutarForm='';
//Tamaño del formulario
	public $formSize='lg';

	//variable para Exportar
	public $rutaToExport='';

	public function __construct(){
        parent::__construct();

        $this->rutaForm=base_url().$this->controlerServer.'/formUsers';
		$this->load->helper('date');
    }

	public function index(){
		$this->load->model('DashboardModel');
		$dashBoard=new DashboardModel();
		$year=date('Y');
		$mes=date('m');
		$pais='PAN';

		$lastDay=days_in_month($mes, $year);


		$fechaInicio=$year.'-'.$mes.'-01 00:00:00';
		$fechaFin=$year.'-'.$mes.'-'.$lastDay.' 23:59:00';

		$dashBoard->_tablename='esb.transactionMonederoLog';
		$dashBoard->_order_by='id';
		$dashBoard->_order='DESC';
		$dashBoard->_limit=0;
		$dashBoard->_timestamps='stamp';
		$dashBoard->_fechaInicio=$fechaInicio;
		$dashBoard->_fechaFin=$fechaFin;
		$datos_content['contadores']=$dashBoard->getCounters();
		$datos_content['formforCounters']=$dashBoard->FormSearchCounters();
		$datos_content['rutaUpdateKPI']='MonederoPaper/UpdateKPI';
		$datos_content['rutaSetGraph']='MonederoPaper/UpdateGraphic';
		$datos_content['controlador']='MonederoPaper';
		$datos_content['titleMenu']=$this->lang->line('Monedero');
		$datos["content_view"]=$this->load->view("paper/monedero/content_monedero",$datos_content,true);
		$datos['navbar_menu']=$this->navbar_header_menu();
		$datos['sidebar_menu']=$this->load->view('paper/menupaper/menu_monedero','',true);

		$this->load->view("paper/base",$datos);
	}

	public function UpdateKPI(){



		$year=$this->input->post('year');
		$mes=$this->input->post('month');
		$pais=$this->input->post('country');

		$lastDay=days_in_month($mes, $year);


		$fechaInicio=$year.'-'.$mes.'-01 00:00:00';
		$fechaFin=$year.'-'.$mes.'-'.$lastDay.' 23:59:00';


		$this->load->model('DashboardModel');
		$dashBoard=new DashboardModel();
		$dashBoard->_tablename='esb.transactionMonederoLog';
		$dashBoard->_order='DESC';
		$dashBoard->_order_by='id';
		$dashBoard->_limit=0;
		$dashBoard->_timestamps='stamp';

		$dashBoard->_fechaInicio=$fechaInicio;
		$dashBoard->_fechaFin=$fechaFin;

		$dashBoard->_especificField='paisId';
		$dashBoard->_especificValue=$pais;

		echo $dashBoard->getCounters();

	}

	public function Graphics(){
		$year=$this->input->post('year');
		$mes=$this->input->post('mes');
		$pais=$this->input->post('pais');
		$canal=$this->input->post('canal');
		$autorizador=$this->input->post('autorizador');
		if($year){
			echo json_encode($this->adminmodel->GraphicalView($year,$pais,$canal,$autorizador));

		}

	}

	public function DatosVentas(){

		$year=$this->input->post('year');
		$mes=$this->input->post('mes');
		$pais=$this->input->post('pais');
		$canal=$this->input->post('canal');
		$autorizador=$this->input->post('autorizador');

		$datos=array('year'=>$year,'month'=>$mes,'pais'=>$pais,'canal'=>$canal,'autorizador'=>$autorizador);
		echo $this->adminmodel->DatosVentasQuery($datos);
	}

	public function SearchPoints(){
		$operador=$this->input->post('operador');
		echo json_encode($this->adminmodel->GetPointForMaps($operador));
	}

	public function formUsers(){
		if($this->input->post('id')){
			$id=$this->input->post('id');
		}else{
			$id=NULL;
		}
		echo $this->modelo->form('lg',$id);
	}

	public function exportUser(){
		$this->modelo->exportToPdf();
	}

	public function Users(){
		$this->dataTable[0]=array('field'=>'id','title'=>'ID','sortable'=>'true','visible'=>'false','align'=>'left');
		$this->dataTable[1]=array('field'=>'name','title'=>'name','sortable'=>'true','visible'=>'true','align'=>'left');
		$this->dataTable[2]=array('field'=>'lastname','title'=>'lastname','sortable'=>'true','visible'=>'true','align'=>'left');
		$this->dataTable[3]=array('field'=>'cedula','title'=>'cedula','sortable'=>'true','visible'=>'true','align'=>'left');
		$this->dataTable[4]=array('field'=>'phone','title'=>'phone','sortable'=>'true','visible'=>'true','align'=>'left');
		$this->dataTable[5]=array('field'=>'email','title'=>'email','sortable'=>'false','visible'=>'false','align'=>'left');
		$this->dataTable[6]=array('field'=>'ROL','title'=>'rol_id','sortable'=>'false','visible'=>'true','align'=>'left');
		$this->dataTable[7]=array('field'=>'paisId','title'=>'country','sortable'=>'false','visible'=>'true','align'=>'left');
		$this->dataTable[8]=array('field'=>'cuentaMonedero','title'=>'account','sortable'=>'false','visible'=>'true','align'=>'left');
		$this->dataTable[9]=array('field'=>'tokenEnviado','title'=>'tokenEnviado','sortable'=>'true','visible'=>'false','align'=>'center','formatter'=>'changeToYesOrNot');
		$this->dataTable[10]=array('field'=>'created_at','title'=>'created_at','sortable'=>'true','visible'=>'true','align'=>'left');
		$this->dataTable[11]=array('field'=>'cantidadTransacciones','title'=>'NoTrx','sortable'=>'true','visible'=>'true','align'=>'left');
		$this->dataTable[12]=array('field'=>'cantidadLogin','title'=>'NoLogin','sortable'=>'true','visible'=>'true','align'=>'left');
		$this->dataTable[13]=array('field'=>'actions','title'=>'actions','class'=>'td-actions text-left','align'=>'left','events'=>'operateEvents','formatter'=>'operateFormatter');

		$data['titleMenu']=$this->lang->line('Monedero');
		$data['navbar_menu']=$this->navbar_header_menu();
		$data['controllerServer']='MonederoPaper';
		$data['columns']=$this->getColumnsForTable();
		$data['rutaForm']=$this->rutaForm;
		$data['sizeModal']='modal-lg';
		$data['populate']='populateUserMonedero';
		$data['title']=$this->lang->line('walletUsers');
		$data['title_form']='Edición de Billetera';
		$data['rutaToExport']=base_url().$this->router->fetch_class().'/exportUsersMonedero';
		$data['localeForTable']=$this->localeForTable;
		$this->load->view('paper/monedero/_layoutMonedero',$data);

	}

	public function exportUsersMonederoByTrxExcel(){

		$search='';
		if($this->input->post('inicio')){
			$fecha_inicio=$this->input->post('inicio').' 00:00:00';
			$fecha_fin=$this->input->post('fin').' 23:59:00';

		}

		if($this->input->post('search')){
			$search=$this->input->post('search');
		}


		$this->db->select('*');

		$_camposBusqueda=array(
			'usuariosMonedero.id',
			'usuariosMonedero.name',
			'usuariosMonedero.lastname',
			'usuariosMonedero.email',
			'usuariosMonedero.phone'
		);


		if(count($_camposBusqueda)> 0 && $search!=''){
			$i=0;
			$this->db->like($_camposBusqueda[$i],$search);
			for($i;$i<count($_camposBusqueda);$i++){
				$this->db->or_like($_camposBusqueda[$i],$search);
			}

		}
		$users=$this->db->get('esb.usuariosMonedero');
		$Usuarios=array();

		if($users->num_rows()>0){
			foreach($users->result() as $value){

				//Buscar el numero de transacciones en el rango de tiempo.
				$this->db->select('id');

				if(strlen($fecha_inicio)>0){
					$this->db->where('stamp > ', $fecha_inicio);
					$this->db->where('stamp < ', $fecha_fin);
				}
				$this->db->where('cuenta',$value->cuentaMonedero);
				$noTransacciones=$this->db->get('esb.transactionMonederoLog')->num_rows();

				//Datos de la ultima transaccion;

				$this->db->select('transactionMonederoLog.stamp, transactionMonederoLog.descripcionTrx');
				$this->db->where('cuenta',$value->cuentaMonedero);
				$this->db->order_by('stamp','DESC');
				$this->db->limit(1);
				$transacciones=$this->db->get('esb.transactionMonederoLog');

				if($transacciones->num_rows()>0){
					foreach($transacciones->result() as $trx){
						$lastTransaction=$trx->descripcionTrx;
						$dateLastTrx=$trx->stamp;
					}
				}

				array_push($Usuarios,array(
					'id'=>$value->id,
					'name'=>$value->name,
					'lastname'=>$value->lastname,
					'cedula'=>$value->cedula,
					'telefono'=>$value->phone,
					'cuentaMonedero'=>$value->cuentaMonedero,
					'noTransacciones'=>$noTransacciones,
					'lastTransaction'=>$lastTransaction,
					'dateLastTrx'=>$dateLastTrx
				));
			}
		}

		$letter=array(
			'a','b','c','d','e','f','g','h','i'
		);
		$rows=array('ID','NOMBRE','APELLIDO','CEDULA','TELEFONO','No. CUENTA','No. TRX',' ULTIMA TRX','FECHA DE ULTIMA TRX');

		$this->load->library('excel');
		$this->excel->setActiveSheetIndex(0);
		$this->excel->getActiveSheet()->setTitle('TRANSACCIONES POR USUARIOS');

		$countletter=0;
		foreach ($rows as $key ){
			$this->excel->getActiveSheet()->setCellValue($letter[$countletter].'1',$key);
			$countletter++;
		}
		$fila=2;
		for($i=0;$i<count($Usuarios);$i++){
			$countletter=0;
			foreach ($Usuarios[$i] as  $value){
				$this->excel->getActiveSheet()->setCellValue($letter[$countletter].$fila,$value);
				$countletter++;
			}
			$fila++;
		}

		//Le ponemos un nombre al archivo que se va a generar.
		$archivo = time().'.xls';
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$archivo.'"');
		header('Cache-Control: max-age=0');
		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
		//Hacemos una salida al navegador con el archivo Excel.
		$objWriter->save('php://output');


	}

	public function DetalleTrxByUsers(){
		$this->dataTable[0]=array('field'=>'id','title'=>'ID','sortable'=>'true','visible'=>'false','align'=>'left');
		$this->dataTable[1]=array('field'=>'name','title'=>'name','sortable'=>'true','visible'=>'true','align'=>'left');
		$this->dataTable[2]=array('field'=>'lastname','title'=>'lastname','sortable'=>'true','visible'=>'true','align'=>'left');
		$this->dataTable[3]=array('field'=>'cedula','title'=>'cedula','sortable'=>'true','visible'=>'true','align'=>'left');
		$this->dataTable[4]=array('field'=>'telefono','title'=>'phone','sortable'=>'true','visible'=>'true','align'=>'left');
		$this->dataTable[5]=array('field'=>'cuentaMonedero','title'=>'account','sortable'=>'true','visible'=>'true','align'=>'left');
		$this->dataTable[6]=array('field'=>'noTransacciones','title'=>'NumTrx','sortable'=>'false','visible'=>'true','align'=>'left');
		$this->dataTable[7]=array('field'=>'lastTransaction','title'=>'lastTransaction','sortable'=>'false','visible'=>'true','align'=>'left');
		$this->dataTable[8]=array('field'=>'dateLastTrx','title'=>'dateLastTrx','sortable'=>'false','visible'=>'true','align'=>'left');
		$data['titleMenu']=$this->lang->line('Monedero');
		$data['navbar_menu']=$this->navbar_header_menu();
		$data['controllerServer']='MonederoPaper';
		$data['columns']=$this->getColumnsForTable();
		$data['rutaForm']=$this->rutaForm;
		$data['sizeModal']='modal-lg';
		$data['populate']='PopulateDetalleTrxByUsers';
		$data['title']=$this->lang->line('walletUsers');
		$data['title_form']='Edición de Billetera';
		$data['rutaToExport']=base_url().$this->router->fetch_class().'/exportUsersMonederoByTrxPdf';
		$data['rutaToExportExcel']=base_url().$this->router->fetch_class().'/exportUsersMonederoByTrxExcel';
		$data['localeForTable']=$this->localeForTable;
		$this->load->view('paper/monedero/_layoutMonederoTrxByUser',$data);

	}

	public function exportUsersMonederoByTrxPdf(){
		$fecha_de_inicio=$this->input->post('inicio');
		$fecha_de_fin=$this->input->post('fin');
		$search=$this->input->post('search');

		$this->load->model('ExportToPdf');
		$reporte = new ExportToPdf();

		$reporte->_camposBusqueda=array(
			'usuariosMonedero.id',
			'usuariosMonedero.name',
			'usuariosMonedero.lastname',
			'usuariosMonedero.email',
			'usuariosMonedero.phone',
		);

		$reporte->_selection=array(
			'usuariosMonedero.id',
			'usuariosMonedero.name',
			'usuariosMonedero.lastname',
			'usuariosMonedero.cedula',
			'usuariosMonedero.phone',
			'usuariosMonedero.email',
			'usuariosMonedero.correo',
			'usuariosMonedero.paisId',
			'usuariosMonedero.cuentaMonedero',
			'usuariosMonedero.created_at'
		);


		$reporte->_filename='UsuariosMonedero_'.time().'_.pdf';
		$reporte->_descriptionPdf='REPORTE DE USUARIOS DE MONEDEROS PRESENTES EN EL SISTEMA';
		$reporte->_headersTablePdf=array('ID','NOMBRE','APELLIDO','CEDULA','TELEFONO','No. CUENTA','TRX FINANCIERAS','ÚLTIMA TRX','FECHA ÚLTIMA TRX');
		$reporte->_template['table_open']= '<table border="0" style="
																	border-left: 1px solid black;
																	border-bottom: 1px solid black;
																	border-top: 1px solid black;
																	">';
		$reporte->_template['thead_open']= '<thead>';



		$reporte->_template['heading_row_start']= '<tr style="">';



		$reporte->_template['heading_cell_start']= '<th style="
																	color: #000000;
																	font-weight: bolder;
																	border-bottom:1px solid black;
																	border-right: 1px solid black;
																	text-align: center;
 																	white-space: nowrap;
																">';


		$reporte->_template['cell_start']= '<td nowrap style="
														
														color: #000000;
 														margin: 5px auto;
 														border-right: 1px solid black;
 														border-bottom: 1px solid black;
 														text-align: left;
 														text-align: center;
 														white-space: nowrap;
 														">';


		$reporte->_template['cell_alt_start']= '<td nowrap style="
														
														color: #000000;
 														margin: 5px auto;
 														border-right: 1px solid black;
 														border-bottom: 1px solid black;
 														text-align: left;
 														text-align: center;
 														white-space: nowrap;
 														background-color: #beffbb;
 														">';

		$reporte->_footerContentPdf='<p>Periodo de '.$fecha_de_inicio. ' a '.$fecha_de_fin.'</p>';

		$reporte->getPdfTrxByUsers();
	}

	public function PopulateDetalleTrxByUsers(){
		$this->load->model('populateModel');
		$pop=new PopulateModel();
		$pop->_camposBusqueda=array(
			'usuariosMonedero.name',
			'usuariosMonedero.lastname',
			'usuariosMonedero.email',
			'usuariosMonedero.cedula'
		);
		echo $pop->transaccionesPorUsusarios();
	}

	public function populateUserMonedero(){


		if($this->input->post('inicio')){
			$fecha_de_inicio=$this->input->post('inicio');
			$fecha_de_fin=$this->input->post('fin');
		}else{
			$fecha_de_inicio=date('Y-m-d');
			$fecha_de_fin=date('Y-m-d');
		}



		$this->load->model('populateModel');
		$users=new populateModel();
		$users->_tablename='esb.usuariosMonedero';
		$users->_primary_key='usuariosMonedero.id';
		$users->_order_by='usuariosMonedero.id';
		$users->withCount=TRUE;

		if(strlen($fecha_de_inicio)>0){
			$users->fecha_fin=$fecha_de_fin.' 23:59:00';
			$users->fecha_inicio=$fecha_de_inicio.' 00:00:00';
			$users->_timestamps='created_at';
		}

		$users->_camposBusqueda=array(
			'usuariosMonedero.id',
			'usuariosMonedero.name',
			'usuariosMonedero.lastname',
			'usuariosMonedero.email',
			'usuariosMonedero.phone',
			'usuariosMonedero.cedula',
			'usuariosMonedero.cuentaMonedero',
		);

		$users->_selection=array(
									'usuariosMonedero.id',
									'usuariosMonedero.email',
									'usuariosMonedero.name',
									'usuariosMonedero.lastname',
									'usuariosMonedero.phone',
									'usuariosMonedero.paisId',
									'usuariosMonedero.created_at',
									'usuariosMonedero.cedula',
									'usuariosMonedero.tokenEnviado',
									'usuariosMonedero.cuentaMonedero',
									'rolesTerminal.descripcion as ROL',

								);
		$users->_joinRelation=array(

			array(

				'table'=>'esb.rolesTerminal',
				'where_equals'=>'usuariosMonedero.rolId=rolesTerminal.id',
				'option'=>'left'
			)
		);

		echo $users->populateUsersMonedero();
	}

	public function exportUsersMonedero(){

		$fecha_de_inicio=$this->input->post('inicio');
		$fecha_de_fin=$this->input->post('fin');



		$this->load->model('ExportToPdf');
		$reporte = new ExportToPdf();

		$reporte->_tablename='esb.usuariosMonedero';
		$reporte->_primary_key='id';
		$reporte->_order_by='usuariosMonedero.id';

		$reporte->withCount=TRUE;

		if($this->input->post('search')){
			$reporte->_search=$this->input->post('search');
		}

		if(strlen($fecha_de_inicio)>0){
			$reporte->fecha_inicio=$fecha_de_inicio.' 00:00:00';
			$reporte->fecha_fin=$fecha_de_fin.' 23:59:00';
			$reporte->_timestamps='created_at';
		}



		$reporte->_camposBusqueda=array(
			'usuariosMonedero.id',
			'usuariosMonedero.name',
			'usuariosMonedero.lastname',
			'usuariosMonedero.email',
			'usuariosMonedero.phone',
		);

		$reporte->_selection=array(
			'usuariosMonedero.id',
			'usuariosMonedero.name',
			'usuariosMonedero.lastname',
			'usuariosMonedero.cedula',
			'usuariosMonedero.phone',
			'usuariosMonedero.email',
			'usuariosMonedero.correo',
			'rolesTerminal.descripcion as ROL',
			'usuariosMonedero.paisId',
			'usuariosMonedero.cuentaMonedero',
			'usuariosMonedero.created_at'


		);
		$reporte->_joinRelation=array(

			array(

				'table'=>'esb.rolesTerminal',
				'where_equals'=>'usuariosMonedero.rolId=rolesTerminal.id',
				'option'=>'left'
			)
		);

		$reporte->_filename='UsuariosMonedero_'.time().'_.pdf';
		$reporte->_descriptionPdf='REPORTE DE USUARIOS DE MONEDEROS PRESENTES EN EL SISTEMA';
		$reporte->_headersTablePdf=array('ID','NOMBRE','APELLIDO','CEDULA','TELEFONO','CELULAR','CORREO','ROL','PAIS','CUENTA','FECHA REGISTRO','TRX FINANCIERAS','TRX NO FINANCIERAS');
		$reporte->_template['table_open']= '<table border="0" style="
																	border-left: 1px solid black;
																	border-bottom: 1px solid black;
																	border-top: 1px solid black;
																	">';
		$reporte->_template['thead_open']= '<thead>';



		$reporte->_template['heading_row_start']= '<tr style="">';



		$reporte->_template['heading_cell_start']= '<th style="
																	color: #000000;
																	font-weight: bolder;
																	border-bottom:1px solid black;
																	border-right: 1px solid black;
																	text-align: center;
 																	white-space: nowrap;
																">';


		$reporte->_template['cell_start']= '<td nowrap style="
														
														color: #000000;
 														margin: 5px auto;
 														border-right: 1px solid black;
 														border-bottom: 1px solid black;
 														text-align: left;
 														text-align: center;
 														white-space: nowrap;
 														">';


		$reporte->_template['cell_alt_start']= '<td nowrap style="
														
														color: #000000;
 														margin: 5px auto;
 														border-right: 1px solid black;
 														border-bottom: 1px solid black;
 														text-align: left;
 														text-align: center;
 														white-space: nowrap;
 														background-color: #beffbb;
 														">';

		$reporte->getPdf();

	}

	public function CreateMonedero(){

		ini_set("max_execution_time",'600');
		$config['upload_path'] = 'assets/uploads/files/depositos';
		$config['allowed_types'] = 'txt';
		$config['max_size'] = '5000';
		$this->load->library('upload');
		$this->upload->initialize($config);

		if ( ! $this->upload->do_upload('monederos'))
		{
			$this->session->set_flashdata('errors','<p class="alert alert-danger">'.$this->upload->display_errors('','').'</p>');
			redirect(base_url().'Wallet/Users','refresh');
		}
		else
		{
			$info=$this->upload->data();
			$finalMessage='';


			//carga de datos en la $tabla

			$info=$this->upload->data();
			$file = fopen($info['full_path'],"r");
			$i=0;

			$linea=array();


			if ($file) {
				while (($bufer = fgets($file, 4096)) !== false) {
					$linea[$i]=$bufer;
					$i++;
				}
				if (!feof($file)) {
					echo "Error: fallo inesperado de fgets()\n";
				}
				fclose($file);
			}

			$cuenta=array();
			$j=0;
			$this->load->library('encryption');
			$this->load->helper('string');
			$cuentasExistente='';
			$cuentasNocreadas=0;
			for ($k=1;$k<(count($linea)-1);$k++){

				$cedula=trim(substr($linea[$k],1,18));
				$telefono=trim(substr($linea[$k],324,11),'');

				if($this->esCuentaNueva($cedula,$telefono)){
					$dia=substr($linea[$k],316,2);
					$mes=substr($linea[$k],318,2);
					$anio=substr($linea[$k],320,4);
					$fecha=new DateTime($anio.'-'.$mes.'-'.$dia);

					$nacimiento = $fecha->format('Y-m-d');
					$sexo='Masculino';

					if(trim(substr($linea[$k],314,1),'') =='F' | trim(substr($linea[$k],314,1),'') =='f'){

						$sexo = 'Femenino';
					}

					$nombrearray=explode(' ',trim(substr($linea[$k],24,59)));


					$apellidoarray=explode(' ',trim(substr($linea[$k],84,30)));

					$direccionarray=explode(' ',trim(substr($linea[$k],204,110)));


					$nombre='';
					$apellido='';
					$direccion='';
					foreach ($nombrearray as $value){
						if(strlen($value)>0){
							$nombre.=$value.' ';
						}
					}
					foreach ($apellidoarray as $value){
						if(strlen($value)>0){
							$apellido.=$value.' ';
						}
					}

					foreach ($direccionarray as $value){
						if(strlen($value)>0){
							$direccion.=$value.' ';
						}
					}


					$cuenta[$j]['sexo']=$sexo;
					$cuenta[$j]['correo']=str_replace(' ','',trim(substr($linea[$k],335,40),''));
					$cuenta[$j]['email']=str_replace(' ','',trim(substr($linea[$k],324,10),''));
					$cuenta[$j]['name']=$nombre;
					$cuenta[$j]['lastname']=$apellido;
					$cuenta[$j]['phone']=str_replace(' ','',trim(substr($linea[$k],324,10),''));
					$cuenta[$j]['status']='A';
					$cuenta[$j]['grupoTrxId']=8;
					$cuenta[$j]['cedula']=str_replace('-','',trim(substr($linea[$k],1,18),' '));
					$cuenta[$j]['created_at']=date('Y-m-d H:i:s');
					$cuenta[$j]['fnacimiento']=$nacimiento;
					$cuenta[$j]['paisId']='PAN';
					$cuenta[$j]['address']=$direccion;
					$cuenta[$j]['rolId']='3';
					$cuenta[$j]['subsidioId']='2';
					$cuenta[$j]['limiteId']='1';

					$p=0;
					while($p==0) {
						//$key=bin2hex($this->encryption->create_key(8));
						$key='6'.random_string('numeric',10);
						if(substr($key,0,1)!= '0' && $this->existeCuenta($key)){
							$p=1;
						}
					}
					$cuenta[$j]['cuentaMonedero']=$key;

					//LA PARTE DE ENVIAR TOKEN
					//$token=getToken();
					//$this->SendToken($cuenta[$j]['phone'],$cuenta[$j]['name'].' '.$cuenta[$j]['lastname'],$token,$cuenta[$j]['cedula']);

					$j++;
				}else{
					$cuentasExistente.=' | '.$cedula.' (Ya existe)';
					$cuentasNocreadas++;
				}

			}

			if($j>0){
				$this->db->insert_batch('esb.usuariosMonedero',$cuenta);

			}

			//var_dump($cuenta);

			unlink($info['full_path']);

			if($j>0){
				$finalMessage.='<br><p class="alert alert-warning">Archivo Cargado... '.
					'<strong>'.$info['file_name'].'</strong> <br>Total de cuentas nuevas creadas : '.$j.'<br>
					Total de Cuentas <strong>NO</strong> Creadas ('.$cuentasNocreadas.') '.$cuentasExistente.' </p>';
			}else{
				$finalMessage.='<br><p class="alert alert-danger">Archivo Cargado... <strong>'.$info['file_name'].'</strong><br>Sin embargo NO se creó Ninguna Cuenta.<br>'.'
					Total de Cuentas <strong>No </strong>creadas : ('.$cuentasNocreadas.')'.$cuentasExistente.'</p>';
			}

			$this->session->set_flashdata('errors',$finalMessage);
			redirect(base_url().'Wallet/Users','refresh');
		}

	}

	private function generateToken(){
		$this->load->helper('string');
		for($i=0;$i<=9;$i++){
			$f=0;
			while($f==0){
				$token=random_string('numeric','6');
				if($this->noExisteToken($token) && substr($token,0,1) !='0'){
					$data['token']=$token;
					$f=1;
					$this->db->insert('esb.tokens',$data);
				}
			}
		}

		//echo "Se Crearon ".$i." Tokens";
		return TRUE;
	}

	private function noExisteToken($token){
		$this->db->select('id');
		$this->db->where('token',$token);
		$query=$this->db->get('esb.tokens');
		if($query->num_rows()>0){
			return FALSE;
		}
		return TRUE;
	}

	public function SetTokenByPhone(){
		ini_set('max_execution_time',300);
		$this->db->select('phone,name, lastname,cedula');
		$this->db->where('tokenEnviado','0');
		$query=$this->db->get('esb.usuariosMonedero');
		foreach($query->result() as $usuario){
			$phone=$usuario->phone;
			$nombre=$usuario->name.' '.$usuario->lastname;
			$cedula=$usuario->cedula;
			$token=$this->getToken();
			$this->SendToken($phone,$nombre,$token,$cedula);
		}
		echo '<p class="alert alert-success">Se enviaron todos los token a monederos nuevos</p>';
	}

	public function set_all_token_used(){

		$data['used']=1;
		$this->db->where('used',0);
		$this->db->update('esb.tokens',$data);

	}

	private function getToken(){
		$token='';
		$this->db->select('token');
		$this->db->where('used',0);
		$this->db->limit(1);
		$query=$this->db->get('esb.tokens');
		if($query->num_rows()>0){
			foreach ($query->result() as $value) {
				$token = $value->token;
				$used['used']=1;
				$this->db->where('token',$token);
				$this->db->update('esb.tokens',$used);
			}
		}else{
			$this->generateToken();
			self::getToken();
		}
		return $token;
	}

	private function SendToken($phone,$name,$token,$cedula){

		$this->load->library('Sms');
		$numberSended=$token;
		$templateMsn='restablecerPass';


		$sms= new Sms();


		$sms->phone=$phone;
		$sms->personName=$name;
		$sms->templateMsn=$templateMsn;
		$sms->token=$numberSended;
		$response=$sms->send();

		if($response->error==1){
			$dataToinsert['error']=$response->error;
			$dataToinsert['content']=$response->content;
		}
		if($response->error==0){
			$dataToinsert['id']=$response->id;
			$dataToinsert['error']=$response->error;
			$dataToinsert['status_id']=$response->status_id;
			$dataToinsert['last_change']=$response->last_change;
			$dataToinsert['content']=$response->content;
		}
		$dataToinsert['tokenSended']=$this->encrypt->encode($numberSended);
		$this->db->insert('esb.responseApiSmsRest',$dataToinsert);
		$data['registerToken']=md5($numberSended);
		//$data['registerToken']=$numberSended;
		$data['tokenEnviado']=1;
		$this->db->where('phone',$phone);
		$this->db->where('cedula',$cedula);
		$this->db->update('esb.usuariosMonedero',$data);

		return TRUE;
	}

	public function MassiveSendWelcome(){
		ini_set('max_execution_time',300);
		$this->db->select('phone,name, lastname,cedula');
		$this->db->where('tokenEnviado','0');
		$query=$this->db->get('esb.usuariosMonedero');
		foreach($query->result() as $usuario){
			$phone=$usuario->phone;
			$nombre=$usuario->name.' '.$usuario->lastname;
			$cedula=$usuario->cedula;
			//$token=$this->getToken();
			$this->SmsBienvenida($phone,$nombre,$cedula);
		}
		echo '<p class="alert alert-success">Bienvenida enviada a monederos nuevos</p>';
	}

	private function SmsBienvenida($phone,$name,$cedula){




		$this->load->library('Sms');
		//$numberSended=$token;
		$templateMsn='Bienvenida';


		$sms= new Sms();


		$sms->phone=$phone;
		$sms->personName=$name;
		$sms->templateMsn=$templateMsn;
		//$sms->token=$numberSended;
		$sms->token='https://play.google.com/store/apps/details?id=com.monedero.bnp';
		$response=$sms->send();

		if($response->error==1){
			$dataToinsert['error']=$response->error;
			$dataToinsert['content']=$response->content;
		}
		if($response->error==0){
			$dataToinsert['id']=$response->id;
			$dataToinsert['error']=$response->error;
			$dataToinsert['status_id']=$response->status_id;
			$dataToinsert['last_change']=$response->last_change;
			$dataToinsert['content']=$response->content;
		}
		//$dataToinsert['tokenSended']=$this->encrypt->encode($numberSended);
		//$this->db->insert('esb.responseApiSmsRest',$dataToinsert);
		//$data['registerToken']=md5($numberSended);
		//$data['registerToken']=$numberSended;
		$data['tokenEnviado']=1;
		$this->db->where('phone',$phone);
		$this->db->where('cedula',$cedula);
		$this->db->update('esb.usuariosMonedero',$data);

		 return TRUE;
	}

	public function TestGraph(){
		$this->load->model('DashboardModel');


		$test=new DashboardModel();


		$test->_tablename='esb.transactionMonederoLog';
		$test->_order_by='id';
		$test->_order='DESC';
		$test->_limit=10;

		echo $test->setGraphic();
	}

	public function UpdateGraphic(){
		if($this->input->post('year')){
			$year=$this->input->post('year');
		}else{
			$year=2018;
		}

		if($this->input->post('pais')){
			$pais=$this->input->post('pais');
		}else{
			$pais='PAN';
		}





		$this->load->model('DashboardModel');
		$dashBoard=new DashboardModel();
		$dashBoard->_tablename='esb.transactionMonederoLog';
		$dashBoard->_order='DESC';
		$dashBoard->_order_by='id';
		$dashBoard->_limit=0;
		$dashBoard->_timestamps='stamp';

		$dashBoard->_fechaInicio=$year.'-01-01 00:00:00';
		$dashBoard->_fechaFin=$year.'-12-31 23:59:00';

		$dashBoard->_especificField='paisId';
		$dashBoard->_especificValue=$pais;



		$allData=json_decode($dashBoard->getData());

		$meses=array();
		for ($i=1;$i<=12;$i++){
			$dia='';
			if($i<10){
				$dia='0';
			}
			array_push($meses,$year.'-'.$dia.$i);
		}

		$transacciones=array();
		$debitos=array();
		$creditos=array();

		$dataGraphic=array();

		//se obtienen los array de Transacciones por mes
		for($mes=0; $mes<count($meses); $mes++){
			$count=0;
			foreach ($allData as $value){
				if(substr($value->stamp,0,7)==$meses[$mes]){
					$count++;
				}
			}
			array_push($transacciones,$count);

			$suma=0;
			foreach ($allData as $value){
				if(substr($value->stamp,0,7)==$meses[$mes] && $value->aux=='+'){
					$suma+=$value->montoTrx;
				}
			}
			array_push($creditos,number_format($suma,'2','.',''));

			$suma=0;
			foreach ($allData as $value){
				if(substr($value->stamp,0,7)==$meses[$mes] && $value->aux=='-'){
					$suma+=$value->montoTrx;
				}
			}
			array_push($debitos,number_format($suma,'2','.',''));
		}

		//echo var_dump($meses).'<br>';
		//echo var_dump($transacciones).'<br>';
		//echo var_dump($creditos).'<br>';
		//echo var_dump($debitos).'<br>';


		for ($k=0;$k<12;$k++){
			array_push($dataGraphic,array('mes'=>$meses[$k],'transacciones'=>$transacciones[$k],'credito'=>$creditos[$k],'debito'=>$debitos[$k]));
		}


		//var_dump($dataGraphic);

		echo json_encode($dataGraphic);



	}

	public function MAPA(){

		$this->load->helper('date');

		if($this->input->post('year')){
			$year=$this->input->post('year');
		}else{
			$year=2018;
		}

		if($this->input->post('pais')){
			$pais=$this->input->post('pais');
		}else{
			$pais='PAN';
		}

		if($this->input->post('mes')){
			$mes=$this->input->post('mes');
		}else{
			$mes=date("m");
		}

		$lastDay=days_in_month($mes,$year);


		$this->load->model('DashboardModel');
		$dashBoard=new DashboardModel();
		$dashBoard->_tablename='esb.transactionMonederoLog';
		$dashBoard->_order='DESC';
		$dashBoard->_order_by='id';
		$dashBoard->_limit=0;
		$dashBoard->_timestamps='stamp';

		$dashBoard->_fechaInicio=$year.'-'.$mes.'-01 00:00:00';
		$dashBoard->_fechaFin=$year.'-'.$mes.'-'.$lastDay.' 23:59:00';

		$dashBoard->_especificField='paisId';
		$dashBoard->_especificValue=$pais;

		$allData=json_decode($dashBoard->getData());
		$points=array();
		$i=0;
		foreach ($allData as $value){
			$points[$i]['location']=array('lat'=>floatval($value->lat),'lng'=>floatval($value->lon));
			$points[$i]['description']='<p class="text-info">Fecha: '.$value->stamp.'</p>';

			if($value->nombre != ''){
				$points[$i]['description'].='<p class="text-info">Usuario: '.$value->userMonedero.' ('.$value->nombre.')</p>';
			}else{
				$points[$i]['description'].='<p class="text-info">Usuario: '.$value->userMonedero.'</p>';
			}

			$points[$i]['description'].='<p class="text-info">Descripción: '.$value->descripcionTrx.'</p>';
			$points[$i]['description'].='<p class="text-info">Monto: '.$value->currency.' '.$value->montoTrx.'</p>';
			$points[$i]['description'].='<p class="text-info">Forma de Pago: '.$value->formaPago.'</p>';

			$points[$i]['title']=$value->descripcionTrx;


			if($value->aux=='+'){
				$color='1fa4e4';
			}else{
				$color='eeb029';
			}
			$points[$i]['color']=$color;

			$i++;
		}

		echo json_encode($points);

	}

	public function ArreglarNombreMonedero(){
		$this->db->select('usuariosMonedero.address,usuariosMonedero.id');
		$query=$this->db->get('esb.usuariosMonedero');
		$datos=array();
		foreach ($query->result() as $value){
			$datos['id']=$value->id;
			$datos['address']=$value->address;
			$nombre=array('address'=>'');
			$array_explode=explode(' ',$datos['address']);
			foreach ($array_explode as $value){
				if(strlen($value)>0){
					$nombre['address'].=$value.' ';
				}
			}
			$this->db->where('id',$datos['id']);
			$this->db->update('esb.usuariosMonedero',$nombre);
		}
	}

	public function delete(){
		$id=$this->input->post('id');
		$cuentaMonedero='';
		$saldo=array();

		$this->db->select('usuariosMonedero.cuentaMonedero,usuariosMonedero.phone,usuariosMonedero.name');
		$this->db->where('id',$id);
		$usuario=$this->db->get('esb.usuariosMonedero')->row_array();
		if($usuario){
			$cuentaMonedero=$usuario['cuentaMonedero'];
			$phoneToSendMessage=$usuario['phone'];
			$nameToSendMessage=$usuario['phone'];
		}
		if(strlen($cuentaMonedero)>0){
			$this->db->select('consultaSaldoMonedero.Saldo');
			$this->db->where('cuentaMonedero',$cuentaMonedero);
			$saldo=$this->db->get('esb.consultaSaldoMonedero')->row_array();
		}
		if(floatval($saldo['Saldo']) > 0 ){
			echo '<p class="alert-danger alert">No es posible eliminar debido a que mantiene un  saldo superior a  B/. 0.00</p>';
		}else{

			$this->load->library('Sms');
			//$numberSended=$token;
			$templateMsn='deleteUser';
			$sms= new Sms();
			$sms->phone=$phoneToSendMessage;
			$sms->personName=$nameToSendMessage;
			$sms->templateMsn=$templateMsn;
			//$sms->token=$numberSended;
			$sms->token='.';
			$response=$sms->send();
			$this->modelo->delete($id);
			echo '<p class="alert-success alert">Se ha eliminado el usuario de Billetera con cuenta : '.$cuentaMonedero.'</p>';
		}

	}


	public function ComisionMonedero(){
		$this->dataTable[0]=array('field'=>'name','title'=>'name','sortable'=>'true','visible'=>'true','align'=>'left');
		$this->dataTable[1]=array('field'=>'lastname','title'=>'lastname','sortable'=>'true','visible'=>'true','align'=>'left');
		$this->dataTable[2]=array('field'=>'cedula','title'=>'cedula','sortable'=>'true','visible'=>'true','align'=>'left');
		$this->dataTable[3]=array('field'=>'phone','title'=>'phone','sortable'=>'true','visible'=>'true','align'=>'left');
		$this->dataTable[4]=array('field'=>'cuenta','title'=>'account','sortable'=>'true','visible'=>'true','align'=>'left');
		$this->dataTable[4]=array('field'=>'comision','title'=>'commissionWallet','sortable'=>'true','visible'=>'true','align'=>'left');

		$data['titleMenu']=$this->lang->line('CNB');
		$data['navbar_menu']=$this->navbar_header_menu();
		$data['controllerServer']='MonederoPaper';
		$data['columns']=$this->getColumnsForTable();
		$data['rutaForm']=$this->rutaForm;
		$data['sizeModal']='modal-lg';
		$data['populate']='populateComisiones';
		$data['title']='Reporte de Comisiones';
		$data['rutaToExport']=base_url().$this->router->fetch_class().'/exportcomissionMonedero';
		$data['localeForTable']=$this->localeForTable;
		$this->load->view('paper/monedero/_layoutReportComisiones',$data);
	}


	public function populateComisiones(){
		$ffinicio='';
		$ffin='';

		if($this->input->post('inicio')){
			$ffinicio=$this->input->post('inicio').' 00:00:00';
			$ffin=$this->input->post('fin').' 23:59:00';
		}

		$search='';

		if($this->input->post('search')){
			$search=$this->input->post('search');
		}

		$data = json_decode(file_get_contents('php://input'), true);


		if(isset($data['search'])){
			$search=utf8_decode($data['search']);
		}

		if(isset($data['inicio'])){
			$ffinicio=$data['inicio'].' 00:00:00';
		}

		if(isset($data['fin'])){
			$ffin=$data['fin'].' 23:59:00';
		}


		$CamposBusqueda=array(
			'usuariosMonedero.id',
			'usuariosMonedero.name',
			'usuariosMonedero.lastname',
			'usuariosMonedero.cedula',
			'usuariosMonedero.phone',
		);

		$usuarioComision=array();
		$rows=array();
		$this->db->select('usuariosMonedero.id,usuariosMonedero.name,
		usuariosMonedero.lastname,usuariosMonedero.cuentaMonedero,usuariosMonedero.phone,usuariosMonedero.cedula');
		if(count($CamposBusqueda) > 0 && $search !=''){
			$i=0;
			$this->db->like($CamposBusqueda[$i],$search);
			for($i;$i<count($CamposBusqueda);$i++){
				$this->db->or_like($CamposBusqueda[$i],$search);
			}

		}
		$usuarios=$this->db->get('esb.usuariosMonedero');
		if($usuarios->num_rows()>0){
			foreach ($usuarios->result() as $usuario){
				$this->db->select('transactionMonederoLog.montoComision');
				$this->db->where('tipoTrx',105);
				$this->db->where('cuenta',$usuario->cuentaMonedero);
				if(strlen($ffinicio)>0){
					$this->db->where('stamp >',$ffinicio);
					$this->db->where('stamp <',$ffin);
				}

				$transaccion=$this->db->get('esb.transactionMonederoLog');
				$sumaComision=0;
				if($transaccion->num_rows()>0){
					foreach ($transaccion->result() as $trx){
						$sumaComision+=$trx->montoComision;
					}
				}
				array_push($rows,array(
					'name'=>$usuario->name,
					'lastname'=>$usuario->lastname,
					'phone'=>$usuario->phone,
					'cedula'=>$usuario->cedula,
					'cuenta'=>$usuario->cuentaMonedero,
					'comision'=>number_format($sumaComision,2,'.',' ')
				));


			}
		}
		$total=$usuarios->num_rows();
		$usuarioComision['rows']=$rows;
		$usuarioComision['total']=$total;

		echo  json_encode($usuarioComision);

	}


	public function exportcomissionMonedero(){
		$ffinicio='';
		$ffin='';
		if($this->input->post('inicio')){
			$ffinicio=$this->input->post('inicio').' 00:00:00';
			$ffin=$this->input->post('fin').' 23:59:00';
		}

		$search='';

		if($this->input->post('search')){
			$search=$this->input->post('search');
		}

		$data = json_decode(file_get_contents('php://input'), true);


		if(isset($data['search'])){
			$search=utf8_decode($data['search']);
		}

		if(isset($data['inicio'])){
			$ffinicio=$data['inicio'].' 00:00:00';
		}

		if(isset($data['fin'])){
			$ffin=$data['fin'].' 23:59:00';
		}


		$CamposBusqueda=array(
			'usuariosMonedero.id',
			'usuariosMonedero.name',
			'usuariosMonedero.lastname',
			'usuariosMonedero.cedula',
			'usuariosMonedero.phone',
		);

		$usuarioComision=array();
		$rows=array();
		$this->db->select('usuariosMonedero.id,usuariosMonedero.name,
		usuariosMonedero.lastname,usuariosMonedero.cuentaMonedero,usuariosMonedero.phone,usuariosMonedero.cedula');
		if(count($CamposBusqueda) > 0 && $search !=''){
			$i=0;
			$this->db->like($CamposBusqueda[$i],$search);
			for($i;$i<count($CamposBusqueda);$i++){
				$this->db->or_like($CamposBusqueda[$i],$search);
			}

		}
		$usuarios=$this->db->get('esb.usuariosMonedero');
		if($usuarios->num_rows()>0){
			foreach ($usuarios->result() as $usuario){
				$this->db->select('transactionMonederoLog.montoComision');
				$this->db->where('tipoTrx',105);
				$this->db->where('cuenta',$usuario->cuentaMonedero);
				if(strlen($ffinicio)>0){
					$this->db->where('stamp >',$ffinicio);
					$this->db->where('stamp <',$ffin);
				}

				$transaccion=$this->db->get('esb.transactionMonederoLog');
				$sumaComision=0;
				if($transaccion->num_rows()>0){
					foreach ($transaccion->result() as $trx){
						$sumaComision+=$trx->montoComision;
					}
				}
				array_push($usuarioComision,array(
					'name'=>$usuario->name,
					'lastname'=>$usuario->lastname,
					'phone'=>$usuario->phone,
					'cedula'=>$usuario->cedula,
					'cuenta'=>$usuario->cuentaMonedero,
					'comision'=>number_format($sumaComision,2,'.',' ')
				));


			}
		}

		$letter=array(
			'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q'
		);
		$rows=array('NOMBRE','APELLIDO','TELEFONO','CEDULA','No. CUENTA','COMISION');

		$this->load->library('excel');
		$this->excel->setActiveSheetIndex(0);
		$this->excel->getActiveSheet()->setTitle('COMISIONES POR USUARIOS');

		$countletter=0;
		foreach ($rows as $key ){
			$this->excel->getActiveSheet()->setCellValue($letter[$countletter].'1',$key);
			$countletter++;
		}
		$fila=2;
		for($i=0;$i<count($usuarioComision);$i++){
			$countletter=0;
			foreach ($usuarioComision[$i] as  $value){
				$this->excel->getActiveSheet()->setCellValue($letter[$countletter].$fila,$value);
				$countletter++;
			}
			$fila++;
		}

		//Le ponemos un nombre al archivo que se va a generar.
		$archivo = time().'.xls';
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$archivo.'"');
		header('Cache-Control: max-age=0');
		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
		//Hacemos una salida al navegador con el archivo Excel.
		$objWriter->save('php://output');


	}

	public function save(){
		$id=NULL;
		$dataToInsert=array();

		if($this->input->post($this->modelo->_primary_key) != ''){
			$id=$this->input->post($this->modelo->_primary_key);
			foreach ($this->modelo->_inputs as $key => $value){
				$key=$key;
				$dataToInsert[$key]=$this->input->post($key);

			}
			unset($dataToInsert[$this->modelo->_primary_key]);
			$reglas=$this->modelo->_rules;
			foreach ($reglas as $key => $value) {
				if(strpos($value['rules'],'unique')){
					$reglas[$key]['rules']='trim|required|max_length[80]';
				}
				if($value['field']=='password'){
					$reglas[$key]['rules']='trim|max_length[80]';
				}
				if($value['field']=='email'){
					$reglas[$key]['rules']='trim|max_length[80]|callback_isEmail';
				}
			}
			$this->form_validation->set_rules($reglas);
			$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
			//var_dump($dataToInsert);
			if($this->form_validation->run()==TRUE){
				if(array_key_exists('password',$dataToInsert)){
					if($dataToInsert['password']!=''){
						$dataToInsert['password']=$this->encrypt->encode($dataToInsert['password']);
					}else{
						unset($dataToInsert['password']);
					}
					//$dataToInsert['password']=='prueba que si existe';
				}

				$dataToInsert['phone']=$this->input->post('email');
				$this->modelo->save($dataToInsert,$id);
				echo 'ok';
			}else{
				echo $this->modelo->form($this->formSize,$id);
			}
		}else{
			foreach ($this->modelo->_inputs as $key => $value) {
				$dataToInsert[$key]=$this->input->post($key);
				if($dataToInsert[$key]==''){
					unset($dataToInsert[$key]);
				}
			}
			unset($dataToInsert[$this->modelo->_primary_key]);
			$this->form_validation->set_rules($this->modelo->_rules);
			$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
			//var_dump($dataToInsert);
			if($this->form_validation->run()==TRUE){

				if(array_key_exists('password',$dataToInsert)){
					$dataToInsert['password']=$this->encrypt->encode($dataToInsert['password']);
					//$dataToInsert['password']=='prueba que si existe';
				}
				$dataToInsert['phone']=$this->input->post('email');
				$this->modelo->save($dataToInsert);
				echo 'ok';
			}else{
				echo $this->modelo->form($this->formSize);
			}
		}
	}




}

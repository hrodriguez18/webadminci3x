<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LimitesMonedero extends MY_Controller {

	public $permisos=array();
	public $mis_permisos=array();

	public $permiso_crear=150;
	public $permiso_actualizar=142;
	public $permiso_eliminar=153;
	public $permiso_ver=149;
	//
	//parametros para la tabla, $controllerServer= controlador donde apuntaran las funciones de la tabla
	//$datatable=arreglo de datos para identificar las columnas(ver formato en controlador)
	/*
      $this->dataTable[0]=array('some_option'=>'some_value');
    */
	//dato para locale table
	public $localeForTable='es-MX';
	//variable para exportar tabla;
	public $rutaToExport='';
	//el value puede o no estar en el diccionario de idiomas
	public $mymodel='LimitesMonedero_m';
	public $controlerServer='LimitesMonedero';
	public $dataTable=array();


	//====================================================================

	//Ruta del formulario para insertar o editar uno nuevo.
	public $rutarForm='';

	//Tamaño del FORMULARIO
	public $formSize='lg';

	public function __construct()
	{
		parent::__construct();

		//Do your magic here
	}

	public function index()
	{
		$this->dataTable[0]=array('field'=>'id','title'=>'ID','sortable'=>'true','visible'=>'false','align'=>'left');
		$this->dataTable[1]=array('field'=>'descripcion','title'=>'description','sortable'=>'true','visible'=>'true','align'=>'left');
		$this->dataTable[2]=array('field'=>'limiteRetiroMes','title'=>'limiteRetiroMes','sortable'=>'true','visible'=>'true','align'=>'left');
		$this->dataTable[3]=array('field'=>'limiteDepositoMes','title'=>'limiteDepositoMes','sortable'=>'true','visible'=>'true','align'=>'left');

		$this->dataTable[4]=array('field'=>'actions','title'=>'actions','class'=>'td-actions text-left','align'=>'left','events'=>'operateEvents','formatter'=>'operateFormatter');
		$data['titleMenu']=$this->lang->line('titleAdmin');
		$data['navbar_menu']=$this->navbar_header_menu();
		$data['controllerServer']=$this->controlerServer;
		$data['columns']=$this->getColumnsForTable();
		$data['rutaForm']=$this->rutaForm;
		$data['title_form']='Límites Monedero';
		$data['sizeModal']='modal-lg';
		$data['documentation']='';
		$data['title']='Límites Monedero';
		$data['rutaToExport']='';
		$data['localeForTable']=$this->localeForTable;
		$this->load->view('paper/admin/_layoutEmail',$data);
	}

}

/* End of file Controllername.php */



?>

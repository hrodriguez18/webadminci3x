<?php if ( ! defined('BASEPATH')) exit('No direct scriptccessllowed');

class AdminCalendarios extends MY_Controller{

	public $contenido=array();
	public function __construct(){
        parent::__construct();
        $this->load->model('AdminCalendarios_m');
        $this->load->model('AdminCalendarios_modelo');

    }

    public function index(){

    	$this->contenido['table']=$this->load->view("paper/tables/table_AdminCal",'',true);
        $this->contenido['title']='Grupo Dias';
        $datos["content_view"]=$this->load->view("paper/admin/paper_view_AdminCal",$this->contenido,true);
        $datos['navbar_menu']=$this->navbar_header_menu();
        $datos['sidebar_menu']=$this->menu_admin();
        $datos['footer']=$this->load->view("paper/footers/footer_AdminCalendarios",'',true);

        $this->load->view("paper/base",$datos);
    }

    public function SaveGrupo(){

        $this->form_validation->set_rules($this->AdminCalendarios_m->rulesGrupoDias);
        $this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
        if($this->form_validation->run()==TRUE){

            $data=array();
            $data['description']=$this->input->post('description');
            $data['status']=$this->input->post('status');
            $dia=$this->input->post('dia');

            $id=$this->AdminCalendarios_m->SaveGrupo($data);

            for($i=0; $i<count($dia); $i++){
                $grupo=$id;
                $valueDay=$dia[$i];
                $d=$this->AdminCalendarios_m->idFromDay($valueDay);
                $this->AdminCalendarios_m->PopulateTableRelGrupoDias($grupo,$d);
            }

            echo "Todo bien!";
        }else{

            echo $this->AdminCalendarios_m->FormNewGrupoDias();
        }

    }

    public function PopulateTableGrupoDias(){
        echo json_encode($this->AdminCalendarios_m->PopulateTableGrupoDias());
    }

    public function UpdateTableGrupo(){


    	echo $this->AdminCalendarios_m->TableGrupoDias();
    }

    public function FormNewHorario(){
        echo  $this->AdminCalendarios_m->FormNewHorario();
    }

    public function FormNewGrupoDias(){
        echo $this->AdminCalendarios_m->FormNewGrupoDias();
    }

    public function SaveDataHorario(){
        $data=array();
        $table='esb.horarios';

        foreach($this->input->post() as $key => $value) {
            $data[$key]=$value;
        }
        $this->AdminCalendarios_m->InsertInfo($data,$table);

        echo  'Información Ingresada Corectamente';
        //var_dump($data);
    }


    public function UpdateTableHorario(){
        echo $this->AdminCalendarios_m->TableHorarios();
    }

    public function NewGrupoForm(){
        echo $this->AdminCalendarios_modelo->form();
    }


    public function DeleteDataGrupoDias(){
        if($this->input->post('Id')){
					$id=$this->input->post('Id');
            $this->AdminCalendarios_m->LimpiarRelGrupoDias($this->input->post('Id'));
            $this->AdminCalendarios_m->Delete($this->input->post('Id'));
						echo '<p class="label label-success">'.$this->lang->line('error_delete').'<strong>'.$id.'</strong></p>';

        }

    }

    public function EditarGrupoDias(){
        $id=$this->input->post('id');

        $grupo=$this->AdminCalendarios_m->get($id,TRUE);
        $dias=$this->AdminCalendarios_m->GetDiasDisponibles($id);
        $checks=array();
        foreach ($dias as $value) {
            array_push($checks, $value->diasId);
        }

        $data=array();
        $data['description']=$grupo->description;
        $data['status_value']=$grupo->status;
        $data['chk']=$this->AdminCalendarios_m->ConstruirDiasCheckBoxs($checks);
        $data['id']=$id;

        echo $this->load->view("paper/forms_modal/form_adminCalendarios_NewGrupoDias_edit",$data,true);


        //var_dump($grupo);
        //var_dump($grupoCkeckBoxs);
    }

    public function UpdateDataGrupoDias(){

        $this->form_validation->set_rules($this->AdminCalendarios_m->rulesGrupoDias);
        $this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
        if($this->form_validation->run()==TRUE){

            $data=array();
            $data['description']=$this->input->post('description');
            $data['status']=$this->input->post('status');
            $id=$this->input->post('id');

            $dia=$this->input->post('dia');

            $this->AdminCalendarios_m->UpdateGrupo($data,$id);
            $this->AdminCalendarios_m->LimpiarRelGrupoDias($id);

            for($i=0; $i<count($dia); $i++){
                $grupo=$id;
                $valueDay=$dia[$i];
                $d=$this->AdminCalendarios_m->idFromDay($valueDay);
                $this->AdminCalendarios_m->PopulateTableRelGrupoDias($grupo,$d);
            }

            echo "Todo bien!";
        }else{

            echo $this->load->view("paper/forms_modal/form_adminCalendarios_NewGrupoDias",'',true);
        }

    }







}
?>

<?php
/**
 * Created by PhpStorm.
 * User: hrodriguez
 * Date: 10/02/18
 * Time: 03:27 PM
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class ActividadesComerciales extends MY_Controller {


	public $formSize='sm';

	public $mymodel='ActividadesComerciales_m';
	public $controlerServer='ActividadesComerciales';


	//dato para locale table
	public $localeForTable='es-MX';


	public function index()
	{
		$this->dataTable[0]=array('field'=>'id','title'=>'ID','sortable'=>'true','visible'=>'false','align'=>'left');
		$this->dataTable[1]=array('field'=>'description','title'=>'description','sortable'=>'true','visible'=>'true','align'=>'left');
		$this->dataTable[2]=array('field'=>'actions','title'=>'actions','class'=>'td-actions text-left','align'=>'left','events'=>'operateEvents','formatter'=>'operateFormatter');
		$data['titleMenu']=$this->lang->line('titleAdmin');
		$data['navbar_menu']=$this->navbar_header_menu();
		$data['controllerServer']=$this->controlerServer;
		$data['columns']=$this->getColumnsForTable();
		$data['rutaForm']=$this->rutaForm;
		$data['title_form']=$this->lang->line('actividadComercial');
		$data['sizeModal']='';
		$data['documentation']='';
		$data['title']=$this->lang->line('actividadComercial');
		$data['rutaToExport']='';
		$data['localeForTable']=$this->localeForTable;
		$this->load->view('paper/admin/vistaActividadComercial',$data);
	}

	public function CargarCsv(){
		$this->load->library('upload');
		$config['upload_path']='assets/uploads/files/listaNegra';
		$config['allowed_types'] = 'csv';
		$config['max_size'] = '2000';
		$this->upload->initialize($config);

		if($this->upload->do_upload('filecsv')){
			$file_info=$this->upload->data();
			$file = fopen($file_info['full_path'],"r");
			$i=0;
			$arreglo=array();
			$dataToInsert=array();
			$td=array('description');
			while(!feof($file))
			{
				$linea = fgets($file);
				$arreglo=explode(',',$linea);
				for($k=0;$k<count($td);$k++){
					if(isset($arreglo[$k])){
						$dataToInsert[$i][$td[$k]]=$arreglo[$k];
					}
				}
				$i++;
			}
			fclose($file);
			$data=array();
			for ($i=0; $i < count($dataToInsert) ; $i++){
				$str=$dataToInsert[$i]['description'];
				if($this->YaExisteActvidadComercial($str)){
					$data['description']=$dataToInsert[$i]['description'];
					if(strlen($data['description'])>0){
						$this->db->insert('esb.actividadComercial',$data);
					}

				}
			}
			unlink($file_info['full_path']);
			$this->session->set_flashdata('message', '<p class="alert alert-success">Carga de actividades comerciales correcta</p>');
		}else{
			$this->session->set_flashdata('message', '<p class="alert alert-danger">No fue posible cargar el archivo favor siga las instrucciones...</p>');

		}

		redirect('ActividadesComerciales','refresh');
	}
	private function YaExisteActvidadComercial($str){
		$this->db->select('*');
		$this->db->where('description',$str);
		$query=$this->db->get('esb.actividadComercial');
		if($query->num_rows()>0){
			return FALSE;
		}
		return TRUE;
	}

}

/* End of file Controllername.php */
?>

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AdminMontos extends MY_Controller {

	public $permiso_crear=85;
	public $permiso_actualizar=86;
	public $permiso_eliminar=87;
	public $permiso_ver=84;

	public $_vista='paper_view_adminMontos';

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		$this->load->model('MontosModel');
	}

	public function index()
	{
		$data['navbar_menu']=$this->navbar_header_menu();
		$this->load->view('paper/admin/'.$this->_vista,$data);
	}

	public function PopulateTable(){
		echo json_encode($this->MontosModel->PopulateTable());
	}

	public function Form(){
		echo $this->MontosModel->Form('sm');
	}

	public function Save(){
		$data=array();
		$inputs=$this->MontosModel->_inputs;
		unset($inputs['submit']);
		$this->form_validation->set_rules($this->MontosModel->_rules);
		$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');

		if($this->form_validation->run()==TRUE){
			foreach ($inputs as $key) {
				$data[$key['name']]=$this->input->post($key['name']);
			}
			$id=$data['id'];
			if($id!=''){
				unset($data['id']);
				$this->MontosModel->save($data,$id);
			}else{
				unset($data['id']);
				$this->MontosModel->save($data);
			}
			echo "Todo bien!";
		}else{
			echo $this->MontosModel->Form('sm');
		}
	}


	public function Edit(){
		$id=$this->input->post('id');
		echo $this->MontosModel->Form('sm',$id);
	}

	public function Delete(){
		$id=$this->input->post('id');
		if($this->MontosModel->delete($id)){
			echo '<p class="label label-success">'.$this->lang->line('error_delete').'<strong>'.$id.'</strong></p>';
		}
	}

}

/* End of file AdminMontos.php */
/* Location: ./application/controllers/AdminMontos.php */

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class AdminMerchants extends MY_Controller{

	public $permiso_crear=58;
	public $permiso_actualizar=59;
	public $permiso_eliminar=60;
	public $permiso_ver=24;

	public $contenido=array();
	protected $dataForm=array('title_form'=>'Datos Merchants');


	public function __construct(){
        parent::__construct();
        $this->load->model('merchants_m');

    }

    public function index(){

    	$this->contenido['table']=$this->load->view("paper/tables/table_merchants",'',true);
    	$this->contenido['title']=$this->lang->line('merchants');
		$datos["content_view"]=$this->load->view("paper/admin/paper_view_merchants",$this->contenido,true);
		$datos['navbar_menu']=$this->navbar_header_menu();
		$datos['sidebar_menu']=$this->menu_admin();
		$datos['footer']=$this->load->view("paper/footers/footer_merchants",'',true);

		$this->load->view("paper/base",$datos);

    }
    public function populateTable(){
    	echo json_encode($this->merchants_m->getMerchantData());
    }

    public function Form(){
    	$this->dataForm['form']=$this->merchants_m->Form();
    	echo $this->load->view("paper/forms_modal/form_merchants",$this->dataForm,true);
    }

    public function saveData(){
		$this->form_validation->set_rules($this->merchants_m->rules);
		$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
		if($this->form_validation->run()==TRUE){
			$data['merchantId']=$this->input->post('merchantId');
			$data['dbaNumber']=$this->input->post('dbaNumber');
			$data['customerNumber']=$this->input->post('custumer_number');
			$data['terminalNumber']=$this->input->post('terminalNumber');
			$data['descripcion']=$this->input->post('descripcion');
			$data['status']=$this->input->post('status');
			$data['procesadorId']=$this->input->post('processor');
			$this->merchants_m->save($data);
			echo "Todo bien!";
		}else{
			$this->dataForm['form']=$this->merchants_m->Form();

    		echo $this->load->view("paper/forms_modal/form_merchants",$this->dataForm,true);
		}
	}

	public function EditData(){
		$id=$this->input->post('Id');
		$data_by_id=$this->merchants_m->get($id,true);

		$dataForm['form']=$this->merchants_m->FormForEdit($data_by_id);
		echo $this->load->view("paper/forms_modal/form_merchants_edit",$dataForm,true);
		//var_dump($id);

	}

	public function UpdateData(){
		$this->form_validation->set_rules($this->merchants_m->rules);
		$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
		if($this->form_validation->run()==TRUE){
			$data['merchantId']=$this->input->post('merchantId');
			$data['dbaNumber']=$this->input->post('dbaNumber');
			$data['customerNumber']=$this->input->post('custumer_number');
			$data['terminalNumber']=$this->input->post('terminalNumber');
			$data['descripcion']=$this->input->post('descripcion');
			$data['status']=$this->input->post('status');
			$data['procesadorId']=$this->input->post('processor');

			$this->merchants_m->save($data,$this->input->post('Id'));
			echo 'Todo bien!';
		}else{
			$id=$this->input->post('Id');
			$data_by_id=$this->merchants_m->get($id,true);
			$dataForm['form']=$this->merchants_m->FormForEdit($data_by_id);
			echo $this->load->view("paper/forms_modal/form_merchants_edit",$dataForm,true);

		}
	}

	public function DeleteData(){
		$id=$this->input->post('id');
		$this->merchants_m->delete($id);
		echo '<p class="label label-success">'.$this->lang->line('error_delete').'<strong>'.$id.'</strong></p>';

	}
}
?>

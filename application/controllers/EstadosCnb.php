<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class EstadosCnb extends MY_Controller{
  //DATOS PARA LOS PERMISOS
  	public $permiso_ver=126;
  	public $permiso_crear=62;
  	public $permiso_actualizar=63;
  	public $permiso_eliminar=64;
  //DATOS PARA LA TABLA
  	public $mymodel='EstadosCnb_m';
  	public $controlerServer='EstadosCnb';
  	public $dataTable=array();
  //Ruta para llamar al formulario;
  	public $rutarForm='';
  //Tamaño del formulario
  	public $formSize='lg';

  	//variable para Exportar
  	public $rutaToExport='';

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
  }

  public function index(){
    $this->dataTable[0]=array('field'=>'id','title'=>'ID','sortable'=>'true','visible'=>'false','align'=>'left');
		$this->dataTable[1]=array('field'=>'name','title'=>'name','sortable'=>'true','visible'=>'true','align'=>'left');
    $this->dataTable[2]=array('field'=>'lastname','title'=>'lastname','sortable'=>'true','visible'=>'true','align'=>'left');
		$this->dataTable[3]=array('field'=>'phone','title'=>'phone','sortable'=>'true','visible'=>'true','align'=>'left');
    $this->dataTable[4]=array('field'=>'email','title'=>'email','sortable'=>'true','visible'=>'true','align'=>'left');
		$this->dataTable[5]=array('field'=>'cedula','title'=>'cedula','sortable'=>'true','visible'=>'false','align'=>'left');
		$this->dataTable[6]=array('field'=>'saldo','title'=>'saldo','sortable'=>'false','visible'=>'true','align'=>'left');
    $this->dataTable[7]=array('field'=>'Descargar','title'=>'btn_down','sortable'=>'false','visible'=>'true','align'=>'left');


    $data['titleMenu']=$this->lang->line('CNB');
		$data['navbar_menu']=$this->navbar_header_menu();
	  $data['controllerServer']=$this->controlerServer;
		$data['columns']=$this->getColumnsForTable();
		$data['rutaForm']=$this->rutaForm;
		$data['title_form']=$this->lang->line('CNB');
		$data['sizeModal']='modal-lg';
    $data['title']=$this->lang->line('accountstatements');
		$data['rutaToExport']=base_url().$this->router->fetch_class().'/exportUser';
		$data['localeForTable']=$this->localeForTable;
		$this->load->view('paper/monedero/_layoutEstados',$data);
  }

  public function Saldo(){
    $cuenta=$this->input->post('cuenta');
    //var_dump($this->modelo->EstadoDeCuenta($cuenta));
  }

  public function DescargarSaldo(){
    $cuenta=$this->input->post('cuenta');
    $this->modelo->ExportToPdf($cuenta);
  }



}

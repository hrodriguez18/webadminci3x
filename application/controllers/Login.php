<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller{
	public function __construct(){
        parent::__construct();
				$i=$this->session->userdata('intents');

				if($i==3){
					$this->session->set_userdata('bad_ip',$this->input->ip_address());
					redirect('https://google.com','refresh');
				}
				$ip=$this->session->userdata('bad_ip');
				if($ip){
					redirect('https://google.com','refresh');
				}
        if($this->session->userdata('is_logged')){
        	redirect('Admin','refresh');
        }
        $this->load->model('Login_m');
    }

    public function index(){

    	$this->load->view($this->config->item("theme").'/Login');

    }

    public function Log_in(){

			if(!$this->session->userdata('intents')){
				$this->session->set_userdata('intents',0);
				$i=$this->session->userdata('intents');
			}else{
				$i=$this->session->userdata('intents');
			}

    $this->form_validation->set_rules($this->Login_m->rules);
		$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
		if($this->form_validation->run()==TRUE){

			foreach ($this->input->post() as $key => $value) {
				$data[$key]=$value;
			}
			if($this->Login_m->Login($data)){
				$this->session->unset_userdata('intents');
				$this->session->unset_userdata('bad_ip');
				redirect('Admin','refresh');
			}else{
				$i++;
				$this->session->set_userdata('intents',$i);
				$this->session->set_flashdata('_message','<blockquote><p class="text-warning">Credenciales Inválidas</p></blockquote>');
        redirect('Login','refresh');
			}
		}else{
			$this->load->view($this->config->item("theme").'/Login');
		}
    }




}

?>

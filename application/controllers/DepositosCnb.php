<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DepositosCnb extends MY_Controller{

  public $permiso_ver='167';

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
  }

  function index()
  {

    $this->dataTable[0]=array('field'=>'id','title'=>'ID','sortable'=>'false','visible'=>'false','align'=>'left');
	$this->dataTable[1]=array('field'=>'name','title'=>'name','sortable'=>'false','visible'=>'true','align'=>'left');
	$this->dataTable[2]=array('field'=>'cuentaAgencia','title'=>'account','sortable'=>'false','visible'=>'true','align'=>'left');
	$this->dataTable[3]=array('field'=>'monto','title'=>'amount','sortable'=>'false','visible'=>'true','align'=>'left');
	$this->dataTable[4]=array('field'=>'description','title'=>'description','sortable'=>'false','visible'=>'true','align'=>'left');
    $data['titleMenu']=$this->lang->line('depositoscnb');
    $data['navbar_menu']=$this->navbar_header_menu();
    $data['controllerServer']='DepositosCnb';
    $data['columns']=$this->getColumnsForTable();
    $data['rutaForm']=$this->rutaForm;
    $data['sizeModal']='modal-lg';
    $data['populate']='populate';
    $data['title']=$this->lang->line('depositoscnb');
    $data['localeForTable']=$this->localeForTable;
    $this->load->view('paper/admin/_layoutDepositosCnb',$data);
  }

  public function populate(){
    $this->load->model('populateModel');
    $tabla=new populateModel();
    $tabla->_tablename='esb.DepositosCnb';
    $tabla->_primary_key='id';
    $tabla->_primary_filter='intval';
    $tabla->_order_by='id';
    $tabla->_order='DESC';
    $tabla->fecha_inicio='';
    $tabla->fecha_fin='';
    $tabla->_timestamps='';
    $tabla->_limit=0;
    $tabla->_search='';
    $tabla->_offset=0;
    $tabla->_selection=array(
                              'id',
                              'name',
                              'cuentaAgencia',
                              'monto',
                              'description'
							);
    $tabla->_valueSearch='';
    $tabla->_searchingValue='';
    echo $tabla->populate();
  }

  public function SaveFile(){
    $name=$_FILES['FileDeposito']['name'];
    $config['upload_path'] = 'assets/uploads/files/depositos';
    $config['allowed_types'] = 'txt';
    $config['max_size'] = '5000';
    $this->load->library('upload');
    $this->upload->initialize($config);

    if ( ! $this->upload->do_upload('FileDeposito'))
	{
		$this->session->set_flashdata('errors','<p class="alert alert-danger">'.$this->upload->display_errors('','').'</p>');
		redirect(base_url().'DepositosCnb','refresh');
	}
	else
	{
		$info=$this->upload->data();
		$finalMessage='';


		//carga de datos en la $tabla

		$info=$this->upload->data();
		$file = fopen($info['full_path'],"r");
		$i=0;

		$linea=array();


		if ($file) {
			while (($bufer = fgets($file, 4096)) !== false) {
				$linea[$i]=$bufer;
				$i++;
			}
			if (!feof($file)) {
				echo "Error: fallo inesperado de fgets()\n";
			}
			fclose($file);
		}

		$cuenta=array();
		$j=0;
		$cuentasQueNoExisten=0;
		$NoCuenta='';
		for ($k=1;$k<(count($linea)-1);$k++){
			$key=trim(substr($linea[$k],2,17));
			if(!$this->existeCuenta($key)){

				$cuenta[$j]['name']=str_replace('Ñ','N',trim(substr($linea[$k],18,30)));
				$cuenta[$j]['cuentaAgencia']=$key;
				$cuenta[$j]['monto']=number_format(intval(substr($linea[$k],59,12))/100,'2','.','');
				$cuenta[$j]['description']=trim(substr($linea[$k],71,33));

				$j++;
			}else{
				$cuentasQueNoExisten++;
				$NoCuenta.=' - '.$key;
			}
		}

		$montoAPagar=number_format(intval(substr($linea[0],10,23))/100,'2','.','');



		if($j>0){$this->db->insert_batch('esb.DepositosCnb',$cuenta);}

		//var_dump($cuenta);

		unlink($info['full_path']);
		$finalMessage.='<br><p class="alert alert-success"><strong>'.$info['file_name'].'</strong> '.$this->lang->line('success').'<br>Total de Depositos Ingresados : <strong>'.$j.'.</strong><br> Monto Total En esta Carga :<strong> B/. '.$montoAPagar.'</strong></p>';
		$this->session->set_flashdata('errors',$finalMessage);
		redirect(base_url().'DepositosCnb','refresh');
	}

  }

  public function cedulaRepetida($str){
      $this->db->select('id');
      $this->db->where('cedula',$str);
      $query=$this->db->get('esb.dispersion');
      if($query->num_rows()>0){
          $this->form_validation->set_message('cedulaRepetida',$this->lang->line('cedula_ya_existe'));
          return FALSE;
      }
      return TRUE;
  }

  public function DeleteTabledispersion(){
    //$this->db->delete('esb.dispersion');
    //$this->db->empty_table('esb.dispersion');
    $this->db->truncate('esb.DepositosCnb');
    $this->session->set_flashdata('errors','<p class="alert alert-info">'.$this->lang->line('dispersion_deleted').'</p>');
    redirect('DepositosCnb','refresh');
  }

  public function ApplyDepositos(){
  	$errores='';
	  ini_set('memory_limit', '1024M'); // or you could use 1G
	  ini_set('max_execution_time', '600'); // or you could use 1G
  	$cuentasNoEncotradas=array();
	  $usuarioNoEncontrado=0;
      $this->db->select('*');
      $Deposito=$this->db->get('esb.DepositosCnb');
	  $this->load->library('Sms');
	  $sms= new Sms();
      if($Deposito->num_rows()>0){


        foreach ($Deposito->result() as  $infoDeposito) {
          $Transaccion=new stdClass();
          $this->db->select('*');
          $this->db->where('cuenta',$infoDeposito->cuentaAgencia);
          $this->db->limit(1);
          $datoAgencia=$this->db->get('esb.Agencias');
          if($datoAgencia->num_rows()>0){
            foreach($datoAgencia->result() as $Agencia) {

            	$this->db->select('*');
            	$this->db->where('Cod_Agencia',$Agencia->Cod_Agencia);
            	$datosTerminal=$this->db->get('esb.Terminales')->row();
				$this->db->select('*');
				$this->db->where('Cod_Operador',$datosTerminal->Cod_Operador);
				$datosOperador=$this->db->get('esb.Operadores')->row();


                  $Transaccion->terminalid=$datosTerminal->ID_Usuario;

                  $Transaccion->tipoTrx='99';
                  $Transaccion->montoTotal=$infoDeposito->monto;
                  $Transaccion->montoTrx=$infoDeposito->monto;


                  $Transaccion->formaPago='Cŕedito';

					$Transaccion->stamp=date('Y-m-d H:i:s');
					$Transaccion->cnbOperador=$datosOperador->Descripcion;
					$Transaccion->cnbAgencia=$Agencia->Descripcion;
					$Transaccion->cuenta=$infoDeposito->cuentaAgencia;

                  $Transaccion->descripcionTrx='Cŕedito';

                  $Transaccion->aux='+';

				$Transaccion->cuentaBNP=$infoDeposito->cuentaAgencia;
				$Transaccion->agenciaCNBId=$Agencia->Cod_Agencia;
				$Transaccion->cuentaComision=$Agencia->cuentaComision;
				$Transaccion->currency='USD';

                  $Transaccion->lat='';
                  $Transaccion->lon='';


                  $Transaccion->email='transferencias@banconal.com';

                  $Transaccion->nombre='BNP';

				$Transaccion->montoTotal=$infoDeposito->monto;
				$Transaccion->cuenta1=$infoDeposito->cuentaAgencia;

                  $this->db->insert('esb.transactionLog',$Transaccion);


                  //================================================================================
				//$Transaccion->terminalid=$datosTerminal->ID_Usuario;
				$Transaccion->tipoTrx='96';
				$Transaccion->montoTotal=$infoDeposito->monto;
				$Transaccion->montoTrx=$infoDeposito->monto;
				$Transaccion->formaPago='Débito';
				$Transaccion->stamp=date('Y-m-d H:i:s');
				$Transaccion->cnbOperador=' ';
				$Transaccion->cnbAgencia=' ';
				$Transaccion->cuenta='2353100100103007';
				$Transaccion->descripcionTrx='Débito';
				$Transaccion->aux='-';
				$Transaccion->cuentaBNP='2353100100103007';
				$Transaccion->agenciaCNBId='';
				$Transaccion->cuentaComision='';
				$Transaccion->currency='USD';
				$Transaccion->lat='';
				$Transaccion->lon='';
				$Transaccion->email='';
				$Transaccion->nombre='BNP';
				$Transaccion->cuenta1='2353100100103007';

				$this->db->insert('esb.transactionLog',$Transaccion);


				//=============================================================================




				$this->db->where('cuentaAgencia',$infoDeposito->cuentaAgencia);
				$this->db->delete('esb.DepositosCnb');



				$this->db->select('terminalPhone');
				$this->db->where('Cod_Agencia', $Agencia->Cod_Agencia);
				$telefonos=$this->db->get('esb.Terminales');

				foreach ($telefonos->result() as $telefono) {
					$sms->token=$infoDeposito->monto;
					$sms->phone=$telefono->terminalPhone;
					$sms->personName=$datosOperador->Descripcion;
					$sms->templateMsn='deposito';
					$sms->send();
				}
				//var_dump($sms->errors);

            }
          }else{
          	$usuarioNoEncontrado++;
          	$cuentasNoEncotradas[$usuarioNoEncontrado]=$infoDeposito->cuentaAgencia;
		  }
        }
        $strNoEncontras='0';
        if(count($usuarioNoEncontrado)>0){
			foreach ($cuentasNoEncotradas as $cuenta){
				$strNoEncontras.=$cuenta.',';
			}
		}
        $this->session->set_flashdata('errors','<p class="alert alert-success">Depositos Aplicados.<br>Cuentas No Encontradas: '.$strNoEncontras.'</p>');
        redirect('DepositosCnb','refresh');

      }else{
        $this->session->set_flashdata('errors','<p class="alert alert-success">Nada para aplicar!</p>');
        redirect('DepositosCnb','refresh');
      }


  }

	public function existeCuenta($str){
		$this->db->select('Agencias.Cod_Agencia');
		$this->db->where('cuenta',$str);
		$query=$this->db->get('esb.Agencias');
		if($query->num_rows()>0){
			return FALSE;
		}
		return TRUE;
	}

}

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class AdminTerminales extends MY_Controller{


	public $mymodel='terminales_m';


	public $contenido=array();
	protected $dataForm=array('title_form'=>'Datos Terminales');


	public function __construct(){
        parent::__construct();
        $this->load->model('terminales_m');

    }

    public function index(){

    	$this->contenido['table']=$this->load->view("paper/tables/table_terminales",'',true);
    	$this->contenido['title']=$this->lang->line('terminal');
		$datos["content_view"]=$this->load->view("paper/admin/paper_view_terminales",$this->contenido,true);
		$datos['navbar_menu']=$this->navbar_header_menu();
		$datos['sidebar_menu']=$this->menu_admin();
		$datos['footer']=$this->load->view("paper/footers/footer_terminales",'',true);

		$this->load->view("paper/base",$datos);

    }
    public function populateTable(){
    	echo json_encode($this->terminales_m->PopulateTable());
    }

    public function Form(){
    	$this->dataForm['form']=$this->terminales_m->Form('lg');
    	echo $this->load->view("paper/forms_modal/form_Terminales",$this->dataForm,true);
    }

    public function saveData(){
			$id=NULL;
			$dataToInsert=array();

			if($this->input->post($this->modelo->_primary_key) != ''){
				$id=$this->input->post($this->modelo->_primary_key);
				foreach ($this->modelo->_inputs as $key => $value){
						$key=$key;
						$dataToInsert[$key]=$this->input->post($key);

				}
				unset($dataToInsert[$this->modelo->_primary_key]);
				$reglas=$this->modelo->_rules;
				foreach ($reglas as $key => $value) {
					if(strpos($value['rules'],'unique')){
						$reglas[$key]['rules']='trim|required|max_length[80]';
					}
					if($value['field']=='password'){
						$reglas[$key]['rules']='trim|max_length[80]';
					}
					if($value['field']=='email'){
						$reglas[$key]['rules']='trim|max_length[80]|callback_isEmail';
					}
				}
				$this->form_validation->set_rules($reglas);
				$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
				//var_dump($dataToInsert);
				if($this->form_validation->run()==TRUE){
					if(array_key_exists('password',$dataToInsert)){
						if($dataToInsert['password']!=''){
							$dataToInsert['password']=$this->encrypt->encode($dataToInsert['password']);
						}else{
							unset($dataToInsert['password']);
						}
						//$dataToInsert['password']=='prueba que si existe';
					}
					$this->modelo->save($dataToInsert,$id);
					echo 'ok';
				}else{
					echo $this->Form();
				}
			}else{
				foreach ($this->modelo->_inputs as $key => $value) {
						$dataToInsert[$key]=$this->input->post($key);
						if($dataToInsert[$key]==''){
							unset($dataToInsert[$key]);
						}
				}
				unset($dataToInsert[$this->modelo->_primary_key]);
				$this->form_validation->set_rules($this->modelo->_rules);
				$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
				//var_dump($dataToInsert);
				if($this->form_validation->run()==TRUE){

					if(array_key_exists('password',$dataToInsert)){
						$dataToInsert['password']=$this->encrypt->encode($dataToInsert['password']);
						//$dataToInsert['password']=='prueba que si existe';
					}
					$this->modelo->save($dataToInsert);
					echo 'ok';
				}else{
				echo $this->Form();
				}
			}
		}

	public function EditData(){
		$id=$this->input->post('Id');
		$dataForm['form']=$this->terminales_m->Form('lg',$id);
        $dataForm['id_terminal']=$id;
		echo $this->load->view("paper/forms_modal/form_terminales_edit",$dataForm,true);
		//var_dump($id);
	}

	public function UpdateData(){
		$id=NULL;
		$dataToInsert=array();

		if($this->input->post($this->modelo->_primary_key) != ''){
			$id=$this->input->post($this->modelo->_primary_key);
			foreach ($this->modelo->_inputs as $key => $value){
					$key=$key;
					$dataToInsert[$key]=$this->input->post($key);

			}
			unset($dataToInsert[$this->modelo->_primary_key]);
			$reglas=$this->modelo->_rules;
			foreach ($reglas as $key => $value) {
				if(strpos($value['rules'],'unique')){
					$reglas[$key]['rules']='trim|required|max_length[80]';
				}
				if($value['field']=='password'){
					$reglas[$key]['rules']='trim|max_length[80]';
				}
				if($value['field']=='email'){
					$reglas[$key]['rules']='trim|max_length[80]|callback_isEmail';
				}
			}
			$this->form_validation->set_rules($reglas);
			$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
			//var_dump($dataToInsert);
			if($this->form_validation->run()==TRUE){
				if(array_key_exists('password',$dataToInsert)){
					if($dataToInsert['password']!=''){
						$dataToInsert['password']=$this->encrypt->encode($dataToInsert['password']);
					}else{
						unset($dataToInsert['password']);
					}
					//$dataToInsert['password']=='prueba que si existe';
				}
				$this->modelo->save($dataToInsert,$id);
				echo 'ok';
			}else{
				echo $this->Form();
			}
		}else{
			foreach ($this->modelo->_inputs as $key => $value) {
					$dataToInsert[$key]=$this->input->post($key);
					if($dataToInsert[$key]==''){
						unset($dataToInsert[$key]);
					}
			}
			unset($dataToInsert[$this->modelo->_primary_key]);
			$this->form_validation->set_rules($this->modelo->_rules);
			$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
			//var_dump($dataToInsert);
			if($this->form_validation->run()==TRUE){

				if(array_key_exists('password',$dataToInsert)){
					$dataToInsert['password']=$this->encrypt->encode($dataToInsert['password']);
					//$dataToInsert['password']=='prueba que si existe';
				}
				$this->modelo->save($dataToInsert);
				echo 'ok';
			}else{
				echo $this->Form();
			}
		}
	}

	public function DeleteData(){
		$id=$this->input->post('id');
		$this->terminales_m->delete($id);
		echo '<p class="label label-success">'.$this->lang->line('error_delete').'<strong>'.$id.'</strong></p>';

	}

	public function diferente_de_0($str)
        {
            if ($str == '0')
            {
                $this->form_validation->set_message('diferente_de_0', 'Elige una opcion Válida');
                return FALSE;
            }
            else
            {
                return TRUE;
            }
        }

     public function rand_float($st_num=0,$end_num=1,$mul=1000000){

			if ($st_num>$end_num){
				return false;
			}
			return mt_rand($st_num*$mul,$end_num*$mul)/$mul;
		}

     /*public function InsertTransaccion($cantidad){
     	$data=array();

     	$data['terminalId']='270';
     	$data['biller']='MPAY';
     	$data['transactionCode']='1';
     	$data['cardno']='************0041';
     	$data['cashAmount']='.00';
     	$data['cardAmount']='.00';
     	$data['otherAmount']='.00';
     	$data['totalAmount']='.00';
     	$data['currencyCode']='840';
     	$data['latitud']='8.9906769';
     	$data['longitud']='-79.4999723';
     	$data['status']='Aprobado';
     	$data['authorizationCode']='634812';
     	$data['answerCode']='00';
     	$data['answerText']='Aprobado';
     	$data['tax']='.00';
     	$data['country']='Panama';
     	$data['canal']='Restaurante Propio';
     	$data['Cod_RespuestaAuth']='';
     	$data['currency']='USD';
     	$data['authGuid']='09KF65B34L95M1NGQER';
     	$data['username']='hrodriguez@mpaycenter.com';
     	$data['operator']='Correos y Telegrafos';
     	$data['agency']='Correos Santiago';
     	$data['address']='Calle 50 y Calle 43 Panama';
     	$data['type']='1';
     	$data['userId']='9';
     	$data['timeStamp']=date('Y-m-d H:i:s');

     	$montos=array(1=>'20.00');
     	$montos[2]='3';
     	$montos[3]='50';
     	$montos[4]='15';
     	$montos[5]='5.65';
     	$montos[6]='10';
     	$montos[7]='30';
     	$montos[8]='2';
     	$montos[9]='1';
     	$montos[10]='6';

     	$this->load->model('logt_m');

     	for($i=0; $i<=$cantidad;$i++){

     		$data['totalAmount']=$montos[rand(1,10)];
     		$data['latitud']=$this->rand_float(9.002550,9.051683);//'8.9906769';
     		$data['longitud']=$this->rand_float(-79.543758,-79.388203);//'-79.4999723';

     		$this->logt_m->save($data);

				echo "Insertando Transaccion No. : ".$i.' Operador : '.$data['operator'].' Monto : '.$data['totalAmount'].'<br>';
				echo "======================================================<br>";
     	}



		}*/


		public function optAgencias(){
			$cod=$this->input->post('Cod_Operador');
			echo $this->terminales_m->optAgencias($cod);
		}



}
?>

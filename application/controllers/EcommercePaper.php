<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class EcommercePaper extends MY_Controller{
	public $permiso_ver=136;
	public $permiso_crear=0;
	public $permiso_actualizar=0;
	public $permiso_eliminar=0;


	public function __construct(){
        parent::__construct();
				$this->load->model('ecommerceModel');
    }

	public function index(){
		$contenido['totalVentas']=number_format($this->ecommerceModel->TotalVentas(),2,'.',',');
		$contenido['transaccionesDiarias']=$this->ecommerceModel->CuentaDeTransaDiarias();
		$contenido['ventasDiarias']=$this->ecommerceModel->VentasDiarias();
		$contenido['ticketPromedio']=$this->ecommerceModel->PromedioVentasDiarias();
		$contenido['topMarca']=$this->ecommerceModel->TopOneMarca();
		$datos["content_view"]=$this->load->view("paper/ecommerce/content_ecommerce",$contenido,true);
		$datos['navbar_menu']=$this->navbar_header_menu();
		$datos['sidebar_menu']=$this->menu_ecommerce();
		$datos['footer']=$this->load->view('paper/footers/footer_ecommerce');
		$this->load->view("paper/base",$datos);
	}



		public function Insertar($brand,$cantidad){
			//echo json_encode($this->ecommerceModel->topMarca());
			$datos['acquirerId']=13;
			$datos['idCommerce']=8458;
			$datos['purchaseOperationNumber']=603000165;
			$datos['purchaseAmount']=5;
			$datos['purchaseCurrencyCode']=840;
			$datos['shippingFirstName']='Renato';
			$datos['shippingLastName']='Santizo';
			$datos['shippingEmail']='rsantizo@mpaycenter.com';
			$datos['descriptionProducts']='PRUEBA';
			$datos['reserved5']=0;
			$datos['brand']=strval($brand);
			$datos['status']='P';
			$datos['timestamp']=date('Y-m-d H:i:s');
			$datos['purchaseOperationNumberAlg']='000001468';
			$datos['operador']='Panama';
			$datos['currency']='American Dolar';
			//$datos['realAmount']=1.07;
			for ($i=0; $i <$cantidad ; $i++) {
				$this->db->insert('ecom.e_transactionLog',$datos);
			}
			if($i==$cantidad){
				echo "He Terminado";
			}




		}

		public function TOPBRAND(){
			echo json_encode($this->ecommerceModel->topMarca(5));

		}


		public function rdiario(){
			$contenido['title']=$this->lang->line('dreport1');
			$ecommerce['footer']='rdiario.js';
			$datos["content_view"]=$this->load->view("paper/ecommerce/content_tables_ecommerce",$contenido,true);
			$datos['navbar_menu']=$this->navbar_header_menu();
			$datos['sidebar_menu']=$this->menu_ecommerce();
			$datos['footer']=$this->load->view('paper/footers/footer_ecommerce',$ecommerce,true);
			$this->load->view("paper/base",$datos);

		}


	public function rsemanal(){

		$contenido['title']=$this->lang->line('wreport1');

		$ecommerce['footer']='rsemanal.js';
		$datos["content_view"]=$this->load->view("paper/ecommerce/content_tables_ecommerce",$contenido,true);
		$datos['navbar_menu']=$this->navbar_header_menu();
		$datos['sidebar_menu']=$this->menu_ecommerce();
		$datos['footer']=$this->load->view('paper/footers/footer_ecommerce',$ecommerce,true);
		$this->load->view("paper/base",$datos);
	}


	public function rmensual(){
		$contenido['title']=$this->lang->line('mreport1');

		$ecommerce['footer']='rmensual.js';
		$datos["content_view"]=$this->load->view("paper/ecommerce/content_tables_ecommerce",$contenido,true);
		$datos['navbar_menu']=$this->navbar_header_menu();
		$datos['sidebar_menu']=$this->menu_ecommerce();
		$datos['footer']=$this->load->view('paper/footers/footer_ecommerce',$ecommerce,true);
		$this->load->view("paper/base",$datos);
	}
	public function ranual(){
		$contenido['title']=$this->lang->line('areport1');

		$ecommerce['footer']='ranual.js';

		$datos["content_view"]=$this->load->view("paper/ecommerce/content_tables_ecommerce",$contenido,true);
		$datos['navbar_menu']=$this->navbar_header_menu();
		$datos['sidebar_menu']=$this->menu_ecommerce();
		$datos['footer']=$this->load->view('paper/footers/footer_ecommerce',$ecommerce,true);
		$this->load->view("paper/base",$datos);
	}

	/*public function rpersonalizado(){
		$contenido['transaccionesDiarias']=$this->ecommerceModel->CuentaDeTransaDiarias();
		$contenido['ventasDiarias']=$this->ecommerceModel->VentasDiarias();
		$contenido['ticketPromedio']=$this->ecommerceModel->PromedioVentasDiarias();
		$contenido['topMarca']=$this->ecommerceModel->TopOneMarca();
		$datos["content_view"]=$this->load->view("paper/ecommerce/content_ecommerce",$contenido,true);
		$datos['navbar_menu']=$this->navbar_header_menu();
		$datos['sidebar_menu']=$this->menu_ecommerce();
		$datos['footer']=$this->load->view('paper/footers/footer_ecommerce');
		$this->load->view("paper/base",$datos);
	}*/

	public function PopulateTableDiario(){
		echo json_encode($this->ecommerceModel->PopulateTableDiario());
	}
	public function PopulateTableSemanal(){
		echo json_encode($this->ecommerceModel->PopulateTableSemanal());
	}
	public function PopulateTableMensual(){
		echo json_encode($this->ecommerceModel->PopulateTableMensual());
	}
	public function PopulateTableAnual(){
		echo json_encode($this->ecommerceModel->PopulateTableAnual());
	}
	public function PopulateTablePersonalizado(){}

		public function Graphics(){
				echo json_encode($this->ecommerceModel->GraphicalView());
		}

}

?>

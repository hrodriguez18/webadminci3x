<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MposPaper extends MY_Controller{

	public $permiso_ver=126;
	public $permiso_crear=62;
	public $permiso_actualizar=63;
	public $permiso_eliminar=64;

	public function __construct(){
        parent::__construct();
				$this->load->model('MposModel','mm');
				$this->load->model('adminmodel');
				$this->load->model('faqs');
    }

		public function index(){
			$datos_content['form_ventas']=$this->adminmodel->FormVentas();
			$datos_content['info_ventas']=
			$this->adminmodel->DatosVentas();

			$datos_content['controlador']='MposPaper';

			$datos['titleMenu']='MPOS';

			$datos_content['form_maps']=$this->adminmodel->FormMapas();
			$datos["content_view"]=$this->load->view("paper/admin/content_admin",$datos_content,true);
			$datos['navbar_menu']=$this->navbar_header_menu();
			$datos['sidebar_menu']=$this->menu_mpos();
			$datos['footer']=$this->load->view("paper/footers/footer_dashboard",'',true);

			$this->load->view("paper/base",$datos);
		}


		public function Graphics(){
			$year=$this->input->post('year');
			$mes=$this->input->post('mes');
			$pais=$this->input->post('pais');
			$canal=$this->input->post('canal');
			$autorizador=$this->input->post('autorizador');
			if($year){
				echo json_encode($this->adminmodel->GraphicalView($year,$pais,$canal,$autorizador));

			}

		}

		public function DatosVentas(){

			$year=$this->input->post('year');
			$mes=$this->input->post('mes');
			$pais=$this->input->post('pais');
			$canal=$this->input->post('canal');
			$autorizador=$this->input->post('autorizador');

			$datos=array('year'=>$year,'month'=>$mes,'pais'=>$pais,'canal'=>$canal,'autorizador'=>$autorizador);
			echo $this->adminmodel->DatosVentasQuery($datos);
		}


		public function SearchPoints(){
			$operador=$this->input->post('operador');
			echo json_encode($this->adminmodel->GetPointForMaps($operador));
		}

	public function faqs(){
		$datos['titleMenu']='MPOS';
		$conteView['title']='FAQs';
		$conteView['table']=$this->load->view('paper/tables/tablefaqs','',true);
		$datos["content_view"]=$this->load->view("paper/mpos/content_mpos",$conteView,true);
		$datos['navbar_menu']=$this->navbar_header_menu();
		$datos['sidebar_menu']=$this->menu_mpos();
		$datos['footer']=$this->load->view("paper/footers/footerfaqs",'',true);
		$this->load->view("paper/base",$datos);
	}

	public function Save(){
		if($this->input->post('Id')){
			$id=$this->input->post('Id');
			$this->form_validation->set_rules($this->faqs->_rules);
			$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
			if($this->form_validation->run()==TRUE){
				foreach ($this->faqs->_inputs as $key => $value) {
					$data[$key]=$this->input->post($key);
					unset($data['Id']);
				}
				$this->faqs->save($data,$id);
				echo 'ok';
			}else{
				echo $this->faqs->form('sm',$id);
			}
		}else{
			$this->form_validation->set_rules($this->faqs->_rules);
			$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
			if($this->form_validation->run()==TRUE){
				foreach ($this->faqs->_inputs as $key => $value) {
					$data[$key]=$this->input->post($key);
					unset($data['Id']);
				}
				$this->faqs->save($data);
				echo 'ok';
			}else{
				echo $this->faqs->form('sm');
			}

		}
	}

	public function Delete(){
		$id=$this->input->post('Id');
		$this->faqs->delete($id);
		echo '<p class="label label-success">'.$this->lang->line('error_delete').'<strong>'.$id.'</strong></p>';

	}

	public function Form(){
		echo $this->faqs->form('sm',$this->input->post('Id'));
	}



	public function Accesos(){
		$conteView['title']=$this->lang->line('logLogin');
		$datos['titleMenu']='MPOS';
		$conteView['table']=$this->load->view('paper/tables/tableaccesos','',true);
		$datos["content_view"]=$this->load->view("paper/mpos/content_mpos",$conteView,true);
		$datos['navbar_menu']=$this->navbar_header_menu();
		$datos['sidebar_menu']=$this->menu_mpos();
		$datos['footer']=$this->load->view("paper/footers/footermposaccesos",'',true);
		$this->load->view("paper/base",$datos);
	}

	public function Transacciones(){
		$datos['titleMenu']='MPOS';
		$conteView['title']=$this->lang->line('transactions');
		$conteView['table']=$this->load->view('paper/tables/tablestransacciones','',true);
		$datos["content_view"]=$this->load->view("paper/mpos/content_mpos",$conteView,true);
		$datos['navbar_menu']=$this->navbar_header_menu();
		$datos['sidebar_menu']=$this->menu_mpos();
		$datos['footer']=$this->load->view("paper/footers/footermpostransaccion",'',true);
		$this->load->view("paper/base",$datos);
	}

	public function populateFaqs(){
		echo json_encode($this->mm->populateFaqs());
	}
	public function populateAccesos(){
		echo json_encode($this->mm->populateAccesos());
	}
	public function populateTransacciones(){
		echo json_encode($this->mm->populateTransacciones());
	}

	public function populateAlertas(){
		echo json_encode($this->mm->populateAlertas());
	}

	public function Alertas(){
		$datos['titleMenu']='MPOS';
		$conteView['title']=$this->lang->line('alertas');
		$conteView['table']=$this->load->view('paper/tables/tablestransacciones','',true);
		$datos["content_view"]=$this->load->view("paper/mpos/content_mpos",$conteView,true);
		$datos['navbar_menu']=$this->navbar_header_menu();
		$datos['sidebar_menu']=$this->menu_mpos();
		$datos['footer']=$this->load->view("paper/footers/mposalertas",'',true);
		$this->load->view("paper/base",$datos);
	}

	public function testp(){

		$this->load->model('populateModel');
		$accesos=New populateModel();
		$accesos->_tablename='esb.Log_Transaccional';
		$accesos->_primary_key='tranNum';
		$accesos->_order_by='tranNum';
		$accesos->_limit=2;
		$accesos->_limit=10;
		$accesos->_selection=array(
							'tranNum',
							'timeStamp',
							'terminalId',
							'biller',
							'transaccionCode'
						);

		print_r( $accesos->populate() );


	}








}

?>

<?php if ( ! defined('BASEPATH')) exit('No direct scriptccessllowed');

class AdminRolesTerminal extends MY_Controller{

	public $permiso_crear=112;
	public $permiso_actualizar=113;
	public $permiso_eliminar=114;
	public $permiso_ver=111;


	public $titulo='';

	public function __construct(){
        parent::__construct();
        $this->load->model('adminRolesTerminal_m');
				$this->titulo=$this->lang->line('rolesterminal');
    }

	public $contenido=array();
	protected $dataForm=array('title_form'=>'Data Roles Terminales');


    public function index(){

    	$this->contenido['table']=$this->load->view("paper/tables/table_adminRolesTerminal",'',true);
    	$this->contenido['title']=$this->titulo;
		$datos["content_view"]=$this->load->view("paper/admin/paper_view_adminRolesTerminal",$this->contenido,true);
		$datos['navbar_menu']=$this->navbar_header_menu();
		$datos['sidebar_menu']=$this->menu_admin();
		$datos['footer']=$this->load->view("paper/footers/footer_adminRolesTerminal",'',true);

		$this->load->view("paper/base",$datos);

    }
    public function populateTable(){
    	echo json_encode($this->adminRolesTerminal_m->get());
    }

    public function Form(){
    	$this->dataForm['form']=$this->adminRolesTerminal_m->Form();
    	$infoPermisos=$this->adminRolesTerminal_m->ListaPermisos();

    	$this->dataForm['permisosActuales']=$infoPermisos['ListaPermisosActuales'];
    	$this->dataForm['PermisosDisponibles']=$infoPermisos['ListaPermisosDisponibles'];
    	echo $this->load->view("paper/forms_modal/form_adminRolesTerminal",$this->dataForm,true);
    }

    public function saveData(){
		$this->form_validation->set_rules($this->adminRolesTerminal_m->rules);
		$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
		if($this->form_validation->run()==TRUE){

			$data['permisos']=$this->input->post('info');
			$data['descripcion']=$this->input->post('descripcion');
			$data['status']=$this->input->post('status');

			if($data['permisos'][0]!=NULL){
				$rolid=$this->adminRolesTerminal_m->InsertarRol($data['descripcion'],$data['status']);

				for($i=0;$i<count($data['permisos']); $i++){
					$rel['rol_id']=$rolid;
					$rel['permiso_id']=$data['permisos'][$i];

					$this->adminRolesTerminal_m->saveTerminalPermiso($rel);
				}
				echo "Todo bien!";

			}else {
				$this->dataForm['form']=$this->adminRolesTerminal_m->Form();
		    	$infoPermisos=$this->adminRolesTerminal_m->ListaPermisos();
		    	$this->dataForm['permisosActuales']=$infoPermisos['ListaPermisosActuales'];
		    	$this->dataForm['PermisosDisponibles']=$infoPermisos['ListaPermisosDisponibles'];
		    	echo $this->load->view("paper/forms_modal/form_adminRolesTerminal",$this->dataForm,true);
			}

		}else{
			$this->dataForm['form']=$this->adminRolesTerminal_m->Form();
	    	$infoPermisos=$this->adminRolesTerminal_m->ListaPermisos();
	    	$this->dataForm['permisosActuales']=$infoPermisos['ListaPermisosActuales'];
	    	$this->dataForm['PermisosDisponibles']=$infoPermisos['ListaPermisosDisponibles'];
	    	echo $this->load->view("paper/forms_modal/form_adminRolesTerminal",$this->dataForm,true);
		}
	}

	public function EditData(){
		$id=$this->input->post('id');
		$data_by_id=$this->adminRolesTerminal_m->get($id,true);



		$dataForm=array('title_form'=>'Data Roles Terminales');
		$dataForm['form']=$this->adminRolesTerminal_m->Form($data_by_id);

		$infoPermisos=$this->adminRolesTerminal_m->ListaPermisos($id);
	    $dataForm['permisosActuales']=$infoPermisos['ListaPermisosActuales'];
	    $dataForm['PermisosDisponibles']=$infoPermisos['ListaPermisosDisponibles'];

		echo $this->load->view("paper/forms_modal/form_adminRolesTerminal_edit",$dataForm,true);
		//var_dump($infoPermisos);

	}

	public function UpdateData(){
		$this->form_validation->set_rules($this->adminRolesTerminal_m->rules);
		$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
		if($this->form_validation->run()==TRUE){

			$data['permisos']=$this->input->post('info');
			$data['descripcion']=$this->input->post('descripcion');
			$data['status']=$this->input->post('status');
			$data['idRol']=$this->input->post('id');

			if($data['permisos'][0]!=NULL){
				$this->adminRolesTerminal_m->UpdateRol($data['descripcion'],$data['status'],$data['idRol']);

				$this->adminRolesTerminal_m->LimpiarRol($data['idRol']);

				for($i=0;$i<count($data['permisos']); $i++){
					$rel['rol_id']=$data['idRol'];
					$rel['permiso_id']=$data['permisos'][$i];

					$this->adminRolesTerminal_m->saveTerminalPermiso($rel);
				}
				echo "Todo bien!";

			}else{
				$this->dataForm['form']=$this->adminRolesTerminal_m->Form();
		    	$infoPermisos=$this->adminRolesTerminal_m->ListaPermisos();
		    	$this->dataForm['permisosActuales']=$infoPermisos['ListaPermisosActuales'];
		    	$this->dataForm['PermisosDisponibles']=$infoPermisos['ListaPermisosDisponibles'];
		    	echo $this->load->view("paper/forms_modal/form_adminRolesTerminal",$this->dataForm,true);
			}


		}else{
			$this->dataForm['form']=$this->adminRolesTerminal_m->Form();
	    	$infoPermisos=$this->adminRolesTerminal_m->ListaPermisos();
	    	$this->dataForm['permisosActuales']=$infoPermisos['ListaPermisosActuales'];
	    	$this->dataForm['PermisosDisponibles']=$infoPermisos['ListaPermisosDisponibles'];
	    	echo $this->load->view("paper/forms_modal/form_adminRolesTerminal",$this->dataForm,true);
		}
	}

	public function DeleteData(){
		$id=$this->input->post('Id');


		$this->adminRolesTerminal_m->LimpiarRol($id);
		$this->adminRolesTerminal_m->delete($id);
		echo '<p class="label label-success">'.$this->lang->line('error_delete').'<strong>'.$id.'</strong></p>';

	}

	public function diferente_de_0($str){
        if ($str == '0')
        {
            $this->form_validation->set_message('diferente_de_0', lang('validate_option'));
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }




    public function usuario_unico($str){
    	$field='username';
    	$value=$str;
    	if ($this->adminRolesTerminal_m->countUserByData($field,$value)>0)
        {
            $this->form_validation->set_message('usuario_unico', lang('already_user'));
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }

    public function email_unico($str){
    	$field='email';
    	$value=$str;
    	if ($this->adminRolesTerminal_m->countUserByData($field,$value)>0)
        {
            $this->form_validation->set_message('usuario_unico', lang('already_email'));
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }


    /*public function Test(){
     	var_dump($this->adminRolesTerminal_m);
		}*/

}
?>

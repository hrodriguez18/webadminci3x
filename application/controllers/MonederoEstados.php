<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MonederoEstados extends MY_Controller{
  //DATOS PARA LOS PERMISOS
  	public $permiso_ver=126;
  	public $permiso_crear=62;
  	public $permiso_actualizar=63;
  	public $permiso_eliminar=64;
  //DATOS PARA LA TABLA
  	public $mymodel='MonederoEstados_m';
  	public $controlerServer='MonederoEstados';
  	public $dataTable=array();
  //Ruta para llamar al formulario;
  	public $rutarForm='';
  //Tamaño del formulario
  	public $formSize='lg';

  	//variable para Exportar
  	public $rutaToExport='';

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
  }

  public function index(){
    $this->dataTable[0]=array('field'=>'id','title'=>'ID','sortable'=>'true','visible'=>'false','align'=>'left');
		$this->dataTable[1]=array('field'=>'name','title'=>'name','sortable'=>'true','visible'=>'true','align'=>'left');
    $this->dataTable[2]=array('field'=>'lastname','title'=>'lastname','sortable'=>'true','visible'=>'true','align'=>'left');
		$this->dataTable[3]=array('field'=>'email','title'=>'phone','sortable'=>'true','visible'=>'true','align'=>'left');
    $this->dataTable[4]=array('field'=>'correo','title'=>'email','sortable'=>'true','visible'=>'true','align'=>'left');
		$this->dataTable[5]=array('field'=>'cedula','title'=>'cedula','sortable'=>'true','visible'=>'false','align'=>'left');
		$this->dataTable[6]=array('field'=>'saldo','title'=>'saldo','sortable'=>'false','visible'=>'true','align'=>'left');
    $this->dataTable[7]=array('field'=>'cuentaMonedero','title'=>'btn_down','sortable'=>'false','visible'=>'true',
		'formatter'=>'setLink','align'=>'left');


    $data['titleMenu']=$this->lang->line('Monedero');
		$data['navbar_menu']=$this->navbar_header_menu();
	  $data['controllerServer']=$this->controlerServer;
		$data['columns']=$this->getColumnsForTable();
		$data['rutaForm']=$this->rutaForm;
		$data['title_form']='Usuarios Monedero';
		$data['sizeModal']='modal-lg';
    	$data['title']=$this->lang->line('accountstatements');
		$data['rutaToExport']=base_url().$this->router->fetch_class().'/exportUser';
		$data['localeForTable']=$this->localeForTable;
		$this->load->view('paper/monedero/_layoutEstados',$data);
  }

  public function Saldo(){
    $cuenta=$this->input->post('cuenta');
    //var_dump($this->modelo->EstadoDeCuenta($cuenta));
  }

  public function DescargarSaldo(){

	  $this->load->model('ExportToPdf');
	  $reporte = new ExportToPdf();

	  $reporte->_tablename='esb.usuariosMonedero';
	  $reporte->_primary_key='id';
	  $reporte->_order_by='usuariosMonedero.id';

	  //$reporte->fecha_inicio=$this->input->post('fecha_inicio').' 00:00:00';
	  //$reporte->fecha_fin=$this->input->post('fecha_fin').' 23:59:00';


	  $reporte->_order='DESC';

	  $reporte->_limit=1;

	  //cuenta: 275fe7fcf7b5f84d

	  $reporte->_especificFieldToSearch='usuariosMonedero.cuentaMonedero';

	  $reporte->_especificValueToSearch=$this->input->post('cuenta');


	  $reporte->_camposBusqueda=array();

	  $reporte->_selection=array(
	  	'usuariosMonedero.name',
		'usuariosMonedero.lastname',
	 	'usuariosMonedero.cedula',
	  	'usuariosMonedero.cuentaMonedero',
	  	'consultaSaldoMonedero.Saldo as saldo'
	  );
	  $reporte->_joinRelation=array(
		  array(

			  'table'=>'esb.consultaSaldoMonedero',
			  'where_equals'=>'usuariosMonedero.cuentaMonedero=consultaSaldoMonedero.cuentaMonedero',
			  'option'=>'left'
		  )
	  );



	  $reporte->_filename='EstadoDeCuenta_'.time().'_.pdf';
	  $reporte->_descriptionPdf='REPORTE DE ESTADO DE CUENTA DE MONEDEROS PRESENTE EN EL SISTEMA';
	  $reporte->_headersTablePdf=array(
		  'NOMBRE',
		  'APELLIDO',
		  'CEDULA',
		  'CUENTA',
		  'SALDO'
	  );
	  $reporte->_template['table_open']= '<table border="0" style="
																	border-left: 1px solid black;
																	border-bottom: 1px solid black;
																	border-top: 1px solid black;
																	">';
	  $reporte->_template['thead_open']= '<thead>';



	  $reporte->_template['heading_row_start']= '<tr style="">';



	  $reporte->_template['heading_cell_start']= '<th style="
																	color: #000000;
																	font-weight: bolder;
																	border-bottom:1px solid black;
																	border-right: 1px solid black;
																	text-align: center;
 																	white-space: nowrap;
																">';


	  $reporte->_template['cell_start']= '<td nowrap style="
														
														color: #000000;
 														margin: 5px auto;
 														border-right: 1px solid black;
 														border-bottom: 1px solid black;
 														text-align: left;
 														text-align: center;
 														white-space: nowrap;
 														">';


	  $reporte->_template['cell_alt_start']= '<td nowrap style="
														
														color: #000000;
 														margin: 5px auto;
 														border-right: 1px solid black;
 														border-bottom: 1px solid black;
 														text-align: left;
 														text-align: center;
 														white-space: nowrap;
 														background-color: #beffbb;
 														">';

	  $reporte->getPdf();

  }

  public function populateTable(){
	  $this->load->model('populateModel');
	  $users=new populateModel();
	  $users->_tablename='esb.usuariosMonedero';
	  $users->_primary_key='id';
	  $users->_order_by='usuariosMonedero.id';

	  $users->_camposBusqueda=array(
		  'usuariosMonedero.id',
		  'usuariosMonedero.name',
		  'usuariosMonedero.lastname',
		  'usuariosMonedero.email',
		  'usuariosMonedero.correo'
	  );

	  $users->_Comisiones=FALSE;

	  $users->_selection=array(
		  'usuariosMonedero.id',
		  'usuariosMonedero.email',
		  'usuariosMonedero.correo',
		  'usuariosMonedero.name',
		  'usuariosMonedero.lastname',
		  'usuariosMonedero.cedula',
		  'usuariosMonedero.cuentaMonedero',
		  'consultaSaldoMonedero.Saldo as saldo'

	  );
	  $users->_joinRelation=array(

		  array(

			  'table'=>'esb.consultaSaldoMonedero',
			  'where_equals'=>'usuariosMonedero.cuentaMonedero=consultaSaldoMonedero.cuentaMonedero',
			  'option'=>'left'
		  )
	  );


	  echo $users->populate_join();

  }



}

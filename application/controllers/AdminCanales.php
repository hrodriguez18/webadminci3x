<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class AdminCanales extends MY_Controller{

	public $contenido=array();
	protected $dataForm=array('title_form'=>'Datos Canal');

	public $permiso_crear=49;
	public $permiso_actualizar=50;
	public $permiso_eliminar=51;
	public $permiso_ver=14;


	public function __construct(){
        parent::__construct();
        $this->load->model('canales_m');

    }

    public function index(){

    	$this->contenido['table']=$this->load->view("paper/tables/table_canales",'',true);
    	$this->contenido['title']=$this->lang->line('Channels');
		$datos["content_view"]=$this->load->view("paper/admin/paper_view_canales",$this->contenido,true);
		$datos['navbar_menu']=$this->navbar_header_menu();
		$datos['sidebar_menu']=$this->menu_admin();
		$datos['footer']=$this->load->view("paper/footers/footer_canales",'',true);

		$this->load->view("paper/base",$datos);

    }
    public function populateTable(){
    	echo json_encode($this->canales_m->get());
    }

    public function Form(){
    	echo $this->load->view("paper/forms_modal/form_canal",$this->dataForm,true);
    }

    public function saveData(){
		$this->form_validation->set_rules($this->canales_m->rules);
		$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
		if($this->form_validation->run()==TRUE){

			$data['Canal']=$this->input->post('Canal');
			$this->canales_m->save($data);
			echo "Todo bien!";
		}else{

    		echo $this->load->view("paper/forms_modal/form_canal",$this->dataForm,true);
		}
	}

	public function EditData(){
		$id=$this->input->post('Id');
		$dataForm=$this->canales_m->get($id,true);
		echo $this->load->view("paper/forms_modal/form_canal_edit",$dataForm,true);
		//var_dump($id);

	}

	public function UpdateData(){
		$this->form_validation->set_rules($this->canales_m->rules);
		$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
		if($this->form_validation->run()==TRUE){


			$data['Canal']=$this->input->post('Canal');
			$this->canales_m->save($data,$this->input->post('Id'));
			echo 'Todo bien!';
		}else{
			$dataForm['Canal']=$this->input->post('Canal');
			$dataForm['Id']=$this->input->post('Id');
			echo $this->load->view("paper/forms_modal/form_canal_edit",$dataForm,true);

		}
	}

	public function DeleteData(){
		$id=$this->input->post('id');
		$this->canales_m->delete($id);
		echo '<p class="label label-success">'.$this->lang->line('error_delete').'<strong>'.$id.'</strong></p>';
	}
}
?>

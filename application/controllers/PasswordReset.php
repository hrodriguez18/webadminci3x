<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PasswordReset extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More

	  $this->load->library('form_validation');
  }


  public function index(){
    $this->load->view('paper/forgotpass');
  }

  public function ResetPass(){
    $correo=$this->input->post('email');
    if($correo){
      if($this->validarCorreo($correo)){
        $this->sendLink($correo);
      }else{
        $this->session->set_flashdata('_message','<p class="alert alert-danger text-center"><strong class="text-white">El Correo Ingresado No existe...</strong></p>');
        redirect('PasswordReset','refresh');
      }
    }else{
      $this->session->set_flashdata('_message','<p class="alert alert-danger text-center"><strong class="text-white">Ingresa un correo válido</strong></p>');
      redirect('PasswordReset','refresh');
    }
  }

  public function validarCorreo($correo){
      $this->db->select('userid');
      $this->db->where('email',$correo);
      $query=$this->db->get('adm.users');
      if($query->num_rows()>0){
        $this->load->helper('string');
        $dataToken['token']=random_string('md5',8);
        $dataToken['email']=$correo;
        $this->db->insert('adm.userToken',$dataToken);
        return TRUE;
      }
      return FALSE;
  }

  public function sendLink($correo){
    $this->load->library('email');
    $this->db->select('*');
    $this->db->where('funcion','Password Reset');
    $query=$this->db->get('adm.emailNotification');
    $arreglo=array();
    if($query->num_rows()>0){
      foreach ($query->result() as $value) {
        $arreglo['password']=$this->encrypt->decode($value->password);
        $arreglo['server']=strtolower($value->server);
        $arreglo['port']=$value->port;
        $arreglo['method']=strtolower($value->method);
        $arreglo['email']=strtolower($value->email);
      }
      $configuracion=array(
        'protocol'=>'smtp',//$arreglo['method'],
        'smtp_host'=>'smtp.zoho.com',//$arreglo['server'],
        'smtp_port'=>587,//$arreglo['port'],
        'smtp_user'=>'hrodriguez@mpaycenter.com',//$arreglo['email'],
        'smtp_pass'=>'123456789',//$arreglo['password'],
        'mailtype'=>'html',
        'charset'=>'utf-8',
        'newline'=>"\r\n",
        'validate'=>true,
        'wordwrap'=>true,
        'smtp_crypto'=>'tls',
      );

      $this->email->initialize($configuracion);
      $data['token']='';
      $this->db->select('token');
      $this->db->where('email',$correo);
      $token=$this->db->get('adm.userToken');
      if($token->num_rows()>0){
        foreach ($token->result() as $value) {
          $data['token']=$value->token;
        }

        $data['email']=$correo;
        $data['link']='<a href="'.base_url().'PasswordReset/ChangePass/'.$correo.'/'.$data['token'].'">enlace</a>';
        $msn=$this->load->view('paper/msnPasswordRest',$data,true);

        $this->email->from('hrodriguez@mpaycenter.com','Banco Nacional de Panamá');
        $this->email->to($correo);
        $this->email->subject('PASSWORD RESET');
        $this->email->message($msn);
        $this->email->send(TRUE);
        $this->session->set_flashdata('_message','<blockquote><p class="text-success">Correo enviado, revisa tu bandeja de entrada.</p></blockquote>');
        redirect('PasswordReset','refresh');
      }else{
        echo "no existe Token";
      }
    }else{
      echo "no se envio mensaje";
      echo "<br>No habia correo con esta funcion";
    }
  }

  public function ChangePass($correo,$token){
    if($this->isSetToken($correo,$token)){
      $data['email']=$correo;
      $data['token']=$token;
      $this->load->view('paper/formInsertNewPass',$data);
    }
  }

  public function isSetToken($correo,$token){
    $this->db->select('*');
    $this->db->where('email',$correo);
    $this->db->where('token',$token);
    $query=$this->db->get('adm.userToken');
    if($query->num_rows()>0){
      return TRUE;
    }
    return FALSE;
  }

  public function InsertPass(){
    $correo=NULL;
    $token=NULL;
    if($this->input->post('token')){
        $token=$this->input->post('token');
    }
    if($this->input->post('email')){
      $correo=$this->input->post('email');
    }

    $pass1=$this->input->post('pass1');
    $pass2=$this->input->post('pass2');

    $rules=array(

          'pass1'=>array('field'=>'pass1','label'=>'Contraseña','rules'=>'required|matches[pass2]|callback_regex_match|max_length[16]|min_length[8]|callback_noContiene'),
          'pass2'=>array('field'=>'pass2','label'=>'Confirmar','rules'=>'required|matches[pass1]')

        );

        $data['password']=$this->encrypt->encode($pass1);
        $data['changePassDate']=date('Y-m-d');


    $this->form_validation->set_rules($rules);
    $this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
    if($this->form_validation->run()==TRUE){
        if($this->isSetToken($correo,$token)){
          $this->db->where('email',$correo);
          $this->db->update('adm.users',$data);

          $this->db->where('email',$correo);
          $this->db->delete('adm.userToken');

          $this->session->set_flashdata('_message','<blockquote><p class="text-success">Contraseña Actualizada, Favor Inicie Sesión</p></blockquote>');
          redirect('Login','refresh');
        }
    }else{
      $data['email']=$correo;
      $data['token']=$token;
      $this->load->view('paper/formInsertNewPass',$data);
    }

  }

  public function regex_match($str){
     if(preg_match('/^(?=(?:.*\d){1})(?=(?:.*[A-Z]){1})(?=(?:.*[a-z]){1})\S{8,}$/',$str)){
       return TRUE;
     }else{
       $this->form_validation->set_message('regex_match', 'El campo de contraseña debe tener al menos: <ul> <li> (1) caracter Mayuscula </li> <li> (1) carcater Numerico </li><li> (8) carcateres mínimos </li><li> (16) carcateres máximos </li></ul>');
       return FALSE;
     }
  }

	public function noContiene($str){
		$palabrasProhibidas=array('BNP','contraseña','password','Password123','password123');

		foreach ($palabrasProhibidas as $palabrasProhibida) {
			$pos = preg_match("/$palabrasProhibida/i", $str);
			if($pos){
				$this->form_validation->set_message('noContiene', 'La contaseña no puede contener la palabra : '.$palabrasProhibida);
				return FALSE;
			}
		}
		return TRUE;
	}

}
?>

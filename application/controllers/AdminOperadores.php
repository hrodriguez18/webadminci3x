<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class AdminOperadores extends MY_Controller{

	public $permiso_crear=39;
	public $permiso_actualizar=40;
	public $permiso_eliminar=41;
	public $permiso_ver=1;



	public $contenido=array();
	protected $dataForm=array('title_form'=>'Datos Operadores');


	public function __construct(){
        parent::__construct();
        $this->load->model('operadores_m');

    }

    public function index(){

    	$this->contenido['table']=$this->load->view("paper/tables/table_operadores",'',true);
    	$this->contenido['title']=$this->lang->line('operators');
		$datos["content_view"]=$this->load->view("paper/admin/paper_view_operadores",$this->contenido,true);
		$datos['navbar_menu']=$this->navbar_header_menu();
		$datos['sidebar_menu']=$this->menu_admin();
		$datos['footer']=$this->load->view("paper/footers/footer_operadores",'',true);

		$this->load->view("paper/base",$datos);

    }
    public function populateTable(){
    	echo json_encode($this->operadores_m->getDataForOperadores());
    }

    public function Form(){
    	$this->dataForm['form']=$this->operadores_m->Form();
    	echo $this->load->view("paper/forms_modal/form_operadores",$this->dataForm,true);
    }

    public function saveData(){
		$this->form_validation->set_rules($this->operadores_m->rules);
		$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
		if($this->form_validation->run()==TRUE){
			$data['Descripcion']=$this->input->post('Descripcion');
			$data['Pais']=$this->input->post('Pais');
			$data['canalId']=$this->input->post('Canal');
			$data['Color']=$this->input->post('Color');
			$data['Estado']=$this->input->post('status');
			$this->operadores_m->save($data);
			echo "Todo bien!";
		}else{
			$this->dataForm['form']=$this->operadores_m->Form();

    		echo $this->load->view("paper/forms_modal/form_operadores",$this->dataForm,true);
		}
	}

	public function EditData(){
		$id=$this->input->post('Id');
		$data_by_id=$this->operadores_m->get($id,true);

		$dataForm['form']=$this->operadores_m->FormForEdit($data_by_id);
		echo $this->load->view("paper/forms_modal/form_operadores_edit",$dataForm,true);
		//var_dump($id);

	}

	public function UpdateData(){
		$this->form_validation->set_rules($this->operadores_m->rules);
		$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
		if($this->form_validation->run()==TRUE){
			$data['Descripcion']=$this->input->post('Descripcion');
			$data['Pais']=$this->input->post('Pais');
			$data['canalId']=$this->input->post('Canal');
			$data['Color']=$this->input->post('Color');
			$data['Estado']=$this->input->post('status');
			$this->operadores_m->save($data,$this->input->post('Cod_Operador'));
			echo 'Todo bien!';
		}else{
			$id=$this->input->post('Cod_Operador');
			$data_by_id=$this->operadores_m->get($id,true);
			$dataForm['form']=$this->operadores_m->FormForEdit($data_by_id);
			echo $this->load->view("paper/forms_modal/form_operadores_edit",$dataForm,true);

		}
	}

	public function DeleteData(){
		$id=$this->input->post('id');
		$this->operadores_m->delete($id);
		echo '<p class="label label-success">'.$this->lang->line('error_delete').'<strong>'.$id.'</strong></p>';

	}

	public function Export(){
		$this->operadores_m->Export();
	}
}
?>

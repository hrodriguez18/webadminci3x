<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class RolesSistema extends MY_Controller {

	public $permiso_crear=35;
	public $permiso_actualizar=36;
	public $permiso_eliminar=37;
	public $permiso_ver=4;

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		$this->load->model('RolesSistemaModel','mrol');
		$this->load->model('RelationRolesPermisosModel','mrel');
	}
	public function index()
	{
		$datos['navbar_menu']=$this->navbar_header_menu();
		$this->load->view('paper/admin/paper_view_roles_sistema',$datos);
	}

	public function populateTable(){
		echo json_encode($this->mrol->get());
	}

	public function Form(){
		$data['ListaPermisos']=$this->mrol->ListaPermisos();
		echo $this->load->view('paper/htmlForm/FormRoles',$data,true);
	}

	public function Save(){
		$this->form_validation->set_rules($this->mrol->_rules);
		$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
		if($this->form_validation->run()==TRUE){
			$rol['status']=$this->input->post('status');
			$rol['Descripcion']=$this->input->post('descripcion');
			$permisos=$this->input->post('info');
			if($permisos[0]!=NULL){
				$rol_permiso['rol_id']=$this->mrol->save($rol);

				for($i=0;$i<count($permisos); $i++){
					$rol_permiso['permiso_id']=$permisos[$i];
					$this->mrel->save($rol_permiso);
				}
				echo "Todo bien!";
			}else{
				$this->Form();
			}

		}else{
			$data['ListaPermisos']=$this->mrol->ListaPermisos();
			echo $this->load->view('paper/htmlForm/FormRoles',$data,true);
		}
	}

	public function Editar(){
		$id=$this->input->post('id');
		$s=$this->mrol->get($id,TRUE);
		$infoRol=array();
		foreach ($s as $key => $value) {
			$infoRol[$key]=$value;
		}

		$permisosAgregados=$this->mrel->getPermisosAgregado($id);
		$arregloPermisosSeteados=array();
		foreach ($permisosAgregados as $key => $value) {
			array_push($arregloPermisosSeteados, $value->permiso_id);
		}

		$infoRol['PermisosSeteados']=$this->mrol->permisosSeteados($arregloPermisosSeteados);
		$infoRol['PermisosDisponibles']=$this->mrol->PermisosDisponibles($arregloPermisosSeteados);

		echo $this->load->view('paper/htmlForm/FormRolesE',$infoRol,true);
		//var_dump($arregloPermisosSeteados);

	}

	public function Update(){
		$this->form_validation->set_rules($this->mrol->_rules);
		$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
		if($this->form_validation->run()==TRUE){

			$rol['status']=$this->input->post('status');
			$rol['Descripcion']=$this->input->post('descripcion');
			$id=$this->input->post('id');
			$permisos=$this->input->post('info');

			$this->mrol->save($rol,$id);

			$this->mrel->Clean($id);
			$rol_permiso['rol_id']=$id;
			for($i=0;$i<count($permisos); $i++){
				$rol_permiso['permiso_id']=$permisos[$i];
				$this->mrel->save($rol_permiso);
			}
			echo "Todo bien!";
		}else{

			echo $this->load->view('paper/htmlForm/FormRolesE',$this->input->post(),true);
		}


	}

	public function Delete(){
		$id=$this->input->post('id');
		$this->mrel->Clean($id);
		$this->mrol->delete($id);

		echo '<p class="label label-success">'.$this->lang->line('error_delete').'<strong>'.$id.'</strong></p>';

	}

}

/* End of file RolesSistema.php */
/* Location: ./application/controllers/RolesSistema.php */

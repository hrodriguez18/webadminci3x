<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class AdminProcesadores extends MY_Controller{

	public $contenido=array();
	protected $dataForm=array('title_form'=>'Datos Procesadores');

	public $permiso_crear=55;
	public $permiso_actualizar=56;
	public $permiso_eliminar=75;
	public $permiso_ver=9;


	public function __construct(){
        parent::__construct();
        $this->load->model('procesadores_m');

    }

    public function index(){

    	$this->contenido['table']=$this->load->view("paper/tables/table_procesadores",'',true);
    	$this->contenido['title']=$this->lang->line('billers');
		$datos["content_view"]=$this->load->view("paper/admin/paper_view_procesadores",$this->contenido,true);
		$datos['navbar_menu']=$this->navbar_header_menu();
		$datos['sidebar_menu']=$this->menu_admin();
		$datos['footer']=$this->load->view("paper/footers/footer_procesadores",'',true);

		$this->load->view("paper/base",$datos);

    }
    public function populateTable(){
    	echo json_encode($this->procesadores_m->get());
    }

    public function Form(){

    	echo $this->load->view("paper/forms_modal/form_procesadores",$this->dataForm,true);
    }

    public function saveData(){
		$this->form_validation->set_rules($this->procesadores_m->rules);
		$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
		if($this->form_validation->run()==TRUE){

			$data['Descripcion']=$this->input->post('Descripcion');
			$this->procesadores_m->save($data);
			echo "Todo bien!";
		}else{

    		echo $this->load->view("paper/forms_modal/form_procesadores",$this->dataForm,true);
		}
	}

	public function EditData(){
		$id=$this->input->post('Id');
		$dataForm=$this->procesadores_m->get($id,true);
		echo $this->load->view("paper/forms_modal/form_procesadores_edit",$dataForm,true);
		//var_dump($id);

	}

	public function UpdateData(){
		$this->form_validation->set_rules($this->procesadores_m->rules);
		$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
		if($this->form_validation->run()==TRUE){


			$data['Descripcion']=$this->input->post('Descripcion');
			$this->procesadores_m->save($data,$this->input->post('Cod_Biller'));
			echo 'Todo bien!';
		}else{
			$dataForm['Descripcion']=$this->input->post('Descripcion');
			$dataForm['Cod_Biller']=$this->input->post('Cod_Biller');
			echo $this->load->view("paper/forms_modal/form_procesadores_edit",$dataForm,true);

		}
	}

	public function DeleteData(){
		$id=$this->input->post('id');
		$this->procesadores_m->delete($id);
		echo '<p class="label label-success">'.$this->lang->line('error_delete').'<strong>'.$id.'</strong></p>';

	}
}
?>

<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class test extends CI_Controller {

	public function index(){
		$permisos=$this->session->userdata('permisos');
		$misPermisos=array();
		foreach ($permisos as $permiso){
			array_push($misPermisos,$permiso);
		}
		foreach ($misPermisos as $value){
			echo $value['controller'].' -> '.$value['method'].'<br>';
		}
	}

	public function getMisPermisos(){
		$id_rol=$this->session->userdata('rol_id');
		$this->db->select('roles_permisos.rol_id,roles_permisos.permiso_id');
		$this->db->select('permisos.Descripcion as D');
		$this->db->join('adm.permisos','roles_permisos.permiso_id=permisos.Id','left');
		$this->db->where('rol_id',$id_rol);
		$query=$this->db->get('adm.roles_permisos');
		$permisos=array();
		foreach ($query->result() as $value) {
			$permisos[$value->permiso_id]=$value->D;
		}
		var_dump($permisos);
	}

	public function populatePermisos(){
		$data=array('method'=>'save');
		$this->db->like('Descripcion','Agregar');
		$this->db->or_like('Descripcion','Editar');
		$this->db->update('adm.permisos',$data);


		$dat=array('method'=>'delete');
		$this->db->like('Descripcion','Eliminar');
		$this->db->update('adm.permisos',$dat);


		$pap=array('method'=>'index');
		$this->db->like('Descripcion','ver');
		$this->db->update('adm.permisos',$pap);

	}

	public function CuandoExpira(){
		$fecha=$this->session->userdata('expire_session');
		echo date('Y-m-d H:i:s',$fecha).'<br>';
		echo $fecha.'<br>';
	}

}

/* End of file Controllername.php */

?>

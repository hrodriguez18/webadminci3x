<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class AdminPermisosTerminal extends MY_Controller{

	public $contenido=array();
	protected $dataForm=array('title_form'=>'Datos Permisos Terminal');


	public function __construct(){
        parent::__construct();
        $this->load->model('adminPermisosTerminal_m');

    }

    public function index(){

    	$this->contenido['table']=$this->load->view("paper/tables/table_PermisoTerminal",'',true);
    	$this->contenido['title']='Bines Bloqueados';
		$datos["content_view"]=$this->load->view("paper/admin/paper_view_permisosTerminal",$this->contenido,true);
		$datos['navbar_menu']=$this->navbar_header_menu();
		$datos['sidebar_menu']=$this->menu_admin();
		$datos['footer']=$this->load->view("paper/footers/footer_adminPermisosTerminal",'',true);

		$this->load->view("paper/base",$datos);

    }
    public function populateTable(){
    	echo json_encode($this->binterminal->PopulateTable());
    }

    public function Form(){

    	$this->dataForm['listaBin']=$this->binterminal->BinDisponibles();
    	echo $this->load->view("paper/forms_modal/form_PermisosTerminal",$this->dataForm,true);
    }

    public function saveData(){
		$this->form_validation->set_rules($this->binterminal->rules);
		$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
		if($this->form_validation->run()==TRUE){



			$data['grupo']=$this->input->post('grupo');
			$data['bine']=$this->input->post('info');
			$data['descripcion']=$this->input->post('descripcion');

			$idGrupo=$this->binterminal->InsertarGrupo($data['grupo']);

			for($i=0;$i<count($data['bine']); $i++){
				$rel['grupoBinId']=$idGrupo;
				$rel['binId']=$data['bine'][$i];
				$rel['descripcion']=$data['descripcion'];
				$this->binterminal->save($rel);
			}

			echo "Todo bien!";
		}else{

			$this->dataForm['listaBin']=$this->binterminal->BinDisponibles();
    		echo $this->load->view("paper/forms_modal/form_binterminal",$this->dataForm,true);
		}
	}

	public function EditData(){
		$id=$this->input->post('Id');
		$data_by_id=$this->binterminal->InfoBinByGroup($id);
		//$dataForm['form']=$this->binterminal->FormForEdit($data_by_id);
        //$dataForm['id_binTerminal']=$id;
		echo $this->load->view("paper/forms_modal/form_binterminal_edit",$data_by_id,true);
		//var_dump($data_by_id);

	}

	public function UpdateData(){
		$this->form_validation->set_rules($this->binterminal->rules);
		$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
		if($this->form_validation->run()==TRUE){

			$this->binterminal->LimpiarBinesBloqueados($this->input->post('idGrupo'));
			//$data['grupo']=$this->input->post('grupo');
			$data['bine']=$this->input->post('info');
			$data['descripcion']=$this->input->post('descripcion');

			//$idGrupo=$this->binterminal->InsertarGrupo($data['grupo']);

			for($i=0;$i<count($data['bine']); $i++){
				$rel['grupoBinId']=$this->input->post('idGrupo');
				$rel['binId']=$data['bine'][$i];
				$rel['descripcion']=$data['descripcion'];
				$this->binterminal->save($rel);
			}
			echo "Todo bien!";
		}else{

			$data_by_id=$this->binterminal->InfoBinByGroup($this->input->post('idGrupo'));
			echo $this->load->view("paper/forms_modal/form_binterminal_edit",$data_by_id,true);
		}
	}

	public function DeleteData(){
		$id=$this->input->post('id');
		$this->binterminal->LimpiarBinesBloqueados($id);
		if($this->binterminal->BorrarBinTerminal($id)){
			echo '<p class="label label-success">'.$this->lang->line('error_delete').'<strong>'.$id.'</strong></p>';

		}

	}




}
?>

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SupportWallet extends MY_Controller{

  public $permiso_ver=155;
  public $mymodel='Monedero_m';

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
  }

  public function index()
  {
    $data['navbar_menu']=$this->navbar_header_menu();
    $data['titleMenu']=$this->lang->line('Monedero');
    $this->load->view('paper/Support/_layoutSupportMonedero',$data);
  }

  public function Search(){
    $telefono=$this->input->post('telefono');
    $fecha=$this->input->post('fecha');
    $cedula=$this->input->post('cedula');
    //$ultimo=$this->input->post('lastTransaction');
    //Buscamos el usuario;
    $this->db->select('*');
    $this->db->where('email',$telefono);
    $this->db->where('cedula',$cedula);
    $this->db->where('fnacimiento',$fecha);
    $this->db->limit(1);
    $query=$this->db->get('esb.usuariosMonedero');
    $id=0;
    if($query->num_rows()==0){
      $this->session->set_flashdata('message','<p class="alert alert-warning">No se encontró resultado...</p>');
      redirect('SupportWallet','refresh');
    }else{
      foreach ($query->result() as $value) {
        $id=$value->id;
        $data['name']=$value->name.' '.$value->lastname;

	  }
		$email=$value->email;
		//$this->load->model('Monedero_m');
		$userMonedero=new Monedero_m();
		//$userMonedero->_inputs['submit']=array('id'=>array('type'=>'submit','value'=>'Actualizar','class'=>'btn btn-success'));
		$data['form']=$userMonedero->form('lg',$id);
		$data['navbar_menu']=$this->navbar_header_menu();
		$data['phone']=$telefono;
		$data['id']=$id;
		$data['titleMenu']=$this->lang->line('Monedero');
		$this->load->view('paper/Support/_layoutResult',$data);

    }


  }


  public function SetToken(){
      $phone=$this->input->post('phone');
      $name=$this->input->post('name');

      $id=$this->input->post('id');

      $this->db->select('email');
      $this->db->where('id',$id);
      $data=$this->db->get('esb.usuariosMonedero');
		foreach($data->result() as $item){
			$phone=$item->email;
		}

      $respuesta=$this->SendToken($phone,$name);
      if($respuesta->error==0){
        echo 'ok';
      }else{
        echo '<p class="alert alert-danger">Token no enviado, Hubo un error : <strong>'.$respuesta->content.'</strong></p>';
      }

  }


  public function ResetPIN(){
	  $phone=$this->input->post('phone');
	  $name=$this->input->post('name');

	  $id=$this->input->post('id');

	  $this->db->select('email');
	  $this->db->where('id',$id);
	  $data=$this->db->get('esb.usuariosMonedero');
	  foreach($data->result() as $item){
		  $phone=$item->email;
	  }



	  $respuesta=$this->SendPIN($phone,$name);
	  if($respuesta->error==0){
		  echo 'ok';
	  }else{
		  echo '<p class="alert alert-danger">PIN no enviado, Hubo un error : <strong>'.$respuesta->content.'</strong></p>';
	  }
  }

  private function SendPIN($phone,$name){
	  $this->load->library('Sms');
	  $numberSended=rand(111111,999999);
	  $templateMsn='ResetPin';
	  $sms= new Sms();
	  $sms->phone=$phone;
	  $sms->personName=$name;
	  $sms->templateMsn=$templateMsn;
	  $sms->token=$numberSended;
	  $response=$sms->send();

	  if($response->error==1){
		  $dataToinsert['error']=$response->error;
		  $dataToinsert['content']=$response->content;
	  }
	  if($response->error==0){
		  $dataToinsert['id']=$response->id;
		  $dataToinsert['error']=$response->error;
		  $dataToinsert['status_id']=$response->status_id;
		  $dataToinsert['last_change']=$response->last_change;
		  $dataToinsert['content']=$response->content;
	  }
	  $dataToinsert['tokenSended']=$this->encrypt->encode($numberSended);
	  $this->db->insert('esb.responseApiSmsRest',$dataToinsert);

	  $data['password']=md5($numberSended);

	  $this->db->where('email',$phone);
	  $this->db->update('esb.usuariosMonedero',$data);

	  return $response;
  }


  private function SendToken($phone,$name){
    $this->load->library('Sms');
    $numberSended=rand(111111,999999);
    $templateMsn='restablecerPass';
    $sms= new Sms();
    $sms->phone=$phone;
    $sms->personName=$name;
    $sms->templateMsn=$templateMsn;
    $sms->token=$numberSended;
		$response=$sms->send();

    if($response->error==1){
      $dataToinsert['error']=$response->error;
      $dataToinsert['content']=$response->content;
    }
    if($response->error==0){
      $dataToinsert['id']=$response->id;
      $dataToinsert['error']=$response->error;
      $dataToinsert['status_id']=$response->status_id;
      $dataToinsert['last_change']=$response->last_change;
      $dataToinsert['content']=$response->content;
    }
    $dataToinsert['tokenSended']=$this->encrypt->encode($numberSended);
    $this->db->insert('esb.responseApiSmsRest',$dataToinsert);

    $data['registerToken']=md5($numberSended);

    $this->db->where('email',$phone);
    $this->db->update('esb.usuariosMonedero',$data);

    return $response;

  }


}

<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class FaqsView extends CI_Controller {

	public function index()
	{
		redirect("Login","refresh");
	}

	public function FaqsCnb(){


		$data['title']=$this->lang->line('TitleFaqsCnb');
		$data['fullText']=$this->PreguntasFrecuentesTipo('CNB');
		$this->load->view('Paper/Faqs',$data);

	}

	public function FaqsBilletera(){
		$data['title']=$this->lang->line('TitleFaqsBilletera');
		$data['fullText']=$this->PreguntasFrecuentesTipo('Billetera');
		$this->load->view('Paper/Faqs',$data);
	}

	private function PreguntasFrecuentesTipo($nameTipo){
		$tipo=0;
		$parrafos='';
		if(strlen($nameTipo)>0){
			if($nameTipo=='CNB'){
				$tipo=1;
			}
			if($nameTipo=='Billetera'){
				$tipo=2;
			}

		}
		$this->db->select('*');
		if($tipo>0){
			$this->db->where('tipo',$tipo);
		}
		$this->db->where('Estado','A');

		$query=$this->db->get('esb.FAQ');

		if($query->num_rows()>0){
			foreach ($query->result() as $item) {
				$parrafos.='<div><h4 class="text-center "><strong class="text-info">'.$item->Titulo.'</strong></h4><p><strong class="text-danger">Respuesta : </strong><br>'.$item->Texto.'</p></div>';
			}
		}

		return $parrafos;

	}

}

/* End of file Controllername.php */

?>

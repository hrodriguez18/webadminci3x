<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AdminLimitesTransacciones extends MY_Controller {

	public $permiso_crear=119;
	public $permiso_actualizar=89;
	public $permiso_eliminar=90;
	public $permiso_ver=88;

	public $_vista='paper_view_adminLimitesTransacciones';

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		$this->load->model('LimitesTransaccionesModel');
	}

	public function index()
	{
		$data['navbar_menu']=$this->navbar_header_menu();
		$this->load->view('paper/admin/'.$this->_vista,$data);
	}

	public function PopulateTable(){
		echo json_encode($this->LimitesTransaccionesModel->PopulateTable());
	}

	public function Form(){
		echo $this->LimitesTransaccionesModel->Form('sm');
	}

	public function Save(){
		$data=array();
		$inputs=$this->LimitesTransaccionesModel->_inputs;
		unset($inputs['submit']);
		$this->form_validation->set_rules($this->LimitesTransaccionesModel->_rules);
		$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');

		if($this->form_validation->run()==TRUE){
			foreach ($inputs as $key) {
				$data[$key['name']]=$this->input->post($key['name']);
			}
			$id=$data['id'];
			if($id!=''){
				unset($data['id']);
				$this->LimitesTransaccionesModel->save($data,$id);
			}else{
				unset($data['id']);
				$this->LimitesTransaccionesModel->save($data);
			}
			echo "Todo bien!";
		}else{
			echo $this->LimitesTransaccionesModel->Form('sm');
		}
	}


	public function Edit(){
		$id=$this->input->post('id');
		echo $this->LimitesTransaccionesModel->Form('sm',$id);
	}

	public function Delete(){
		$id=$this->input->post('id');
		$this->LimitesTransaccionesModel->delete($id);
		echo '<p class="label label-success">'.$this->lang->line('error_delete').'<strong>'.$id.'</strong></p>';

	}

}

/* End of file AdminLimitesTransacciones.php */
/* Location: ./application/controllers/AdminLimitesTransacciones.php */

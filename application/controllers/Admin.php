<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MY_Controller{


	public function __construct(){
        parent::__construct();
        $this->load->model('adminmodel');
        $this->load->model('moneda_m');
        $this->load->model('pais_m');
        $this->load->library('upload');
        $this->load->helper('date');

    }
	public function index(){
		$this->load->model('DashboardModel');
		$dashBoard=new DashboardModel();
		$year=date('Y');
		$mes=date('m');
		$pais='PAN';
		$lastDay=days_in_month($mes, $year);
		$fechaInicio=$year.'-'.$mes.'-01 00:00:00';
		$fechaFin=$year.'-'.$mes.'-'.$lastDay.' 23:59:00';
		$dashBoard->_tablename='esb.transactionLog';
		$dashBoard->_order_by='id';
		$dashBoard->_order='DESC';
		$dashBoard->_limit=0;
		$dashBoard->_timestamps='stamp';
		$dashBoard->_fechaInicio=$fechaInicio;
		$dashBoard->_fechaFin=$fechaFin;
		$datos_content['contadores']=$dashBoard->getAllCounter();
		$datos_content['formforCounters']=$dashBoard->FormSearchCountersAdmin();
		$datos_content['rutaUpdateKPI']='Admin/UpdateKPI';
		$datos_content['rutaSetGraph']='Admin/UpdateGraphic';
		$datos_content['controlador']='Admin';
		//$datos['footer']=$this->load->view("paper/footers/footer",'',true);
		$datos["content_view"]=$this->load->view("paper/admin/content_Admin",$datos_content,true);
		$datos['navbar_menu']=$this->navbar_header_menu();
		$datos['sidebar_menu']=$this->load->view('paper/menupaper/menu_admin','',true);

		$this->load->view("paper/base",$datos);


	}






	public function UpdateKPI(){

		$this->load->model('DashboardModel');
		echo $this->DashboardModel->getAllCounter();

	}

	public function UpdateGraphic(){
		if($this->input->post('year')){
			$year=$this->input->post('year');
		}else{
			$year=2018;
		}

		$this->load->model('DashboardModel');

		$objCnb=new DashboardModel();
		$objCnb->_tablename='esb.transactionLog';

		$objCnb->_order='DESC';
		$objCnb->_order_by='id';
		$objCnb->_limit=0;
		$objCnb->_timestamps='stamp';

		$objCnb->_fechaInicio=$year.'-01-01 00:00:00';
		$objCnb->_fechaFin=$year.'-12-31 23:59:00';
		$objMonedero=new DashboardModel();
		$objMonedero->_tablename='esb.transactionMonederoLog';
		$objMonedero->_order='DESC';
		$objMonedero->_order_by='id';
		$objMonedero->_limit=0;
		$objMonedero->_timestamps='stamp';

		$objMonedero->_fechaInicio=$year.'-01-01 00:00:00';
		$objMonedero->_fechaFin=$year.'-12-31 23:59:00';




		$allDataMonedero=json_decode($objMonedero->getData());
		$allDataCnb=json_decode($objCnb->getData());

		$meses=array();
		for ($i=1;$i<=12;$i++){
			$dia='';
			if($i<10){
				$dia='0';
			}
			array_push($meses,$year.'-'.$dia.$i);
		}

		$transacciones=array();
		$debitos=array();
		$creditos=array();

		$dataGraphic=array();

		$this->load->helper('date');
		$year=date('Y');



		//se obtienen los array de Transacciones por mes
		for($mes=0; $mes<count($meses); $mes++){
			$count=0;
			foreach ($allDataMonedero as $value){
				if(substr($value->stamp,0,7)==$meses[$mes]){
					$count++;
				}
			}

			foreach ($allDataCnb as $value){
				if(substr($value->stamp,0,7)==$meses[$mes]){
					$count++;
				}
			}

			array_push($transacciones,$count);
			$i='';
			if($mes<10){
				$i='0';
			}
			$mo=$mes;
			$mo+=1;

			$lastDay=days_in_month($mo, $year);
			$fechaInicio=$year.'-'.$i.$mo.'-01 00:00:00';
			$fechaFin=$year.'-'.$i.$mo.'-'.$lastDay.' 23:59:00';

			$this->db->select_sum('montoTrx');
			$this->db->where('aux','-');
			$this->db->where('stamp > ',$fechaInicio);
			$this->db->where('stamp < ',$fechaFin);
			$query=$this->db->get('esb.transactionMonederoLog')->result_array();
			$debitoMonedero=$query[0]['montoTrx'];

			$this->db->select_sum('montoTrx');
			$this->db->where('aux','+');
			$this->db->where('stamp > ',$fechaInicio);
			$this->db->where('stamp < ', $fechaFin);
			$query=$this->db->get('esb.transactionMonederoLog')->result_array();
			$creditoMonedero=$query[0]['montoTrx'];

			$this->db->select_sum('montoTrx');
			$this->db->where('aux','-');
			$this->db->where('stamp > ',$fechaInicio);
			$this->db->where('stamp < ', $fechaFin);
			$query=$this->db->get('esb.transactionLog')->result_array();
			$debitoCnb=$query[0]['montoTrx'];

			$this->db->select_sum('montoTrx');
			$this->db->where('aux','+');
			$this->db->where('stamp > ',$fechaInicio);
			$this->db->where('stamp < ', $fechaFin);
			$query=$this->db->get('esb.transactionLog')->result_array();
			$creditoCnb=$query[0]['montoTrx'];


			array_push($creditos,number_format($creditoMonedero+$creditoCnb,'2','.',''));

			array_push($debitos,number_format($debitoCnb+$debitoMonedero,'2','.',''));
		}

		//echo var_dump($meses).'<br>';
		//echo var_dump($transacciones).'<br>';
		//echo var_dump($creditos).'<br>';
		//echo var_dump($debitos).'<br>';


		for ($k=0;$k<12;$k++){
			array_push($dataGraphic,array('mes'=>$meses[$k],'transacciones'=>$transacciones[$k],'credito'=>$creditos[$k],'debito'=>$debitos[$k]));
		}


		//var_dump($dataGraphic);

		echo json_encode($dataGraphic);
	}


	public function MAPA(){

		$this->load->helper('date');

		if($this->input->post('year')){
			$year=$this->input->post('year');
		}else{
			$year=2018;
		}


		if($this->input->post('mes')){
			$mes=$this->input->post('mes');
		}else{
			$mes=date("m");
		}

		$lastDay=days_in_month($mes,$year);


		$this->load->model('DashboardModel');
		$dashBoard=new DashboardModel();
		$dashBoard->_tablename='esb.transactionLog';
		$dashBoard->_order='DESC';
		$dashBoard->_order_by='id';
		$dashBoard->_limit=0;
		$dashBoard->_timestamps='stamp';

		$dashBoard->_fieldsSelected=array(
			'transactionLog.id',
			'transactionLog.userMonedero',
			'transactionLog.terminalMonedero',
			'transactionLog.tipoTrx',
			'transactionLog.montoTotal',
			'transactionLog.montoTrx',
			'transactionLog.formaPago',
			'transactionLog.stamp',
			'transactionLog.cnbOperador',
			'transactionLog.cnbAgencia',
			'transactionLog.cnbUser',
			'transactionLog.cnbTerminal',
			'transactionLog.cuenta',
			'transactionLog.descripcionTrx',
			'transactionLog.aux',
			'transactionLog.agenciaCNBId',
			'transactionLog.cuentaComision',
			'transactionLog.montoComision',
			'transactionLog.currency',
			'transactionLog.terminalId',
			'transactionLog.lat',
			'transactionLog.lon',
			'transactionLog.email',
			'transactionLog.nombre',
			'Operadores.Color as color'
		);

		$dashBoard->_joins=array(
			array(
				'table'=>'esb.Operadores',
				'relation'=>'transactionLog.cnbOperador=Operadores.Descripcion',
				'type'=>'left'
			)
		);

		$dashBoard->_fechaInicio=$year.'-'.$mes.'-01 00:00:00';
		$dashBoard->_fechaFin=$year.'-'.$mes.'-'.$lastDay.' 23:59:00';

		$allData=json_decode($dashBoard->getData());
		$points=array();
		$i=0;
		foreach ($allData as $value){
			$points[$i]['location']=array('lat'=>floatval($value->lat),'lng'=>floatval($value->lon));
			$points[$i]['description']='<p class="text-info">Fecha: '.$value->stamp.'</p>';

			if($value->nombre != ''){
				$points[$i]['description'].='<p class="text-info">Usuario: '.$value->userMonedero.' ('.$value->nombre.')</p>';
			}else{
				$points[$i]['description'].='<p class="text-info">Usuario: '.$value->userMonedero.'</p>';
			}

			$points[$i]['description'].='<p class="text-info">Descripción: '.$value->descripcionTrx.'</p>';
			$points[$i]['description'].='<p class="text-info">Monto: '.$value->currency.' '.$value->montoTrx.'</p>';
			$points[$i]['description'].='<p class="text-info">Forma de Pago: '.$value->formaPago.'</p>';

			$points[$i]['title']=$value->descripcionTrx;

			$points[$i]['color']=str_replace('#','',$value->color);
			//$points[$i]['color']=$value->color;

			$i++;
		}

		echo json_encode($points);

	}

	public function unico($str){
		return $this->moneda_m->unico($str);
	}

	public function Monedas(){
		$info=array('bodyTable'=>$this->moneda_m->RefreshTableMoneda());
		$datos_content['tableMonedas']=$this->load->view("paper/tables/monedastable",$info,true);
		$datos["content_view"]=$this->load->view("paper/admin/paper_view_monedas",$datos_content,true);
		$datos['navbar_menu']=$this->navbar_header_menu();
		$datos['sidebar_menu']=$this->menu_admin();
		$datos['footer']=$this->load->view("paper/footers/footer_monedas",'',true);
		$this->load->view("paper/base",$datos);
	}

	public function RefreshTable(){

		echo $this->moneda_m->RefreshTableMoneda();
	}

	public function ReloadTableMoneda(){
		echo json_encode($this->moneda_m->get());

	}

	public function Modal(){
		$form=$this->input->post('form');
		if($form=='Moneda'){
			echo $this->load->view("paper/forms_modal/form_moneda",'',TRUE);
		}
	}

	public function EditarElemento(){
		$id=$this->input->post('id');
		$datos=$this->moneda_m->get($id);
		echo $this->load->view("paper/forms_modal/form_moneda_edit",$datos,true);
	}

	public function UpdateMoneda(){
		$reglas=$this->moneda_m->rules_update;
		$this->form_validation->set_rules($reglas);
		$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
		$datos=array('Cod_Moneda'=>$this->input->post('Cod_Moneda'),
						'Descripcion'=>$this->input->post('Descripcion'),
						'DescripcionCorta'=>$this->input->post('DescripcionCorta')
						);
		if($this->form_validation->run()==TRUE){
			if($this->moneda_m->save($datos,$datos['Cod_Moneda'])){
				echo '';
			}
		}else{
			echo $this->load->view("paper/forms_modal/form_moneda_edit",$datos,true);
		}

	}

	public function DeleteMoneda(){
		$id=$this->input->post('id');
		$this->moneda_m->delete($id);
		echo "Has eliminado el elemento con ID: ".$id;
	}

	public function SaveMoneda(){
		$reglas=$this->moneda_m->rules;
		$this->form_validation->set_rules($reglas);
		$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
		if($this->form_validation->run()==TRUE){
			$datos=array('Cod_Moneda'=>$this->input->post('Cod_Moneda'),
						'Descripcion'=>$this->input->post('Descripcion'),
						'DescripcionCorta'=>$this->input->post('DescripcionCorta')
						);

			if($this->moneda_m->save($datos)){
				echo '';
			}



		}else{
			echo $this->load->view("paper/forms_modal/form_moneda",'',true);
		}

	}

	//FUNCIONES PARA PAISES
	//=================================================================================

	public function Paises(){

		$datos_content['tablePaises']=$this->load->view("paper/tables/paisestable",'',true);
		$datos["content_view"]=$this->load->view("paper/admin/paper_view_paises",$datos_content,true);
		$datos['navbar_menu']=$this->navbar_header_menu();
		$datos['sidebar_menu']=$this->menu_admin();
		$datos['footer']=$this->load->view("paper/footers/footer_paises",'',true);
		$this->load->view("paper/base",$datos);

	}

	public function ReloadTablePais(){
		echo json_encode($this->pais_m->get());

	}

	public function populateTablePais(){
		echo json_encode($this->pais_m->PopulateTable());
	}



	public function formPais(){
		$data=array('form'=>$this->pais_m->FormularioPais());
		echo $this->load->view("paper/forms_modal/form_paises",$data,true);
	}

	public function SavePais(){

		if (empty($_FILES['bandera']['name']))
		{
		    $this->form_validation->set_rules('bandera', 'Bandera', 'required');
		}

		$this->form_validation->set_rules($this->pais_m->rules);
		$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
		if($this->form_validation->run()==TRUE){
			$data=array();
			$config['upload_path']='assets/uploads/files';
			$config['allowed_types'] = 'gif|jpg|png';
			$config['max_size'] = '2000';
			$this->upload->initialize($config);
			$this->upload->do_upload('bandera');
			$file_info=$this->upload->data();
			$data['Cod_Pais']=$this->input->post('cod_pais');
			$data['Descripcion']=$this->input->post('descripcion');
			$data['Lat1']=$this->input->post('lat1');
			$data['Lat2']=$this->input->post('lat2');
			$data['Lon1']=$this->input->post('lon1');
			$data['Lon2']=$this->input->post('lon2');
			$data['Cod_Moneda']=$this->input->post('moneda');
			$data['Bandera']=$file_info['file_name'];
			$this->pais_m->save($data);
			echo "Todo bien!";
		}else{
			$form=array('form'=>$this->pais_m->FormularioPais());
			echo $this->load->view("paper/forms_modal/form_paises",$form,true);
		}
	}

	public function unico_pais($str){
		return $this->pais_m->unico_pais($str);
	}

	public function EditarPais(){
		$id=$this->input->post('id');
		$datos=$this->pais_m->get($id,TRUE);
		$form['form']=$this->pais_m->FormularioPaisEdit($datos);
		echo $this->load->view("paper/forms_modal/form_paises_edit",$form,true);

	}

	public function DeletePais(){
		$id=$this->input->post('id');
		$this->pais_m->delete($id);
		echo "Has eliminado el elemento con ID: ".$id;
	}

	public function UpdatePais(){

		$data=array();

		$this->form_validation->set_rules($this->pais_m->rules_update);
		$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
		if($this->form_validation->run()==TRUE){
			$data=array();
			$config['upload_path']='assets/uploads/files';
			$config['allowed_types'] = 'gif|jpg|png';
			$config['max_size'] = '2000';
			$this->upload->initialize($config);
			$this->upload->do_upload('bandera');
			$file_info=$this->upload->data();

			$data['Cod_Pais']=$this->input->post('cod_pais');
			$data['Descripcion']=$this->input->post('descripcion');
			$data['Lat1']=$this->input->post('lat1');
			$data['Lat2']=$this->input->post('lat2');
			$data['Lon1']=$this->input->post('lon1');
			$data['Lon2']=$this->input->post('lon2');
			$data['Cod_Moneda']=$this->input->post('moneda');
			$data['Bandera']=$file_info['file_name'];
			$this->pais_m->save($data,$data['Cod_Pais']);
			echo '';
		}else{
			$form=array('form'=>FormularioPaisEdit($data));
			echo $this->load->view("paper/forms_modal/form_paises_edit",$form,true);
		}
	}

	public function Logout(){
			$datalog['userid']=$this->session->userdata('userid');

			$info=array('is_logged'=>0);


			$this->db->where('userid',$datalog['userid']);
			$this->db->update('adm.users',$info);

			$datalog['action']='Cerrar Sessión';
			$datalog['status']='Completado';
			$datalog['category']='Logout';
			$datalog['username']=$this->session->userdata('username');
			$datalog['name']=$this->session->userdata('name');
			$datalog['ip']=$this->input->ip_address();
			$this->logUsers->save($datalog);
        $this->session->sess_destroy();
        redirect('Login','refresh');
    }

    public function test(){
		echo config_item('sess_expiration');

		$this->load->model('Parameters');
		$parametros = new Parameters();
		$parametros->sess_expiration;
		$this->config->set_item('sess_expiration', $parametros->sess_expiration);

		echo '<br>'.config_item('sess_expiration');


	}

}

?>

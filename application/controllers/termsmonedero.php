<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class termsmonedero extends CI_Controller {

	public function index()
	{
		$this->load->model('Contrato_m');
		$contrato=$this->Contrato_m->get(17);

		$htmlData['title']=$contrato->Titulo;
		$htmlData['fullText']=$contrato->Texto;
		$this->load->view('paper/TerminosMonedero',$htmlData);
	}

	public function Ayuda(){
		$data['name_file']=array(

			'WalletDepRetiro.svg',
			'WalletNvoPago.svg',
			'WalletRetiroATM.svg',
			'WalletTransferencia.svg',
			'WalletMenu.svg',

			);
		$data['title']='Help Monedero';
		echo $this->load->view('paper/Help', $data, TRUE);
	}

}

/* End of file Controllername.php */

?>

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AdminHorarios extends MY_Controller {
	public $permiso_crear=121;
	public $permiso_actualizar=100;
	public $permiso_eliminar=101;
	public $permiso_ver=99;

	public function __construct()
	{
		parent::__construct();
		$this->load->model('HorarioModel');
	}

	public function index()
	{
		$datos['navbar_menu']=$this->navbar_header_menu();
		$datos['title']=$this->lang->line('scheduling');;
		$this->load->view('paper/admin/_AdminLayout',$datos);
	}

	public function populateTable(){

		echo json_encode($this->HorarioModel->DataTable());
	}

	/*public function Form(){
		echo $this->HorarioModel->form('sm');
	}*/

	public function Form($fail=FALSE){
		if($fail==TRUE){
			$data['fail']='<p class="text-danger">'.$this->lang->line('required').'<p>';
		}
		//$data['fail']='<p class="text-danger">'.$this->lang->line('required').'<p>';
		$data['excepciones']=$this->HorarioModel->SelectorExcepciones();
		echo $this->load->view('FORM',$data,true);
	}

	public function EditData(){
		$this->input->post('id');
		$data=array();
		$data['id_horario']=$this->input->post('id');
		$data['description']=$this->HorarioModel->description($data['id_horario']);
		$data['excepciones']=$this->HorarioModel->SelectorExcepciones();
		$data['table_horario']=$this->HorarioModel->TableHorario($data['id_horario']);
		echo $this->load->view('FORMEDIT',$data,true);
	}

	public function DeleteData(){
		$id=$this->input->post('id');
		$this->HorarioModel->deleteHorario($id);
		echo '<p class="label label-success">'.$this->lang->line('error_delete').'<strong>'.$id.'</strong></p>';

	}

	public function SaveHorario(){
		if($this->input->post('id')){
			$dias=array('Lunes','Martes','Miercoles','Jueves','Viernes','Sabado','Domingo');
			$h['description']=$this->input->post('nombre_horario');
			$h['exceptionId']=$this->input->post('excepciones');
			$idHorario=$this->input->post('id');
			if($h['description']!=''){


				$data['dias']=$this->input->post('dias');


				$i=0;
				if($data['dias']){
					$this->HorarioModel->UpdateHorario($idHorario,$h);
					$this->HorarioModel->CleanRelationGroup($idHorario);
					$data['hora_inicio']=$this->input->post('hora_inicio');
					$data['hora_fin']=$this->input->post('hora_fin');

					foreach ($data['dias'] as $value){

						$horario[$i]['day']=$value;
						$horario[$i]['hora_inicio']=$data['hora_inicio'][$i];
						$horario[$i]['hora_fin']=$data['hora_fin'][$i];
						$horario[$i]['horarioId']=$idHorario;
						$this->db->insert('esb.relationHorariosDays',$horario[$i]);

						$i++;
					}
					echo "Todo bien!";

				}else{
					$this->Form();
				}

			}else{
				$this->Form(TRUE);
			}

		}else{
			$dias=array('Lunes','Martes','Miercoles','Jueves','Viernes','Sabado','Domingo');
			$h['description']=$this->input->post('nombre_horario');
			$h['exceptionId']=$this->input->post('excepciones');

			if($h['description']!=''){


				$data['dias']=$this->input->post('dias');
				$data['hora_inicio']=$this->input->post('hora_inicio');
				$data['hora_fin']=$this->input->post('hora_fin');



				$i=0;
				if($data['dias']){
					//var_dump($data['dias']);
					$idHorario=$this->HorarioModel->AlmacenarNuevoHorario($h);
					foreach ($data['dias'] as $value){
						$horario[$i]['day']=$value;
						$horario[$i]['hora_inicio']=$data['hora_inicio'][$i];
						$horario[$i]['hora_fin']=$data['hora_fin'][$i];
						$horario[$i]['horarioId']=$idHorario;
						$this->db->insert('esb.relationHorariosDays',$horario[$i]);
						$i++;
					}
					echo "Todo bien!";
			}else{
				$this->Form();
			}

			}else{
					$this->Form(TRUE);
			}

		}


	}

	public function UpdateHorario(){

		$dias=array('Lunes','Martes','Miercoles','Jueves','Viernes','Sabado','Domingo');
		$h['description']=$this->input->post('nombre_horario');
		$h['exceptionId']=$this->input->post('excepciones');
		$idHorario=$this->HorarioModel->AlmacenarNuevoHorario($h);
		foreach ($dias as  $value){
			$data['dias']=$this->input->post('dias');
			$data['hora_inicio']=$this->input->post('hora_inicio');
			$data['hora_fin']=$this->input->post('hora_fin');
		}
		$i=0;
		foreach ($data['dias'] as $value){
			if($value){
				$horario[$i]['day']=$value;
				$horario[$i]['hora_inicio']=$data['hora_inicio'][$i];
				$horario[$i]['hora_fin']=$data['hora_fin'][$i];
				$horario[$i]['horarioId']=$idHorario;
				$this->db->insert('esb.relationHorariosDays',$horario[$i]);
			}
			$i++;
		}
		echo "Todo bien!";
	}



}

/* End of file AdminHorarios.php */
/* Location: ./application/controllers/AdminHorarios.php */

<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class Perfil extends MY_Controller{

		public function __construct(){
        parent::__construct();
				$this->load->model('ProfileModel', 'pm');

    }

		public function index(){

			$infoContent['title']=$this->lang->line('profile');
			$infoContent['formulario']=$this->load->view('paper/forms_modal/formProfile','',true);
			$data['navbar_menu']=$this->navbar_header_menu();
			$data['sidebar_menu']=$this->menu_admin();
			$data['footer']=$this->load->view('paper/footers/footPerfil','',true);
			$data['content_view']=$this->load->view('paper/admin/view_profile',$infoContent,true);
			echo $this->load->view('paper/base',$data,true);
		}

		public function UpdatePerfil(){
			$this->form_validation->set_rules($this->pm->_rules);
			$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
			if($this->form_validation->run()==TRUE){
				$id=$this->input->post('userid');
				$data['password']=$this->encrypt->encode($this->input->post('newPassword'));
				$this->pm->save($data,$id);
				$this->session->set_flashdata('message','<p class="alert alert-success">La Contraseña ha sido actualizada.</p>');
				redirect ('Perfil','refresh');
			}else{
				$this->session->set_flashdata('message','<p class="alert alert-danger">No se Actualizó la Contraseña.</p>');
				$this->index();
			}
		}

		public function _validaHoldPass($str){
			$id=$this->input->post('userid');
			$this->db->select('password');
			$data=$this->pm->get($id);
			if($str==$this->encrypt->decode($data->password)){
				return TRUE;
			}else{
				$this->form_validation->set_message('_validaHoldPass', 'La antigua <strong>Contraseña</strong> No es correcta.');
				return FALSE;
			}
		}


	}


 ?>

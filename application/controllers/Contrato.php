<?php
/**
 * Created by PhpStorm.
 * User: hrodriguez
 * Date: 10/01/18
 * Time: 02:39 AM
 */


defined('BASEPATH') OR exit('No direct script access allowed');

class Contrato extends MY_Controller {


	public $permiso_actualizar=61;
	public $permiso_ver=158;
	public $mymodel='Contrato_m';
	public $controlerServer='Contrato';
	public $formSize='sm';

	//dato para locale table
	public $localeForTable='es-MX';


	public function __construct()
	{
		parent::__construct();
		//Codeigniter : Write Less Do More
	}



	public function index()
	{
		$this->dataTable[0]=array('field'=>'Id','title'=>'ID','sortable'=>'true','visible'=>'false','align'=>'left');
		$this->dataTable[1]=array('field'=>'Titulo','title'=>'titulo','sortable'=>'true','visible'=>'true','align'=>'left');
		$this->dataTable[2]=array('field'=>'Texto','title'=>'texto','sortable'=>'true','visible'=>'true','align'=>'left');
		$this->dataTable[3]=array('field'=>'Estado','title'=>'status','sortable'=>'true','visible'=>'true','align'=>'left');
		$this->dataTable[4]=array('field'=>'actions','title'=>'actions','class'=>'td-actions text-left','align'=>'left','events'=>'operateEvents','formatter'=>'operateFormatter');
		$data['titleMenu']=$this->lang->line('titleAdmin');
		$data['navbar_menu']=$this->navbar_header_menu();
		$data['controllerServer']=$this->controlerServer;
		$data['columns']=$this->getColumnsForTable();
		$data['rutaForm']=$this->rutaForm;
		$data['title_form']=$this->lang->line('contrato');
		$data['sizeModal']='modal-lg';
		$data['documentation']='';
		$data['title']=$this->lang->line('contrato');
		$data['rutaToExport']='';
		$data['localeForTable']=$this->localeForTable;
		$this->load->view('paper/admin/vista_contrato',$data);
	}

}

/* End of file Controllername.php */
?>

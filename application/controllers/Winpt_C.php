<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Winpt_C extends MY_Controller{

  public $permiso_ver='157';

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
  }

  function index()
  {

		$data['titleMenu']=$this->lang->line('titleAdmin');
		$data['navbar_menu']=$this->navbar_header_menu();
		$data['controllerServer']='Winpt_C';
		$data['rutaForm']='';
		$data['title_form']='Email';
		$data['sizeModal']='modal-lg';
    $data['documentation']='';
    $data['title']='WINPT';
		$data['rutaToExport']='';
		$data['localeForTable']=$this->localeForTable;
    $this->load->view('paper/admin/_layoutWINPT',$data);
  }


  public function DownWin(){

    $letter=array(
                  'a','b','c','d','e','f','g','h','i',
                  'j','k','l','m','n','o','p','q','r',
                  's','t','u','v','w','x','y','z','aa',
                  'ab','ac','ad','ae','af','ag','ah','ai',
                  'aj'
                 );
    $ancho=array(
                  1,9,2,3,2,3,3,16,12,2,2,2,2,
                  30,15,15,2,15,2,15,2,15,2,2,
                  2,2,4,5,3,1,16,5,8,11,1,5

                );

    $inicio=date('Y-m-d').' 00:00:00';
    $fin=date('Y-m-d').' 23:59:00';
    $this->db->select('*');

    if($this->input->post('inicio') && $this->input->post('fin')){
      $inicio=$this->input->post('inicio').' 00:00:00';
      $fin=$this->input->post('fin').' 23:59:00';
    }
    $this->db->where('stamp >',$inicio);
    $this->db->where('stamp <',$fin);
    $log=$this->db->get('esb.transactionMonederoLog');

      $this->load->library('excel');
      $this->excel->setActiveSheetIndex(0);
      $this->excel->getActiveSheet()->setTitle('WINPT-MONEDERO');

      //aplicamos el ancho de cada celda

      for($i=0;$i<count($letter);$i++){

        $this->excel->getActiveSheet()->getColumnDimension(strtoupper($letter[$i]))->setWidth($ancho[$i]+0.73);
      }

      $fila=2;
		$contador=1;
	  foreach($log->result() as $value){
		  for ($k=0; $k <count($letter) ; $k++) {


			  //Primero Ingresmos Vacío
			  $this->excel->getActiveSheet()->setCellValue($letter[$k].$fila, '0');
			  //Luego Condicionamos que letra y si debemos ingresar algo en esa celda

			  $espacios=$ancho[$k]-strlen('0');
			  $contenido='';
			  for($p=0;$p==$espacios;$p++){
				  $contenido.=' ';
			  }
			  $contenido.='0';
			  $this->excel->getActiveSheet()->setCellValue($letter[$k].$fila, $contenido);

			  if($letter[$k]=='a'){
				  $contenido='T';
				  $this->excel->getActiveSheet()->setCellValueExplicit($letter[$k].$fila,$contenido,PHPExcel_Cell_DataType::TYPE_STRING);

			  }

			  if($letter[$k]=='c'){
				  $contenido='01';
				  $this->excel->getActiveSheet()->setCellValueExplicit($letter[$k].$fila,$contenido,PHPExcel_Cell_DataType::TYPE_STRING);
			  }

			  if($letter[$k]=='e'){

				  $contenido='01';
				  $this->excel->getActiveSheet()->setCellValueExplicit($letter[$k].$fila,$contenido,PHPExcel_Cell_DataType::TYPE_STRING);
			  }

			  if($letter[$k]=='d'){
				  $espacios=$ancho[$k]-strlen('608');
				  $contenido='';

				  for($p=0;$p==$espacios;$p++){
					  $contenido.=' ';
				  }
				  $contenido.='608';
				  $this->excel->getActiveSheet()->setCellValue($letter[$k].$fila, $contenido);
			  }


			  if($letter[$k]=='f'){

				  $espacios=$ancho[$k]-strlen('608');
				  $contenido='';

				  for($p=0;$p==$espacios;$p++){
					  $contenido.=' ';
				  }
				  $contenido.='608';
				  $this->excel->getActiveSheet()->setCellValueExplicit($letter[$k].$fila,$contenido,PHPExcel_Cell_DataType::TYPE_NUMERIC);
			  }

			  if($letter[$k]=='g'){

				  $contenido='B/.';
				  $this->excel->getActiveSheet()->setCellValue($letter[$k].$fila, $contenido);

			  }
			  //Rubro Contable
			  if($letter[$k]=='h'){

				  $contenido=$value->cuenta1;
				  $this->excel->getActiveSheet()->setCellValueExplicit($letter[$k].$fila,$contenido,PHPExcel_Cell_DataType::TYPE_STRING);
			  }
			  // es lo que identifica a un producto
			  if($letter[$k]=='i'){
				  $espacios=$ancho[$k]-strlen('');
				  $contenido='';

				  for($p=0;$p==$espacios;$p++){
					  $contenido.=' ';
				  }
				  $contenido.='';
				  $this->excel->getActiveSheet()->setCellValue($letter[$k].$fila, $contenido);
			  }
			  //Codigo de la transaccion
			  if($letter[$k]=='j'){
				  $codigo='MC';
				  if($value->aux=='-'){
					  $codigo='MD';
				  }

				  $contenido=$codigo;
				  $this->excel->getActiveSheet()->setCellValueExplicit($letter[$k].$fila,$contenido,PHPExcel_Cell_DataType::TYPE_STRING);
			  }

			  //Descripcion de la transaccion / telefono con la fecha = 00000000/2018-01-01
			  if($letter[$k]=='n'){

				  $this->db->select('phone');
				  $this->db->where('imei',$value->terminalMonedero);
				  $q=$this->db->get('esb.usuariosMonedero');
				  $phone='';
				  foreach ($q->result() as $usuario) {
					  $phone=$usuario->phone;
				  }
				  $valor=str_replace('-','',$phone).'/'.substr($value->stamp,0,10);

				  $espacios=$ancho[$k]-strlen($valor);
				  $contenido='';

				  for($p=0;$p==$espacios;$p++){
					  $contenido.=' ';
				  }
				  $contenido.=$valor;
				  $this->excel->getActiveSheet()->setCellValue($letter[$k].$fila, $contenido);
			  }

			  if($letter[$k]=='o'){
				  $valor='0';
				  if($value->aux=='-'){
					  $valor=str_replace('.','',$value->montoTotal);
				  }


				  $espacios=$ancho[$k]-strlen($valor);
				  $contenido='';

				  for($p=0;$p==$espacios;$p++){
					  $contenido.=' ';
				  }
				  $contenido.=$valor;
				  $this->excel->getActiveSheet()->setCellValue($letter[$k].$fila, $contenido);
			  }

			  if($letter[$k]=='p'){
				  $valor='0';
				  if($value->aux=='+'){
					  $valor=str_replace('.','',$value->montoTotal);
				  }

				  $espacios=$ancho[$k]-strlen($valor);
				  $contenido='';

				  for($p=0;$p==$espacios;$p++){
					  $contenido.=' ';
				  }
				  $contenido.=$valor;
				  $this->excel->getActiveSheet()->setCellValue($letter[$k].$fila, $contenido);
			  }


			  //TODO verificar si es la secuencia de la fila o de la transaccion
			  if($letter[$k]=='ab'){



				  $this->excel->getActiveSheet()->setCellValueExplicit($letter[$k].$fila,$contador,PHPExcel_Cell_DataType::TYPE_NUMERIC);
				  $contador++;
			  }

			  //Centro de Costo; ????
			  if($letter[$k]=='ag'){
			  	$valor='8020';
				  $espacios=$ancho[$k]-strlen('8020');
				  $contenido='';

				  for($p=0;$p==$espacios;$p++){
					  $contenido.=' ';
				  }
				  $contenido.=$valor;
				  $this->excel->getActiveSheet()->setCellValue($letter[$k].$fila, $contenido);

			  }

			  //Centro de Costo; ????
			  if($letter[$k]=='ai'){
				  $valor=' ';
				  $espacios=$ancho[$k]-strlen(' ');
				  $contenido='';

				  for($p=0;$p==$espacios;$p++){
					  $contenido.=' ';
				  }
				  $contenido.=$valor;
				  $this->excel->getActiveSheet()->setCellValue($letter[$k].$fila, $contenido);

			  }

			  //Centro de Costo; ????
			  if($letter[$k]=='ae'){
				  $valor=' ';
				  $espacios=$ancho[$k]-strlen(' ');
				  $contenido='';

				  for($p=0;$p==$espacios;$p++){
					  $contenido.=' ';
				  }
				  $contenido.=$valor;
				  $this->excel->getActiveSheet()->setCellValue($letter[$k].$fila, $contenido);

			  }

			  //Centro de Costo; ????
			  if($letter[$k]=='ad'){
				  $valor=' ';
				  $espacios=$ancho[$k]-strlen(' ');
				  $contenido='';

				  for($p=0;$p==$espacios;$p++){
					  $contenido.=' ';
				  }
				  $contenido.=$valor;
				  $this->excel->getActiveSheet()->setCellValue($letter[$k].$fila, $contenido);

			  }


		  }
		  $fila++;
	  }






	  $this->db->select('*');
	  if($this->input->post('inicio') && $this->input->post('fin')){
		  $inicio=$this->input->post('inicio').' 00:00:00';
		  $fin=$this->input->post('fin').' 23:59:00';
	  }
	  $this->db->where('stamp >',$inicio);
	  $this->db->where('stamp <',$fin);
	  $logCnb=$this->db->get('esb.transactionLog');


	  foreach($logCnb->result() as $value){
		  for ($k=0; $k <count($letter) ; $k++) {


			  //Primero Ingresmos Vacío
			  $this->excel->getActiveSheet()->setCellValue($letter[$k].$fila, '0');
			  //Luego Condicionamos que letra y si debemos ingresar algo en esa celda

			  $espacios=$ancho[$k]-strlen('0');
			  $contenido='';
			  for($p=0;$p==$espacios;$p++){
				  $contenido.=' ';
			  }
			  $contenido.='0';
			  $this->excel->getActiveSheet()->setCellValue($letter[$k].$fila, $contenido);

			  if($letter[$k]=='a'){
				  $contenido='T';
				  $this->excel->getActiveSheet()->setCellValueExplicit($letter[$k].$fila,$contenido,PHPExcel_Cell_DataType::TYPE_STRING);

			  }

			  if($letter[$k]=='c'){
				  $contenido='01';
				  $this->excel->getActiveSheet()->setCellValueExplicit($letter[$k].$fila,$contenido,PHPExcel_Cell_DataType::TYPE_STRING);
			  }

			  if($letter[$k]=='e'){

				  $contenido='01';
				  $this->excel->getActiveSheet()->setCellValueExplicit($letter[$k].$fila,$contenido,PHPExcel_Cell_DataType::TYPE_STRING);
			  }

			  if($letter[$k]=='d'){
				  $espacios=$ancho[$k]-strlen('608');
				  $contenido='';

				  for($p=0;$p==$espacios;$p++){
					  $contenido.=' ';
				  }
				  $contenido.='608';
				  $this->excel->getActiveSheet()->setCellValue($letter[$k].$fila, $contenido);
			  }


			  if($letter[$k]=='f'){

				  $espacios=$ancho[$k]-strlen('608');
				  $contenido='';

				  for($p=0;$p==$espacios;$p++){
					  $contenido.=' ';
				  }
				  $contenido.='608';
				  $this->excel->getActiveSheet()->setCellValueExplicit($letter[$k].$fila,$contenido,PHPExcel_Cell_DataType::TYPE_NUMERIC);
			  }

			  if($letter[$k]=='g'){
				  $contenido='B/.';
				  $this->excel->getActiveSheet()->setCellValue($letter[$k].$fila, $contenido);

			  }
			  //Rubro Contable
			  if($letter[$k]=='h'){

				  $contenido=$value->cuenta1;
				  $this->excel->getActiveSheet()->setCellValueExplicit($letter[$k].$fila,$contenido,PHPExcel_Cell_DataType::TYPE_STRING);
			  }
			  // es lo que identifica a un producto
			  if($letter[$k]=='i'){
				  $espacios=$ancho[$k]-strlen('');
				  $contenido='';

				  for($p=0;$p==$espacios;$p++){
					  $contenido.=' ';
				  }
				  $contenido.='';
				  $this->excel->getActiveSheet()->setCellValue($letter[$k].$fila, $contenido);
			  }
			  //Codigo de la transaccion
			  if($letter[$k]=='j'){
				  $codigo='MC';
				  if($value->aux=='-'){
					  $codigo='MD';
				  }

				  $contenido=$codigo;
				  $this->excel->getActiveSheet()->setCellValueExplicit($letter[$k].$fila,$contenido,PHPExcel_Cell_DataType::TYPE_STRING);
			  }

			  //Descripcion de la transaccion / telefono con la fecha = 00000000/2018-01-01
			  if($letter[$k]=='n'){

				  $this->db->select('phone');
				  $this->db->where('imei',$value->terminalMonedero);
				  $q=$this->db->get('esb.usuariosMonedero');
				  $phone='';
				  foreach ($q->result() as $usuario) {
					  $phone=$usuario->phone;
				  }
				  $valor=str_replace('-','',$phone).'/'.substr($value->stamp,0,10);
				  $valor=str_replace(' ','',$valor);
				  $contenido=$valor;
				  $this->excel->getActiveSheet()->setCellValue($letter[$k].$fila, $contenido);
			  }

			  if($letter[$k]=='o'){
				  $valor='0';
				  if($value->aux=='-'){
					  $valor=str_replace('.','',$value->montoTotal);
				  }


				  $espacios=$ancho[$k]-strlen($valor);
				  $contenido='';

				  for($p=0;$p==$espacios;$p++){
					  $contenido.=' ';
				  }
				  $contenido=$valor;
				  $this->excel->getActiveSheet()->setCellValue($letter[$k].$fila, $contenido);
			  }

			  if($letter[$k]=='p'){
				  $valor='0';
				  if($value->aux=='+'){
					  $valor=str_replace('.','',$value->montoTotal);
				  }

				  $espacios=$ancho[$k]-strlen($valor);
				  $contenido='';

				  for($p=0;$p==$espacios;$p++){
					  $contenido.=' ';
				  }
				  $contenido.=$valor;
				  $this->excel->getActiveSheet()->setCellValue($letter[$k].$fila, $contenido);
			  }


			  //TODO verificar si es la secuencia de la fila o de la transaccion
			  if($letter[$k]=='ab'){



				  $this->excel->getActiveSheet()->setCellValueExplicit($letter[$k].$fila,$contador,PHPExcel_Cell_DataType::TYPE_NUMERIC);
				  $contador++;
			  }

			  //Centro de Costo; ????
			  if($letter[$k]=='ag'){
				  $valor='8020';
				  $espacios=$ancho[$k]-strlen('8020');
				  $contenido='';

				  for($p=0;$p==$espacios;$p++){
					  $contenido.=' ';
				  }
				  $contenido.=$valor;
				  $this->excel->getActiveSheet()->setCellValue($letter[$k].$fila, $contenido);

			  }

			  //Centro de Costo; ????
			  if($letter[$k]=='ai'){
				  $valor=' ';
				  $espacios=$ancho[$k]-strlen(' ');
				  $contenido='';

				  for($p=0;$p==$espacios;$p++){
					  $contenido.=' ';
				  }
				  $contenido.=$valor;
				  $this->excel->getActiveSheet()->setCellValue($letter[$k].$fila, $contenido);

			  }

			  //Centro de Costo; ????
			  if($letter[$k]=='ae'){
				  $valor=' ';
				  $espacios=$ancho[$k]-strlen(' ');
				  $contenido='';

				  for($p=0;$p==$espacios;$p++){
					  $contenido.=' ';
				  }
				  $contenido.=$valor;
				  $this->excel->getActiveSheet()->setCellValue($letter[$k].$fila, $contenido);

			  }

			  //Centro de Costo; ????
			  if($letter[$k]=='ad'){
				  $valor=' ';
				  $espacios=$ancho[$k]-strlen(' ');
				  $contenido='';

				  for($p=0;$p==$espacios;$p++){
					  $contenido.=' ';
				  }
				  $contenido.=$valor;
				  $this->excel->getActiveSheet()->setCellValue($letter[$k].$fila, $contenido);

			  }


		  }
		  $fila++;
	  }




	  $suenta=$log->num_rows()+$logCnb->num_rows()+1;

	  $this->excel->getActiveSheet()->setCellValue('O1','=SUM(O2:O'.$suenta.')');
	  $this->excel->getActiveSheet()->setCellValue('P1','=SUM(P2:P'.$suenta.')');



	  $this->excel->getActiveSheet()->setCellValue('A1','B');
	  $this->excel->getActiveSheet()->setCellValue('B1','0');
	  $this->excel->getActiveSheet()->setCellValue('C1','0');
	  $this->excel->getActiveSheet()->setCellValue('D1','0');
	  $this->excel->getActiveSheet()->setCellValue('E1','0');
	  $this->excel->getActiveSheet()->setCellValue('F1','00');
	  $this->excel->getActiveSheet()->setCellValue('G1','0');
	  $this->excel->getActiveSheet()->setCellValue('H1','0');
	  $this->excel->getActiveSheet()->setCellValue('I1','0');
	  $this->excel->getActiveSheet()->setCellValue('Q1','0');
	  $this->excel->getActiveSheet()->setCellValue('R1','0');
	  $this->excel->getActiveSheet()->setCellValue('S1','0');
	  $this->excel->getActiveSheet()->setCellValue('U1','0');
	  $this->excel->getActiveSheet()->setCellValue('V1','0');
	  $this->excel->getActiveSheet()->setCellValue('L1','0');
	  $this->excel->getActiveSheet()->setCellValue('X1','0');
	  $this->excel->getActiveSheet()->setCellValue('Y1','0');
	  $this->excel->getActiveSheet()->setCellValue('Z1','0');
	  $this->excel->getActiveSheet()->setCellValue('K1','0');
	  $this->excel->getActiveSheet()->setCellValue('M1','0');
	  $this->excel->getActiveSheet()->setCellValue('T1','0');
	  $this->excel->getActiveSheet()->setCellValue('W1','0');
	  $this->excel->getActiveSheet()->setCellValue('AA1','0');
	  $this->excel->getActiveSheet()->setCellValue('AB1',0);
	  $this->excel->getActiveSheet()->setCellValue('AC1','0');
	  $this->excel->getActiveSheet()->setCellValue('AF1','0');
	  $this->excel->getActiveSheet()->setCellValue('AG1','0');
	  $this->excel->getActiveSheet()->setCellValue('AH1','0');
	  $this->excel->getActiveSheet()->setCellValue('AJ1','0');
	  $this->excel->getActiveSheet()->setCellValue('AI1',' ');

	  //$this->excel->getActiveSheet()->getColumnDimension(strtoupper('J'))->setWidth($ancho[$i]+0.73);



	  //Le ponemos un nombre al archivo que se va a generar.
	  $archivo = 'WINPT_'.time().'.xls';
	  header('Content-Type: application/vnd.ms-excel');
	  header('Content-Disposition: attachment;filename="'.$archivo.'"');
	  header('Cache-Control: max-age=0');
	  $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
	  //Hacemos una salida al navegador con el archivo Excel.
	  $objWriter->save('php://output');

  }


	/*public function DownWinCnb(){

		$letter=array(
			'a','b','c','d','e','f','g','h','i',
			'j','k','l','m','n','o','p','q','r',
			's','t','u','v','w','x','y','z','aa',
			'ab','ac','ad','ae','af','ag','ah','ai',
			'aj'
		);
		$ancho=array(
			1,
			9,
			2,
			3,
			2,
			3,
			3,
			16,
			12,
			2,
			2,
			2,
			2,
			30,
			15,
			15,
			2,
			15,
			2,
			15,
			2,
			15,
			2,
			2,
			2,
			2,
			4,
			5,
			3,
			1,
			16,
			5,
			8,
			11,
			1,
			5

		);

		$inicio=date('Y-m-d').' 00:00:00';
		$fin=date('Y-m-d').' 23:59:00';
		$this->db->select('*');

		if($this->input->post('inicio') && $this->input->post('fin')){
			$inicio=$this->input->post('inicio').' 00:00:00';
			$fin=$this->input->post('fin').' 23:59:00';
		}
		$this->db->where('stamp >',$inicio);
		$this->db->where('stamp <',$fin);
		$log=$this->db->get('esb.transactionLog');

		$this->load->library('excel');
		$this->excel->setActiveSheetIndex(0);
		$this->excel->getActiveSheet()->setTitle('WINPT-CNB');

		//aplicamos el ancho de cada celda

		for($i=0;$i<count($letter);$i++){

			$this->excel->getActiveSheet()->getColumnDimension(strtoupper($letter[$i]))->setWidth($ancho[$i]+0.73);
		}

		$fila=2;
		$contador=1;
		foreach($log->result() as $value){
			for ($k=0; $k <count($letter) ; $k++) {


				//Primero Ingresmos Vacío
				$this->excel->getActiveSheet()->setCellValue($letter[$k].$fila, '0');
				//Luego Condicionamos que letra y si debemos ingresar algo en esa celda

				$espacios=$ancho[$k]-strlen('0');
				$contenido='';
				for($p=0;$p==$espacios;$p++){
					$contenido.=' ';
				}
				$contenido.='0';
				$this->excel->getActiveSheet()->setCellValue($letter[$k].$fila, $contenido);

				if($letter[$k]=='a'){
					$contenido='T';
					$this->excel->getActiveSheet()->setCellValueExplicit($letter[$k].$fila,$contenido,PHPExcel_Cell_DataType::TYPE_STRING);

				}

				if($letter[$k]=='c'){
					$contenido='01';
					$this->excel->getActiveSheet()->setCellValueExplicit($letter[$k].$fila,$contenido,PHPExcel_Cell_DataType::TYPE_STRING);
				}

				if($letter[$k]=='e'){

					$contenido='01';
					$this->excel->getActiveSheet()->setCellValueExplicit($letter[$k].$fila,$contenido,PHPExcel_Cell_DataType::TYPE_STRING);
				}

				if($letter[$k]=='d'){
					$espacios=$ancho[$k]-strlen('608');
					$contenido='';

					for($p=0;$p==$espacios;$p++){
						$contenido.=' ';
					}
					$contenido.='608';
					$this->excel->getActiveSheet()->setCellValue($letter[$k].$fila, $contenido);
				}


				if($letter[$k]=='f'){

					$espacios=$ancho[$k]-strlen('608');
					$contenido='';

					for($p=0;$p==$espacios;$p++){
						$contenido.=' ';
					}
					$contenido.='608';
					$this->excel->getActiveSheet()->setCellValueExplicit($letter[$k].$fila,$contenido,PHPExcel_Cell_DataType::TYPE_NUMERIC);
				}

				if($letter[$k]=='g'){
					$espacios=$ancho[$k]-strlen('B/.');
					$contenido='';

					for($p=0;$p==$espacios;$p++){
						$contenido.=' ';
					}
					$contenido.='B/.';
					$this->excel->getActiveSheet()->setCellValue($letter[$k].$fila, $contenido);

				}
				//Rubro Contable
				if($letter[$k]=='h'){
					$espacios=$ancho[$k]-strlen($value->cuenta1);
					$contenido='';

					for($p=0;$p==$espacios;$p++){
						$contenido.=' ';
					}
					$contenido.=$value->terminalMonedero;
					$this->excel->getActiveSheet()->setCellValueExplicit($letter[$k].$fila,$contenido,PHPExcel_Cell_DataType::TYPE_STRING);
				}
				// es lo que identifica a un producto
				if($letter[$k]=='i'){
					$espacios=$ancho[$k]-strlen('');
					$contenido='';

					for($p=0;$p==$espacios;$p++){
						$contenido.=' ';
					}
					$contenido.='';
					$this->excel->getActiveSheet()->setCellValue($letter[$k].$fila, $contenido);
				}
				//Codigo de la transaccion
				if($letter[$k]=='j'){
					$codigo='MC';
					if($value->aux=='-'){
						$codigo='MD';
					}

					$contenido=$codigo;
					$this->excel->getActiveSheet()->setCellValueExplicit($letter[$k].$fila,$contenido,PHPExcel_Cell_DataType::TYPE_STRING);
				}

				//Descripcion de la transaccion / telefono con la fecha = 00000000/2018-01-01
				if($letter[$k]=='n'){

					$this->db->select('phone');
					$this->db->where('imei',$value->terminalMonedero);
					$q=$this->db->get('esb.usuariosMonedero');
					$phone='';
					foreach ($q->result() as $usuario) {
						$phone=$usuario->phone;
					}
					$valor=str_replace('-','',$phone).'/'.substr($value->stamp,0,10);

					$espacios=$ancho[$k]-strlen($valor);
					$contenido='';

					for($p=0;$p==$espacios;$p++){
						$contenido.=' ';
					}
					$contenido.=$valor;
					$this->excel->getActiveSheet()->setCellValue($letter[$k].$fila, $contenido);
				}

				if($letter[$k]=='o'){
					$valor='0';
					if($value->aux=='-'){
						$valor=str_replace('.','',$value->montoTotal);
					}


					$espacios=$ancho[$k]-strlen($valor);
					$contenido='';

					for($p=0;$p==$espacios;$p++){
						$contenido.=' ';
					}
					$contenido.=$valor;
					$this->excel->getActiveSheet()->setCellValue($letter[$k].$fila, $contenido);
				}

				if($letter[$k]=='p'){
					$valor='0';
					if($value->aux=='+'){
						$valor=str_replace('.','',$value->montoTotal);
					}

					$espacios=$ancho[$k]-strlen($valor);
					$contenido='';

					for($p=0;$p==$espacios;$p++){
						$contenido.=' ';
					}
					$contenido.=$valor;
					$this->excel->getActiveSheet()->setCellValue($letter[$k].$fila, $contenido);
				}


				//TODO verificar si es la secuencia de la fila o de la transaccion
				if($letter[$k]=='ab'){



					$this->excel->getActiveSheet()->setCellValueExplicit($letter[$k].$fila,$contador,PHPExcel_Cell_DataType::TYPE_NUMERIC);
					$contador++;
				}

				//Centro de Costo; ????
				if($letter[$k]=='ag'){
					$valor='8020';
					$espacios=$ancho[$k]-strlen('8020');
					$contenido='';

					for($p=0;$p==$espacios;$p++){
						$contenido.=' ';
					}
					$contenido.=$valor;
					$this->excel->getActiveSheet()->setCellValue($letter[$k].$fila, $contenido);

				}

				//Centro de Costo; ????
				if($letter[$k]=='ai'){
					$valor=' ';
					$espacios=$ancho[$k]-strlen(' ');
					$contenido='';

					for($p=0;$p==$espacios;$p++){
						$contenido.=' ';
					}
					$contenido.=$valor;
					$this->excel->getActiveSheet()->setCellValue($letter[$k].$fila, $contenido);

				}

				//Centro de Costo; ????
				if($letter[$k]=='ae'){
					$valor=' ';
					$espacios=$ancho[$k]-strlen(' ');
					$contenido='';

					for($p=0;$p==$espacios;$p++){
						$contenido.=' ';
					}
					$contenido.=$valor;
					$this->excel->getActiveSheet()->setCellValue($letter[$k].$fila, $contenido);

				}

				//Centro de Costo; ????
				if($letter[$k]=='ad'){
					$valor=' ';
					$espacios=$ancho[$k]-strlen(' ');
					$contenido='';

					for($p=0;$p==$espacios;$p++){
						$contenido.=' ';
					}
					$contenido.=$valor;
					$this->excel->getActiveSheet()->setCellValue($letter[$k].$fila, $contenido);

				}


			}
			$fila++;
		}

		$this->excel->getActiveSheet()->setCellValue('O1','=SUM(O2:O'.$fila.')');
		$this->excel->getActiveSheet()->setCellValue('P1','=SUM(P2:P'.$fila.')');


		$this->excel->getActiveSheet()->setCellValue('A1','B');
		$this->excel->getActiveSheet()->setCellValue('B1','0');
		$this->excel->getActiveSheet()->setCellValue('C1','0');
		$this->excel->getActiveSheet()->setCellValue('D1','0');
		$this->excel->getActiveSheet()->setCellValue('E1','0');
		$this->excel->getActiveSheet()->setCellValue('F1','00');
		$this->excel->getActiveSheet()->setCellValue('G1','0');
		$this->excel->getActiveSheet()->setCellValue('H1','0');
		$this->excel->getActiveSheet()->setCellValue('I1','0');
		$this->excel->getActiveSheet()->setCellValue('Q1','0');
		$this->excel->getActiveSheet()->setCellValue('R1','0');
		$this->excel->getActiveSheet()->setCellValue('S1','0');
		$this->excel->getActiveSheet()->setCellValue('U1','0');
		$this->excel->getActiveSheet()->setCellValue('V1','0');
		$this->excel->getActiveSheet()->setCellValue('L1','0');
		$this->excel->getActiveSheet()->setCellValue('X1','0');
		$this->excel->getActiveSheet()->setCellValue('Y1','0');
		$this->excel->getActiveSheet()->setCellValue('Z1','0');
		$this->excel->getActiveSheet()->setCellValue('K1','0');
		$this->excel->getActiveSheet()->setCellValue('M1','0');
		$this->excel->getActiveSheet()->setCellValue('T1','0');
		$this->excel->getActiveSheet()->setCellValue('W1','0');
		$this->excel->getActiveSheet()->setCellValue('AA1','0');
		$this->excel->getActiveSheet()->setCellValue('AB1','0');
		$this->excel->getActiveSheet()->setCellValue('AC1','0');
		$this->excel->getActiveSheet()->setCellValue('AF1','0');
		$this->excel->getActiveSheet()->setCellValue('AG1','0');
		$this->excel->getActiveSheet()->setCellValue('AH1','0');
		$this->excel->getActiveSheet()->setCellValue('AJ1','0');
		$this->excel->getActiveSheet()->setCellValue('AI1',' ');





		//Le ponemos un nombre al archivo que se va a generar.
		$archivo = 'WINPT_'.time().'.xls';
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$archivo.'"');
		header('Cache-Control: max-age=0');
		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
		//Hacemos una salida al navegador con el archivo Excel.
		$objWriter->save('php://output');

	}*/


}

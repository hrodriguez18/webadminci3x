<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AdminTransacciones extends MY_Controller {

	public $permiso_crear=83;
	public $permiso_actualizar=81;
	public $permiso_eliminar=82;
	public $permiso_ver=80;

	public $_vista='paper_view_adminTransacciones';

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		$this->load->model('TransaccionesModel');
	}

	public function index()
	{
		$data['navbar_menu']=$this->navbar_header_menu();
		$this->load->view('paper/admin/'.$this->_vista,$data);
	}

	public function PopulateTable(){
		echo json_encode($this->TransaccionesModel->PopulateTable());
	}

	public function Form(){
		echo $this->TransaccionesModel->Form('sm');
	}

	public function Save(){
		$data=array();
		$inputs=$this->TransaccionesModel->_inputs;
		unset($inputs['submit']);
		$this->form_validation->set_rules($this->TransaccionesModel->_rules);
		$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');

		if($this->form_validation->run()==TRUE){
			foreach ($inputs as $key) {
				$data[$key['name']]=$this->input->post($key['name']);
			}
			$id=$data['id'];
			if($id!=''){
				unset($data['id']);
				$this->TransaccionesModel->save($data,$id);
			}else{
				unset($data['id']);
				$this->TransaccionesModel->save($data);
			}
			echo "Todo bien!";
		}else{
			echo $this->TransaccionesModel->Form('sm');
		}
	}


	public function Edit(){
		$id=$this->input->post('id');
		echo $this->TransaccionesModel->Form('sm',$id);
	}

	public function Delete(){
		$id=$this->input->post('id');
		$this->TransaccionesModel->delete($id);
		echo '<p class="label label-success">'.$this->lang->line('error_delete').'<strong>'.$id.'</strong></p>';

	}



}

/* End of file AdminMontos.php */
/* Location: ./application/controllers/AdminMontos.php */

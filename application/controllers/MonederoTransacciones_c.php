<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MonederoTransacciones_c extends MY_Controller{

//DATOS PARA LOS PERMISOS
	public $permiso_ver=126;
	public $permiso_crear=62;
	public $permiso_actualizar=63;
	public $permiso_eliminar=64;
//DATOS PARA LA TABLA
	public $mymodel='MonederoTransacciones_m';
	public $controlerServer='MonederoTransacciones_c';
	public $dataTable=array();
//Ruta para llamar al formulario;
	public $rutarForm='';
//Tamaño del formulario
	public $formSize='lg';

	//variable para Exportar
	public $rutaToExport='';

	public function __construct(){
        parent::__construct();
				$this->rutaForm='';
    }
		public function index(){

    		$this->dataTable[0]=array('field'=>'id','title'=>'ID','sortable'=>'true','visible'=>'false','align'=>'left');
    		$this->dataTable[1]=array('field'=>'stamp','title'=>'date','sortable'=>'true','visible'=>'true','align'=>'left');
    		$this->dataTable[2]=array('field'=>'userMonedero','title'=>'name','sortable'=>'true','visible'=>'true','align'=>'left');
    		$this->dataTable[3]=array('field'=>'TIPO','title'=>'type','sortable'=>'true','visible'=>'true','align'=>'left');
    		$this->dataTable[4]=array('field'=>'montoTotal','title'=>'amount','sortable'=>'true','visible'=>'true','align'=>'left');
    		$this->dataTable[5]=array('field'=>'formaPago','title'=>'methodPay','sortable'=>'true','visible'=>'false','align'=>'left');
    		$this->dataTable[6]=array('field'=>'cuenta','title'=>'account','sortable'=>'true','visible'=>'false','align'=>'left');
    		$this->dataTable[7]=array('field'=>'descripcionTrx','title'=>'description','sortable'=>'true','visible'=>'false','align'=>'left');
    		$this->dataTable[8]=array('field'=>'terminalId','title'=>'terminalid','sortable'=>'true','visible'=>'false','align'=>'left');
    		//$this->dataTable[10]=array('field'=>'currency','title'=>'currency','sortable'=>'true','visible'=>'true','align'=>'left');
    		$this->dataTable[9]=array('field'=>'MAPA','title'=>'maps','sortable'=>'true','visible'=>'true','align'=>'left');
    		//$this->dataTable[14]=array('field'=>'actions','title'=>'actions','class'=>'td-actions text-left','align'=>'left','events'=>'operateEvents','formatter'=>'operateFormatter');

        $data['titleMenu']=$this->lang->line('Monedero');
    		$data['navbar_menu']=$this->navbar_header_menu();
    		$data['controllerServer']=$this->controlerServer;
    		$data['columns']=$this->getColumnsForTable();
    		$data['rutaForm']=$this->rutaForm;
    		$data['title_form']='Transacciones Monedero';
    		$data['sizeModal']='modal-lg';
    		$data['populate']='populateTable';
    		$data['rutaToExport']=base_url().$this->router->fetch_class().'/export';
    		$data['localeForTable']=$this->localeForTable;
				$data['myScript']=$this->getScript();
    		$this->load->view('paper/monedero/_layout',$data);
    }

		public function getScript(){
			return 'function ShowPointmap(url){
				$("#iframeMap").attr("src",url);
				$("#ModalMAP").modal("show");
			}';
		}

		public function export(){
			$this->load->model('ExportToPdf');
			$reporte = new ExportToPdf();

			$reporte->_tablename='esb.transactionMonederoLog';
			$reporte->_primary_key='transactionMonederoLog.id';
			$reporte->_order_by='transactionMonederoLog.id';

			$reporte->_timestamps='stamp';

			$reporte->fecha_inicio=$this->input->post('fecha_inicio').' 00:00:00';
			$reporte->fecha_fin=$this->input->post('fecha_fin').' 23:59:00';

			$reporte->_order_by='transactionMonederoLog.stamp';
			$reporte->_order='DESC';
			if($this->input->post('search')){
				$reporte->_search=$this->input->post('search');
			}



			$reporte->_camposBusqueda=array(
				'transactionMonederoLog.userMonedero',
				'transactionMonederoLog.email',
				'transactionMonederoLog.cuenta'
			);

			$reporte->_joinRelation= array(
				array(
					'table'=>'esb.tipotransaccionesCnb',
					'where_equals'=>'transactionMonederoLog.tipoTrx=tipotransaccionesCnb.id',
					'option'=>'left',
				)
			);

			$reporte->_selection=array(
				'transactionMonederoLog.stamp',
				'transactionMonederoLog.userMonedero',
				'tipotransaccionesCnb.name as TIPO',
				'transactionMonederoLog.montoTotal',
				'transactionMonederoLog.formaPago',
				'transactionMonederoLog.cuenta',
				'transactionMonederoLog.descripcionTrx',
				'transactionMonederoLog.terminalId',
				'transactionMonederoLog.currency',
				'transactionMonederoLog.lat',
				'transactionMonederoLog.lon');



			$reporte->_filename='TransactionLog_'.time().'_.pdf';
			$reporte->_descriptionPdf='REPORTE DE TRANSACCIONES DE MONEDEROS PRESENTES EN EL SISTEMA';
			$reporte->_headersTablePdf=array(
				'FECHA',
				'USUARIO',
				'TIPO TRX',
				'MONTO',
				'FORMA PAGO',
				'CUENTA',
				'DESCRIPCION',
				'TERMINAL',
				'CURRENCY',
				'LAT',
				'LON'
			);
			$reporte->_template['table_open']= '<table border="0" style="
																	border-left: 1px solid black;
																	border-bottom: 1px solid black;
																	border-top: 1px solid black;
																	">';
			$reporte->_template['thead_open']= '<thead>';



			$reporte->_template['heading_row_start']= '<tr style="">';



			$reporte->_template['heading_cell_start']= '<th style="
																	color: #000000;
																	font-weight: bolder;
																	border-bottom:1px solid black;
																	border-right: 1px solid black;
																	text-align: center;
 																	white-space: nowrap;
																">';


			$reporte->_template['cell_start']= '<td nowrap style="
														
														color: #000000;
 														margin: 5px auto;
 														border-right: 1px solid black;
 														border-bottom: 1px solid black;
 														text-align: left;
 														text-align: center;
 														white-space: nowrap;
 														">';


			$reporte->_template['cell_alt_start']= '<td nowrap style="
														
														color: #000000;
 														margin: 5px auto;
 														border-right: 1px solid black;
 														border-bottom: 1px solid black;
 														text-align: left;
 														text-align: center;
 														white-space: nowrap;
 														background-color: #beffbb;
 														">';

			$reporte->getPdf();
		}


	public function populateTable(){
		//
		$data = json_decode(file_get_contents('php://input'), true);

		$limit=NULL;
		$offset=NULL;
		$search=NULL;
		$sort=NULL;
		$sortOrder=NULL;
		$fecha_inicio=NULL;
		$fecha_fin=NULL;



		if($this->input->post('search')){$search=$this->input->post('search');}


		if(isset($data['limit'])){
			$limit= $data['limit'];
		}

		if(isset($data['offset'])){
			$offset= $data['offset'];
		}

		if(isset($data['search'])){
			$search=utf8_decode($data['search']);
		}

		if(isset($data['sort'])){
			$sort=$data['sort'];
		}

		if(isset($data['sortOrder'])){
			$sortOrder=$data['sortOrder'];
		}



		echo json_encode($this->modelo->populate($limit,$offset,$search,$sort,$sortOrder));

	}





}

?>

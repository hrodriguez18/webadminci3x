<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LogControl extends MY_Controller{
  public $permiso_crear=0;
  public $permiso_actualizar=0;
  public $permiso_eliminar=0;
  public $permiso_ver=74;

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
    $this->load->model('logUsers');
  }

  public function index(){

    $this->contenido['table']=$this->load->view("paper/tables/table_LogSystem",'',true);
    $this->contenido['title']='Logs';
    $datos["content_view"]=$this->load->view("paper/admin/paper_view_LogSystem",$this->contenido,true);
    $datos['navbar_menu']=$this->navbar_header_menu();
    $datos['sidebar_menu']=$this->menu_admin();
    $datos['footer']=$this->load->view("paper/footers/footer_LogSystem",'',true);

    $this->load->view("paper/base",$datos);

  }
  public function populateTable(){

	  $this->load->model('populateModel');
	  $users=new populateModel();
	  $users->_tablename='adm.logUsers';
	  $users->_primary_key='id';
	  $users->_timestamps='stamp';
	  $users->_order_by='stamp';
	  $users->_order='DESC';

	  $users->_camposBusqueda=array(
		  'logUsers.id',
		  'logUsers.name',
		  'logUsers.username',
		  'logUsers.action',
		  'logUsers.status',
		  'logUsers.category',
		  'logUsers.userid'
	  );

	  $users->_selection=array(
		  'logUsers.id',
		  'logUsers.userid',
		  'logUsers.action',
		  'logUsers.status',
		  'logUsers.category',
		  'logUsers.stamp',
		  'logUsers.name',
		  'logUsers.username',
		  'logUsers.ip',
	  );
	  $users->_joinRelation=array();


	  echo $users->populate_join();

  }

  public function Export(){
  	ini_set('memory_limit', '1024M'); // or you could use 1G
  	ini_set('max_execution_time', '300'); // or you could use 1G
    $this->load->model('ExportToPdf');

    $logPdf=new ExportToPdf();

	  $logPdf->_tablename='adm.logUsers';
	  $logPdf->_primary_key='id';
	  $logPdf->_order_by='logUsers.stamp';
	  $logPdf->_timestamps='stamp';

	  $logPdf->fecha_inicio=$this->input->post('fecha_inicio').' 00:00:00';
	  $logPdf->fecha_fin=$this->input->post('fecha_fin').' 23:59:00';


	  $logPdf->_order='DESC';

	  $logPdf->_limit=1000;


	  //$logPdf->_especificFieldToSearch='logUsers.cuentaMonedero';

	  //$logPdf->_especificValueToSearch=$this->input->post('cuenta');


	  $logPdf->_camposBusqueda=array(
		  'logUsers.id',
		  'logUsers.name',
		  'logUsers.username',
		  'logUsers.action',
		  'logUsers.status',
		  'logUsers.category',
		  'logUsers.userid'
	  );

	  $logPdf->_selection=array(
		  'logUsers.id',
		  'logUsers.userid',
		  'logUsers.stamp',
		  'logUsers.name',
		  'logUsers.action',
		  'logUsers.status',
		  'logUsers.category',
		  'logUsers.ip',
	  );
	  $logPdf->_joinRelation=array();



	  $logPdf->_filename='Log_'.time().'_.pdf';
	  $logPdf->_descriptionPdf='REPORTE DE LOGs De  USUARIOS EN EL SISTEMA';
	  $logPdf->_headersTablePdf=array(
		  'No.',
		  'USER ID',
		  'FECHA',
		  'NOMBRE',
		  'EVENTO',
		  'ESTADO',
		  'CATEGORIA',
		  'DIRECCIÓN IP'
	  );
	  $logPdf->_template['table_open']= '<table border="0" style="
																	border-left: 1px solid black;
																	border-bottom: 1px solid black;
																	border-top: 1px solid black;
																	">';
	  $logPdf->_template['thead_open']= '<thead>';



	  $logPdf->_template['heading_row_start']= '<tr style="">';



	  $logPdf->_template['heading_cell_start']= '<th style="
																	color: #000000;
																	font-weight: bolder;
																	border-bottom:1px solid black;
																	border-right: 1px solid black;
																	text-align: center;
 																	white-space: nowrap;
																">';


	  $logPdf->_template['cell_start']= '<td nowrap style="
														
														color: #000000;
 														margin: 5px auto;
 														border-right: 1px solid black;
 														border-bottom: 1px solid black;
 														text-align: left;
 														text-align: center;
 														white-space: nowrap;
 														">';


	  $logPdf->_template['cell_alt_start']= '<td nowrap style="
														
														color: #000000;
 														margin: 5px auto;
 														border-right: 1px solid black;
 														border-bottom: 1px solid black;
 														text-align: left;
 														text-align: center;
 														white-space: nowrap;
 														background-color: #beffbb;
 														">';

	  $logPdf->getPdf();

  }

}

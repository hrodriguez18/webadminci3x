<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class termscnb extends CI_Controller {

	public function index()
	{
		$this->load->model('Contrato_m');
		$contrato=$this->Contrato_m->get(16);

		$htmlData['title']=$contrato->Titulo;
		$htmlData['fullText']=$contrato->Texto;
		$this->load->view('paper/TerminosCnb',$htmlData);
	}

	public function Ayuda(){
		$data['name_file']=array(

			'CNBDeposito.svg',
			'CNBNvoCobro.svg',
			'CNBRetiro.svg',
			'CNBReversar.svg',
			'CNBSaldoYMov.svg',
			'CNBUltCobro.svg',
			'CNBMenu.svg',

		);
		$data['title']='Help CNB';
		echo $this->load->view('paper/Help', $data, TRUE);
	}

}

/* End of file Controllername.php */

?>

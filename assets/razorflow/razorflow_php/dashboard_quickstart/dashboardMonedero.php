<?php

// Welcome to the RazorFlow PHP Dashbord Quickstart. Simply copy this "dashboard_quickstart"
// to somewhere in your PHP server to have a dashboard ready to use.
// This is a great way to get started with RazorFlow with minimal time in setup.
// However, once you're ready to go into deployment consult our documentation on tips for how to 
// maintain the most stable and secure 

// Require the library file
require "razorflow_php/razorflow.php";

class MyDashboard extends EmbeddedDashboard {
	
	function __construct($login = NULL, $tx = NULL, $venta = NULL, $kpi1 = NULL, $permisos = NULL, $pais = NULL, $canal = NULL,
						 $aut = NULL, $mfilter = NULL, $yfilter = NULL) {
		if($pais == 'Todos' || $pais == 'All') { $pais = ""; }
		if($canal == 'Todos' || $canal == 'All') { $canal = ""; }
		if($aut == 'Todos' || $aut == 'All') { $aut = ""; }
		$this->login = $login;
		$this->tx = $tx;
		$this->venta = $venta;
		$this->kpi1 = $kpi1;
		$this->permisos = $permisos;
		$this->pais = $pais;
		$this->canal = $canal;
		$this->aut = $aut;
		$this->mfilter = $mfilter;
		$this->yfilter = $yfilter;
		
		$connectionInfo = array("Database"=>"SwitchMPOS", "UID"=>"ESB.DataBaseWeb", "PWD"=>"8urfFh#sdv3FD");
		$conn = sqlsrv_connect("172.31.42.29", $connectionInfo);
		$this->conn = $conn;
	}

	public function buildDashboard(){
		//// KPI /////
		$data = json_decode($this->kpi1);
		
		foreach($data as $result)
		{
			$kpi_data[0] = $result->Cant;
			$kpi_data[1] = $result->Venta;
			$kpi_data[2] = $result->Prom;
		}
		
		if($kpi_data[0] == "") { $kpi_data[0] = 0; }
		if($kpi_data[1] == "") { $kpi_data[1] = 0; }
		if($kpi_data[2] == "") { $kpi_data[2] = 0; }
		
		$kpi = new KPIGroupComponent ('kpi');
		$kpi->setDimensions (12, 2);
		$title = "KPI";
		if($this->pais != "") { $title = $title." / ".$this->pais; }
		if($this->canal != "") { $title = $title." / ".$this->canal; }
		if($this->aut != "") { $title = $title." / ".$this->aut; }
			
		$kpi->setCaption($title);
		if(array_search('15', $this->permisos) > 0) {
			$title = lang('depositos');
			$kpi->addKPI('ventas', array(
			  'caption' => $title,
			  'value' => $kpi_data[1],
			  'numberPrefix' => '$',
			  'numberForceDecimals' => 2
			));
		}

		if(array_search('16', $this->permisos) > 0) {
			$title = lang('retiros');
			$kpi->addKPI('transacciones', array(
			  'caption' => $title,
			  'value' => $kpi_data[0],
			  'numberPrefix' => '$',
			  'numberForceDecimals' => 2,
			  'numberSuffix' => ''
			));
		}
		
		if(array_search('22', $this->permisos) > 0) {
			$title = lang('pagos');
			$kpi->addKPI('promedio', array(
			  'caption' => $title,
			  'value' => $kpi_data[2],
			  'numberPrefix' => '$'
			));
		}

		if(array_search('15', $this->permisos) > 0 || array_search('16', $this->permisos) > 0 || array_search('22', $this->permisos) > 0) {
			$this->addComponent ($kpi);
		}
		//// KPI /////
		
		//// LOGIN ////
		/*if(array_search('19', $this->permisos) > 0) {
			$chart = new ChartComponent("chart");
			$data = json_decode($this->login);
			$title = "Login";
			if($this->canal != "") { $title = $title." / ".$this->canal; }
			$chart->setCaption($title);
			$chart->setDimensions (6, 4);
			
			$i = 0;
			foreach($data as $result)
			{
				$labels[$i] = $result->Pais;
				$login_data[$i] = $result->Cant;
				$i++;
			}
			
			if($labels[0] == "") { $labels[0] = ""; }
			if($login_data[0] == "") { $login_data[0] = 0; }
			
			$chart->setYAxis("Cantidad");
			$chart->setLabels($labels);
			$chart->addSeries("login", "Login Total", $login_data);

			$chart->addDrillStep("getLoginUser", $this);
			
			$chart->addDrillStep("getLoginMonth", $this);
			
			$chart->addDrillStep("getLoginDay", $this);
			
			$chart->addDrillStep("getLoginHour", $this);
		}
		*/
		//// LOGIN ////
		
		//// TX/VENTAS ////
		if(array_search('17', $this->permisos) > 0) {
			$chart1 = new ChartComponent("chart1");
			$data = json_decode($this->tx);
			$title = lang('depositos');
			if($this->canal != "") { $title = $title." / ".$this->canal; }
			if($this->aut != "") { $title = $title." / ".$this->aut; }
			$chart1->setCaption($title);
			$chart1->setDimensions (6, 4);
			
			$i = 0;
			foreach($data as $result)
			{
				$labels[$i] = $result->Pais;
				$tx_data[$i] = $result->Cant;
				$i++;
			}
			
			if($labels[0] == "") { $labels[0] = ""; }
			if($tx_data[0] == "") { $tx_data[0] = 0; }
			
			$chart1->setYAxis(lang('quantity'));
			$chart1->setLabels($labels);
			$chart1->addSeries("Depositos", "", $tx_data);
								
			$chart1->addDrillStep("getTxUser", $this);
								
			$chart1->addDrillStep("getTxMonth", $this);
			
			$chart1->addDrillStep("getTxDay", $this);
			
			$chart1->addDrillStep("getTxHour", $this);	
		}
		
		if(array_search('23', $this->permisos) > 0) {
			$chart2 = new ChartComponent("chart2");
			$data1 = json_decode($this->venta);
			$title = lang('retiros');
			if($this->canal != "") { $title = $title." / ".$this->canal; }
			if($this->aut != "") { $title = $title." / ".$this->aut; }
			$chart2->setCaption($title);
			$chart2->setDimensions (6, 4);
			
			$i = 0;
			foreach($data1 as $result)
			{
				$labels[$i] = $result->Pais;
				$sales_data[$i] = $result->Cant;
				$i++;
			}
			
			if($labels[0] == "") { $labels[0] = ""; }
			if($sales_data[0] == "") { $sales_data[0] = 0; }
			
			$chart2->setYAxis(lang('quantity'));
			$chart2->setLabels($labels);
			$chart2->addSeries("Retiros", "", $sales_data, array(
								  "numberPrefix" => "$",
								  "numberForceDecimals" => true
								));
			$chart2->addDrillStep("getVUser", $this);
								
			$chart2->addDrillStep("getVMonth", $this);
			
			$chart2->addDrillStep("getVDay", $this);
			
			$chart2->addDrillStep("getVHour", $this);
		}
		//// TX/VENTAS ////
		
		if(array_search('19', $this->permisos) > 0) {
			//$this->addComponent ($chart);
		}
		if(array_search('17', $this->permisos) > 0) {
			$this->addComponent ($chart1);
		}
		
		if(array_search('23', $this->permisos) > 0) {
			$this->addComponent ($chart2);
		}
	}
	
	/*
	public function getLoginUser($source, $target, $params)
	{
		$cSelect = $_GET['cSelect'];
		if($cSelect == 0) { $cSelectM = "> '0'"; }
		else { $cSelectM = "= '".$cSelect."'"; }
			
		$query = "SELECT Terminales.Usuario AS Usuario, COUNT(LogNum) AS Cant
					FROM [esb].[Log_Login]
					INNER JOIN esb.Terminales ON Terminales.ID_Usuario = Log_Login.ID_Usuario
					INNER JOIN esb.Pais ON Pais.Cod_Pais = Log_Login.Pais
					INNER JOIN esb.Canales ON Canales.Id = Log_Login.Canal
					WHERE Pais.Descripcion = '".$params["label"]."'
					AND Canales.Id $cSelectM
					GROUP BY Terminales.Usuario";
		$result = sqlsrv_query($this->conn, $query);
		$i = 0;
		while($row = @sqlsrv_fetch_array($result, SQLSRV_FETCH_NUMERIC)) {
			$labels[$i] = $row[0];
            $login_data[$i] = $row[1];
			$i++;
		}
		
		$source->setCaption("Login de ".$params["label"]);
        $source->setLabels($labels);
        $source->addSeries("login", "Login Total por Usuario", $login_data);
	}
	
	public function getLoginMonth($source, $target, $params)
	{
		$cSelect = $_GET['cSelect'];
		if($cSelect == 0) { $cSelectM = "> '0'"; }
		else { $cSelectM = "= '".$cSelect."'"; }
		
		$query = "SELECT SUBSTRING(CAST(Time_Stamp AS VARCHAR),1,3) as Mes, COUNT(LogNum)
					FROM [esb].[Log_Login]
					INNER JOIN esb.Terminales ON Terminales.ID_Usuario = Log_Login.ID_Usuario
					INNER JOIN esb.Pais ON Pais.Cod_Pais = Log_Login.Pais
					INNER JOIN esb.Canales ON Canales.Id = Log_Login.Canal
					WHERE Terminales.Usuario = '".$params["label"]."'
					AND Pais.Descripcion = '".$params['drillLabelList'][0]."'
					AND Canales.Id $cSelectM
					GROUP BY SUBSTRING(CAST(Time_Stamp AS VARCHAR),1,3)";
		$result = sqlsrv_query($this->conn, $query);
		$i = 0;
		while($row = @sqlsrv_fetch_array($result, SQLSRV_FETCH_NUMERIC)) {
			$labels[$i] = $row[0];
            $login_data[$i] = $row[1];
			$i++;
		}
		
		$source->setCaption("Login de ".$params["label"]);
        $source->setLabels($labels);
        $source->addSeries("login", "Login Total por Mes", $login_data);
	}
	
	public function getLoginDay($source, $target, $params)
	{
		$cSelect = $_GET['cSelect'];
		if($cSelect == 0) { $cSelectM = "> '0'"; }
		else { $cSelectM = "= '".$cSelect."'"; }
		
		$query = "SELECT SUBSTRING(CAST(Time_Stamp AS VARCHAR),5,2) as Dia, COUNT(LogNum)
					FROM [esb].[Log_Login]
					INNER JOIN esb.Terminales ON Terminales.ID_Usuario = Log_Login.ID_Usuario
					INNER JOIN esb.Pais ON Pais.Cod_Pais = Log_Login.Pais
					INNER JOIN esb.Canales ON Canales.Id = Log_Login.Canal
					WHERE Terminales.Usuario = '".$params['drillLabelList'][1]."' AND
					SUBSTRING(CAST(Time_Stamp AS VARCHAR),1,3) = '".$params["label"]."' AND
					Pais.Descripcion = '".$params['drillLabelList'][0]."'
					AND Canales.Id $cSelectM
					GROUP BY SUBSTRING(CAST(Time_Stamp AS VARCHAR),5,2)";
		$result = sqlsrv_query($this->conn, $query);
		$i = 0;
		while($row = @sqlsrv_fetch_array($result, SQLSRV_FETCH_NUMERIC)) {
			$labels[$i] = $row[0];
            $login_data[$i] = $row[1];
			$i++;
		}
		
		$source->setCaption("Mes: ".$params["label"]);
        $source->setLabels($labels);
        $source->addSeries("login", "Login Total por Dia", $login_data);
	}
	
	public function getLoginHour($source, $target, $params)
	{
		$cSelect = $_GET['cSelect'];
		if($cSelect == 0) { $cSelectM = "> '0'"; }
		else { $cSelectM = "= '".$cSelect."'"; }
		
		$query = "SELECT SUBSTRING(CAST(CAST(Time_Stamp as time) AS VARCHAR), 1, 2), COUNT(LogNum)
					FROM [esb].[Log_Login]
					INNER JOIN esb.Terminales ON Terminales.ID_Usuario = Log_Login.ID_Usuario
					INNER JOIN esb.Pais ON Pais.Cod_Pais = Log_Login.Pais
					INNER JOIN esb.Canales ON Canales.Id = Log_Login.Canal
					WHERE Terminales.Usuario = '".$params['drillLabelList'][1]."' AND
					SUBSTRING(CAST(Time_Stamp AS VARCHAR),1,3) = '".$params['drillLabelList'][2]."' AND
					SUBSTRING(CAST(Time_Stamp AS VARCHAR),5,2) = '".$params["label"]."' AND
					Pais.Descripcion = '".$params['drillLabelList'][0]."'
					AND Canales.Id $cSelectM
					GROUP BY SUBSTRING(CAST(CAST(Time_Stamp as time) AS VARCHAR), 1, 2)";
		$result = sqlsrv_query($this->conn, $query);
		$i = 0;
		while($row = @sqlsrv_fetch_array($result, SQLSRV_FETCH_NUMERIC)) {
			$labels[$i] = $row[0];
            $login_data[$i] = $row[1];
			$i++;
		}
		
		$source->setCaption("Dia: ".$params["label"]);
        $source->setLabels($labels);
        $source->addSeries("login", "Login Total por Hora", $login_data);
	}
	*/
	
	public function getTxUser($source, $target, $params)
	{
		$yfilter = @$_GET['yfilter'];
		if($yfilter == "") { $yfilter = date('Y'); }
		
		$mfilter = @$_GET['mfilter'];
		if($mfilter == "") { $mfilter = date('m'); }
		if($mfilter == "12") { $yfilter1 = $yfilter + 1; } else { $yfilter1 = $yfilter; }
		
		$mfilter1 = date('m', (strtotime('1 month', strtotime(date($yfilter.'-'.$mfilter.'-01')))));
		
		$cSelect = @$_GET['cSelect'];
		if($cSelect == 0 || $cSelect == "") { $cSelectM = "> '0'"; }
		else { $cSelectM = "= '".$cSelect."'"; }
		
		$aSelect = @$_GET['aSelect'];
		if($aSelect == 0 || acSelect == "") { $aSelectM = "> '0'"; }
		else { $aSelectM = "= '".$aSelect."'"; }
		
		$query = "SELECT Usuario AS Usuario, COUNT(TranNum) AS Cant
					FROM esb.Log_Transaccional
					WHERE Log_Transaccional.Cod_Respuesta = '00'
					AND NPais = '".$params["label"]."'
					AND Canal $cSelectM
					AND Cod_Biller $aSelectM
					And Time_Stamp Between '$yfilter-$mfilter-01' And '$yfilter1-$mfilter1-01'
					GROUP BY Usuario";
		$result = sqlsrv_query($this->conn, $query);
		$i = 0;
		while($row = @sqlsrv_fetch_array($result, SQLSRV_FETCH_NUMERIC)) {
			$labels[$i] = $row[0];
            $tx_data[$i] = $row[1];
			$i++;
		}
		
		$source->setCaption($params["label"]);
        $source->setLabels($labels);
        $source->addSeries("Transacciones", "", $tx_data);
	}
	
	public function getTxMonth($source, $target, $params)
	{
		$yfilter = @$_GET['yfilter'];
		if($yfilter == "") { $yfilter = date('Y'); }
		
		$mfilter = @$_GET['mfilter'];
		if($mfilter == "") { $mfilter = date('m'); }
		if($mfilter == "12") { $yfilter1 = $yfilter + 1; } else { $yfilter1 = $yfilter; }
		
		$mfilter1 = date('m', (strtotime('1 month', strtotime(date($yfilter.'-'.$mfilter.'-01')))));
		
		$cSelect = @$_GET['cSelect'];
		if($cSelect == 0 || $cSelect == "") { $cSelectM = "> '0'"; }
		else { $cSelectM = "= '".$cSelect."'"; }
		
		$aSelect = @$_GET['aSelect'];
		if($aSelect == 0 || acSelect == "") { $aSelectM = "> '0'"; }
		else { $aSelectM = "= '".$aSelect."'"; }
		
		$query = "SELECT SUBSTRING(CAST(Time_Stamp AS VARCHAR),1,3) as Mes, COUNT(TranNum)
					FROM [esb].[Log_Transaccional]
					WHERE Usuario = '".$params["label"]."' AND
					Cod_Respuesta = '00' AND
					NPais = '".$params['drillLabelList'][0]."'
					AND Canal $cSelectM
					AND Cod_Biller $aSelectM
					And Time_Stamp Between '$yfilter-$mfilter-01' And '$yfilter1-$mfilter1-01'
					GROUP BY SUBSTRING(CAST(Time_Stamp AS VARCHAR),1,3)";
		$result = sqlsrv_query($this->conn, $query);
		$i = 0;
		while($row = @sqlsrv_fetch_array($result, SQLSRV_FETCH_NUMERIC)) {
			$labels[$i] = $row[0];
            $tx_data[$i] = $row[1];
			$i++;
		}
		
		$source->setCaption($params["label"]);
        $source->setLabels($labels);
        $source->addSeries("Transacciones", "", $tx_data, array(
							  "seriesDisplayType" => "line",
							  "seriesColor" => "#a4c9f3" 
							));
	}
	
	public function getTxDay($source, $target, $params)
	{
		$yfilter = @$_GET['yfilter'];
		if($yfilter == "") { $yfilter = date('Y'); }
		
		$mfilter = @$_GET['mfilter'];
		if($mfilter == "") { $mfilter = date('m'); }
		if($mfilter == "12") { $yfilter1 = $yfilter + 1; } else { $yfilter1 = $yfilter; }
		
		$mfilter1 = date('m', (strtotime('1 month', strtotime(date($yfilter.'-'.$mfilter.'-01')))));
		
		$cSelect = @$_GET['cSelect'];
		if($cSelect == 0 || $cSelect == "") { $cSelectM = "> '0'"; }
		else { $cSelectM = "= '".$cSelect."'"; }
		
		$aSelect = @$_GET['aSelect'];
		if($aSelect == 0 || acSelect == "") { $aSelectM = "> '0'"; }
		else { $aSelectM = "= '".$aSelect."'"; }
		
		$query = "SELECT SUBSTRING(CAST(Time_Stamp AS VARCHAR),5,2) as Dia, COUNT(TranNum)
					FROM [esb].[Log_Transaccional]
					WHERE Usuario = '".$params['drillLabelList'][1]."' AND
					SUBSTRING(CAST(Time_Stamp AS VARCHAR),1,3) = '".$params["label"]."' AND
					Cod_Respuesta = '00' AND
					NPais = '".$params['drillLabelList'][0]."'
					AND Canal $cSelectM
					AND Cod_Biller $aSelectM
					And Time_Stamp Between '$yfilter-$mfilter-01' And '$yfilter1-$mfilter1-01'
					GROUP BY SUBSTRING(CAST(Time_Stamp AS VARCHAR),5,2)";
		$result = sqlsrv_query($this->conn, $query);
		$i = 0;
		while($row = @sqlsrv_fetch_array($result, SQLSRV_FETCH_NUMERIC)) {
			$labels[$i] = $row[0];
            $tx_data[$i] = $row[1];
			$i++;
		}
		
        $source->setCaption($params["label"]);
        $source->setLabels($labels);
        $source->addSeries("Transacciones", "", $tx_data, array(
							  "seriesDisplayType" => "line",
							  "seriesColor" => "#a4c9f3" 
							));
	}
	
	public function getTxHour($source, $target, $params)
	{
		$yfilter = @$_GET['yfilter'];
		if($yfilter == "") { $yfilter = date('Y'); }
		
		$mfilter = @$_GET['mfilter'];
		if($mfilter == "") { $mfilter = date('m'); }
		if($mfilter == "12") { $yfilter1 = $yfilter + 1; } else { $yfilter1 = $yfilter; }
		
		$mfilter1 = date('m', (strtotime('1 month', strtotime(date($yfilter.'-'.$mfilter.'-01')))));
		
		$cSelect = @$_GET['cSelect'];
		if($cSelect == 0 || $cSelect == "") { $cSelectM = "> '0'"; }
		else { $cSelectM = "= '".$cSelect."'"; }
		
		$aSelect = @$_GET['aSelect'];
		if($aSelect == 0 || acSelect == "") { $aSelectM = "> '0'"; }
		else { $aSelectM = "= '".$aSelect."'"; }
		
		$query = "SELECT SUBSTRING(CAST(CAST(Time_Stamp as time) AS VARCHAR), 1, 2), COUNT(TranNum)
					FROM [esb].[Log_Transaccional]
					WHERE Usuario = '".$params['drillLabelList'][1]."' AND
					SUBSTRING(CAST(Time_Stamp AS VARCHAR),1,3) = '".$params['drillLabelList'][2]."' AND
					SUBSTRING(CAST(Time_Stamp AS VARCHAR),5,2) = '".$params["label"]."' AND
					Cod_Respuesta = '00' AND
					NPais = '".$params['drillLabelList'][0]."'
					AND Canal $cSelectM
					AND Cod_Biller $aSelectM
					And Time_Stamp Between '$yfilter-$mfilter-01' And '$yfilter1-$mfilter1-01'
					GROUP BY SUBSTRING(CAST(CAST(Time_Stamp as time) AS VARCHAR), 1, 2)";
		$result = sqlsrv_query($this->conn, $query);
		$i = 0;
		while($row = @sqlsrv_fetch_array($result, SQLSRV_FETCH_NUMERIC)) {
			$labels[$i] = $row[0];
            $tx_data[$i] = $row[1];
			$i++;
		}
		
		$source->setCaption($params["label"]);
        $source->setLabels($labels);
        $source->addSeries("Transacciones", "", $tx_data, array(
							  "seriesDisplayType" => "line",
							  "seriesColor" => "#a4c9f3" 
							));
	}
	
	public function getVUser($source, $target, $params)
	{
		$yfilter = @$_GET['yfilter'];
		if($yfilter == "") { $yfilter = date('Y'); }
		
		$mfilter = @$_GET['mfilter'];
		if($mfilter == "") { $mfilter = date('m'); }
		if($mfilter == "12") { $yfilter1 = $yfilter + 1; } else { $yfilter1 = $yfilter; }
		
		$mfilter1 = date('m', (strtotime('1 month', strtotime(date($yfilter.'-'.$mfilter.'-01')))));
		
		$cSelect = @$_GET['cSelect'];
		if($cSelect == 0 || $cSelect == "") { $cSelectM = "> '0'"; }
		else { $cSelectM = "= '".$cSelect."'"; }
		
		$aSelect = @$_GET['aSelect'];
		if($aSelect == 0 || acSelect == "") { $aSelectM = "> '0'"; }
		else { $aSelectM = "= '".$aSelect."'"; }
		
		$query = "SELECT Usuario AS Usuario, SUM(Monto_Total) AS Cant
					FROM esb.Log_Transaccional
					WHERE Cod_Respuesta = '00'
					AND NPais = '".$params["label"]."'
					AND Canal $cSelectM
					AND Cod_Biller $aSelectM
					And Time_Stamp Between '$yfilter-$mfilter-01' And '$yfilter1-$mfilter1-01'
					GROUP BY Usuario";
		$result = sqlsrv_query($this->conn, $query);
		$i = 0;
		while($row = @sqlsrv_fetch_array($result, SQLSRV_FETCH_NUMERIC)) {
			$labels[$i] = $row[0];
            $sales_data[$i] = $row[1];
			$i++;
		}
		
		$source->setCaption($params["label"]);
        $source->setLabels($labels);
        $source->addSeries("Ventas", "", $sales_data, array(
							  "numberPrefix" => "$",
							  "numberForceDecimals" => true
							));
	}
	
	public function getVMonth($source, $target, $params)
	{
		$yfilter = @$_GET['yfilter'];
		if($yfilter == "") { $yfilter = date('Y'); }
		
		$mfilter = @$_GET['mfilter'];
		if($mfilter == "") { $mfilter = date('m'); }
		if($mfilter == "12") { $yfilter1 = $yfilter + 1; } else { $yfilter1 = $yfilter; }
		
		$mfilter1 = date('m', (strtotime('1 month', strtotime(date($yfilter.'-'.$mfilter.'-01')))));
		
		$cSelect = @$_GET['cSelect'];
		if($cSelect == 0 || $cSelect == "") { $cSelectM = "> '0'"; }
		else { $cSelectM = "= '".$cSelect."'"; }
		
		$aSelect = @$_GET['aSelect'];
		if($aSelect == 0 || acSelect == "") { $aSelectM = "> '0'"; }
		else { $aSelectM = "= '".$aSelect."'"; }
		
		$query = "SELECT SUBSTRING(CAST(Time_Stamp AS VARCHAR),1,3) as Mes, SUM(Monto_Total)
					FROM [esb].[Log_Transaccional]
					WHERE Usuario = '".$params["label"]."' AND
					Cod_Respuesta = '00' AND
					NPais = '".$params['drillLabelList'][0]."'
					AND Canal $cSelectM
					AND Cod_Biller $aSelectM
					And Time_Stamp Between '$yfilter-$mfilter-01' And '$yfilter1-$mfilter1-01'
					GROUP BY SUBSTRING(CAST(Time_Stamp AS VARCHAR),1,3)";
		$result = sqlsrv_query($this->conn, $query);
		$i = 0;
		while($row = @sqlsrv_fetch_array($result, SQLSRV_FETCH_NUMERIC)) {
			$labels[$i] = $row[0];
            $sales_data[$i] = $row[1];
			$i++;
		}
		
		$source->setCaption($params["label"]);
        $source->setLabels($labels);
        $source->addSeries("Ventas", "", $sales_data, array(
							  "seriesDisplayType" => "line",
							  "seriesColor" => "#a4c9f3" 
							));
	}
	
	public function getVDay($source, $target, $params)
	{
		$yfilter = @$_GET['yfilter'];
		if($yfilter == "") { $yfilter = date('Y'); }
		
		$mfilter = @$_GET['mfilter'];
		if($mfilter == "") { $mfilter = date('m'); }
		if($mfilter == "12") { $yfilter1 = $yfilter + 1; } else { $yfilter1 = $yfilter; }
		
		$mfilter1 = date('m', (strtotime('1 month', strtotime(date($yfilter.'-'.$mfilter.'-01')))));
		
		$cSelect = @$_GET['cSelect'];
		if($cSelect == 0 || $cSelect == "") { $cSelectM = "> '0'"; }
		else { $cSelectM = "= '".$cSelect."'"; }
		
		$aSelect = @$_GET['aSelect'];
		if($aSelect == 0 || acSelect == "") { $aSelectM = "> '0'"; }
		else { $aSelectM = "= '".$aSelect."'"; }
		
		$query = "SELECT SUBSTRING(CAST(Time_Stamp AS VARCHAR),5,2) as Dia, SUM(Monto_Total)
					FROM [esb].[Log_Transaccional]
					WHERE Usuario = '".$params['drillLabelList'][1]."' AND
					SUBSTRING(CAST(Time_Stamp AS VARCHAR),1,3) = '".$params["label"]."' AND
					Cod_Respuesta = '00' AND
					NPais = '".$params['drillLabelList'][0]."'
					AND Canal $cSelectM
					AND Cod_Biller $aSelectM
					And Time_Stamp Between '$yfilter-$mfilter-01' And '$yfilter1-$mfilter1-01'
					GROUP BY SUBSTRING(CAST(Time_Stamp AS VARCHAR),5,2)";
		$result = sqlsrv_query($this->conn, $query);
		$i = 0;
		while($row = @sqlsrv_fetch_array($result, SQLSRV_FETCH_NUMERIC)) {
			$labels[$i] = $row[0];
            $sales_data[$i] = $row[1];
			$i++;
		}
		
        $source->setCaption($params["label"]);
        $source->setLabels($labels);
        $source->addSeries("Ventas", "", $sales_data, array(
							  "seriesDisplayType" => "line",
							  "seriesColor" => "#a4c9f3" 
							));
	}
	
	public function getVHour($source, $target, $params)
	{
		$yfilter = @$_GET['yfilter'];
		if($yfilter == "") { $yfilter = date('Y'); }
		
		$mfilter = @$_GET['mfilter'];
		if($mfilter == "") { $mfilter = date('m'); }
		if($mfilter == "12") { $yfilter1 = $yfilter + 1; } else { $yfilter1 = $yfilter; }
		
		$mfilter1 = date('m', (strtotime('1 month', strtotime(date($yfilter.'-'.$mfilter.'-01')))));
		
		$cSelect = @$_GET['cSelect'];
		if($cSelect == 0 || $cSelect == "") { $cSelectM = "> '0'"; }
		else { $cSelectM = "= '".$cSelect."'"; }
		
		$aSelect = @$_GET['aSelect'];
		if($aSelect == 0 || acSelect == "") { $aSelectM = "> '0'"; }
		else { $aSelectM = "= '".$aSelect."'"; }
		
		$query = "SELECT SUBSTRING(CAST(CAST(Time_Stamp as time) AS VARCHAR), 1, 2), SUM(Monto_Total)
					FROM [esb].[Log_Transaccional]
					WHERE Usuario = '".$params['drillLabelList'][1]."' AND
					SUBSTRING(CAST(Time_Stamp AS VARCHAR),1,3) = '".$params['drillLabelList'][2]."' AND
					SUBSTRING(CAST(Time_Stamp AS VARCHAR),5,2) = '".$params["label"]."' AND
					Cod_Respuesta = '00' AND
					NPais = '".$params['drillLabelList'][0]."'
					AND Canal $cSelectM
					AND Cod_Biller $aSelectM
					And Time_Stamp Between '$yfilter-$mfilter-01' And '$yfilter1-$mfilter1-01'
					GROUP BY SUBSTRING(CAST(CAST(Time_Stamp as time) AS VARCHAR), 1, 2)";
		$result = sqlsrv_query($this->conn, $query);
		$i = 0;
		while($row = @sqlsrv_fetch_array($result, SQLSRV_FETCH_NUMERIC)) {
			$labels[$i] = $row[0];
            $sales_data[$i] = $row[1];
			$i++;
		}
		
		$source->setCaption($params["label"]);
        $source->setLabels($labels);
        $source->addSeries("Ventas", "", $sales_data, array(
							  "seriesDisplayType" => "line",
							  "seriesColor" => "#a4c9f3" 
							));
	}
}
?>
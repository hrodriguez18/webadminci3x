<?php

// Welcome to the RazorFlow PHP Dashbord Quickstart. Simply copy this "dashboard_quickstart"
// to somewhere in your PHP server to have a dashboard ready to use.
// This is a great way to get started with RazorFlow with minimal time in setup.
// However, once you're ready to go into deployment consult our documentation on tips for how to 
// maintain the most stable and secure 

// Require the library file
require "razorflow_php/razorflow.php";

class MyDashboard extends EmbeddedDashboard {
	
	function __construct($tx = NULL, $venta = NULL, $permisos = NULL, $acquiredId = NULL, $idCommerce = NULL) {
		$this->tx = $tx;
		$this->venta = $venta;
		$this->permisos = $permisos;
		$this->acquiredId = $acquiredId;
		$this->idCommerce = $idCommerce;
		
		$connectionInfo = array("Database"=>"SwitchMPOS", "UID"=>"ESB.DataBaseUser", "PWD"=>"8urfFh#sdv3FD");
		$conn = sqlsrv_connect("172.31.42.29", $connectionInfo);
		$this->conn = $conn;
	}

	public function buildDashboard(){
		//// VENTAS ////
		$labels = $sale_data = $tx_data = null;
		/*$permisos = @json_decode($this->permisos);
		$i = 0;
		foreach($permisos as $result)
		{
			$permiso[$i] = $result->permiso_id;
			$i++;
		}*/
		
		$chart1 = new ChartComponent("chart1");
		$data = json_decode($this->venta);
		$title = lang('sales');
		$chart1->setDimensions (6, 4);
		
		$i = 0;
		foreach($data as $result)
		{
			$labels[$i] = $result->fechas.'';
			$sale_data[$i] = $result->Cant;
			$i++;
		}
		
		if(sizeof($labels) > 0 ) { if($labels[0] == "") { $labels[0] = ""; } }
		if(sizeof($sale_data) > 0 ) { if($sale_data[0] == "") { $sale_data[0] = 0; } }
		
		$chart1->setYAxis(lang('quantity'));
		$chart1->setLabels($labels);
		$chart1->addSeries("Ventas", "", $sale_data, array(
						  "seriesDisplayType" => "column",
						  "seriesColor" => "#845BD4" 
						));
							
		$chart1->addDrillStep("getSaleYear", $this);
		
		$chart1->addDrillStep("getSaleMonth", $this);
		
		$chart1->addDrillStep("getSaleDay", $this);
		
		$chart1->addDrillStep("getSaleHour", $this);	
		
		$this->addComponent ($chart1);
		
		$chart2 = new ChartComponent("chart2");
		$data = json_decode($this->tx);
		$title = lang('transacciones');
		$chart2->setDimensions (6, 4);
		
		$i = 0;
		foreach($data as $result)
		{
			$labels[$i] = $result->fechas.'';
			$tx_data[$i] = $result->Cant;
			$i++;
		}
		
		if($labels[0] == "") { $labels[0] = ""; }
		if($tx_data[0] == "") { $tx_data[0] = 0; }
		
		$chart2->setYAxis(lang('quantity'));
		$chart2->setLabels($labels);
		$chart2->addSeries("Transacciones", "", $tx_data, array(
						  "seriesDisplayType" => "column",
						  "seriesColor" => "#71C73E" 
						));
							
		$chart2->addDrillStep("getTxYear", $this);
		
		$chart2->addDrillStep("getTxMonth", $this);
		
		$chart2->addDrillStep("getTxDay", $this);
		
		$chart2->addDrillStep("getTxHour", $this);
		
		$this->addComponent ($chart2);
	}
	
	//// VENTAS ////
	public function getSaleYear($source, $target, $params)
	{
		$query = "SELECT CAST(YEAR(timestamp) AS VARCHAR(4)) as fechas, SUM(realAmount) AS Cant
									FROM ecom.e_transactionLog
									WHERE authorizationResult = '00'
									AND status = 'C'
									--AND acquirerId = '$acquirerId'
									--AND idCommerce = '$idCommerce'
									AND brand = '".$params["label"]."'
									GROUP BY CAST(YEAR(timestamp) AS VARCHAR(4));
				 ";
		$result = sqlsrv_query($this->conn, $query);
		$i = 0;
		while($row = @sqlsrv_fetch_array($result, SQLSRV_FETCH_NUMERIC)) {
			$labels[$i] = $row[0].'';
            $sales_data[$i] = $row[1];
			$i++;
		}
		
		//$source->setCaption($params["label"]);
        $source->setLabels($labels);
        $source->addSeries("Ventas", "", $sales_data, array(
							  "seriesDisplayType" => "column",
							  "seriesColor" => "#845BD4" 
							));
	}
	
	public function getSaleMonth($source, $target, $params)
	{
		$query = "SELECT CAST(YEAR(timestamp) AS VARCHAR(4))+'-'+CAST(MONTH(timestamp) AS VARCHAR(2)) as fechas, SUM(realAmount) AS Cant
									FROM ecom.e_transactionLog
									WHERE authorizationResult = '00'
									AND status = 'C'
									--AND acquirerId = '$acquirerId'
									--AND idCommerce = '$idCommerce'
									AND brand = '".$params['drillLabelList'][0]."'
									AND timestamp BETWEEN '".$params["label"]."-01-01 00:00:00' AND '".$params["label"]."-12-31 23:59:59'
									GROUP BY CAST(YEAR(timestamp) AS VARCHAR(4))+'-'+CAST(MONTH(timestamp) AS VARCHAR(2));
				 ";
		$result = sqlsrv_query($this->conn, $query);
		$i = 0;
		while($row = @sqlsrv_fetch_array($result, SQLSRV_FETCH_NUMERIC)) {
			$labels[$i] = $row[0].'';
            $sales_data[$i] = $row[1];
			$i++;
		}
		
		//$source->setCaption($params["label"]);
        $source->setLabels($labels);
        $source->addSeries("Ventas", "", $sales_data, array(
							  "seriesDisplayType" => "column",
							  "seriesColor" => "#845BD4" 
							));
	}
	
	public function getSaleDay($source, $target, $params)
	{
		$y = substr($params['label'], 0, strpos($params['label'], "-"));
		$m = substr($params['label'], strpos($params['label'], "-") + 1, strlen($params['label']));
		$tday = date("t", mktime(0,0,0,$m,1,$y));
		$query = "SELECT CAST(YEAR(timestamp) AS VARCHAR(4))+'-'+CAST(MONTH(timestamp) AS VARCHAR(2))+'-'+CAST(DAY(timestamp) AS VARCHAR(2)) as fechas, SUM(realAmount) AS Cant
									FROM ecom.e_transactionLog
									WHERE authorizationResult = '00'
									AND status = 'C'
									--AND acquirerId = '$acquirerId'
									--AND idCommerce = '$idCommerce'
									AND brand = '".$params['drillLabelList'][0]."'
									AND timestamp BETWEEN '".$params["label"]."-01 00:00:00' AND '".$params["label"]."-$tday 23:59:59'
									GROUP BY CAST(YEAR(timestamp) AS VARCHAR(4))+'-'+CAST(MONTH(timestamp) AS VARCHAR(2))+'-'+CAST(DAY(timestamp) AS VARCHAR(2));
				 ";
		$result = sqlsrv_query($this->conn, $query);
		$i = 0;
		while($row = @sqlsrv_fetch_array($result, SQLSRV_FETCH_NUMERIC)) {
			$labels[$i] = $row[0].'';
            $sales_data[$i] = $row[1];
			$i++;
		}
		
        //$source->setCaption($params["label"]);
        $source->setLabels($labels);
        $source->addSeries("Ventas", "", $sales_data, array(
							  "seriesDisplayType" => "line",
							  "seriesColor" => "#845BD4" 
							));
	}
	
	public function getSaleHour($source, $target, $params)
	{
		$query = "SELECT CAST(YEAR(timestamp) AS VARCHAR(4))+'-'+CAST(MONTH(timestamp) AS VARCHAR(2))+'-'+CAST(DAY(timestamp) AS VARCHAR(2))+' '+CAST(DATEPART(hh, timestamp) AS VARCHAR(2)) as fechas, SUM(realAmount) AS Cant
									FROM ecom.e_transactionLog
									WHERE authorizationResult = '00'
									AND status = 'C'
									--AND acquirerId = '$acquirerId'
									--AND idCommerce = '$idCommerce'
									AND brand = '".$params['drillLabelList'][0]."'
									AND timestamp BETWEEN '".$params["label"]." 00:00:00' AND '".$params["label"]." 23:59:59'
									GROUP BY CAST(YEAR(timestamp) AS VARCHAR(4))+'-'+CAST(MONTH(timestamp) AS VARCHAR(2))+'-'+CAST(DAY(timestamp) AS VARCHAR(2))+' '+CAST(DATEPART(hh, timestamp) AS VARCHAR(2));
				 ";
		$result = sqlsrv_query($this->conn, $query);
		$i = 0;
		while($row = @sqlsrv_fetch_array($result, SQLSRV_FETCH_NUMERIC)) {
			$labels[$i] = $row[0].'';
            $sales_data[$i] = $row[1];
			$i++;
		}
		
        //$source->setCaption($params["label"]);
        $source->setLabels($labels);
        $source->addSeries("Ventas", "", $sales_data, array(
							  "seriesDisplayType" => "line",
							  "seriesColor" => "#845BD4" 
							));
	}
	
	//// TRANSACCIONES ////
	public function getTxYear($source, $target, $params)
	{
		$query = "SELECT CAST(YEAR(timestamp) AS VARCHAR(4)) as fechas, COUNT(Id) AS Cant
									FROM ecom.e_transactionLog
									WHERE authorizationResult = '00'
									AND status = 'C'
									--AND acquirerId = '$acquirerId'
									--AND idCommerce = '$idCommerce'
									AND brand = '".$params["label"]."'
									GROUP BY CAST(YEAR(timestamp) AS VARCHAR(4));
				 ";
		$result = sqlsrv_query($this->conn, $query);
		$i = 0;
		while($row = @sqlsrv_fetch_array($result, SQLSRV_FETCH_NUMERIC)) {
			$labels[$i] = $row[0].'';
            $sales_data[$i] = $row[1];
			$i++;
		}
		
		//$source->setCaption($params["label"]);
        $source->setLabels($labels);
        $source->addSeries("Ventas", "", $sales_data, array(
							  "seriesDisplayType" => "column",
							  "seriesColor" => "#71C73E" 
							));
	}
	
	public function getTxMonth($source, $target, $params)
	{
		$query = "SELECT CAST(YEAR(timestamp) AS VARCHAR(4))+'-'+CAST(MONTH(timestamp) AS VARCHAR(2)) as fechas, COUNT(Id) AS Cant
									FROM ecom.e_transactionLog
									WHERE authorizationResult = '00'
									AND status = 'C'
									--AND acquirerId = '$acquirerId'
									--AND idCommerce = '$idCommerce'
									AND brand = '".$params['drillLabelList'][0]."'
									AND timestamp BETWEEN '".$params["label"]."-01-01 00:00:00' AND '".$params["label"]."-12-31 23:59:59'
									GROUP BY CAST(YEAR(timestamp) AS VARCHAR(4))+'-'+CAST(MONTH(timestamp) AS VARCHAR(2));
				 ";
		$result = sqlsrv_query($this->conn, $query);
		$i = 0;
		while($row = @sqlsrv_fetch_array($result, SQLSRV_FETCH_NUMERIC)) {
			$labels[$i] = $row[0].'';
            $sales_data[$i] = $row[1];
			$i++;
		}
		
		//$source->setCaption($params["label"]);
        $source->setLabels($labels);
        $source->addSeries("Ventas", "", $sales_data, array(
							  "seriesDisplayType" => "column",
							  "seriesColor" => "#71C73E" 
							));
	}
	
	public function getTxDay($source, $target, $params)
	{
		$y = substr($params['label'], 0, strpos($params['label'], "-"));
		$m = substr($params['label'], strpos($params['label'], "-") + 1, strlen($params['label']));
		$tday = date("t", mktime(0,0,0,$m,1,$y));
		$query = "SELECT CAST(YEAR(timestamp) AS VARCHAR(4))+'-'+CAST(MONTH(timestamp) AS VARCHAR(2))+'-'+CAST(DAY(timestamp) AS VARCHAR(2)) as fechas, COUNT(Id) AS Cant
									FROM ecom.e_transactionLog
									WHERE authorizationResult = '00'
									AND status = 'C'
									--AND acquirerId = '$acquirerId'
									--AND idCommerce = '$idCommerce'
									AND brand = '".$params['drillLabelList'][0]."'
									AND timestamp BETWEEN '".$params["label"]."-01 00:00:00' AND '".$params["label"]."-$tday 23:59:59'
									GROUP BY CAST(YEAR(timestamp) AS VARCHAR(4))+'-'+CAST(MONTH(timestamp) AS VARCHAR(2))+'-'+CAST(DAY(timestamp) AS VARCHAR(2));
				 ";
		$result = sqlsrv_query($this->conn, $query);
		$i = 0;
		while($row = @sqlsrv_fetch_array($result, SQLSRV_FETCH_NUMERIC)) {
			$labels[$i] = $row[0].'';
            $sales_data[$i] = $row[1];
			$i++;
		}
		
        //$source->setCaption($params["label"]);
        $source->setLabels($labels);
        $source->addSeries("Ventas", "", $sales_data, array(
							  "seriesDisplayType" => "line",
							  "seriesColor" => "#71C73E" 
							));
	}
	
	public function getTxHour($source, $target, $params)
	{
		$query = "SELECT CAST(YEAR(timestamp) AS VARCHAR(4))+'-'+CAST(MONTH(timestamp) AS VARCHAR(2))+'-'+CAST(DAY(timestamp) AS VARCHAR(2))+' '+CAST(DATEPART(hh, timestamp) AS VARCHAR(2)) as fechas, COUNT(Id) AS Cant
									FROM ecom.e_transactionLog
									WHERE authorizationResult = '00'
									AND status = 'C'
									--AND acquirerId = '$acquirerId'
									--AND idCommerce = '$idCommerce'
									AND brand = '".$params['drillLabelList'][0]."'
									AND timestamp BETWEEN '".$params["label"]." 00:00:00' AND '".$params["label"]." 23:59:59'
									GROUP BY CAST(YEAR(timestamp) AS VARCHAR(4))+'-'+CAST(MONTH(timestamp) AS VARCHAR(2))+'-'+CAST(DAY(timestamp) AS VARCHAR(2))+' '+CAST(DATEPART(hh, timestamp) AS VARCHAR(2));
				 ";
		$result = sqlsrv_query($this->conn, $query);
		$i = 0;
		while($row = @sqlsrv_fetch_array($result, SQLSRV_FETCH_NUMERIC)) {
			$labels[$i] = $row[0].'';
            $sales_data[$i] = $row[1];
			$i++;
		}
		
        //$source->setCaption($params["label"]);
        $source->setLabels($labels);
        $source->addSeries("Ventas", "", $sales_data, array(
							  "seriesDisplayType" => "line",
							  "seriesColor" => "#71C73E" 
							));
	}
}
?>
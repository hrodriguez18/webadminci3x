
	var servidor="https://secure.prestopago.com/CI3/AdminTerminales/";
	var assets="https://secure.prestopago.com/CI3/assets/uploads/files/"
	var $table = $('#table_terminales');



	$table.ready(function(){
		$table.bootstrapTable({
	            toolbar: ".toolbar",
	            clickToSelect: false,
	            showRefresh: true,
	            paginationHAlign:'right',
	            search: true,
	            showToggle: true,
	            showColumns: true,
	            pagination: true,
	            searchAlign: 'right',
	            pageSize: 8,
	            pageList: [8,10,25,50,100],
	            paginationVAlign:'bottom',
	            locale:'es-MX',
	            silent:true,
	            url:servidor+'populateTable',
	            method:'post',
	            contentType:'application/json',
	            dataType:'json',
	            columns: [
	            		{
	                        field: 'ID_Usuario',
	                        title: 'ID Terminal',
	                        sortable: true,
	                        visible:false,
	                     },
	                     {
	                        field: 'OPERADORES',
	                        title: 'Operador',
	                        sortable: true,
	                        visible:true,
	                     },
	                     {
	                        field: 'AGENCIAS',
	                        title: 'Agencia',
	                        sortable: true,
	                        visible:true,
	                     },
	                     {
	                        field: 'Estado',
	                        title: 'Estado',
	                        sortable: true,
	                        visible:true,
	                     },
	                     {
	                        field: 'No_Imei',
	                        title: 'No. IMEI',
	                        sortable: true,
	                        visible:false,
	                     },
	                     {
	                        field: 'No_SwipeCard',
	                        title: 'Swipe Card',
	                        sortable: true,
	                        visible:false,
	                     },
	                     {
	                        field: 'merchantId1',
	                        title: 'Merchant',
	                        sortable: true,
	                        visible:false,
	                     },
	                     {
	                        field: 'merchantId2',
	                        title: 'Merchant 2',
	                        sortable: true,
	                        visible:false,
	                     },


	                     {
	                        field: 'terminalNumber',
	                        title: 'No. Terminal',
	                        sortable: true,
	                        visible:false,
	                     },
	                     {
	                        field: 'limiteId',
	                        title: 'Limite ID',
	                        sortable: true,
	                        visible:false,
	                     },



	                     {
	                        field: 'consumoDia',
	                        title: 'Consumo Díario',
	                        sortable: true,
	                        visible:false,
	                     },{
	                        field: 'consumoMes',
	                        title: 'Cosumo Mes',
	                        sortable: true,
	                        visible:false,
	                     },
	                     {
	                        field: 'disponibleDia',
	                        title: 'Disponible Día',
	                        sortable: true,
	                        visible:false,
	                     },
	                     {
	                        field: 'disponibleMes',
	                        title: 'Disponible Mes',
	                        sortable: true,
	                        visible:false,
	                     },

	                     {
	                        field: 'consumoTx',
	                        title: 'Consumo TX',
	                        sortable: true,
	                        visible:false,
	                     },
	                     {
	                        field: 'binFilter',
	                        title: 'Filtro Bin',
	                        sortable: true,
	                        visible:false,
	                     },
	                     {
	                        field: 'disponibleTx',
	                        title: 'Disponible TX',
	                        sortable: true,
	                        visible:false,
	                     },
	                     {
	                        field: 'cuenta',
	                        title: 'Cuenta',
	                        sortable: true,
	                        visible:true,
	                     },
	                     {
	                        field: 'tipoDoc',
	                        title: 'Tipo Documento',
	                        sortable: true,
	                        visible:false,
	                     },
	                     {
	                        field: 'documento',
	                        title: 'Documento',
	                        sortable: true,
	                        visible:false,
	                     },

	                      {
	                      	field:'actions',
	                      	title:'Acciones',
	                      	class:'td-actions text-right',
	                      	events:'operateEvents',
	                      	formatter:'operateFormatter',
	                      },
	                ],

	            formatShowingRows: function(pageFrom, pageTo, totalRows){
	                //do nothing here, we don't want to show the text "showing x of y from..."
	            },
	            formatRecordsPerPage: function(pageNumber){
	                return pageNumber + " rows visible";
	            },
	            icons: {
	                refresh: 'fa fa-refresh',
	                toggle: 'fa fa-th-list',
	                columns: 'fa fa-columns',
	                detailOpen: 'fa fa-plus-circle',
	                detailClose: 'fa fa-minus-circle'
	            },

	    });
	    //activate the tooltips after the data table is initialized
	        $('[rel="tooltip"]').tooltip();

	        $(window).resize(function () {
	            $table.bootstrapTable('resetView');
	        });

	        window.operateEvents = {

	            'click .edit': function (e, value, row, index) {

	                $.post(servidor+"EditData",{Id:row['ID_Usuario']},function(data){
						$("#modal_Content").html(data);
						$("#ModalApp").modal('show');
						$("#ModalApp").appendTo("body");
	                });
	            },
	            'click .remove': function (e, value, row, index) {
	                info = JSON.stringify(row);
	                swal({
	                    title: '¿Estas Seguro?',
	                    text: 'Estas a punto de elimnar un elemento de esta tabla...',
	                    type: 'warning',
	                    showCancelButton: true,
	                    confirmButtonText: 'Sí, Eliminalo',
	                    cancelButtonText: 'No estoy seguro',
	                    confirmButtonClass: "btn btn-success btn-fill",
	                    cancelButtonClass: "btn btn-danger btn-fill",
	                    buttonsStyling: false
	                }).then(function(){
	                	$.post(servidor+"DeleteData",{id:row['ID_Usuario'],},function(data){
	                		swal({
			                    title: 'Eliminado!',
			                    html: data,
			                    type: 'success',
			                    confirmButtonClass: "btn btn-success btn-fill",
			                    buttonsStyling: false
			                 })

	                	})
	                	$table.bootstrapTable('refresh');
	                }, function(dismiss) {
	                  // dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
	                  if (dismiss === 'cancel') {
	                    swal({
	                      title: 'Cancelado',
	                      text: 'El elemento está a salvo!',
	                      type: 'error',
	                      confirmButtonClass: "btn btn-info btn-fill",
	                      buttonsStyling: false
	                    })
	                  }
	                })

	            }
	        };

	});

	function operateFormatter(value, row, index) {
	    return ['<a rel="tooltip" title="Edit" class="btn btn-simple btn-info  table-action edit" href="javascript:void(0)">',
	                '<i class="ti-eye"></i>',
	            '</a>',
	            '<a rel="tooltip" title="Remove" class="btn btn-simple btn-danger  table-action remove" href="javascript:void(0)">',
	                '<i class="fa fa-remove "></i>',
	            '</a>'].join('');
	}


function insertNew(){

	$.post(servidor+'Form',{},function(data){
		if(data!=''){
			$("#modal_Content").html(data);
		}
		$("#ModalApp").modal('show');
		$("#ModalApp").appendTo("body");
	});
}

function SaveData(){
	var formData = new FormData(document.getElementById("form_page"));

	$("#modal_Content").html('<div class="loader"></div>');
	$.ajax({
	    url: servidor+'saveData',
	    type: "post",
	    dataType: "html",
	    data: formData,
	    cache: false,
	    contentType: false,
	    processData: false
	})
    .done(function(res){

    	if(res!='Todo bien!'){
    		$("#modal_Content").html(res);
    	}else{
    		$table.bootstrapTable('refresh');
    		$("#ModalApp").modal('hide');
    		swal({
	                title: 'Dato Insertado',
	                text: res,
	                type: 'success',
	                confirmButtonClass: "btn btn-success btn-fill",
	                buttonsStyling: false
	             });
    	}
    });
}

function UpdateData(){
	var formData = new FormData(document.getElementById("form_page_update"));

	$("#modal_Content").html('<div class="loader"></div>');
	$.ajax({
	    url: servidor+'UpdateData',
	    type: "post",
	    dataType: "html",
	    data: formData,
	    cache: false,
	    contentType: false,
	    processData: false
	})
    .done(function(res){

    	if(res!='Todo bien!'){
    		$("#modal_Content").html(res);
    	}else{
    		$table.bootstrapTable('refresh');
    		$("#ModalApp").modal('hide');
    		swal({
	                title: 'Operador Actualizado',
	                text: res,
	                type: 'success',
	                confirmButtonClass: "btn btn-success btn-fill",
	                buttonsStyling: false
	             });
    	}
    });

}

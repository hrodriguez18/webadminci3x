
	var servidor="https://secure.prestopago.com/CI3/AdminLimitesTransacciones/";
	var assets="https://secure.prestopago.com/CI3/assets/uploads/files/";
	var $table = $('#tableLimitesTransacciones');



	$table.ready(function(){
		$table.bootstrapTable({
	            toolbar: ".toolbar",
	            clickToSelect: false,
	            showRefresh: true,
	            paginationHAlign:'right',
	            search: true,
	            showToggle: true,
	            showColumns: true,
	            pagination: true,
	            searchAlign: 'right',
	            pageSize: 8,
	            pageList: [8,10,25,50,100],
	            paginationVAlign:'bottom',
	            locale:'es-MX',
	            silent:true,
	            url:servidor+'populateTable',
	            method:'post',
	            contentType:'application/json',
	            dataType:'json',
	            columns: [
	            		{
	                        field: 'id',
	                        title: 'ID',
	                        sortable: false,
	                        visible:false,
	                     },
	                     {
	                        field: 'description',
	                        title: 'Descripcion',
	                        sortable: true
	                     },

	                     {
	                        field: 'initLimit',
	                        title: 'Mínimo',
	                        sortable: true
	                     },

	                     {
	                        field: 'finalLimit',
	                        title: 'Máximo',
	                        sortable: true
	                     },
	                      {
	                      	field:'actions',
	                      	title:'Acciones',
	                      	class:'td-actions text-right',
	                      	events:'operateEvents',
	                      	formatter:'operateFormatter',
	                      },
	                ],

	            formatShowingRows: function(pageFrom, pageTo, totalRows){
	                //do nothing here, we don't want to show the text "showing x of y from..."
	            },
	            formatRecordsPerPage: function(pageNumber){
	                return pageNumber + " rows visible";
	            },
	            icons: {
	                refresh: 'fa fa-refresh',
	                toggle: 'fa fa-th-list',
	                columns: 'fa fa-columns',
	                detailOpen: 'fa fa-plus-circle',
	                detailClose: 'fa fa-minus-circle'
	            },

	    });
	    //activate the tooltips after the data table is initialized
	        $('[rel="tooltip"]').tooltip();

	        $(window).resize(function () {
	            $table.bootstrapTable('resetView');
	        });

	        window.operateEvents = {

	            'click .edit': function (e, value, row, index) {
	                $.post(servidor+"Edit",{id:row['id']},function(data){
						$("#modal_Content").html(data);
						$("#ModalApp").modal('show');
						$("#ModalApp").appendTo("body");
	                });
	            },
	            'click .remove': function (e, value, row, index) {
	                info = JSON.stringify(row);
	                swal({
	                    title: '¿Estas Seguro?',
	                    text: 'Estas a punto de elimnar un elemento de esta tabla...',
	                    type: 'warning',
	                    showCancelButton: true,
	                    confirmButtonText: 'Sí, Eliminalo',
	                    cancelButtonText: 'No estoy seguro',
	                    confirmButtonClass: "btn btn-success btn-fill",
	                    cancelButtonClass: "btn btn-danger btn-fill",
	                    buttonsStyling: false
	                }).then(function(){
	                	$.post(servidor+"Delete",{id:row['id'],},function(data){
	                		swal({
			                    title: 'Eliminado!',
			                    html: data,
			                    type: 'success',
			                    confirmButtonClass: "btn btn-success btn-fill",
			                    buttonsStyling: true
			                 })

	                	})
	                	$table.bootstrapTable('refresh');
	                }, function(dismiss) {
	                  // dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
	                  if (dismiss === 'cancel') {
	                    swal({
	                      title: 'Cancelado',
	                      text: 'El elemento está a salvo!',
	                      type: 'error',
	                      confirmButtonClass: "btn btn-info btn-fill",
	                      buttonsStyling: false
	                    })
	                  }
	                })

	            }
	        };

	});

	function operateFormatter(value, row, index) {
	    return ['<a rel="tooltip" title="Edit" class="btn btn-simple btn-info  table-action edit" href="javascript:void(0)">',
	                '<i class="fa fa-edit "></i>',
	            '</a>',
	            '<a rel="tooltip" title="Remove" class="btn btn-simple btn-danger  table-action remove" href="javascript:void(0)">',
	                '<i class="fa fa-remove "></i>',
	            '</a>'].join('');
	}

function setLogo(value, row, index){
	return '<img src="'+row['imagen']+'" width="80"  >';
}


function InsertData(){

	$.post(servidor+'Form',{},function(data){
		if(data!=''){
			$("#modal_Content").html(data);
		}
		$("#ModalApp").modal('show');
	});
}

function SaveData(){
	var formData = new FormData(document.getElementById("form_page"));

	$("#modal_Content").html('<div class="loader"></div>');
	$.ajax({
	    url: servidor+'Save',
	    type: "post",
	    dataType: "html",
	    data: formData,
	    cache: false,
	    contentType: false,
	    processData: false
	})
    .done(function(res){

    	if(res!='Todo bien!'){
    		$("#modal_Content").html(res);
    	}else{
    		$table.bootstrapTable('refresh');
    		$("#ModalApp").modal('hide');
    		swal({
	                title: 'Dato Insertado',
	                text: res,
	                type: 'success',
	                confirmButtonClass: "btn btn-success btn-fill",
	                buttonsStyling: false
	             });
    	}
    });
}

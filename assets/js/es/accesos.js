
	var servidor="https://secure.prestopago.com/CI3/MposPaper/";

	var $table = $('#table_accesos');



	$table.ready(function(){
		$table.bootstrapTable({
	            toolbar: ".toolbar",
	            clickToSelect: false,
	            showRefresh: true,
							height:400,
	            paginationHAlign:'right',
	            search: true,
	            showToggle: true,
	            showColumns: true,
	            pagination: true,
	            searchAlign: 'right',
	            pageSize: 8,
	            pageList: [8,10,25,50,100],
	            paginationVAlign:'bottom',
	            locale:'es-MX',
	            silent:true,
	            url:servidor+'populateAccesos',
	            method:'post',
	            contentType:'application/json',
	            dataType:'json',
	            columns: [
	            		{
	                        field: 'id',
	                        title: 'ID',
	                        sortable: true,
	                        visible:false,
	                     },
	                     {
	                        field: 'timeStamp',
	                        title: 'Fecha',
	                        sortable: true,
	                        visible:true,
	                     },
	                     {
	                        field: 'userId',
	                        title: 'No. User',
	                        sortable: true,
	                        visible:true,
	                     },
	                     {
	                        field: 'imei',
	                        title: 'IMEI',
	                        sortable: true,
	                        visible:true,
	                     },
	                     {
	                        field: 'latitud',
	                        title: 'LAT.',
	                        sortable: true,
	                        visible:true,
	                     },
	                     {
	                        field: 'logitud',
	                        title: 'LONG.',
	                        sortable: true,
	                        visible:true,
	                     },
	                     {
	                        field: 'username',
	                        title: 'Usuario',
	                        sortable: true,
	                        visible:true,
	                     },
	                     {
	                        field: 'operator',
	                        title: 'Operador',
	                        sortable: true,
	                        visible:true,
	                     },
	                     {
	                        field: 'agency',
	                        title: 'Agencia',
	                        sortable: true,
	                        visible:true,
	                     },
	                     {
	                        field: 'canal',
	                        title: 'Canal',
	                        sortable: true,
	                        visible:true,
	                     }
						 ,
	                     {
	                        field: 'country',
	                        title: 'País',
	                        sortable: true,
	                        visible:true,
	                     },


	                     {
	                        field: 'terminalId',
	                        title: 'Terminal',
	                        sortable: true,
	                        visible:true,
	                     }

	                ],

	            formatShowingRows: function(pageFrom, pageTo, totalRows){
	                //do nothing here, we don't want to show the text "showing x of y from..."
	            },
	            formatRecordsPerPage: function(pageNumber){
	                return pageNumber + " rows visible";
	            },
	            icons: {
	                refresh: 'fa fa-refresh',
	                toggle: 'fa fa-th-list',
	                columns: 'fa fa-columns',
	                detailOpen: 'fa fa-plus-circle',
	                detailClose: 'fa fa-minus-circle'
	            },

	    });
	    //activate the tooltips after the data table is initialized
	        $('[rel="tooltip"]').tooltip();

	        $(window).resize(function () {
	            $table.bootstrapTable('resetView');
	        });

	        window.operateEvents = {

	            'click .edit': function (e, value, row, index) {

	                $.post(servidor+"EditarGrupoDias",{id:row['id']},function(data){
						$("#modal_Content").html(data);
						$("#ModalApp").modal('show');
						$("#ModalApp").appendTo("body");
	                });
	            },
	            'click .remove': function (e, value, row, index) {
	                info = JSON.stringify(row);
	                swal({
	                    title: '¿Estas Seguro?',
	                    text: 'Estas a punto de elimnar un elemento de esta tabla...',
	                    type: 'warning',
	                    showCancelButton: true,
	                    confirmButtonText: 'Sí, Eliminalo',
	                    cancelButtonText: 'No estoy seguro',
	                    confirmButtonClass: "btn btn-success btn-fill",
	                    cancelButtonClass: "btn btn-danger btn-fill",
	                    buttonsStyling: false
	                }).then(function(){
	                	$.post(servidor+"DeleteDataGrupoDias",{Id:row['id'],},function(data){
	                		swal({
			                    title: 'Atención!',
			                    html: data,
			                    type: 'success',
			                    confirmButtonClass: "btn btn-success btn-fill",
			                    buttonsStyling: false
			                 })

	                	})
	                	$table.bootstrapTable('refresh');
	                }, function(dismiss) {
	                  // dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
	                  if (dismiss === 'cancel') {
	                    swal({
	                      title: 'Cancelado',
	                      text: 'El elemento está a salvo!',
	                      type: 'error',
	                      confirmButtonClass: "btn btn-info btn-fill",
	                      buttonsStyling: false
	                    })
	                  }
	                })

	            }
	        };

	});

	function setFlag(value, row, index){
	return '<img src="'+assets+row['Bandera']+'">'
}

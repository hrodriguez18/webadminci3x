var servidor="https://secure.prestopago.com/CI3/admin/";



var $table = $('#bootstrap-table');


$table.ready(function(){
	$table.bootstrapTable({
            toolbar: ".toolbar",
            clickToSelect: false,
            showRefresh: true,
            paginationHAlign:'right',
            search: true,
            showToggle: true,
						height:400,
            showColumns: true,
            pagination: true,
            searchAlign: 'right',
            pageSize: 8,
            pageList: [8,10,25,50,100],
            paginationVAlign:'bottom',
            locale:'es-MX',
            silent:true,
            url:servidor+'ReloadTableMoneda',
            method:'post',
            contentType:'application/json',
            dataType:'json',
            columns: [
                     {
                         field: 'Cod_Moneda',
                         title: 'Código',
												 visible:true,
                         sortable: true
                     },
                     {
                         field: 'Descripcion',
                         title: 'Descripción',
                         sortable: true
                     },
                      {
                          field: 'DescripcionCorta',
                          title: 'Etiqueta',
                          sortable:true
                      },
                      {
                      	field:'actions',
                      	title:'Acciones',
                      	class:'td-actions text-right',
                      	events:'operateEvents',
                      	formatter:'operateFormatter',
                      },
                ],

            formatShowingRows: function(pageFrom, pageTo, totalRows){
                //do nothing here, we don't want to show the text "showing x of y from..."
            },
            formatRecordsPerPage: function(pageNumber){
                return pageNumber + " rows visible";
            },
            icons: {
                refresh: 'fa fa-refresh',
                toggle: 'fa fa-th-list',
                columns: 'fa fa-columns',
                detailOpen: 'fa fa-plus-circle',
                detailClose: 'fa fa-minus-circle'
            },

    });



    //activate the tooltips after the data table is initialized
        $('[rel="tooltip"]').tooltip();

        $(window).resize(function () {
            $table.bootstrapTable('resetView');
        });

        window.operateEvents = {

            'click .edit': function (e, value, row, index) {
                $.post(servidor+"EditarElemento",{id:row['Cod_Moneda']},function(data){
					$("#modal_Content").html(data);
					$("#ModalMoneda").modal('show');
					$("#ModalMoneda").appendTo("body");
                });
            },
            'click .remove': function (e, value, row, index) {
                info = JSON.stringify(row);
                swal({
                    title: '¿Estas Seguro?',
                    text: 'Estas a punto de elimnar un elemento de esta tabla...',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Sí, Eliminalo',
                    cancelButtonText: 'No estoy seguro',
                    confirmButtonClass: "btn btn-success btn-fill",
                    cancelButtonClass: "btn btn-danger btn-fill",
                    buttonsStyling: false
                }).then(function(){
                	$.post(servidor+"DeleteMoneda",{id:row['Cod_Moneda'],},function(data){
                		swal({
		                    title: 'Atención!',
		                    html: data,
		                    type: 'success',
		                    confirmButtonClass: "btn btn-success btn-fill",
		                    buttonsStyling: false
		                 })

                	})
                	$table.bootstrapTable('refresh');
                }, function(dismiss) {
                  // dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
                  if (dismiss === 'cancel') {
                    swal({
                      title: 'Cancelado',
                      text: 'El elemento está a salvo!',
                      type: 'error',
                      confirmButtonClass: "btn btn-info btn-fill",
                      buttonsStyling: false
                    })
                  }
                })

            }
        };

});

function operateFormatter(value, row, index) {
        return [
            '<a rel="tooltip" title="Edit" class="btn btn-simple btn-info  table-action edit" href="javascript:void(0)">',
                '<i class="fa fa-edit "></i>',
            '</a>',
            '<a rel="tooltip" title="Remove" class="btn btn-simple btn-danger  table-action remove" href="javascript:void(0)">',
                '<i class="fa fa-remove "></i>',
            '</a>'
        ].join('');
    }

function MostrarInfoVentas(){
	var year=$("#anio").val();
	var mes=$("#mes").val();
	var pais=$("#pais").val();
	var canal = $("#canal").val();
	var autorizador=$("#autorizador").val();


	$.post(servidor+'DatosVentas',{
		year:year,
		mes:mes,
		pais:pais,
		canal:canal,
		autorizador:autorizador,

	},function(data){
		$("#counters").html(data);

	});
	SetGraphic();
}

function editMoneda(e){
	alert('Editando Moneda : '+e);
}

function deleteMoneda(f){
	alert('EliminadoMoneda : '+f);
}

function insertNew(modal){
	$.post(servidor+'Modal',{form:modal},function(data){
		if(data!=''){
			$("#modal_Content").html(data);
		}
		$("#ModalMoneda").modal('show');
		$("#ModalMoneda").appendTo("body");
	})
}

function SaveMoneda(){
	var cod=$("#Cod_Moneda").val();
	var desc=$("#Descripcion").val();
	var descorta=$("#descorta").val();
	$("#ModalMoneda").modal('hide');

	$.post(servidor+"SaveMoneda",{

		Cod_Moneda:cod,
		Descripcion:desc,
		DescripcionCorta:descorta,
	},function(data){

		if(data!=''){
			$("#modal_Content").html(data);
			$("#ModalMoneda").modal('show');
			$("#ModalMoneda").appendTo("body");
		}else{
			UpdateTableMoneda();
			swal({
                    title: "Excelente!",
                    text: "Tu información ha sido guardada..",
                    type:'success',
                    buttonsStyling: false,
                	confirmButtonClass: "btn btn-info btn-fill"
                    });
		}
	});
}

function UpdateMoneda(){
	var cod=$("#Cod_Moneda").val();
	var desc=$("#Descripcion").val();
	var descorta=$("#descorta").val();
	$("#ModalMoneda").modal('hide');

	$.post(servidor+"UpdateMoneda",{

		Cod_Moneda:cod,
		Descripcion:desc,
		DescripcionCorta:descorta,
	},function(data){

		if(data!=''){
			$("#modal_Content").html(data);
			$("#ModalMoneda").modal('show');
			$("#ModalMoneda").appendTo("body");
		}else{
			UpdateTableMoneda();
			swal({
                    title: "Excelente!",
                    text: "Tu información ha sido guardada..",
                    type:'success',
                    buttonsStyling: false,
                	confirmButtonClass: "btn btn-info btn-fill"
                    });
		}
	});
}



function UpdateTableMoneda(){

	$table.bootstrapTable('refresh');
}

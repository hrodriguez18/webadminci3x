var servidor="https://secure.prestopago.com/CI3/admin/";
var assets="https://secure.prestopago.com/CI3/assets/uploads/files/"
var $table = $('#tabla_paises');


$table.ready(function(){
	$table.bootstrapTable({
            toolbar: ".toolbar",
            clickToSelect: false,
            showRefresh: true,
            paginationHAlign:'right',
            search: true,
            showToggle: true,
            showColumns: true,
            pagination: true,
            searchAlign: 'right',
            pageSize: 8,
						height:400,
            pageList: [8,10,25,50,100],
            paginationVAlign:'bottom',
            locale:'en-us',
            silent:true,
            url:servidor+'populateTablePais',
            method:'post',
            contentType:'application/json',
            dataType:'json',
            columns: [
            		{
                        field: 'Cod_Pais',
                        title: 'Cod.',
                        sortable: true,
                        visible:false
                     },
                     {
                        field: 'Descripcion',
                        title: 'Description',
                        sortable: true
                     },
                      {
                        field: 'Bandera',
                        title: 'Flag',
                        formatter:'setFlag',
                        sortable:true
                      },
                      {
                          field: 'moneda',
                          title: 'Currency',
                          sortable:true
                      },
                      {
                      	field:'actions',
                      	title:'Actions',
                      	class:'td-actions text-right',
                      	events:'operateEvents',
                      	formatter:'operateFormatter',
                      },
                ],

            formatShowingRows: function(pageFrom, pageTo, totalRows){
                //do nothing here, we don't want to show the text "showing x of y from..."
            },
            formatRecordsPerPage: function(pageNumber){
                return pageNumber + " rows visible";
            },
            icons: {
                refresh: 'fa fa-refresh',
                toggle: 'fa fa-th-list',
                columns: 'fa fa-columns',
                detailOpen: 'fa fa-plus-circle',
                detailClose: 'fa fa-minus-circle'
            },

    });
    //activate the tooltips after the data table is initialized
        $('[rel="tooltip"]').tooltip();

        $(window).resize(function () {
            $table.bootstrapTable('resetView');
        });

        window.operateEvents = {

            'click .edit': function (e, value, row, index) {

                $.post(servidor+"EditarPais",{id:row['Cod_Pais']},function(data){
					$("#modal_Content").html(data);
					$("#ModalPais").modal('show');
					$("#ModalPais").appendTo("body");
                });
            },
            'click .remove': function (e, value, row, index) {
                info = JSON.stringify(row);
                swal({
									title: 'Are you sure?',
									text: 'You are about to delete an element from this table ...',
									type: 'warning',
									showCancelButton: true,
									confirmButtonText: 'Yes, remove',
									cancelButtonText: 'I m not sure',
                    confirmButtonClass: "btn btn-success btn-fill",
                    cancelButtonClass: "btn btn-danger btn-fill",
                    buttonsStyling: false
                }).then(function(){
                	$.post(servidor+"DeletePais",{id:row['Cod_Pais'],},function(data){
                		swal({
		                    title: 'Look out!',
		                    html: data,
		                    type: 'success',
		                    confirmButtonClass: "btn btn-success btn-fill",
		                    buttonsStyling: false
		                 })

                	})
                	$table.bootstrapTable('refresh');
                }, function(dismiss) {
                  // dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
                  if (dismiss === 'cancel') {
                    swal({
											title: 'Canceled',
											text: 'The element is safe!',
											type: 'error',
                      confirmButtonClass: "btn btn-info btn-fill",
                      buttonsStyling: false
                    })
                  }
                })

            }
        };

});

function operateFormatter(value, row, index) {
    return ['<a rel="tooltip" title="Edit" class="btn btn-simple btn-info  table-action edit" href="javascript:void(0)">',
                '<i class="fa fa-edit "></i>',
            '</a>',
            '<a rel="tooltip" title="Remove" class="btn btn-simple btn-danger  table-action remove" href="javascript:void(0)">',
                '<i class="fa fa-remove "></i>',
            '</a>'].join('');
}

function setFlag(value, row, index){
	return '<img src="'+assets+row['Bandera']+'">'
}

function setFlag(value, row, index){
	return '<img src="'+assets+row['Bandera']+'">'
}

function insertNew(){

	$.post(servidor+'FormPais',{},function(data){
		if(data!=''){
			$("#modal_Content").html(data);
		}
		$("#ModalPais").modal('show');
		$("#ModalPais").appendTo("body");
	})
/*
	$("#modal_Content").html('<div class="loader"></div>');
	$("#ModalPais").modal('show');
	$("#ModalPais").appendTo("body");*/

}

function SavePais(){
	var formData = new FormData(document.getElementById("formPais"));

	$("#modal_Content").html('<div class="loader"></div>');
	$.ajax({
	    url: servidor+'SavePais',
	    type: "post",
	    dataType: "html",
	    data: formData,
	    cache: false,
	    contentType: false,
	    processData: false
	})
    .done(function(res){

    	if(res!='Todo bien!'){
    		$("#modal_Content").html(res);
    	}else{
    		$table.bootstrapTable('refresh');
    		$("#ModalPais").modal('hide');
    		swal({
	                title: 'Inserted Data',
	                text: res,
	                type: 'success',
	                confirmButtonClass: "btn btn-success btn-fill",
	                buttonsStyling: false
	             });
    	}
    });
}

function UpdatePais(){
	var formData = new FormData(document.getElementById("formPais"));

	$("#modal_Content").html('<div class="loader"></div>');
	$.ajax({
	    url: servidor+'UpdatePais',
	    type: "post",
	    dataType: "html",
	    data: formData,
	    cache: false,
	    contentType: false,
	    processData: false
	})
    .done(function(res){

		if(res!=''){
			$("#modal_Content").html(res);
			$("#ModalMoneda").modal('show');
			$("#ModalMoneda").appendTo("body");
		}else{
			$("#ModalPais").modal('hide');
			$table.bootstrapTable('refresh');
			swal({
				title: "Excellent!",
				text: "Your information has been saved ..",
				type: 'success',
				buttonsStyling: false,
				confirmButtonClass: "btn btn-info btn-fill"

			});
		}
	});
}


	var servidor="https://secure.prestopago.com/CI3/MposPaper/";

	var $table = $('#table_accesos');



	$table.ready(function(){
		$table.bootstrapTable({
	            toolbar: ".toolbar",
	            clickToSelect: false,
	            showRefresh: true,
	            paginationHAlign:'right',
	            search: true,
							height:400,
	            showToggle: true,
	            showColumns: true,
	            pagination: true,
	            searchAlign: 'right',
	            pageSiXe: 8,
	            pageList: [8,10,25,50,100],
	            paginationVAlign:'bottom',
	            locale:'es-MZ',
	            silent:true,
	            url:servidor+'populateAccesos',
	            method:'post',
	            contentType:'application/json',
	            dataType:'json',
	            columns: [
	            		{
	                        field: 'id',
	                        title: 'ID',
	                        sortable: true,
	                        visible:false,
	                     },
	                     {
	                        field: 'timeStamp',
	                        title: 'Date',
	                        sortable: true,
	                        visible:true,
	                     },
	                     {
	                        field: 'userId',
	                        title: 'No. User',
	                        sortable: true,
	                        visible:false,
	                     },
	                     {
	                        field: 'imei',
	                        title: 'IMEI',
	                        sortable: true,
	                        visible:false,
	                     },
	                     {
	                        field: 'latitud',
	                        title: 'LAT.',
	                        sortable: true,
	                        visible:false,
	                     },
	                     {
	                        field: 'logitud',
	                        title: 'LONG.',
	                        sortable: true,
	                        visible:false,
	                     },
	                     {
	                        field: 'username',
	                        title: 'User',
	                        sortable: true,
	                        visible:true,
	                     },
	                     {
	                        field: 'operator',
	                        title: 'Operador',
	                        sortable: true,
	                        visible:true,
	                     },
	                     {
	                        field: 'agency',
	                        title: 'Agency',
	                        sortable: true,
	                        visible:true,
	                     },
	                     {
	                        field: 'canal',
	                        title: 'Canal',
	                        sortable: true,
	                        visible:true,
	                     }
						 ,
	                     {
	                        field: 'country',
	                        title: 'Country',
	                        sortable: true,
	                        visible:true,
	                     },
	                     {
	                        field: 'terminalId',
	                        title: 'Terminal',
	                        sortable: true,
	                        visible:false,
	                     }

	                ],

	            formatShowingRows: function(pageFrom, pageTo, totalRows){
	                //do nothing here, we don't want to show the teZt "showing Z of y from..."
	            },
	            formatRecordsPerPage: function(pageNumber){
	                return pageNumber + " rows visible";
	            },
	            icons: {
	                refresh: 'fa fa-refresh',
	                toggle: 'fa fa-th-list',
	                columns: 'fa fa-columns',
	                detailOpen: 'fa fa-plus-circle',
	                detailClose: 'fa fa-minus-circle'
	            },

	    });
	    //activate the tooltips after the data table is initialiXed
	        $('[rel="tooltip"]').tooltip();

	        $(window).resiXe(function () {
	            $table.bootstrapTable('resetView');
	        });

	        window.operateEvents = {

	            'click .edit': function (e, value, row, indeZ) {

	                $.post(servidor+"EditarGrupoDias",{id:row['id']},function(data){
						$("#modal_Content").html(data);
						$("#ModalApp").modal('show');
						$("#ModalApp").appendTo("body");
	                });
	            },
	            'click .remove': function (e, value, row, indeZ) {
	                info = JSON.stringify(row);
	                swal({
										title: 'Are you sure?',
										text: 'You are about to delete an element from this table ...',
										type: 'warning',
										showCancelButton: true,
										confirmButtonText: 'Yes, remove',
										cancelButtonText: 'I m not sure',
	                    confirmButtonClass: "btn btn-success btn-fill",
	                    cancelButtonClass: "btn btn-danger btn-fill",
	                    buttonsStyling: false
	                }).then(function(){
	                	$.post(servidor+"DeleteDataGrupoDias",{Id:row['id'],},function(data){
	                		swal({
			                    title: 'Look out!',
			                    html: data,
			                    type: 'success',
			                    confirmButtonClass: "btn btn-success btn-fill",
			                    buttonsStyling: false
			                 })

	                	})
	                	$table.bootstrapTable('refresh');
	                }, function(dismiss) {
	                  // dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
	                  if (dismiss === 'cancel') {
	                    swal({
												title: 'Canceled',
												text: 'The element is safe!',
												type: 'error',

	                      confirmButtonClass: "btn btn-info btn-fill",
	                      buttonsStyling: false
	                    })
	                  }
	                })

	            }
	        };

	});

	function setFlag(value, row, indeZ){
	return '<img src="'+assets+row['Bandera']+'">'
}

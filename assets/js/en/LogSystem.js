
	var servidor="https://secure.prestopago.com/CI3/LogControl/";

	var $table = $('#table_LogSystem');

	$table.ready(function(){
		$table.bootstrapTable({
	            toolbar: ".toolbar",
	            clickToSelect: false,
	            showRefresh: true,
	            paginationHAlign:'right',
							sidePagination:'server',
	            search: false,
							searchOnEnterKey:true,
	            showToggle: true,
							height:400,
	            showColumns: true,
	            pagination: true,
	            searchAlign: 'right',
	            pageSize: 8,
							onlyInfoPagination:true,
	            pageList: [8,10,25,50,100,200],
	            paginationVAlign:'bottom',
	            locale:'es-MX',
	            silentSort:false,
	            url:servidor+'populateTable',
	            method:'post',
	            contentType:'application/json',
	            dataType:'json',
							queryParamsType:'limit',
							queryParams: function (p) {
							    return { 'limit':p.limit,
									'offset':p.offset,
									'sortName':p.sortName,
									'sortOrder':p.sortOrder,
									'searchText':p.searchText
								};
							},
	            columns: [
	            		{
	                        field: 'id',
	                        title: 'ID',
	                        sortable: false,
	                        visible:false,
	                     },
	                     {
	                        field: 'userid',
	                        title: 'User',
	                        sortable: false,
	                        visible:false,
	                     },
	                     {
	                        field: 'action',
	                        title: 'Action',
	                        sortable: false,
	                        visible:true,
	                     },
	                     {
	                        field: 'status',
	                        title: 'Status',
	                        sortable: false,
	                        visible:true,
	                     },
											 {
	                        field: 'category',
	                        title: 'Category',
	                        sortable: false,
	                        visible:true,
	                     },
											 {
	                        field: 'stamp',
	                        title: 'Date',
	                        sortable: false,
	                        visible:true,
	                     },
											 {
	                        field: 'name',
	                        title: 'Name',
	                        sortable: false,
	                        visible:true,
	                     },
											 {
	                        field: 'username',
	                        title: 'User Name',
	                        sortable: false,
	                        visible:true,
	                     },

	                ],

	            formatShowingRows: function(pageFrom, pageTo, totalRows){
	                //do nothing here, we don't want to show the text "showing x of y from..."
	            },
	            formatRecordsPerPage: function(pageNumber){
	                return pageNumber + " rows visible";
	            },
	            icons: {
	                refresh: 'fa fa-refresh',
	                toggle: 'fa fa-th-list',
	                columns: 'fa fa-columns',
	                detailOpen: 'fa fa-plus-circle',
	                detailClose: 'fa fa-minus-circle'
	            },



	    });
	    //activate the tooltips after the data table is initialized
	        $('[rel="tooltip"]').tooltip();

	        $(window).resize(function () {
	            $table.bootstrapTable('resetView');
	        });

	        window.operateEvents = {

	            'click .edit': function (e, value, row, index) {

	                $.post(servidor+"EditData",{Id:row['userid']},function(data){
						$("#modal_Content").html(data);
						$("#ModalApp").modal('show');
						$("#ModalApp").appendTo("body");
	                });
	            },
	            'click .remove': function (e, value, row, index) {
	                info = JSON.stringify(row);
	                swal({
	                    title: '¿Estas Seguro?',
	                    text: 'Estas a punto de eliminar un elemento de esta tabla...',
	                    type: 'warning',
	                    showCancelButton: true,
	                    confirmButtonText: 'Sí, Eliminalo',
	                    cancelButtonText: 'No estoy seguro',
	                    confirmButtonClass: "btn btn-success btn-fill",
	                    cancelButtonClass: "btn btn-danger btn-fill",
	                    buttonsStyling: false
	                }).then(function(){
	                	$.post(servidor+"DeleteData",{id:row['userid'],},function(data){
	                		swal({
			                    title: 'Atención!',
			                    html: data,
			                    type: 'success',
			                    confirmButtonClass: "btn btn-success btn-fill",
			                    buttonsStyling: false
			                 })

	                	})
	                	$table.bootstrapTable('refresh');
	                }, function(dismiss) {
	                  // dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
	                  if (dismiss === 'cancel') {
	                    swal({
	                      title: 'Cancelado',
	                      text: 'El elemento está a salvo!',
	                      type: 'error',
	                      confirmButtonClass: "btn btn-info btn-fill",
	                      buttonsStyling: false
	                    })
	                  }
	                })

	            }
	        };

	});

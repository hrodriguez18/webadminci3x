
	var servidor="https://secure.prestopago.com/CI3/EcommercePaper/";

	var $table = $('#table_ecommerce');


	$table.ready(function(){
		$table.bootstrapTable({
	            toolbar: ".toolbar",
	            clickToSelect: false,
	            showRefresh: true,
	            paginationHAlign:'right',
	            search: true,
	            showToggle: true,
	            showColumns: true,
							height:400,
	            pagination: true,
	            searchAlign: 'right',
	            pageSize: 8,
	            pageList: [8,10,25,50,100],
	            paginationVAlign:'bottom',
	            locale:'es-MX',
	            silent:true,
	            url:servidor+'PopulateTableSemanal',
	            method:'post',
	            contentType:'application/json',
	            dataType:'json',
	            columns: [
	            		{
	                        field: 'id',
	                        title: 'ID',
	                        sortable: true,
	                        visible:false,
	                     },
						 {
	                        field: 'timestamp',
	                        title: 'Date',
	                        sortable: true,
	                        visible:true,
	                     },
	                     {
	                        field: 'acquirerId',
	                        title: 'Acquired',
	                        sortable: true,
	                        visible:true,
	                     },
	                     {
	                        field: 'idCommerce',
	                        title: 'Agency.',
	                        sortable: true,
	                        visible:true,
	                     },


	                     {
	                        field: 'purchaseAmount',
	                        title: 'Amount',
	                        sortable: true,
	                        visible:true,
	                     },
	                     {
	                        field: 'purchaseCurrencyCode',
	                        title: 'Cod. Currency',
	                        sortable: true,
	                        visible:false,
	                     },
	                     {
	                        field: 'shippingFirstName',
	                        title: 'F. Name',
	                        sortable: true,
	                        visible:false,
	                     },
	                     {
	                        field: 'shippingLastName',
	                        title: 'Last Name',
	                        sortable: true,
	                        visible:true,
	                     },
	                     {
	                        field: 'shippingEmail',
	                        title: 'Email',
	                        sortable: true,
	                        visible:true,
	                     },
	                     {
	                        field: 'shippingAddress',
	                        title: 'Address',
	                        sortable: true,
	                        visible:true,
	                     },
	                     {
	                        field: 'shippingZIP',
	                        title: 'ZIP',
	                        sortable: true,
	                        visible:true,
	                     },
	                     {
	                        field: 'shippingCity',
	                        title: 'City',
	                        sortable: true,
	                        visible:true,
	                     },
	                     {
	                        field: 'shippingState',
	                        title: 'State',
	                        sortable: true,
	                        visible:true,
	                     },
	                     {
	                        field: 'shippingCountry',
	                        title: 'Country',
	                        sortable: true,
	                        visible:true,
	                     }
						 ,
	                     {
	                        field: 'descriptionProducts',
	                        title: 'Description',
	                        sortable: true,
	                        visible:true,
	                     }
						 ,
	                     {
	                        field: 'reserved5',
	                        title: 'Reserved',
	                        sortable: true,
	                        visible:false,
	                     }
						 ,
	                     {
	                        field: 'authorizationResult',
	                        title: 'Response',
	                        sortable: true,
	                        visible:false,
	                     }

						 ,
	                     {
	                        field: 'authorizationCode',
	                        title: 'Code',
	                        sortable: true,
	                        visible:false,
	                     }

						 ,
	                     {
	                        field: 'bin',
	                        title: 'Bin',
	                        sortable: true,
	                        visible:false,
	                     }

						 ,
	                     {
	                        field: 'brand',
	                        title: 'Brand',
	                        sortable: true,
	                        visible:true,
	                     }

						 ,
	                     {
	                        field: 'purchaseOperationNumber',
	                        title: 'Operation Number',
	                        sortable: true,
	                        visible:false,
	                     }
						 ,
	                     {
	                        field: 'operador',
	                        title: 'Operator',
	                        sortable: true,
	                        visible:true,
	                     }
						 ,
	                     {
	                        field: 'currency',
	                        title: 'Respuesta',
	                        sortable: true,
	                        visible:false,
	                     }
						 ,
	                     {
	                        field: 'realAmount',
	                        title: 'Total',
	                        sortable: true,
	                        visible:false,
	                     }


	                ],

	            formatShowingRows: function(pageFrom, pageTo, totalRows){
	                //do nothing here, we don't want to show the text "showing x of y from..."
	            },
	            formatRecordsPerPage: function(pageNumber){
	                return pageNumber + " rows visible";
	            },
	            icons: {
	                refresh: 'fa fa-refresh',
	                toggle: 'fa fa-th-list',
	                columns: 'fa fa-columns',
	                detailOpen: 'fa fa-plus-circle',
	                detailClose: 'fa fa-minus-circle'
	            },

	    });
	    //activate the tooltips after the data table is initialized
	        $('[rel="tooltip"]').tooltip();

	        $(window).resize(function () {
	            $table.bootstrapTable('resetView');
	        });

	        window.operateEvents = {

	            'click .edit': function (e, value, row, index) {

	                $.post(servidor+"EditarGrupoDias",{id:row['id']},function(data){
						$("#modal_Content").html(data);
						$("#ModalApp").modal('show');
						$("#ModalApp").appendTo("body");
	                });
	            },
	            'click .remove': function (e, value, row, index) {
	                info = JSON.stringify(row);
	                swal({
										title: 'Are you sure?',
										text: 'You are about to delete an element from this table ...',
										type: 'warning',
										showCancelButton: true,
										confirmButtonText: 'Yes, remove',
										cancelButtonText: 'I m not sure',
	                    confirmButtonClass: "btn btn-success btn-fill",
	                    cancelButtonClass: "btn btn-danger btn-fill",
	                    buttonsStyling: false
	                }).then(function(){
	                	$.post(servidor+"DeleteDataGrupoDias",{Id:row['id'],},function(data){
	                		swal({
			                    title: 'Look out!',
			                    html: data,
			                    type: 'success',
			                    confirmButtonClass: "btn btn-success btn-fill",
			                    buttonsStyling: false
			                 })

	                	})
	                	$table.bootstrapTable('refresh');
	                }, function(dismiss) {
	                  // dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
	                  if (dismiss === 'cancel') {
	                    swal({
												title: 'Canceled',
												text: 'The element is safe!',
												type: 'error',
	                      confirmButtonClass: "btn btn-info btn-fill",
	                      buttonsStyling: false
	                    })
	                  }
	                })

	            }
	        };

	});

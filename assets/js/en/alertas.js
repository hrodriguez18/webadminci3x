
	var servidor="https://secure.prestopago.com/CI3/MposPaper/";

	var $table = $('#table_transacciones');

	$table.ready(function(){
		$table.bootstrapTable({
	            toolbar: ".toolbar",
	            clickToSelect: false,
	            showRefresh: true,
	            paginationHAlign:'right',
	            search: true,
	            showToggle: true,
	            showColumns: true,
							height:400,
	            pagination: true,
	            searchAlign: 'right',
	            pageSize: 8,
	            pageList: [8,10,25,50,100],
	            paginationVAlign:'bottom',
	            locale:'es-MX',
	            silent:true,
	            url:servidor+'populateAlertas',
	            method:'post',
	            contentType:'application/json',
	            dataType:'json',
	            columns: [{
	                        field: 'tranNum',
	                        title: 'ID',
	                        sortable: true,
	                        visible:false,
	                     },
						 				 {
	                        field: 'timeStamp',
	                        title: 'Date.',
	                        sortable: true,
	                        visible:true,
	                     },
	                     {
	                        field: 'operator',
	                        title: 'Operator',
	                        sortable: true,
	                        visible:true,
	                     },
	                     {
	                        field: 'agency',
	                        title: 'Agency.',
	                        sortable: true,
	                        visible:true,
	                     },

	                     {
	                        field: 'address',
	                        title: 'Address.',
	                        sortable: true,
	                        visible:false,
	                     },
	                     {
	                        field: 'cardno',
	                        title: 'No. Card',
	                        sortable: true,
	                        visible:true,
	                     },
	                     {
	                        field: 'currencyCode',
	                        title: 'Cod. Currency',
	                        sortable: true,
	                        visible:false,
	                     },
	                     {
	                        field: 'cardAmount',
	                        title: 'Card Amount',
	                        sortable: true,
	                        visible:false,
	                     },
	                     {
	                        field: 'currency',
	                        title: 'Currency',
	                        sortable: true,
	                        visible:true,
	                     },
	                     {
	                        field: 'tax',
	                        title: 'TAX',
	                        sortable: true,
	                        visible:false,
	                     },
	                     {
	                        field: 'totalAmount',
	                        title: 'Total',
	                        sortable: true,
	                        visible:true,
	                     },
	                     {
	                        field: 'authorizationCode',
	                        title: 'Authorization',
	                        sortable: true,
	                        visible:false,
	                     },
	                     {
	                        field: 'Longitud',
	                        title: 'longitude',
	                        sortable: true,
	                        visible:false,
	                     },
	                     {
	                        field: 'Latitud',
	                        title: 'Latitude',
	                        sortable: true,
	                        visible:false,
	                     },
	                     {
	                        field: 'answerCode',
	                        title: 'Response',
	                        sortable: true,
	                        visible:false,
	                     }

						],

	            formatShowingRows: function(pageFrom, pageTo, totalRows){
	                //do nothing here, we don't want to show the text "showing x of y from..."
	            },
	            formatRecordsPerPage: function(pageNumber){
	                return pageNumber + " rows visible";
	            },
	            icons: {
	                refresh: 'fa fa-refresh',
	                toggle: 'fa fa-th-list',
	                columns: 'fa fa-columns',
	                detailOpen: 'fa fa-plus-circle',
	                detailClose: 'fa fa-minus-circle'
	            },

	    });
	    //activate the tooltips after the data table is initialized
	        $('[rel="tooltip"]').tooltip();

	        $(window).resize(function () {
	            $table.bootstrapTable('resetView');
	        });

	        window.operateEvents = {

	            'click .edit': function (e, value, row, index) {

	                $.post(servidor+"EditarGrupoDias",{id:row['id']},function(data){
						$("#modal_Content").html(data);
						$("#ModalApp").modal('show');
						$("#ModalApp").appendTo("body");
	                });
	            },
	            'click .remove': function (e, value, row, index) {
	                info = JSON.stringify(row);
	                swal({
										title: 'Are you sure?',
										text: 'You are about to delete an element from this table ...',
										type: 'warning',
										showCancelButton: true,
										confirmButtonText: 'Yes, remove',
										cancelButtonText: 'I m not sure',
	                    confirmButtonClass: "btn btn-success btn-fill",
	                    cancelButtonClass: "btn btn-danger btn-fill",
	                    buttonsStyling: false
	                }).then(function(){
	                	$.post(servidor+"DeleteDataGrupoDias",{Id:row['id'],},function(data){
	                		swal({
			                    title: 'Eliminado!',
			                    html: data,
			                    type: 'success',
			                    confirmButtonClass: "btn btn-success btn-fill",
			                    buttonsStyling: false
			                 })

	                	})
	                	$table.bootstrapTable('refresh');
	                }, function(dismiss) {
	                  // dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
	                  if (dismiss === 'cancel') {
	                    swal({
												title: 'Canceled',
												text: 'The element is safe!',
												type: 'error',
	                      confirmButtonClass: "btn btn-info btn-fill",
	                      buttonsStyling: false
	                    })
	                  }
	                })

	            }
	        };

	});
